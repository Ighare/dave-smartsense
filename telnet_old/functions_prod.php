<?php

// //echo phpinfo();
require_once("db.php");
//require_once('PHPMailer/class.phpmailer.php');

function deadme($str){
	WriteLogROZ(" [sqls] ".$str);
	die("");
}
function validateLatLong($lat, $long) {
  return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $lat.','.$long);
}
function deadme_di($str){
	WriteLogNewDevice(" Device: ".$str);
	//die("");
}
function alerts_log($string) {
    $t_date = date('Y-m-d');
    $myFile = "alert_log" . $t_date . ".txt";
    $fh = fopen($myFile, 'a') or die("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
    die($string);
}
// This function is used to get the time difference from two dates passed into it.
// Difference can have different formats, such as seconds, minutes, hours, weeks, days et.c

function timeBetween($startDate, $endDate, $format = 1) {
    list($date, $time) = explode(' ', $endDate);
    $startdate = explode("-", $date);
    $starttime = explode(":", $time);

    list($date, $time) = explode(' ', $startDate);
    $enddate = explode("-", $date);
    $endtime = explode(":", $time);

    $secondsDifference = mktime($endtime[0], $endtime[1], $endtime[2], $enddate[1], $enddate[2], $enddate[0]) - mktime($starttime[0], $starttime[1], $starttime[2], $startdate[1], $startdate[2], $startdate[0]);

    switch ($format) {
        // Difference in Minutes
        case 1:
            return floor($secondsDifference / 60);
        // Difference in Hours    
        case 2:
            return floor($secondsDifference / 60 / 60);
        // Difference in Days    
        case 3:
            return floor($secondsDifference / 60 / 60 / 24);
        // Difference in Weeks    
        case 4:
            return floor($secondsDifference / 60 / 60 / 24 / 7);
        // Difference in Months    
        case 5:
            return floor($secondsDifference / 60 / 60 / 24 / 7 / 4);
        // Difference in Years    
        default:
            return floor($secondsDifference / 365 / 60 / 60 / 24);
    }
}
function sendMobileNotification($conn,$user_id, $smsText1, $asset_id="") 
{
	$type="Alert";
	//$smsText1=$smsText.'<br/>DateTime: '.date('Y-m-d H:i:s');
    $url = 'http://199.217.112.41/dev/pushnotification.php?user_id=' . urlencode($user_id) . '&msg=' . urlencode($smsText1) . '&type=' . urlencode($type) . '&assets_id=' . urlencode($asset_id) . '';	
	$query = "INSERT INTO tbl_pending_notifications ( `user_id`, `msg`, `type`, `assets_id`, `add_date` ) VALUES ('". ($user_id)."','". ($smsText1)."','". ($type)."','". ($assets_id)."','". gmdate(DATE_TIME)."' )";
    mysqli_query($conn,$query) or deadme(mysqli_error($conn) . ":" . $query); 
	WriteLogROZ($url);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($curl, CURLOPT_HEADER, false);
    curl_exec($curl); 	
    if (curl_errno($curl)) {
        $error = curl_error($curl); 

        WriteLogROZ("CURL Error: in sendMobileNotification(): " . $error);
    } else {
        WriteLogROZ("in sendMobileNotification(): mobile_no:  " . $user_id . "SMS text: " . $smsText1);
        curl_close($curl);
    } 
} 
function check_inout_geofence_alert($conn,$asset_id,$device_name,$device_dt,$address,$latitude,$longitude){ 
	$data_inserted=false;
	$device_users=date_create($device_dt);
	$device_users=date_format($device_users,"d F Y,H:i:s");
	global $current_landmark, $current_landmark_id, $dts;	
	$query="SELECT * FROM `tbl_geofence` WHERE FIND_IN_SET($asset_id,assigned_devices) and status=1";
	$rs = mysqli_query($conn,$query);
	while ($row = mysqli_fetch_array($rs)){ 		
		$geo_id = $row['id'];
		$distance_value = $row['geofence_radius'];
		$geofence_name = $row['geofence_name'];
		$geofence_latitude = $row['geofence_latitude'];
		$geofence_longitude = $row['geofence_longitude'];
		$geofence_in_alert = $row['geofence_in_alert'];
		$geofence_out_alert = $row['geofence_out_alert'];		 
		$user_id = $row['add_uid'];				
		$unit="KM";
		$distanceFromLandmark =(getDistance($latitude, $longitude, $geofence_latitude, $geofence_longitude, $unit) * 1000);			
	if($distanceFromLandmark < $distance_value){
			//in part of geo-fence s
		$checkLast = "SELECT geo_id, status FROM tbl_geofence_log WHERE device_id = $asset_id and geo_id = $geo_id order by id desc limit 1";		
		$checkRs = mysqli_query($conn,$checkLast) or deadme(mysqli_error($conn) . ":" . $checkLast);
		$checkRow = mysqli_fetch_array($checkRs);		
        $lastLandmarkId = $checkRow['geo_id'];
        $inOutStatus = $checkRow['status'];
		$current_landmark = $geofence_name;
        $current_landmark_id = $lastLandmarkId;
		//var_dump(mysqli_num_rows($checkRs));
		if (!mysqli_num_rows($checkRs)) {  //check for first time log
                $inOutStatus = 'out';
         }
		 if ($inOutStatus == 'out'){
			 $insert="INSERT INTO `tbl_geofence_log`(`device_id`, `status`, `distance`, `geo_id`) VALUES ('$asset_id','in','$distanceFromLandmark','$geo_id')";			
			  mysqli_query($conn,$insert) or deadme(mysqli_error($conn) . ":" . $insert);
			  $data_inserted=true; 
			  //var_dump($geofence_in_alerts);
			 if($geofence_in_alert==1){
				 //The vehicle MRS0125 is entered in jalna geofence at 2019-11-07 11:30 AM
				 $smsText="The vehicle $device_name is entered in $geofence_name at $device_users.";
				 sendMobileNotification($conn,$user_id,$smsText,$asset_id);
			 }
		 }
		}else{
			//out part of geo-fence
			$checkLast = "select id from tbl_geofence_log where device_id = $asset_id and geo_id = $geo_id and status = 'in' order by id desc limit 1";			
            $checkRs = mysqli_query($conn,$checkLast) or deadme(mysqli_error($conn) . ":" . $checkLast);
			 if (mysqli_num_rows($checkRs) > 0){
				$checkRow = mysqli_fetch_array($checkRs);
                $lId = $checkRow['id'];
				$uLSql = "update tbl_geofence_log  set status = 'out',distance = '$distanceFromLandmark' where id = $lId";	
                mysqli_query($conn,$uLSql) or deadme(mysqli_error($conn) . ":" . $uLSql);
			if($geofence_out_alert==1){
			$smsText="The vehicle $device_name is exited in $geofence_name at $device_users.";
			sendMobileNotification($conn,$user_id,$smsText,$asset_id);	
			}
			 }
			
		}
	}
	
}
function convertSpeed($kms, $unit = "miles") {

    if ($unit == 'angstrom') {
        $ratio = 10000000000000;
        $result = $kms * $ratio;
    }
    if ($unit == 'centimeters') {
        $ratio = 100000;
        $result = $kms * $ratio;
    }
    if ($unit == 'feet') {
        $ratio = 3280.84;
        $result = $kms * $ratio;
    }
    if ($unit == 'furlongs') {
        $ratio = 4.97;
        $result = $kms * $ratio;
    }
    if ($unit == 'inches') {
        $ratio = 39370.08;
        $result = $kms * $ratio;
    }
    if ($unit == ' meters') {
        $ratio = 1000;
        $result = $kms * $ratio;
    }
    if ($unit == 'microns') {
        $ratio = 1000000000;
        $result = $kms * $ratio;
    }
    if ($unit == 'miles') {
        $ratio = 1.609344;
        $result = $kms / $ratio;
    }
    if ($unit == 'millimeters') {
        $ratio = 1000000;
        $result = $kms * $ratio;
    }
    if ($unit == 'yards') {
        $ratio = 1093.61;
        $result = $kms * $ratio;
    }
    return $result;
}

function create_image_file($str, $img_name) {

    $strCamData = $str;

    $StrAscii = "";
    While (strlen($strCamData) > 0) {
        $strTemp = substr($strCamData, 0, 2);
        $Value1 = chr(hexdec($strTemp));
        $StrAscii = $StrAscii . $Value1;
        //remove first 2 chars
        $strCamData = substr($strCamData, 2);
    }
    //8859_1 is the ISO code for ASCII plus Latin. Require this for image.
    mb_detect_encoding($StrAscii, 'UTF-8, ISO-8859-1', true);
    // write to file
    $fp = fopen(CAPTURE_PATH . $img_name, 'w');
    fwrite($fp, $StrAscii);
    fclose($fp);
}

function convert_time_zone($date_time, $to_tz, $ret_format = 'Y-m-d H:i:s', $from_tz = 'UTC') {
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    $time_object->setTimezone(new DateTimeZone($to_tz));
    return $time_object->format($ret_format);
}
function deg_to_decimal_satish($deg) {

    if ($deg == '')
        return 0.000000;

    $sign = substr($deg, -1);

    if (strtoupper($sign) == "E" || strtoupper($sign) == "W")
        $sign = -1;
    if (strtoupper($sign) == "N" || strtoupper($sign) == "S")
        $sign = 1;

    $deg = substr($deg, 0, strlen($deg) - 1);

    $deg = preg_replace('/^0+/', '', $deg);

    $t_deg = explode('.', $deg);

    if (strlen($t_deg[0]) > 4)
        $len = 3;
    elseif ($sign == -1)
        $len = 1;
    else
        $len = 2;

    $degree = substr($deg, 0, $len);

    $deg = substr($deg, $len);

    $decimal = $sign * number_format(floatval((($degree * 1.0) + ($deg / 60))), 6);

    return $decimal;
}

function deg_to_decimal($deg) {

    if ($deg == '')
        return 0.000000;

    $sign = substr($deg, -1);

    if (strtoupper($sign) == "N" || strtoupper($sign) == "E")
        $sign = 1;
    if (strtoupper($sign) == "W" || strtoupper($sign) == "S")
        $sign = -1;

    $deg = substr($deg, 0, strlen($deg) - 1);

    // $deg = floatval($deg);

    $degree = substr($deg, 0, -7);
    $decimal = substr($deg, -7);

    ////echo "Degree : $degree, Decimal : $decimal";
    ////echo "$sign * number_format(floatval((($degree * 1.0) + ($deg/60))),6);";

    $decimal = $sign * number_format(floatval((($degree * 1.0) + ($decimal / 60))), 6);

    return $decimal;
}

function deg_to_decimal_new($deg) {

    if ($deg == '')
        return 0.000000;

    if ($deg < 0)
        $sign = -1;
    else
        $sign = 1;

    $deg = substr($deg, 1, strlen($deg));

    $deg = preg_replace('/^0+/', '', $deg);

    $degree = substr($deg, 0, 2);

    $deg = substr($deg, 2);

    $decimal = $sign * number_format(floatval((($degree * 1.0) + ($deg / 60))), 6);

    return $decimal;
}

function getNearestaddress($lat, $lng,$conn) {
	//return "INDIA";//Rajesh did this  
	$lat = round($lat,3);
	$lng = round($lng,3);
	$lat1 = round($lat,1);
	$lng1 = round($lng,1);
   $query = "SELECT  address, 
    ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
     * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) 
     * sin( radians( latitude ) ) ) ) AS distance 
   FROM tbl_geodata where latitude between ".($lat1-0.2)." and ".($lat1+0.2)." and longitude between ".($lng1-0.2)." and ".($lng1+0.2)."  HAVING distance < 0.25
   ORDER BY distance ASC LIMIT 1";     
    $res = mysqli_query($conn,$query) or deadme(mysqli_error($conn,$query) . ":" . $query);
    if (mysqli_num_rows($res) > 0) {
        $row = mysqli_fetch_assoc($res);
		print_r($row);
        return trim(trim(trim($row['address']),","));
    } else{
        $temp = getAddress($lat, $lng);
		print_r($temp);
        return $temp;
    } 
}
function getNearest($lat, $lng) {
	//return 'INDIA';   
		$address=googleGeocode($lat, $lng);
		//print_r($address);exit;
		return $address;
	 
	
	
	
/*
	$conn_79 = @mysql_connect("localhost","root","admin123") or deadme('{"success2":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db("geocode" ,$conn_79);
*/
//commented by Rajesh ighare 2017-08-18
	/* $lat = round($lat,3);
	$lng = round($lng,3);
	$lat1 = round($lat,1);
	$lng1 = round($lng,1);
   $query = "SELECT  address, 
    ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
     * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) 
     * sin( radians( latitude ) ) ) ) AS distance 
   FROM tbl_geodata where latitude between ".($lat1-0.2)." and ".($lat1+0.2)." and longitude between ".($lng1-0.2)." and ".($lng1+0.2)."  HAVING distance < 0.25
   ORDER BY distance ASC LIMIT 1";   
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($res) > 0) {
        $row = mysql_fetch_assoc($res);        
        return trim(trim(trim($row['address']),","));
    }else{
        $temp = getAddress($lat, $lng);
        return $temp;
    } */
	//end
} 
function OurOwnGeocoding($lat, $lng) { 
//return 'INDIA'; 
	// Developed by Rajesh ighare 2017-08-18
	//This is for our geocode server and using json formate.
	//http://94.23.203.31/nominatim/reverse.php?format=json&lat=24.016362&lon=79.544752
	/*$strURL="http://94.23.203.31/nominatim/reverse.php?format=json&lat=$lat&lon=$lng";	
	$address = "";	
	$resURL = curl_init();
	//WriteLog($strURL); 
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYHOST, FALSE);
    $jsonstr = curl_exec($resURL);
	if (!curl_errno($resURL)) {
		if(curl_getinfo($resURL, CURLINFO_HTTP_CODE)==200){
			$StringAddress = json_decode($jsonstr);		
			$address="";
			if(isset($StringAddress->address->road)) 
				$address .= $StringAddress->address->road.",";
			if(isset($StringAddress->address->neighbourhood)) 
				$address .= $StringAddress->address->neighbourhood.",";
			if(isset($StringAddress->address->village)) 
				$address .= $StringAddress->address->village.",";
			if(isset($StringAddress->address->town)) 
				$address .= $StringAddress->address->town.",";
			if(isset($StringAddress->address->city)) 
				$address .= $StringAddress->address->city.",";
			if(isset($StringAddress->address->state)) 
				$address .= $StringAddress->address->state.",";
			if(isset($StringAddress->address->postcode)) 
				$address .= $StringAddress->address->postcode.",";
			if(isset($StringAddress->address->country)) 
				$address .= $StringAddress->address->country;
			setlocale(LC_ALL, "as_IN.UTF-8");
			$address=iconv('UTF-8', 'ASCII//TRANSLIT',$address);
		}else{
			WriteLogROZ(" [nominatim Code] : ".curl_getinfo($resURL, CURLINFO_HTTP_CODE));
			$address = getNearestaddress($lat, $lng);
		}
	}else{
		$address = getNearestaddress($lat, $lng);
	}*/
	//$address = getNearestaddress($lat, $lng);
	//curl_close($resURL);
     if ($address != "") {
        return $address;
    } else{
		return getAddress($lat, $lng);
	}
}

function distance($a, $b) {
    list($lat1, $lon1) = $a;
    list($lat2, $lon2) = $b;
    $theta = $lon1 - $lon2;
    $dist = sin(@deg2rad($lat1)) * sin(@deg2rad($lat2)) + cos(@deg2rad($lat1)) * cos(@deg2rad($lat2)) * cos(@deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    return $miles;
}
function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y) {
    $i = $j = $c = 0;
    for ($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i++) {
        if ((($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
                ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i])))
            $c = !$c;
			}
    return $c;
}

function getAddress($lat, $lng) 
{
	WriteLogROZ(" [Google Address] : ".$lat." : ".$lng);
	//return "";
    $today = gmdate(DATE);
    if (intval($lat) == 0 && intval($lng) == 0) 
    {
        return '';
    }
	$conn = @mysql_connect(DIHOST,DIUSER,DIPASS) or deadme('{"success3":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db(DIDB,$conn);


        $limit_query = "SELECT google, mapquest, yahoo FROM api_requests WHERE add_date = '$today'";
        $limit_res = mysql_query($limit_query) or deadme(mysql_error() . ":" . $limit_query);

        if (mysql_num_rows($limit_res) > 0) 
	{
            $row = mysql_fetch_assoc($limit_res);
            if ($row['google'] <= 100000) 
	    {
             	$address = googleGeocode($lat, $lng);	
                $qryUpdt = "UPDATE api_requests SET google=" . intval($row['google'] + 1) . " WHERE add_date='$today'";
                $insrtGeo = mysql_query($qryUpdt) or deadme("SQL : $qryUpdt : Error : " . mysql_error());
            } 
	    elseif ($row['yahoo'] <= 4500) 
	    {
            	$address = yahooGeocode($lat, $lng);				
            	$qryUpdt = "UPDATE api_requests SET yahoo=" . intval($row['yahoo'] + 1) . " WHERE add_date='$today'";
                $insrtGeo = mysql_query($qryUpdt) or deadme(mysql_error() . ":" . $qryUpdt);
            } 
	    else 
	    { 
                $address =googleGeocode($lng, $lat );//photonGeocode($lng,$lat);//return address from Photon reverse geocode
            }
        } 
	else 
	{
		$qryGeo = "Insert into api_requests values(NULL,1,0,0,'$today')";
		$insrtGeo = mysql_query($qryGeo) or deadme(mysql_error() . ":" . $qryGeo);
		$address = googleGeocode($lng, $lat );
	}

	if ($address == '')
	return '';      
	
	/*$conn_79 = @mysql_connect("localhost","root","admin123") or deadme('{"success4":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db("geocode" ,$conn_79);*/
	
	$insert = "INSERT INTO tbl_geodata(latitude, longitude, address, add_date) VALUES ('" . addslashes($lat) . "', '" . addslashes($lng) . "', '" . addslashes($address) . "', '" . gmdate("Y-m-d H:i:s") . "')";
	mysql_query($insert) or deadme(mysql_error() . ":" . $insert);
	
	$conn = @mysql_connect(DIHOST,DIUSER,DIPASS) or deadme('{"success5":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db(DIDB,$conn);
	
	return $address;
}

/*
    Name :Rahul Funde
    Date :- 4-july-2017
    comment :- Added new Photon reverse geocode support. Not recording the daily count as this is unlimited and free
 */
function photonGeocode($lon,$lat) 
{
   //photon
    $lonlat = $lon . "&lat=" . $lat; //http://photon.komoot.de/reverse?lon=72.903455&lat=22.688816666666668
    $strURL = "http://photon.komoot.de/reverse?lon=".$lonlat ;


    $address = "";
    $resURL = curl_init();
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYHOST, FALSE);
    $xmlstr = curl_exec($resURL);

    $json = json_decode($xmlstr);
    foreach ($json->features as $val)
    {
        $address .= $val->properties->country." ";
        $address .= $val->properties->osm_key." ";
        $address .=$val->properties->city." ";
        $address .= $val->properties->postcode." ";
        $address .= $val->properties->name." ";
        $address .= $val->properties->state." ";

    }
    if ($address != "") {
        return $address;
    }else{
        return '';
    }
        
    
}
//end by rahul

function googleGeocode($lat, $lng) {
    //Google Map
	//https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&location_type=ROOFTOP&result_type=street_address&key=AIzaSyCuuefygL4r3OzV8qLQHOf_QAmTrcWdsY8
    $latlng = $lat . "," . $lng;    
    //$strURL = "https://maps.google.com/maps/api/geocode/xml?latlng=" . $latlng . "&key=AIzaSyAC8z-hOALCDcVgab9-tst8R3g7r66D7fU&sensor=false&region=in";
    $strURL = "https://maps.google.com/maps/api/geocode/xml?latlng=" . $latlng . "&key=AIzaSyBDc4TzkQ86YERrtCOCexTAyFe1MXAfVf8&sensor=false";
    $address = "";
//    $string = " Address = ".$strURL;
//        alerts_log($string);
   WriteLog('google->'.$strURL);
    $resURL = curl_init();
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYHOST, FALSE);
    $xmlstr = curl_exec($resURL);
	//WriteLog('add-issu:'.$xmlstr);

    $objDOM = new DOMDocument();
    

    if (!@$objDOM->loadXML($xmlstr)) {
       
        return '';
    }

    $address = @$objDOM->getElementsByTagName("formatted_address")->item(0)->nodeValue;
    
    $address = iconv('UTF-8', '', $address);
    if ($address != "") {
        return $address;
    }else{
        return '';
    }
}
function yahooGeocode($lat, $lng) {
    $address = "";
   
    $strURL = 'http://query.yahooapis.com/v1/public/yql?q=' . urlencode('select * from geo.places where text="(' . $lat . ',' . $lng . ')"') . '&diagnostics=true';  
    $resURL = curl_init();
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    $xmlstr = curl_exec($resURL);

    $objDOM = new DOMDocument();

    if (!@$objDOM->loadXML($xmlstr)) {       
        return '';
    }
   
    $address = '';
    if (@$objDOM->getElementsByTagName("locality1")->item(0)->nodeValue != "") {
        $address .= @$objDOM->getElementsByTagName("locality1")->item(0)->nodeValue;
    }
    ////WriteLog("yahoo_address".$address);
    if (@$objDOM->getElementsByTagName("admin2")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("admin2")->item(0)->nodeValue;
    }
    if (@$objDOM->getElementsByTagName("admin1")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("admin1")->item(0)->nodeValue;
    }
    if (@$objDOM->getElementsByTagName("postal")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("postal")->item(0)->nodeValue;
    }
    if (@$objDOM->getElementsByTagName("country")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("country")->item(0)->nodeValue;
    }    
    $address = iconv('UTF-8', '', $address);
    return $address;
}

function MapQuestGeocode($lat, $lng) {

    $strURL = "http://www.mapquestapi.com/geocoding/v1/reverse?key=Fmjtd|luuan16b21%2Ca2%3Do5-96rauy&lat=$lat&lng=$lng&callback=renderReverse&inFormat=kvp&outFormat=xml";
    $address = "";

    $resURL = curl_init();
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    $xmlstr = curl_exec($resURL);

    $objDOM = new DOMDocument();

    if (!@$objDOM->loadXML($xmlstr)) {        
        return '';
    }

    $address = @$objDOM->getElementsByTagName("street")->item(0)->nodeValue;
    if (@$objDOM->getElementsByTagName("adminArea5")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("adminArea5")->item(0)->nodeValue;
    }
    if (@$objDOM->getElementsByTagName("adminArea3")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("adminArea3")->item(0)->nodeValue;
    }
    if (@$objDOM->getElementsByTagName("adminArea4")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("adminArea4")->item(0)->nodeValue;
    }
    if (@$objDOM->getElementsByTagName("adminArea1")->item(0)->nodeValue != "") {
        $address .= ", " . @$objDOM->getElementsByTagName("adminArea1")->item(0)->nodeValue;
    }
    $address = iconv('UTF-8', '', $address);
    return $address;
}
function getDistance($lat1, $lng1, $lat2, $lng2, $unit) {
    $distance = 0;

    if ($lat1 && $lng1) {
        $dist = 0;
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
		 $unit = strtoupper($unit);
        if ($unit == "KM") {
			$distance=($miles * 1.609344);           
        } else if ($unit == "N") {
            $distance = ($miles * 0.8684);
        } else {
            $distance = $miles;
        }
    }
    return $distance;
}
function getDistance_ools($lat1, $lng1, $lat2, $lng2, $unit) {
    $distance = 0;

    if ($lat1 && $lng1) {
        $dist = 0;
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        if ($unit == "KM") {

            $dstn = round(($miles * 1.609344), 2);
            if (!is_nan($dstn)) {

                $distance = $dstn;
            }
        } else if ($unit == "N") {
            $distance = ($miles * 0.8684);
        } else {
            $distance = $miles;
        }
    }
    return $distance;
}

function chat_alert($to_mail, $to_mesg) {
    if ($to_mail != '' && $to_mesg != '') {

        $data = array('to' => $to_mail, 'msg' => $to_mesg);

        $ch = curl_init();
        $url = "http://localhost/telnet/chat_alert.php";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $curl_result = curl_exec($ch);
        curl_close($ch);
    }
}
 
function WriteLog($string) {
    $myFile = "log_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}
function Write_near_area_Log($string) {
    $myFile = "Near area log_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}
function WriteLogROZ($string) {
    $myFile = "log_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}
function WriteLogodo($string) {
    $myFile = "log_Odemeter_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	 
}
function WriteKm($device_dt,$string) {
    $myFile = "log_KM_" .$device_dt. ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}
function WriteLogNewDevice($string) {
    $myFile = "log_New" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}
function WriteLogExpire($string) {
    $myFile = "log_exp_device_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	 
}

function sec2HourMinute($seconds) {

    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds / 60) % 60);
    //$seconds = $seconds % 60;
    $HourMinute = "";
    if ($hours > 0)
        $HourMinute .= "$hours Hours ";
    if ($minutes > 0)
        $HourMinute .= "$minutes Minutes ";

    return $HourMinute;
}

function send_sms($mobile, $smsText, $template = 0, $template_data = NULL) {
	WriteLogNewDevice("$mobile $smsText");
	//return true;
	 if (trim($mobile) == '')
        return '';  
            $mobile = explode(",", $mobile);
            $mobNew = array();
            foreach ($mobile as $mob) {
                if (strlen($mob) >= 9)
                    $mobNew[] = trim(str_replace(',', '', $mob));
            }
            $mob = implode(",", $mobNew);
            $url = 'http://apis.numeraliot.com:9998/sms/user?username=fleet&pass=KND5W3!@3';
            $url .= '&mobileno=' . urlencode($mob);          
            $url .= '&message=' . urlencode($smsText); 
            $ch = curl_init(); 			
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            if (curl_errno($ch)) {
                $error = curl_error($ch);
                WriteLogNewDevice("CURL Error: in send_sms(): " . $error);
            } else {
                curl_close($ch);
                WriteLogNewDevice("Mobile no:" . $mob . "smsText:" . $smsText." URL: ".$url." Responce:".htmlspecialchars($output));              
            }
            return htmlspecialchars($output);
}

function send_email($to, $subject, $msg) {

    require_once('PHPMailer/class.phpmailer.php');
//Email Address: mycar@saitracker.in
//Password: N%#oAXu1
	$to ="cgsrajesh84@gmail.com";
    $to_e = explode(',', $to);
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->SMTPAuth = true;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->Username = "noreply.angelseye@gmail.com";
    $mail->Password = "angelseye@#2019";
    $mail->SetFrom('noreply.angelseye@gmail.com', 'Tracking');
    $mail->Subject = $subject; 
    $mail->MsgHTML($msg);
    foreach ($to_e as $to_mail) {
        $mail->AddAddress($to_mail);
    }
	
	$log_stamp1[0] 	= time() ;
	
    if ($mail->Send()) {
        //WriteLog("Mail Sent to $to\n Message :\n$msg");
		$log_stamp1[1] 	= time() - $log_stamp1[0];
		
		WriteLogNewDevice("[".$log_stamp1[1]."] [EMAILLOG] $to => $msg");
		
        return true;
		
    } else {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        WriteLogNewDevice("Mail Sent to $to\n Message :\n$error");
        return false;
    }
}

function sms_log($mobile, $smsText, $user_id = 1) {
    $mobile = explode(",", $mobile);
    $values = array();
    foreach ($mobile as $mob) {
        $values[] = "($user_id, '$mob', '$smsText', '" . gmdate(DATE_TIME) . "')";
    }
    $values = implode(",", $values);
    $sqlU = "INSERT INTO smslog (user_id, mobile, sms_text, add_date) VALUES $values";
    mysql_query($sqlU) or deadme(mysql_error() . ":" . $sqlU);
}

function email_log($emailid, $smsText, $user_id = 1, $desc = "") {
    $emailid = explode(",", $emailid);
    $values = array();
    foreach ($emailid as $em) {
        $values[] = "($user_id, '$em', '" . $smsText . "', '$desc', '" . gmdate(DATE_TIME) . "')";
    }
    $values = implode(",", $values);
    $sqlU = "INSERT INTO emaillog (user_id, email_id, email_text, description, add_date) VALUES $values";
    mysql_query($sqlU) or deadme(mysql_error() . ":" . $sqlU);
}

function final_result_xml($theString, $bool, $SQLError = '', $ex_tag = '') {

    $myFile = "xml_log_" . date(DISP_DATE) . ".txt";
    if ($SQLError != '') {
        $fh = fopen($myFile, 'a') or deadme("can't open file");
        $stringData = "\r\n" . CURRENT_TIME . " : " . $theString . " : SQL : " . $SQLError;
        fwrite($fh, $stringData);
        fclose($fh);
    }

    $final_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<root>";
    $final_xml .= "<time>" . CURRENT_TIME . "</time>\n\n<result>$bool</result>\n<message>$theString</message>";

    if ($ex_tag != '') {
        $final_xml .= $ex_tag;
    }
    $final_xml .= "\n</root>";
    @mysql_close();
    deadme($final_xml);
}

function log_raw_data($asset_id,$device,$datetime="",$data,$conn) {	
    $rawsql = "INSERT INTO tbl_raw_data (asset_id,device_id, log_date,log_data,add_date) VALUES ('" . $asset_id . "','" . $device . "','".date("Y-m-d")."','" . $data . "','" . $datetime . "')";
    $raw_res = mysqli_query($conn,$rawsql) or deadme(mysqli_error($conn) . ":" . $rawsql);
} 

function CheckDeviceIdMo($device, $sim_number, $email_id) {
    $sim_number = trim($sim_number);
    $email_id = trim($email_id);
    $group_id = 2;
    $user = 794;
    $query = "SELECT id,assets_name,email_id  FROM assests_master WHERE device_id = '$device' and status=1";
    $result = mysql_query($query) or deadme(mysql_error() . ":" . $query);

    if (mysql_num_rows($result) == 0) {
        if (strlen($sim_number) > 11) {
            $lSql = "INSERT INTO assests_master (assets_name,device_id, assets_type_id,assets_category_id,assets_group_id,device_status,sim_number,add_uid,add_date,battery_size,status, email_id) VALUES ('UNKOWN', '" . $device . "' , 3,4,$group_id,1,'" . $sim_number . "',794,'" . date('Y-m-d H:i:s') . "',12,1,'" . $email_id . "')";
            mysql_query($lSql) or deadme("SQL : $lSql : Error : " . mysql_error());
        } else {
            $lSql = "INSERT INTO assests_master (assets_name,device_id, assets_type_id,assets_category_id,assets_group_id,device_status,sim_number,add_uid,add_date,battery_size,status, email_id) VALUES ('" . $sim_number . "', '" . $device . "' , 3,4,$group_id,1,'" . $sim_number . "',794,'" . date('Y-m-d H:i:s') . "',12,1,'" . $email_id . "')";
            mysql_query($lSql) or deadme("SQL : $lSql : Error : " . mysql_error());
        }

        $insert_id = mysql_insert_id();
    } else {
        $a_row = mysql_fetch_assoc($result);
        if ($email_id != "" && $a_row['email_id'] != $email_id) {
            $sql = "Update assests_master set email_id = '" . $email_id . "' where device_id = '$device'";
            mysql_query($sql) or deadme("SQL : $lSql : Error : " . mysql_error());
        }

        // //WriteLog(strlen($sim_number)." ".$a_row['assets_name']);
        if ($a_row['assets_name'] != $sim_number) {
            $sql = "Update assests_master set assets_name ='" . $sim_number . "' where device_id = '$device'";
            mysql_query($sql) or deadme("SQL : $lSql : Error : " . mysql_error());
        }
        ////WriteLog("devcie already exist ".$sim_number);
    }


    $sql = "SELECT * FROM `user_assets_map` where user_id = $user";

    //$query = $this->db->query($sql);
    $query = mysql_query($sql) or deadme("SQL : $lSql : Error : " . mysql_error());

    //foreach ($query->result() as $row) {
    while ($row = mysql_fetch_array($query)) {
        if ($row['assets_ids'] != "") {
            $temp_as = explode(',', $row['assets_ids']);
            if (!in_array($insert_id, $temp_as)) {
                $upd = 'UPDATE user_assets_map SET assets_ids = concat(assets_ids, ",' . $insert_id . '") WHERE id=' . $row['id'];
                //$this->db->query($upd);
                mysql_query($upd) or deadme("SQL : $upd : Error : " . mysql_error());
            }
        } else {
            $upd = 'UPDATE user_assets_map SET assets_ids = ' . $insert_id . ' WHERE id=' . $row['id'];
            //$this->db->query($upd);
            mysql_query($upd) or deadme("SQL : $upd : Error : " . mysql_error());
        }
        if ($row['group_id'] != "") {
            $temp_as = explode(',', $row['group_id']);
            if (!in_array($group_id, $temp_as)) {
                $upd = 'UPDATE user_assets_map SET group_id = concat(group_id, ",' . $group_id . '") WHERE id=' . $row['id'];
                //$this->db->query($upd);
                mysql_query($upd) or deadme("SQL : $upd : Error : " . mysql_error());
            }
        } else {
            $upd = 'UPDATE user_assets_map SET group_id = ' . $group_id . ' WHERE id=' . $row['id'];
            //$this->db->query($upd);
            mysql_query($upd) or deadme("SQL : $upd : Error : " . mysql_error());
        }
    }
}

//Added by Poonam
function CheckDeviceID($device,$conn) {	
		$query = "SELECT id,COUNT(1) FROM `tbl_devices` where device_id='$device' and status=1";
		//print_r($query);
		$result = mysqli_query($conn,$query) or deadme(mysqli_error($conn,$query) . ":" . $query);
		//var_dump(mysqli_num_rows($result));
		if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_array($result);
		return $row;
		}
		
		return 0;    
}
function  add_device($device_id,$vehicle_name,$user_id,$phone_number,$installation_date,$installation_address,$installation_company,$installation_person,$installation_position){
	//$device_id 
	$date=gmdate(DATE_TIME);	
	$query="INSERT INTO `assests_master` (`assets_name`, `device_id`, `assets_friendly_nm`, `device_desc`, `icon_id`, `sim_number`, `assets_type_id`, `assets_category_id`, `assets_division`, `assets_owner`, `assets_group_id`, `assets_image_path`, `driver_name`, `driver_image`, `driver_mobile`, `device_status`, `max_speed_limit`, `max_fuel_capacity`, `max_fuel_liters`, `battery_size`, `telecom_provider`, `km_reading`, `min_temprature`, `max_temprature`, `eng_runtime`, `tank_type`, `tank_diameter`, `tank_width`, `tank_length`, `fuel_in_per_lit`, `fuel_in_company_name`, `fuel_in_product_code`, `fuel_out_per_lit`, `fuel_out_company_name`, `fuel_out_product_code`, `email_id`, `vehiclesNumber`, `lock_msg`, `unlock_msg`, `ExpInsuranceDate`, `OilChangeKM`, `NextServiceKM`, `Tyresfields`, `add_expire`, `add_date`, `add_uid`, `fuel_in_out_sensor`, `xyz_sensor`, `rollover_tilt`, `panic`, `runtime`, `sensor_type`, `asset_users_id`, `status`,installation_date,installation_address,installation_company,installation_person,installation_position) VALUES ('".$vehicle_name."', '".$device_id."', '', '', '12', '".$phone_number."', '11', '107', '', '', '50', '', '', '', '', '1', '80', '', '', '12', '1', '', '', '', '', 'horizontal_cylinder', '', '', '', '', '', '', '', '', '', '', '".$device_id."', '', '', '$date', '', '', '', '2050-09-28 16:43:22', '$date', '$user_id', 0, 0, 0, 0, 0, '', '', 1,'".$installation_date."','".$installation_address."','".$installation_company."','".$installation_person."','".$installation_position."')"; 
	//print_r($query);  
	$result = mysql_query($query) or deadme(mysql_error() . ":" . $query);
	$row['result']="false";
	 if (mysql_num_rows($result) == 0){
		$insert_id = mysql_insert_id();
		$row['result']="true";
		$row['last_inserted_id']=$insert_id;
	 }
	die(json_encode($insert_id)); 
}

function CheckExprityDate($device) {
    $query = "SELECT COUNT(1) AS count FROM user_assets_map uam LEFT JOIN tbl_users tu ON uam.user_id=tu.user_id WHERE FIND_IN_SET ((SELECT id FROM assests_master am WHERE am.device_id='$device'  AND am.status=1), assets_ids) AND uam.status=1 AND tu.status=1 AND date(tu.acc_expiry_date) < CURDATE()";
    $result = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($result) > 0) {
        $row = mysql_fetch_array($result);
        return $row['count'];
    }
}

// added by harshal date 21-4-2015
function current_event_track($current_event, $assets_id, $device_id, $event_name) {
    ////WriteLog($current_event." ".$device_id);
    $query = "SELECT id, assets_id FROM tbl_track WHERE assets_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($res) > 0) {
        $row = mysql_fetch_array($res);
        ////WriteLog($current_event." ".$row['id']);
        $query = "UPDATE tbl_track SET current_event = '" . $current_event . "',event_geofence = '" . $event_name . "' WHERE id = " . $row['id'];
        mysql_query($query) or deadme(mysql_error() . ":" . $query);
        $query = "UPDATE tbl_last_point SET current_event = '" . $current_event . "' WHERE device_id = '" . $device_id . "'";
        mysql_query($query) or deadme(mysql_error() . ":" . $query);
    }
}

// end
// added for push notifilation
function sendMobileNotification_olds($user_id, $smsText, $type = 'Alert', $assets_id="") 
{
	$log_stamp1[0] 	= time();
	
	$smsText1=$smsText.'<br/>DateTime: '.date('Y-m-d H:i:s');
    $url = 'http://62.75.141.223/dev/pushnotification.php?user_id=' . urlencode($user_id) . '&msg=' . urlencode($smsText1) . '&type=' . urlencode($type) . '&assets_id=' . urlencode($assets_id) . '';
        
	$query = "INSERT INTO tbl_pending_notifications ( `user_id`, `msg`, `type`, `assets_id`, `add_date` ) VALUES ('". ($user_id)."','". ($smsText1)."','". ($type)."','". ($assets_id)."','". date(DATE_TIME)."' )";
    mysql_query($query) or deadme(mysql_error() . ":" . $query);
        
	
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($curl, CURLOPT_HEADER, false);
    curl_exec($curl);  
    //die('push notifilation');
	$log_stamp1[1] 	= time() - $log_stamp1[0];	
	WriteLogROZ("[".$log_stamp1[1]."] = $url");	
    if (curl_errno($curl)) {
        $error = curl_error($curl); 

        WriteLogROZ("CURL Error: in sendMobileNotification(): " . $error);
    } else {
        WriteLogROZ("in sendMobileNotification(): mobile_no:  " . $user_id . "SMS text: " . $smsText);
        curl_close($curl);
    } 
}  

function area_in_out($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $current_speed, $x_address, $ist) {
	
    global $current_area, $current_area_id, $dts;

		//WriteLog('area:'.$current_area); 
    $insert_data = false;

    //$sqlP = "SELECT DISTINCT (am.polyid) AS area_id, am.out_alert, am.in_alert, am.speed_value, am.speed_unit, am.email_alert as email_alert, am.sms_alert as sms_alert, um.user_id, um.first_name, um.username, um.mobile_number, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, (SELECT group_concat(mobile_no) as mobile_no FROM addressbook where find_in_set(id, am.addressbook_ids)) as addressbook_mobile, sr.area_in_ch_alert, sr.area_out_ch_alert FROM areas am LEFT JOIN tbl_users um ON um.user_id = am.Audit_Enter_uid left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET( $assets_id, deviceid ) and am.Audit_Del_Dt is null and am.Audit_Status = 1";
    
	$sqlP = "SELECT am.center_point_lat,am.center_point_long,am.polyid AS area_id, am.out_alert, am.in_alert, am.speed_value, am.speed_unit, am.email_alert as email_alert, am.sms_alert as sms_alert, 
	um.user_id, um.first_name, um.username, um.mobile_number, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, 
	(SELECT group_concat(mobile_no) as mobile_no FROM addressbook where find_in_set(id, am.addressbook_ids)) as addressbook_mobile, sr.area_in_ch_alert, sr.area_out_ch_alert 
	FROM areas_mst am 
	LEFT JOIN tbl_users um ON um.user_id = am.Audit_Enter_uid 
	left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 
	WHERE FIND_IN_SET( $assets_id, deviceid ) and am.Audit_Del_Dt is null and am.Audit_Status = 1";

    $rsP = mysql_query($sqlP) or deadme("Failed to Execute, SQL : $sqlP, Error : " . mysql_error());
    while ($rowP = mysql_fetch_array($rsP)) 
	{
        $area_id = $rowP['area_id'];
        $out_alert = $rowP['out_alert'];
        $in_alert = $rowP['in_alert'];
        $l_speed = $rowP['speed_value'];
        $l_unit = $rowP['speed_unit'];
        $area_email_alert = $rowP['email_alert'];
        $addressbook_mobile = $rowP['addressbook_mobile'];
       // $center_point_lat = $rowP['center_point_lat'];
        //$center_point_long = $rowP['center_point_long'];


        $user_id = $rowP['user_id'];
        $fname = $rowP['first_name'];
        $mobile = $rowP['mobile_number'];
        $email = $rowP['email_address'];
        $user_email_alert = $rowP['user_email_alert'];
        $user_sms_alert = $rowP['user_sms_alert'];
        $area_sms_alert = $rowP['sms_alert'];
        $area_in_ch_alert = $rowP['area_in_ch_alert'];
        $area_out_ch_alert = $rowP['area_out_ch_alert'];
        $language = $rowP['language'];
        if ($language == "portuguese") {
            $file = "portuguese_alert_lang.php";
        } else {
            $file = "english_alert_lang.php";
        }
        include($file);
		/* if($center_point_lat !='' && $center_point_lat !=0 && $center_point_long !='' && $center_point_long !=0){
			//Amol
			$distance_area=getDistance($latitude_y,$longitude_x,$center_point_lat,$center_point_long,'KM');
			WriteLog('area_distance'.$distance_area);
			
		} */
		
		
		
        /*
          if($l_speed) $current_speed = convertSpeed($current_speed, $l_unit);

          if($current_speed > $l_speed) {

          }
         */

        $sql = "SELECT pointid,lat,lng,polyname FROM areas WHERE polyid ='$area_id'  and Audit_Status=1";
        $rs = mysql_query($sql) or deadme("Failed to Execute, SQL : $sql, Error : " . mysql_error());
        $vertices_x = array();
        $vertices_y = array();
        while ($row = mysql_fetch_array($rs)) {
            $point_id = $row['pointid'];
            $vertices_x[$point_id] = $row['lat'];
            $vertices_y[$point_id] = $row['lng'];
            $area_name = $row['polyname'];
        }

        $vx = array_values($vertices_x);
        $vy = array_values($vertices_y);

        //$vertices_x = array(22.304732, 22.304573, 22.315134, 22.315809); // x-coordinates of the vertices of the polygon
        //$vertices_y = array(70.763755,70.77178,70.761781,70.771737); // y-coordinates of the vertices of the polygon
        $points_polygon = count($vx); // number vertices
        //$longitude_x = $_GET["longitude"]; // x-coordinate of the point to test
        //$latitude_y = $_GET["latitude"]; // y-coordinate of the point to test
        //// For testing.  This point lies inside the test polygon.
        // $longitude_x = 37.62850;
        // $latitude_y = -77.4499;

        $sql = "SELECT inout_status FROM area_inout_log where area_id = $area_id and device_id = $assets_id order by id desc limit 1";
        $rs = mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
        if (mysql_num_rows($rs) > 0) {
            $row = mysql_fetch_array($rs);
            if ($row['inout_status'] == 'in') 
                $status = 1;
            else
                $status = 0;
        }else {
            $status = 0;
        }

        if (is_in_polygon($points_polygon, $vx, $vy, $longitude_x, $latitude_y)) {
            ////echo "In polygon!";
            ////WriteLog("\nArea Name : $area_name");
            $current_area = $area_name;
            $current_area_id = $area_id;
            if ($status == 0) {

                $areaLogSql = "insert into area_inout_log (user_id, device_id, area_id, lat, lng, address, date_time, inout_status) values($user_id, '" . $assets_id . "', '" . $area_id . "', '" . $longitude_x . "', '" . $latitude_y . "', '" . $x_address . "', '" . date(DATE_TIME) . "', 'in')";
                mysql_query($areaLogSql) or deadme(mysql_error() . ":" . $areaLogSql);
                $insert_data = true;
                current_event_track("Area In", $assets_id, $device_id, "Area Name :$area_name");
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if (isset($driver_mobile) && $driver_mobile != '') {
                    if ($txt == '') {
                        $txt .= $driver_mobile;
                    } else {
                        $txt .= ', ' . $driver_mobile;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ') ';
                else
                    $str = '';
                $smsText = $lang['Alert for'] . " $assets_name " . $str . '' . $lang['Entered Area'] . " $area_name, " . convert_time_zone($ist, $dts, DISP_TIME); // date(DISP_TIME);
                //sms template
                //Dear  [F1],  [F2] ( [F3],  [F4]) is now in area  [F5]
                $template_id = '3822';
                $f1 = $fname;
                $f2 = $assets_name;
                $f3 = $nick_name;
                $f4 = $driver_name;
                $f5 = $area_name;
                $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);
                    sendMobileNotification($user_id, $smsText,'Alert',$assets_id);

                //

                if ($mobile != "" && $user_sms_alert == 1 && $area_sms_alert == 1 && $in_alert == 1 && ($area_in_ch_alert == 1 || $area_in_ch_alert == NULL)) {
                    send_sms($mobile, $smsText, $template_id, $template_data);
                    sms_log($mobile, $smsText, $user_id);
                }

                if ($email != "" && $user_email_alert == 1 && $area_email_alert == 1 && $in_alert == 1 && ($area_in_ch_alert == 1 || $area_in_ch_alert == NULL)) {
                    // $smsText .= ', Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$longitude_x.','.$latitude_y.'('.$assets_name.')&ie=UTF8&z=12&om=1">here</a> to view on map.';
                    $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $longitude_x . ',' . $latitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                    send_email($email, $lang['In Area Alert to User'], $smsText);
                    email_log($email, $smsText, $user_id, 'In Area Alert to User');

                    // chat_alert($email, $smsText);
                }
                if ($addressbook_mobile != "") {
                    //send sms addressbook contact
                    //send_sms($mobile, $smsText, $template_id, $template_data);
                    send_sms($addressbook_mobile, $smsText, $template_id, $template_data);
                    sms_log($addressbook_mobile, $smsText, $user_id);
                }
                if ($area_in_ch_alert == 1 || $area_in_ch_alert == NULL) {
                    sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                    //insert in alert master
                    $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( '" . $lang['In Area Alert to User'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude_y."','".$longitude_x."','".$x_address."')";
					//$longitude_x, $latitude_y
                    mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
                }
            }
        } else {
            ////echo "Is not in polygon";
            if ($status == 1) {

                $areaLogSql = "INSERT INTO area_inout_log (user_id, device_id, area_id, lat, lng, address, date_time, inout_status) VALUES ($user_id, '" . $assets_id . "', '" . $area_id . "', '" . $longitude_x . "', '" . $latitude_y . "', '" . $x_address . "', '" . date(DATE_TIME) . "', 'out')";
                mysql_query($areaLogSql) or deadme(mysql_error() . ":" . $areaLogSql);
                $insert_data = true;
                current_event_track("Area Out", $assets_id, $device_id, "Area Name : $area_name");
                //Dear $fname, 
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if (isset($driver_mobile) && $driver_mobile != '') {
                    if ($txt == '') {
                        $txt .= $driver_mobile;
                    } else {
                        $txt .= ', ' . $driver_mobile;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ') ';
                else
                    $str = '';
                $smsText = "$assets_name " . $str . '' . $lang['is now out of area'] . " $area_name, " . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME);
                //sms template
                //Dear [F1], [F2] ([F3], [F4]) is now out of area [F5]
                $template_id = '3823';
                $f1 = $fname;
                $f2 = $assets_name;
                $f3 = $nick_name;
                $f4 = $driver_name;
                $f5 = $area_name;
                $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);

                if ($mobile != "" && $user_sms_alert == 1 && $area_sms_alert == 1 && $out_alert == 1 && ($area_in_ch_alert == 1 || $area_in_ch_alert == NULL)) {
                    send_sms($mobile, $smsText, $template_id, $template_data);
                    sms_log($mobile, $smsText, $user_id);
                }

                if ($email != "" && $user_email_alert == 1 && $area_email_alert == 1 && $out_alert == 1 && ($area_in_ch_alert == 1 || $area_in_ch_alert == NULL)) {
                    $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $longitude_x . ',' . $latitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                    send_email($email, $lang['Out of Area alert to User'], $smsText);
                    email_log($email, $smsText, $user_id, 'Out of Area alert to User');
                    // chat_alert($email, $smsText);
                }

                if ($addressbook_mobile != "") {     //send sms addressbook contact
                    //send_sms($mobile, $smsText, $template_id, $template_data);					
                    send_sms($addressbook_mobile, $smsText, $template_id, $template_data);
                    sms_log($addressbook_mobile, $smsText, $user_id);
                }
                if ($area_in_ch_alert == 1 || $area_in_ch_alert == NULL) {
                    sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                    //insert in alert master
                    $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( '" . $lang['Out of Area alert to User'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$longitude_x."','".$latitude_y."','".$x_address."')";//$x_address
                    mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
                }
            }
        }
    }
    return $insert_data;
}

function zone_in_out($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $current_speed, $ist) {
    global $current_zone, $current_zone_id, $dts;

    $insert_data = false;

    $sqlP = "SELECT DISTINCT (am.polyid) AS area_id, am.out_alert, am.in_alert, am.speed_value, am.speed_unit, am.email_alert as email_alert, am.sms_alert as sms_alert, um.user_id, um.first_name, um.username, um.mobile_number, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, (SELECT group_concat(mobile_no) as mobile_no FROM addressbook where find_in_set(id, am.addressbook_ids)) as addressbook_mobile FROM landmark_areas am LEFT JOIN tbl_users um ON um.user_id = am.Audit_Enter_uid WHERE FIND_IN_SET( $assets_id, deviceid ) and am.Audit_Del_Dt is null and am.Audit_Status = 1";

    $rsP = mysql_query($sqlP) or deadme("Failed to Execute, SQL : $sqlP, Error : " . mysql_error());
    while ($rowP = mysql_fetch_array($rsP)) {
        $area_id = $rowP['area_id'];
        $user_id = $rowP['user_id'];
        $fname = $rowP['first_name'];
        $mobile = $rowP['mobile_number'];
        $email = $rowP['email_address'];
        $user_email_alert = $rowP['user_email_alert'];
        $user_sms_alert = $rowP['user_sms_alert'];
        $area_email_alert = $rowP['email_alert'];
        $area_sms_alert = $rowP['sms_alert'];
        $l_speed = $rowP['speed_value'];
        $l_unit = $rowP['speed_unit'];
        $out_alert = $rowP['out_alert'];
        $in_alert = $rowP['in_alert'];

        $addressbook_ids = $rowP['addressbook_ids'];
        $language = $rowP['language'];
        if ($language == "portuguese") {
            $file = "portuguese_alert_lang.php";
        } else {
            $file = "english_alert_lang.php";
        }
        include($file);
        if ($l_speed)
            $current_speed = convertSpeed($current_speed, $l_unit);

        if ($current_speed > $l_speed) {
            
        }

        $sql = "SELECT * FROM landmark_areas WHERE polyid = $area_id";
        $rs = mysql_query($sql) or deadme("Failed to Execute, SQL : $sql, Error : " . mysql_error());
        $vertices_x = array();
        $vertices_y = array();
        while ($row = mysql_fetch_array($rs)) {
            $point_id = $row['pointid'];
            $vertices_x[$point_id] = $row['lat'];
            $vertices_y[$point_id] = $row['lng'];
            $area_name = $row['polyname'];
        }

        $vx = array_values($vertices_x);
        $vy = array_values($vertices_y);

        //$vertices_x = array(22.304732, 22.304573, 22.315134, 22.315809); // x-coordinates of the vertices of the polygon
        //$vertices_y = array(70.763755,70.77178,70.761781,70.771737); // y-coordinates of the vertices of the polygon
        $points_polygon = count($vx); // number vertices
        //$longitude_x = $_GET["longitude"]; // x-coordinate of the point to test
        //$latitude_y = $_GET["latitude"]; // y-coordinate of the point to test
        //// For testing.  This point lies inside the test polygon.
        // $longitude_x = 37.62850;
        // $latitude_y = -77.4499;

        $sql = "SELECT * FROM zone_inout_log where area_id = $area_id and device_id = $assets_id order by id desc limit 1";
        // //WriteLog("\nPoly SQL : $sql");
        $rs = mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
        if (mysql_num_rows($rs) > 0) {
            $row = mysql_fetch_array($rs);
            if ($row['inout_status'] == 'in')
                $status = 1;
            else
                $status = 0;
        }else {
            $status = 0;
        }

        /*
          if($area_id == 44 && $assets_id == 381) {
          $sub[] = print_r($vx, true);
          $sub[] = print_r($vy, true);
          $sub[] = $points_polygon;
          $sub[] = $longitude_x;
          $sub[] = $latitude_y;
          $sub_str = implode(': ', $sub);
          $polygon = is_in_polygon($points_polygon, $vx, $vy, $longitude_x, $latitude_y);
          //WriteLog("\ZONE SQL : $sql, $sub_str, Polygon Status : $polygon");
          }
         */

        if (is_in_polygon($points_polygon, $vx, $vy, $longitude_x, $latitude_y)) {
            ////echo "In polygon!";
            $current_zone = $area_name;
            $current_zone_id = $area_id;
            if ($status == 0) {

                $areaLogSql = "insert into zone_inout_log (user_id, device_id, area_id, lat, lng, date_time, inout_status) values($user_id, '" . $assets_id . "', '" . $area_id . "', '" . $longitude_x . "', '" . $latitude_y . "', '" . date(DATE_TIME) . "', 'in')";
                mysql_query($areaLogSql)or deadme(mysql_error() . ":" . $areaLogSql);
                $insert_data = true;
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if (isset($driver_mobile) && $driver_mobile != '') {
                    if ($txt == '') {
                        $txt .= $driver_mobile;
                    } else {
                        $txt .= ', ' . $driver_mobile;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                $smsText = $lang['Alert for'] . " $assets_name $str " . $lang['Entered Zone'] . " $area_name, " . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME);
                //sms template
                //Dear  [F1],  [F2] ( [F3],  [F4]) is now in area  [F5]
                $template_id = '3822';
                $f1 = $fname;
                $f2 = $assets_name;
                $f3 = $nick_name;
                $f4 = $driver_name;
                $f5 = $area_name;
                $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);

                if ($mobile != "" && $user_sms_alert == 1 && $area_sms_alert == 1 && $in_alert == 1) {
                    send_sms($mobile, $smsText, $template_id, $template_data);
                    sms_log($mobile, $smsText, $user_id);
                }

                if ($email != "" && $user_email_alert == 1 && $area_email_alert == 1 && $in_alert == 1) {
                    $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $longitude_x . ',' . $latitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                    send_email($email, $lang['In Zone Alert to User'], $smsText);
                    email_log($email, $smsText, $user_id, 'In Zone Alert to User');
                    // chat_alert($email, $smsText);
                }
                if ($addressbook_mobile != "") {     //send sms addressbook contact
                    //send_sms($mobile, $smsText, $template_id, $template_data);
                    send_sms($addressbook_mobile, $smsText, $template_id, $template_data);
                    sms_log($addressbook_mobile, $smsText, $user_id);
                }
                //insert in alert master
				// $longitude_x, $latitude_y
                if ($area_sms_alert == 1 || $area_email_alert == 1) {
                    $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $lang['Zone In Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude_y."','".$longitude_x."')";
                     mysql_query($alertSql);
                }
            }
        } else {
            ////echo "Is not in polygon";
            if ($status == 1) {

                $areaLogSql = "INSERT INTO zone_inout_log (user_id, device_id, area_id, lat, lng, date_time, inout_status) VALUES ($user_id, '" . $assets_id . "', '" . $area_id . "', '" . $longitude_x . "', '" . $latitude_y . "', '" . date(DATE_TIME) . "', 'out')";
                mysql_query($areaLogSql) or deadme(mysql_error() . ":" . $areaLogSql);
                $insert_data = true;
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if (isset($driver_mobile) && $driver_mobile != '') {
                    if ($txt == '') {
                        $txt .= $driver_mobile;
                    } else {
                        $txt .= ', ' . $driver_mobile;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                //Dear $fname, 
                $smsText = "$assets_name $str " . $lang['is now out of zone'] . " $area_name, " . convert_time_zone($ist, $dts, DISP_TIME); // . date(DISP_TIME);
                //sms template
                //Dear [F1], [F2] ([F3], [F4]) is now out of area [F5]
                $template_id = '3823';
                $f1 = $fname;
                $f2 = $assets_name;
                $f3 = $nick_name;
                $f4 = $driver_name;
                $f5 = $area_name;
                $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);

                if ($mobile != "" && $user_sms_alert == 1 && $area_sms_alert == 1 && $out_alert == 1) {
                    send_sms($mobile, $smsText, $template_id, $template_data);
                    sms_log($mobile, $smsText, $user_id);
                }

                if ($email != "" && $user_email_alert == 1 && $area_email_alert == 1 && $out_alert == 1) {
                    $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $longitude_x . ',' . $latitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                    send_email($email, $lang['Out of Zone alert to User'], $smsText);
                    email_log($email, $smsText, $user_id, 'Out of Zone alert to User');
                    // chat_alert($email, $smsText);
                }

                if ($addressbook_mobile != "") {     //send sms addressbook contact
                    //send_sms($mobile, $smsText, $template_id, $template_data);					
                    send_sms($addressbook_mobile, $smsText, $template_id, $template_data);
                    sms_log($addressbook_mobile, $smsText, $user_id);
                }
                //insert in alert master
                if ($area_sms_alert == 1 || $area_email_alert == 1) {
                    $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $lang['Zone Out Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude_y."','".$longitude_x."')";
                    // mysql_query($alertSql);
                }
            }
        }
    }
    return $insert_data;
}

function checkparked($device_id, $assets_id, $assets_name, $lati, $longi, $parking_lat, $parking_lang, $user_id) {
    if ($parking_lat != "" && $parking_lang != "") {
        $unit = "K";
        $smsText = $assets_name . " is Moved From Parking Location";
        $distanceFromparking = getDistance($lati, $longi, $parking_lat, $parking_lang, $unit) * 1000;
        if ($distanceFromparking >= 50) {
            $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id  WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
            //WriteLog($us_sql);
            $rs = mysql_query($us_sql) or deadme(mysql_error() . ":" . $us_sql);
            while ($row = mysql_fetch_array($rs)) {
                $user_id = $row['user_id'];
                sendMobileNotification($user_id, $smsText, 'parking', $assets_id);
            }
            $query = "update assests_master set parking_lat=Null,parking_lang=Null where id='" . $assets_id . "'";
            mysql_query($query) or deadme($query);
        }
    }
}

function checkLandmark($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $lati, $longi, $current_speed, $ist, $odometer,$x_address) {

    global $current_landmark, $current_landmark_id, $dts;
    $insert_data = false;

    $sql = "SELECT group_concat(landmark_id) as device_landmark FROM assets_landmark WHERE assets_id = '$assets_id'";
    $rs = mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
    $row = mysql_fetch_array($rs);
    $device_landmark = $row['device_landmark'];

    $sqlP = "SELECT lm.*, um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, (SELECT group_concat(mobile_no) as mobile_no FROM addressbook where find_in_set(id, lm.addressbook_ids)) as addressbook_mobile, sr.landmark_in_alert, sr.landmark_out_alert FROM landmark lm left join tbl_users um on um.user_id = lm.add_uid left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET( $assets_id, lm.device_ids ) and lm.del_date is null and lm.status = 1";
    if ($device_landmark != "") {
        $sqlP .= " and lm.id not in($device_landmark)";
    }
    $rs = mysql_query($sqlP) or deadme(mysql_error() . ":" . $sqlP);
    while ($row = mysql_fetch_array($rs)) {
        $distance_value = $row['radius'];
        $alert_before_landmark = "";
        if ($row['alert_before_landmark'] != "")
            $alert_before_landmark = floatval($distance_value + $row['alert_before_landmark']);

        if ($distance_value == "") {
            continue;
        }

        $landmark_id = $row['id'];
        $dealer_code = $row['comments'];
        $landmark_name = $row['name'];
        $distance_unit = $row['distance_unit'];
        $user_id = $row['user_id'];
        $fname = $row['first_name'];
        $mobile = $row['mobile_number'];
        $email = $row['email_address'];
        $user_email_alert = $row['user_email_alert'];
        $user_sms_alert = $row['user_sms_alert'];
        $landmark_email_alert = $row['email_alert'];
        $landmark_sms_alert = $row['sms_alert'];
        $alert_start_time = $row['alert_start_time'];
        $alert_stop_time = $row['alert_stop_time'];
        $landmark_in_alert = $row['landmark_in_alert'];
        $landmark_out_alert = $row['landmark_out_alert'];
        $language = $row['language'];
        if ($language == "portuguese") {
            $file = "portuguese_alert_lang.php";
        } else {
            $file = "english_alert_lang.php";
        }
        include($file);
        if ($distance_unit == "Mile") {
            $unit = "Mile";
        } else {
            $unit = "K";
        }

        $send_sms_now = true;
        if ($alert_start_time != "" && $alert_stop_time != "") {
            if (time() < strtotime($alert_start_time) && time() > strtotime($alert_stop_time)) {
                $send_sms_now = false;
            }
        }
        $distanceFromLandmark = getDistance($lati, $longi, $row['lat'], $row['lng'], $unit);

        if ($distance_unit == "Meter")
            $distanceFromLandmark = $distanceFromLandmark * 1000;

        // //WriteLog("\n$landmark_name : $distanceFromLandmark < $distance_value");

        if ($distanceFromLandmark < $distance_value) { //"Device is near to Landmark"
            $checkLast = "SELECT landmark_id, in_out FROM landmark_log WHERE device_id = $assets_id and landmark_id = $landmark_id order by id desc limit 1";
            $checkRs = mysql_query($checkLast) or deadme(mysql_error() . ":" . $checkLast);
            $checkRow = mysql_fetch_array($checkRs);
            $lastLandmarkId = $checkRow['landmark_id'];
            $inOutStatus = $checkRow['in_out'];
            //if($landmark_id != $lastLandmarkId){		//check for second time

            $current_landmark = $landmark_name;
            $current_landmark_id = $landmark_id;

            if (!mysql_num_rows($checkRs)) {  //check for first time log
                $inOutStatus = 'out';
            }
            if ($inOutStatus == 'out') {

                $distanceText = number_format($distanceFromLandmark, 2) . " " . $distance_unit;

                $checkLast1 = "select landmark_id, odometer from landmark_log where device_id = $assets_id order by id desc limit 1";
                $checkRs1 = mysql_query($checkLast1) or deadme(mysql_error() . ":" . $checkLast1);
                $checkRow1 = mysql_fetch_array($checkRs1);
                $last_landmark_id = $checkRow1['landmark_id'];
                $last_odometer = $checkRow1['odometer'];
                $distance_from_last = ($odometer - $last_odometer) / 1000;
                $distance_from_last += $distanceFromLandmark;

                $ins = "INSERT INTO landmark_log(device_id, landmark_id, date_time, lat, lng, distance, in_out, odometer, last_landmark_id, distance_from_last) VALUES 	($assets_id, $landmark_id, '" . date(DATE_TIME) . "', '$lati', '$longi', '$distanceText', 'in', '$odometer', '$last_landmark_id', '$distance_from_last')";
                mysql_query($ins) or deadme(mysql_error() . ":" . $ins);
                $insert_data = true;
                current_event_track("Near Landmark", $assets_id, $device_id, "Landmark Name :$landmark_name");

                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if (isset($driver_mobile) && $driver_mobile != '') {
                    if ($txt == '') {
                        $txt .= $driver_mobile;
                    } else {
                        $txt .= ', ' . $driver_mobile;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                $smsText = "$assets_name $str " . $lang['is near landmark'] . " $landmark_name (" . $lang['Distance'] . " : $distanceText), " . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME);
                // $smsText .= ' Landmark Points : '.$row['lat'].', '.$row['lng'].' and Asset Points : '.$lati.', '.$longi;
                //sms template
                //Dear [F1], [F2] ([F3], [F4]) is near [F5] (Distance is [F6])

                $template_id = '3824';
                $f1 = $fname;
                $f2 = $assets_name;
                $f3 = $nick_name;
                $f4 = $driver_name;
                $f5 = $landmark_name;
                $f6 = $distanceText;
                $f7 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6, "F7" => $f7);

                if ($mobile != "" && $user_sms_alert == 1 && $landmark_sms_alert == 1 && ($landmark_in_alert == 1 || $landmark_in_alert == NULL)) {
                    send_sms($mobile, $smsText, $template_id, $template_data);
                    sms_log($mobile, $smsText, $user_id);
                }

                if ($email != "" && $user_email_alert == 1 && $landmark_email_alert == 1 && ($landmark_in_alert == 1 || $landmark_in_alert == NULL)) {
                    $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $lati . ',' . $longi . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                    send_email($email, $lang['Near Landmark Alert'], $smsText);
                    email_log($email, $smsText, $user_id, 'Near Landmark Alert');
                    // chat_alert($email, $smsText);
                }

                if ($addressbook_mobile != "") {     //send sms addressbook contact
                    //send_sms($mobile, $smsText, $template_id, $template_data);
                    send_sms($addressbook_mobile, $smsText, $template_id, $template_data);
                    sms_log($addressbook_mobile, $smsText, $user_id);
                }

                //insert in alert master
				//$lati, $longi
                if (($landmark_sms_alert == 1 || $landmark_email_alert == 1) && ($landmark_in_alert == 1 || $landmark_in_alert == NULL)) {
                    $alertSql = "INSERT INTO alert_master (alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) VALUES ('" . $lang['Near Landmark Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$lati."','".$longi."','".$x_address."')";
                    mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
                }

                /*                 * ************************* */
                //rfid alert
                $sub_sql = "select * from tbl_rfid where del_date is null and status = 1 and landmark_id = $landmark_id";
                $sub_rs = mysql_query($sub_sql) or deadme(mysql_error() . ":" . $sub_sql);
                while ($sub_row = mysql_fetch_array($sub_rs)) {
                    $person = $sub_row['person'];
                    $inform_mobile = $sub_row['inform_mobile'];
                    $inform_email = $sub_row['inform_email'];
                    $send_sms = $sub_row['send_sms'];
                    $send_email = $sub_row['send_email'];

                    //Dear $person
                    $txt = '';
                    if (isset($nick_name) && $nick_name != '')
                        $txt .= $nick_name;
                    if (isset($driver_name) && $driver_name != '') {
                        if ($txt == '') {
                            $txt .= $driver_name;
                        } else {
                            $txt .= ', ' . $driver_name;
                        }
                    }
                    if (isset($driver_mobile) && $driver_mobile != '') {
                        if ($txt == '') {
                            $txt .= $driver_mobile;
                        } else {
                            $txt .= ', ' . $driver_mobile;
                        }
                    }
                    if ($txt != '')
                        $str = '(' . $txt . ')';
                    else
                        $str = '';
                    $smsText = "$assets_name $str is near landmark $landmark_name (Distance : $distanceText), " . convert_time_zone($ist, $dts, DISP_TIME); //date(DISP_TIME);

                    if ($inform_mobile != "" && $send_sms == 1 && $landmark_sms_alert == 1) {
                        send_sms($inform_mobile, $smsText);
                        sms_log($mobile, $smsText, $user_id);
                    }
                    if ($inform_email != "" && $send_email == 1 && $landmark_email_alert == 1) {
                        $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $lati . ',' . $longi . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                        send_email($inform_email, $lang['RFID alert to Person'], $smsText);
                        email_log($inform_email, $smsText, $user_id, $lang['RFID alert to Person']);
                        // chat_alert($email, $smsText);
                    }
                }
            }
        } else {  //out
            $checkLast = "select id from landmark_log where device_id = $assets_id and landmark_id = $landmark_id and in_out = 'in' order by id desc limit 1";
            $checkRs = mysql_query($checkLast) or deadme(mysql_error() . ":" . $checkLast);

            if (mysql_num_rows($checkRs) > 0) {
                $checkRow = mysql_fetch_array($checkRs);
                $lId = $checkRow['id'];

                //update next landmark
                $sqlAss = "select trip.landmark_ids, am.current_trip, am.next_trip_landmark from assests_master am left join tbl_routes trip on trip.id = am.current_trip where am.id = $assets_id";
                $rsAss = mysql_query($sqlAss) or deadme(mysql_error() . ":" . $sqlAss);
                $rowAss = mysql_fetch_array($rsAss);
                if ($rowAss['current_trip'] != "" && $rowAss['current_trip'] != 0) {
                    $landmark_ids = explode(",", $rowAss['landmark_ids']);
                    if (in_array($landmark_id, $landmark_ids)) {
                        $lorder = array_search($landmark_id, $landmark_ids) + 1;
                        $next_trip_landmark = $landmark_ids[$lorder];
                        $nextLSql = "update assests_master set next_trip_landmark = $next_trip_landmark where id = $assets_id";
                        mysql_query($nextLSql) or deadme(mysql_error() . ":" . $nextLSql);
                    }
                }

                $uLSql = "update landmark_log set in_out = 'out' where id = $lId";
                mysql_query($uLSql) or deadme(mysql_error() . ":" . $uLSql);

                //update sub-route
                $tSql = "select trip_id from trip_log where device_id = $assets_id and is_complete = 0 order by id desc limit 1";
                $tRs = mysql_query($tSql) or deadme(mysql_error() . ":" . $tSql);
                if (mysql_num_rows($tRs) > 0) {
                    $tRow = mysql_fetch_array($tRs);
                    $trip_id = $tRow['trip_id'];
                    $query = "update tbl_sub_routes set start_time = '" . $ist . "', start_km_reading = (select km_reading from assests_master where id = $assets_id) WHERE route_id = $trip_id and landmark_ids like '$lId,%' and start_time is null";
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }

                if ($dealer_code != "") {
                    //update user assets mapping, remove assets id
                    $sub_sql = "select * from user_assets_map where user_id = (select user_id from tbl_users where username = '$dealer_code')";
                    $sub_rs = mysql_query($sub_sql) or deadme(mysql_error() . ":" . $sub_sql);
                    if (mysql_num_rows($tRs) > 0) {
                        $sub_row = mysql_fetch_array($sub_rs);
                        $uam_id = $sub_row['id'];
                        $assetsIds = $sub_row['assets_ids'];
                        $assetsIds = str_replace("$assets_id", "", $assetsIds);
                        $assetsIds = str_replace(",,", ",", $assetsIds);
                        $assetsIds = trim($assetsIds, ",");

                        $sql = "update user_assets_map set assets_ids = '$assetsIds' where id = '$uam_id'";
                        mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
                    }
                }
            }
        }

        /*
          if($alert_before_landmark!=""){

          $distanceText = number_format($distanceFromLandmark, 2)." ".$distance_unit;
          if($distanceFromLandmark < $alert_before_landmark){	//"Device is near to Landmark"
          $checkLast = "select landmark_id from landmark_distance_log where device_id = $assets_id order by id desc limit 1";
          $checkRs = mysql_query($checkLast);
          $numRows = mysql_num_rows($checkRs);

          $checkRow = mysql_fetch_array($checkRs);
          $lastLandmarkId = $checkRow['landmark_id'];
          if(($lastLandmarkId != $landmark_id) || ($numRows==0)){

          $ins = "INSERT INTO landmark_distance_log (device_id, landmark_id, date_time, distance) values($assets_id, $landmark_id, '".date('Y-m-d H:i:s', strtotime($ist))."', '$distanceText')";
          mysql_query($ins);

          // Dear $dealer_fname,
          $smsText = "$assets_name ($driver_name, $driver_mobile) is $distanceText away from Landmark $landmark_name, ".date(DISP_TIME, strtotime($ist));

          $emailText = $smsText;
          // //WriteLog("$smsText");

          if($mobile != "" && $user_sms_alert == 1 && $landmark_sms_alert == 1 && $send_sms_now){
          send_sms($mobile, $smsText, $template_id, $template_data);
          sms_log($mobile, $smsText, $user_id);
          }

          if($email!="" && $user_email_alert == 1 && $landmark_email_alert == 1) {
          $smsText .= ', Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$lati.','.$longi.'('.$assets_name.')&ie=UTF8&z=12&om=1">here</a> to view on map.';
          send_email($email, "Device is near to Landmark", $smsText);
          email_log($email, $smsText, $user_id,'Device is near to Landmark');
          // chat_alert($email, $smsText);
          }

          if($addressbook_mobile != ""){					//send sms addressbook contact
          //send_sms($mobile, $smsText, $template_id, $template_data);
          send_sms($addressbook_mobile, $smsText, $template_id, $template_data);
          sms_log($addressbook_mobile, $smsText, $user_id);
          }

          if($landmark_sms_alert == 1 || $landmark_email_alert == 1) {
          $alertSql = "insert into alert_master(assets_id, alert_header, alert_msg, alert_type, user_id, add_date) values ($assets_id, 'Near Landmark Alert', '".$smsText."', 'alert', '".$user_id."', '".date(DATE_TIME)."')";
          mysql_query($alertSql);
          }
          }
          }
          }
         */
    }
    return $insert_data;
}
//added by Rajesh ighare 2019-09-10
function send_alert_repair_status($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $repair_status, $x_address, $ist){
$us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
	//print_r($us_sql);
	$rs = mysql_query($us_sql);
	  while ($row = mysql_fetch_array($rs)) {
		$user_id = $row['user_id'];
		$fname = $row['first_name'];
		$mobile = $row['mobile_number'];
		$email = $row['email_address'];
		$user_sms_alert = $row['user_sms_alert'];
		$user_email_alert = $row['user_email_alert'];
		if ($language == "portuguese") {
		$file = "portuguese_alert_lang.php";
		} else {
		$file = "english_alert_lang.php";
		}
		include($file);
		$wire_status=array("0"=>"Normal Mode","1"=>"Repair Mode"); 
		$staus=$wire_status[$repair_status];		
		$smsText="";
		if($x_address==""){
		$x_address="Location not found";
		}
		$smsText ="The $assets_name vehicle on $staus at place $x_address on $ist.";		
		if($mobile !="" && $user_sms_alert ==1){
		 send_sms($mobile, $smsText);		
         sms_log($mobile, $smsText, $user_id);
		}
		if($email != "" && $user_email_alert == 1) {
         send_email($email, "$assets_name on $staus", $smsText1);
         }
		$alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ('$assets_name on $staus', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$longitude_x."','".$latitude_y."','".$x_address."')";
		mysql_query($alertSql);  
	  }  
}
function send_alert_power_wire($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $powerWireStatus, $x_address, $ist){
$us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
	//print_r($us_sql);
	$rs = mysql_query($us_sql);
	  while ($row = mysql_fetch_array($rs)) {
		$user_id = $row['user_id'];
		$fname = $row['first_name'];
		$mobile = $row['mobile_number'];
		$email = $row['email_address'];
		$user_sms_alert = $row['user_sms_alert'];
		$user_email_alert = $row['user_email_alert'];
		if ($language == "portuguese") {
		$file = "portuguese_alert_lang.php";
		} else {
		$file = "english_alert_lang.php";
		}
		include($file);
		$wire_status=array("0"=>"OFF","1"=>"ON");
		$staus=$wire_status[$powerWireStatus];		
		$smsText="";
		if($x_address==""){
		$x_address="Location not found";
		}
		$smsText ="The $assets_name vehicle Power Wire Status is $staus at place $x_address on ".date(DATE_TIME).".";		
		if($mobile !="" && $user_sms_alert ==1){
		 send_sms($mobile, $smsText);		
         sms_log($mobile, $smsText, $user_id);
		}
		if($email != "" && $user_email_alert == 1) {
         send_email($email, "Power Wire Status is $staus", $smsText);
         }
		 $sqls="INSERT INTO `tbl_device_power_log`(`user_id`, `assets_id`, `lat`, `lng`, `status`, `add_date`) VALUES ('" . $user_id."','" . $assets_id . "','".$longitude_x."','".$latitude_y."','".$powerWireStatus."','".date(DATE_TIME)."')";
		 mysql_query($sqls);
		 
		$alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ('Power Wire Status is $staus', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$longitude_x."','".$latitude_y."','".$x_address."')";
		mysql_query($alertSql);
	  }  
}
function send_alert_temper_wire($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $tamperStatus, $x_address, $ist){
$us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
	//print_r($us_sql);
	$rs = mysql_query($us_sql);
	  while ($row = mysql_fetch_array($rs)) {
		$user_id = $row['user_id'];
		$fname = $row['first_name'];
		$mobile = $row['mobile_number'];
		$email = $row['email_address'];
		$user_sms_alert = $row['user_sms_alert'];
		$user_email_alert = $row['user_email_alert'];
		if ($language == "portuguese") {
		$file = "portuguese_alert_lang.php";
		} else {
		$file = "english_alert_lang.php";
		}
		include($file);
		$wire_status=array("0"=>"Not Tampered","1"=>"Tampered");
		$staus=$wire_status[$tamperStatus];		
		$smsText="";
		if($x_address==""){
		$x_address="Location not found";
		}
		$smsText ="The $assets_name vehicle Tamper Status is $staus at place $x_address on ".date(DATE_TIME).".";		
		if($mobile !="" && $user_sms_alert ==1){
		 send_sms($mobile, $smsText);		
         sms_log($mobile, $smsText, $user_id);
		}
		if($email != "" && $user_email_alert == 1) {
         send_email($email, "Tamper status $staus", $smsText);
         }
		 $sqls="INSERT INTO `tbl_device_tampered_log`(`user_id`, `assets_id`, `lat`, `lng`, `status`, `add_date`) VALUES ('" . $user_id."','" . $assets_id . "','".$longitude_x."','".$latitude_y."','".$tamperStatus."','".date(DATE_TIME)."')";
		 mysql_query($sqls);
		$alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ('Tamper status $staus', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$longitude_x."','".$latitude_y."','".$x_address."')";
		mysql_query($alertSql);
	  }  
}
function send_alert_signal_wire($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $signalWire, $x_address, $ist){
global $dts;
	$us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
	//print_r($us_sql);
	$rs = mysql_query($us_sql);
	  while ($row = mysql_fetch_array($rs)) {
		$user_id = $row['user_id'];
		$fname = $row['first_name'];
		$mobile = $row['mobile_number'];
		$email = $row['email_address'];
		$user_sms_alert = $row['user_sms_alert'];
		$user_email_alert = $row['user_email_alert'];
		if ($language == "portuguese") {
		$file = "portuguese_alert_lang.php";
		} else {
		$file = "english_alert_lang.php";
		}
		include($file);
		$Signal_wire=array("0"=>"Disconnected","1"=>"Connected");
		$staus=$Signal_wire[$signalWire];
		$smsText="";
		if($x_address==""){
		$x_address="Location not found";
		}
		$smsText ="The $assets_name vehicle Signal Wire is $staus at place $x_address on ".date(DATE_TIME).".";		
		if($mobile !="" && $user_sms_alert ==1){
		 send_sms($mobile, $smsText);		
         sms_log($mobile, $smsText, $user_id);
		} 
		if($email != "" && $user_email_alert == 1) {
         send_email($email, "Signal Wire $staus", $smsText);
         }
		 $sqls="INSERT INTO `tbl_device_signal_log`(`user_id`, `assets_id`, `lat`, `lng`, `status`, `add_date`) VALUES ('" . $user_id."','" . $assets_id . "','".$longitude_x."','".$latitude_y."','".$signalWire."','".date(DATE_TIME)."')";
		 mysql_query($sqls);
		$alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ('Signal Wire $staus', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$longitude_x."','".$latitude_y."','".$x_address."')";
		mysql_query($alertSql);
	  }

}
function checkSpeedGps($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $max, $current_speed, $ist, $lati, $longi,$x_address) {
    global $cross_speed, $dts;
    global $uRow;
    $insert_data = false;
    
    if ($current_speed > $max && $max != "" && $max != 0) {
        $speedSql = "select date_time from over_speed_report where assets_id = '$assets_id' order by id desc limit 1";
        $speedRs = mysql_query($speedSql);
        $minutes = 11;
        if (mysql_num_rows($speedRs) > 0) {
            $speedRow = mysql_fetch_array($speedRs);
            $start = $speedRow['date_time'];
            $minutes = round(abs(strtotime($ist) - strtotime($start)) / 60, 2);
        }
        

        if ($minutes > 3) {
			//die("HII");

            $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert, sr.overspeed_alert, sr.overspeed FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
			
            // //WriteLog($us_sql);
            $rs = mysql_query($us_sql);
     
            current_event_track("Over GPS Speed", $assets_id, $device_id, '');
            while ($row = mysql_fetch_array($rs)) {

                $user_id = $row['user_id'];
                $fname = $row['first_name'];
                $mobile = $row['mobile_number'];
                $email = $row['email_address'];
                $user_sms_alert = $row['user_sms_alert'];
                $user_email_alert = $row['user_email_alert'];
                $overspeed_alert = $row['overspeed_alert'];
                $overspeed_limit = $row['overspeed'];
                $language = $row['language'];
                if ($language == "portuguese") {
                    $file = "portuguese_alert_lang.php";
                } else {
                    $file = "english_alert_lang.php";
                }
                include($file);
                if ($current_speed > $overspeed_limit) {
                    $txt = '';
                    if (isset($nick_name) && $nick_name != '')
                        $txt .= $nick_name;
                    if (isset($driver_name) && $driver_name != '') {
                        if ($txt == '') {
                            $txt .= $driver_name;
                        } else {
                            $txt .= ', ' . $driver_name;
                        }
                    }
                    if (isset($driver_mobile) && $driver_mobile != '') {
                        if ($txt == '') {
                            $txt .= $driver_mobile;
                        } else {
                            $txt .= ', ' . $driver_mobile;
                        }
                    }
                    if ($txt != '')
                        $str = '(' . $txt . ')';
                    else
                        $str = '';
                    // Dear $fname, 
                   // $smsText = "$assets_name $str " . $lang['cross the maximum speed limit'] . " (" . $lang['Speed'] . " : $current_speed Km/H)," . convert_time_zone($ist, $dts, DISP_TIME);
				   if($x_address==""){
					   $x_address="Location not found";
				   }
				   $smsText1 ="The $assets_name vehicle is crossed maximum GPS speed limit (GPS Speed:$current_speed Km/H) at place $x_address on ".convert_time_zone($ist, $dts, DISP_TIME); 					
                    //$smsText1 = "$assets_name $str " . $lang['cross the maximum speed limit'] . " (" . $lang['Speed'] . " : $current_speed Km/H)," . convert_time_zone($ist, $dts, DISP_TIME);

                    $smsText1 .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $lati . ',' . $longi . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                    //Dear [F1], [F2] ([F3], [F4]), cross the maximum speed limit (Speed : [F5])
                    $template_id = '3825';
                    $f1 = $fname;
                    $f2 = $assets_name;
                    $f3 = $nick_name;
                    $f4 = $driver_name;
                    $f5 = $current_speed . " Km/H";
                    $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); //.date(DISP_TIME, strtotime($ist));
                    $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);


                    $cross_speed = 1;
                    $insert_data = true;
					//die(var_dump($mobile));
					$user_sms_alert=1;
					$overspeed_alert=1;
					$user_email_alert=1; 
                    if ($mobile != "" && $user_sms_alert == 1 && ($overspeed_alert == 1)) {
                    send_sms($mobile, $smsText); 
                    sms_log($mobile, $smsText, $user_id);
                    }					 
					if($email != "" && $user_email_alert == 1 && ($overspeed_alert == 1)) {
                    send_email($email, "Over GPS Speed Alert", $smsText1);
                    }
                    $speedSql = "insert into over_speed_report(user_id, assets_id, max_speed_limit, speed, date_time, comments) values ( '$user_id', '" . $assets_id . "', '$max', '$current_speed', '" . $ist . "', '" . $smsText1 . "')";
                    mysql_query($speedSql);
                    if ($overspeed_alert == 1) {
						//$lati, $longi
                        sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( 'Over GPS Speed Alert', '" . $smsText1 . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$lati."','".$longi."','".$x_address."')";						
                        mysql_query($alertSql);
            
                    }
                }
            }
        }
    }
    return $insert_data;
}
//end by Rajesh ighare
function checkSpeed($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $max, $current_speed, $ist, $lati, $longi,$x_address) {
    global $cross_speed, $dts;
    global $uRow;
    $insert_data = false;
    
    if ($current_speed > $max && $max != "" && $max != 0) {
        $speedSql = "select date_time from over_speed_report where assets_id = '$assets_id' order by id desc limit 1";
        $speedRs = mysql_query($speedSql);
        $minutes = 11;
        if (mysql_num_rows($speedRs) > 0) {
            $speedRow = mysql_fetch_array($speedRs);
            $start = $speedRow['date_time'];
            $minutes = round(abs(strtotime($ist) - strtotime($start)) / 60, 2);
        }
        

        if ($minutes > 3) {
			//die("HII");

            $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert, sr.overspeed_alert, sr.overspeed FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
			
            // //WriteLog($us_sql);
            $rs = mysql_query($us_sql);
     
            current_event_track("Over Speed", $assets_id, $device_id, '');
            while ($row = mysql_fetch_array($rs)) {

                $user_id = $row['user_id'];
                $fname = $row['first_name'];
                $mobile = $row['mobile_number'];
                $email = $row['email_address'];
                $user_sms_alert = $row['user_sms_alert'];
                $user_email_alert = $row['user_email_alert'];
                $overspeed_alert = $row['overspeed_alert'];
                $overspeed_limit = $row['overspeed'];
                $language = $row['language'];
                if ($language == "portuguese") {
                    $file = "portuguese_alert_lang.php";
                } else {
                    $file = "english_alert_lang.php";
                }
                include($file);
                if ($current_speed > $overspeed_limit) {
                    $txt = '';
                    if (isset($nick_name) && $nick_name != '')
                        $txt .= $nick_name;
                    if (isset($driver_name) && $driver_name != '') {
                        if ($txt == '') {
                            $txt .= $driver_name;
                        } else {
                            $txt .= ', ' . $driver_name;
                        }
                    }
                    if (isset($driver_mobile) && $driver_mobile != '') {
                        if ($txt == '') {
                            $txt .= $driver_mobile;
                        } else {
                            $txt .= ', ' . $driver_mobile;
                        }
                    }
                    if ($txt != '')
                        $str = '(' . $txt . ')';
                    else
                        $str = '';
                    // Dear $fname, 
                    $smsText = "$assets_name $str " . $lang['cross the maximum speed limit'] . " (" . $lang['Speed'] . " : $current_speed Km/H)," . convert_time_zone($ist, $dts, DISP_TIME);

                    $smsText1 = "$assets_name $str " . $lang['cross the maximum speed limit'] . " (" . $lang['Speed'] . " : $current_speed Km/H)," . convert_time_zone($ist, $dts, DISP_TIME);

                    $smsText1 .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $lati . ',' . $longi . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                    //Dear [F1], [F2] ([F3], [F4]), cross the maximum speed limit (Speed : [F5])
                    $template_id = '3825';
                    $f1 = $fname;
                    $f2 = $assets_name;
                    $f3 = $nick_name;
                    $f4 = $driver_name;
                    $f5 = $current_speed . " Km/H";
                    $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); //.date(DISP_TIME, strtotime($ist));
                    $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);


                    $cross_speed = 1;
                    $insert_data = true;
					//die(var_dump($mobile));
					$user_sms_alert=1;
					$overspeed_alert=1;
					$user_email_alert=1; 
                    if ($mobile != "" && $user_sms_alert == 1 && ($overspeed_alert == 1)) {
                    send_sms($mobile, $smsText);
                    sms_log($mobile, $smsText, $user_id);
                    }					
					if($email != "" && $user_email_alert == 1 && ($overspeed_alert == 1)) {
                    send_email($email, $lang['Over Speed Alert'], $smsText1);
                    }
                    $speedSql = "insert into over_speed_report(user_id, assets_id, max_speed_limit, speed, date_time, comments) values ( '$user_id', '" . $assets_id . "', '$max', '$current_speed', '" . $ist . "', '" . $smsText . "')";
                    mysql_query($speedSql);
                    if ($overspeed_alert == 1) {
						//$lati, $longi
                        sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( '" . $lang['Over Speed Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$lati."','".$longi."','".$x_address."')";						
                        mysql_query($alertSql);
            
                    }
                }
            }
        }
    }
    return $insert_data;
}

/* * *********#Developed By: Prashant D. Date: 28-02-2015 For Stop Report#************************** */

function stop_report_insert($speed, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ignition, $latitude, $longitude, $x_address, $current, $odomVal) {
    global $uRow, $dts;
    global $current_area;
    global $current_landmark;
    global $analysis_id;
    global $ignition_off_datetime;
	
	$log_stamp3[0] 	= time() ;
	
    if ($ignition == 0) {
        $ignitionStatus = "Ignition Off";
    } else if ($ignition == 1) {
        $ignitionStatus = "Ignition On";
    }

    if ($odomVal == "") {
        $sql = "SELECT km_reading FROM assests_master WHERE id = '" . $assets_id . "'";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) > 0) {
            $row_odo = mysql_fetch_array($rs);
            $odomVal = $row_odo['km_reading'];
        }
    }
    ////WriteLog("odometer:".$odomVal);
    $query = "SELECT id, ignition_off, ignition_on, ignition_status, alert_given FROM tbl_stop_report WHERE device_id = '" . $assets_id . "' ORDER BY id DESC LIMIT 0,1";
    $res = mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());
    $rowcount = mysql_num_rows($res);
    $row = mysql_fetch_array($res);
    $lastignitionstatus = $row['ignition_status'];
    $defaultspeed = 0.5;
	
	$log_stamp3[1] 	= time() - $log_stamp3[0];
	
    ////WriteLog($speed." < ".$defaultspeed);
    if ((floatval($speed) < $defaultspeed) && ($lastignitionstatus == $ignitionStatus)) 
	{

        if ($rowcount == 1)
		{
            $stop_report_id = $row['id'];
            if (trim($row['ignition_on'] != "")) {
                ////WriteLog("odometer:".$odomVal);
                $query = "INSERT INTO tbl_stop_report (device_id, ignition_off, address, lat, lng, ignition_status, odometer) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $ignitionStatus . "', '" . addslashes($odomVal) . "')";
                ////WriteLog("odometer:".$query);
                @mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());
                $ignition_off_datetime = $current;
            }

            //alert if stop more than given time
            if (trim($row['ignition_on'] == ""))
			{
                $start = strtotime($row['ignition_off']);
                $end = strtotime($current);
                $minutes = round(abs($end - $start) / 60, 2);

                $user_id = $uRow['user_id'];
                $fname = $uRow['first_name'];
                $mobile = $uRow['mobile_number'];
                $email = $uRow['email_address'];
                $user_sms_alert = $uRow['sms_alert'];
                $user_email_alert = $uRow['email_alert'];
                $alert_start_time = $uRow['alert_start_time'];
                $alert_stop_time = $uRow['alert_stop_time'];
                $max_stop_time = $uRow['max_stop_time']; //in minutes
                //if stop time more than set time and alert not given
                
				
				if ($max_stop_time != "" && $max_stop_time != 0 && $minutes > $max_stop_time && $row['alert_given'] == 0)
				{
                    $stop_time = sec2HourMinute($max_stop_time * 60);

                    //Dear $fname, 
                    $txt = '';
                    if (isset($nick_name) && $nick_name != '')
                        $txt .= $nick_name;
                    if (isset($driver_name) && $driver_name != '') {
                        if ($txt == '') {
                            $txt .= $driver_name;
                        } else {
                            $txt .= ', ' . $driver_name;
                        }
                    }
                    if (isset($driver_mobile) && $driver_mobile != '') {
                        if ($txt == '') {
                            $txt .= $driver_mobile;
                        } else {
                            $txt .= ', ' . $driver_mobile;
                        }
                    }
                    if ($txt != '')
                        $str = '(' . $txt . ')';
                    else
                        $str = '';
                    $smsText = "$assets_name $str stop more than $stop_time,";
                    $smsText1 = "$assets_name $str stop more than $stop_time,";
                    //sms template
                    //STOP LIMIT CROSS	Dear [F1], [F2] ([F3], [F4]) stopped more than [F5][F6][F7]
                    $template_id = '3827';
                    $f1 = $fname;
                    $f2 = $assets_name;
                    $f3 = $nick_name;
                    $f4 = $driver_name;
                    $f5 = " " . $stop_time;

                    if ($current_landmark != '') 
					{
                        $smsText .= " near Landmark $current_landmark";
                        $smsText1 .= " near Landmark $current_landmark";
                        list($f6, $f7) = str_split(" near Landmark $current_landmark", 30);
                        if ($f7 == "")
                            $f7 = " ";
                    }
					else if ($current_area != "") 
					{
                        $smsText .= " in Area $current_area";
                        $smsText1 .= " in Area $current_area";
                        list($f6, $f7) = str_split(" in Area $current_area", 30);
                        if ($f7 == "")
                            $f7 = " ";
                    }
                    else if ($x_address) 
					{
                        $smsText .= " at $x_address";
                        $smsText1 .= " at $x_address";
                        list($f6, $f7) = str_split(" at " . $x_address, 30);
                        if ($f7 == "")
                            $f7 = " ";
                    }
					else
					{
                        $f6 = " ";
                        $f7 = " ";
                    }
//                    $f8 = "," . convert_time_zone($current, $dts, DISP_TIME); //.date(DISP_TIME, strtotime($current));
                    $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6, "F7" => $f7, "F8" => $f8);

                    $smsText .= ", " . convert_time_zone($current, $dts, DISP_TIME); //.date(DISP_TIME, strtotime($current));	
                    $smsText1 .= ", " . convert_time_zone($current, $dts, DISP_TIME); //.date(DISP_TIME, strtotime($current));	
                    //////////
					
					
                    if ($mobile != "" && $user_sms_alert == 1)
					{
                        send_sms($mobile, $smsText, $template_id, $template_data);
                        //send_sms($mobile, $alert_text);
                        sms_log($mobile, $smsText, $user_id);
                    }
					$log_stamp3[2] 	= time() - $log_stamp3[0];

                    if ($email != "" && $user_email_alert == 1)
					{

                        $smsText1 .= ', Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $latitude . ',' . $longitude . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">here</a> to view on map.';
                        
						send_email($email, "Vehicle Stop time more than set time", $smsText1);
                        
						email_log($email, $smsText1, $user_id, 'Vehicle Stop time more than set time');
                        
						// chat_alert($email, $smsText);
                    }

                    //update alert given
                    $uSql = "update tbl_stop_report set alert_given = 1 where id = $stop_report_id";
                    @mysql_query($uSql);
					
					$log_stamp3[3] 	= time() - $log_stamp3[0];
					//$latitude, $longitude
                    //insert in alert master
                    sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                    $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( 'Vehicle Stop Alert', '" . $smsText . "', 'alert', '" . $user_id . "', '" . $assets_id . "', '" . $current . "','".$latitude."','".$longitude."','".$x_address."')";
                    mysql_query($alertSql)or deadme("SQL : $alertSql : Error : " . mysql_error());
                }
				
				$log_stamp3[4] 	= time() - $log_stamp3[0];
            }
			
        } 
		else if ($rowcount == 0) 
		{
            ////WriteLog("odometer ".$odomVal);
            $query = "INSERT INTO tbl_stop_report (device_id, ignition_off, address, lat, lng, ignition_status, odometer) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $ignitionStatus . "', '" . addslashes($odomVal) . "')";
            @mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());
            $ignition_off_datetime = $current;
        }
    }
	else 
	{

        if ($rowcount == 1) 
		{
            //$row = mysql_fetch_array($res);

            $row_id = $row['id'];
			
            if (trim($row['ignition_on']) == "")
			{
                $start = strtotime($row['ignition_off']);
                $end = strtotime($current);
                $delta = $end - $start;
				
                if ($delta > 5) 
				{
                    //$hours = floor($delta / 3600);
                    //$remainder = $delta - $hours * 3600;
                    //$formattedDelta = sprintf('%02d', $hours) . date(':i:s', $remainder);

                    $datetime1 = new DateTime($row['ignition_off']);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');

                    $query = "UPDATE tbl_stop_report SET ignition_on = '" . $current . "', duration='" . addslashes($formattedDelta) . "', add_date = '" . $current . "' WHERE  id = " . $row_id;
                    @mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());

                    if (floatval($speed) < $defaultspeed) 
					{
                        ////WriteLog("odometer1".$odomVal);
                        $query = "INSERT INTO tbl_stop_report (device_id, ignition_off, address, lat, lng, ignition_status, odometer) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $ignitionStatus . "', '" . addslashes($odomVal) . "')";
                        @mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());
                    }
					
					$log_stamp3[5] 	= time() - $log_stamp3[0];
					
                }
				
            } 
			else
			{
                if (floatval($speed) < $defaultspeed)
				{
                    $query = "INSERT INTO tbl_stop_report (device_id, ignition_off, address, lat, lng, ignition_status, odometer) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $ignitionStatus . "', '" . addslashes($odomVal) . "')";
                    @mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());
                }
            }
			
			$log_stamp3[6] 	= time() - $log_stamp3[0];
			
        }
		else if ($rowcount == 0)
		{
            ////WriteLog("odometer_ ".$odomVal);
            $query = "INSERT INTO tbl_stop_report (device_id, ignition_off, address, lat, lng, ignition_status, odometer) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $ignitionStatus . "', '" . addslashes($odomVal) . "')";
            @mysql_query($query)or deadme("SQL : $query : Error : " . mysql_error());
        }
		
		$log_stamp3[7] 	= time() - $log_stamp3[0];
		
		
		if(!isset($log_stamp3[2])) $log_stamp3[2] = "";
		if(!isset($log_stamp3[3])) $log_stamp3[3] = "";
		if(!isset($log_stamp3[4])) $log_stamp3[4] = "";
		if(!isset($log_stamp3[5])) $log_stamp3[5] = "";
		if(!isset($log_stamp3[6])) $log_stamp3[6] = "";
		
		$d6 = "a0[-], a1[".$log_stamp3[1]."], a2[".$log_stamp3[2]."], a3[".$log_stamp3[3]."], a4[".$log_stamp3[4]."], a5[".$log_stamp3[5]."], a6[".$log_stamp3[6]."], a7[".$log_stamp3[7]."]";
		
		mysql_query("update a_logger set d6 = '$d6' where id = '$analysis_id'") or deadme("SQL : update a_logger set d6 = '$d6' where id = $analysis_id : Error : " . mysql_error());

		

    }
}

function start_report_insert($speed, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ignition, $latitude, $longitude, $x_address, $ist, $odometer) {
    $current = date(DATE_TIME);
    ////WriteLog($current);
    global $current_area;
    global $current_landmark;

    global $uRow;
    $query = "SELECT ignition_flag,id,ignition_on,add_date FROM tbl_start_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if ($ignition == 1) {
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            if ($row['ignition_flag'] == 1) {
                $query = "INSERT INTO tbl_start_report (device_id, ignition_on, start_odometer, stop_odometer, distance, address, lat, lng, current_area, current_landmark, add_date, ignition_on_address) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $odometer . "', '" . $odometer . "', 0,'" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $current . "', '" . addslashes($x_address) . "')";
                mysql_query($query) or deadme(mysql_error() . ":" . $query);
            } else if ($row['ignition_flag'] == 0) {
                $row_id = $row['id'];
                $start = strtotime($row['ignition_on']);
                $end = strtotime($current);
                $delta = $end - $start;
                // if($assets_id == 407) //WriteLog("UPDATE tbl_start_report SET ignition_off = '".$current."', stop_odometer = '".$odometer."', duration = '". addslashes($formattedDelta)."', distance = ($odometer - start_odometer) WHERE id = " . $row_id);
                if ($delta > 5) {
                    //$hours = floor($delta / 3600);
                    //$remainder = $delta - $hours * 3600;
                    //$formattedDelta = sprintf('%02d', $hours) . date(':i:s', $remainder);
                    $datetime1 = new DateTime($row['ignition_on']);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');
                    $query = "UPDATE tbl_start_report SET ignition_off = '" . $current . "', stop_odometer = '" . $odometer . "', duration = '" . addslashes($formattedDelta) . "', distance = ($odometer - start_odometer) WHERE  id = " . $row_id;
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }
            }
        } else if (mysql_num_rows($res) == 0) {
            $query = "INSERT INTO tbl_start_report (device_id, ignition_on, start_odometer, stop_odometer, distance, address, lat, lng, current_area, current_landmark, add_date, ignition_on_address) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $odometer . "', '" . $odometer . "', 0,'" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $current . "','" . addslashes($x_address) . "')";
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
        }
    } else {
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $add_date = $row['add_date'];
            $avg_speed = 0;
            $max_speed = 0;
            /*$maxspeed_query = "select max(speed) as max_speed, avg(speed) as avg_speed from tbl_track where dt BETWEEN  '" . $add_date . "' AND '" . $current . "' and assets_id = $assets_id";
            $res1 = mysql_query($maxspeed_query) or deadme(mysql_error() . ":" . $maxspeed_query);
            if (mysql_num_rows($res1) == 1) {
                $row_speed = mysql_fetch_array($res1);
                $max_speed = $row_speed['max_speed'];
                $avg_speed = $row_speed['avg_speed'];
                // //WriteLog($avg_speed." ".$max_speed." ".$maxspeed_query);
            }*/
            // If the data is coming for smae date.
//				if(date('Y-m-d', strtotime($add_date)) == date('Y-m-d')) {
            $row_id = $row['id'];
            if ($row['ignition_flag'] == 0) {

                $start = $row['ignition_on'];
                // //echo $start;
                $end = strtotime($current);
                $delta = $end - $start;
                if ($delta > 5) {
                    //$hours = floor($delta / 3600);
                    //$remainder = $delta - $hours * 3600;
                    //$formattedDelta = sprintf('%02d', $hours) . date(':i:s', $remainder);
                    $datetime1 = new DateTime($start);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');
                    $query = "UPDATE tbl_start_report SET ignition_off = '" . $current . "', stop_odometer = '" . $odometer . "', distance = ($odometer - start_odometer), duration='" . addslashes($formattedDelta) . "', ignition_flag = 1 , ignition_off_address = '" . addslashes($x_address) . "'  ,max_speed = '" . $max_speed . "' ,avg_speed = '" . $avg_speed . "' WHERE id = " . $row_id;
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }
            }
//				}
        }
    }
}

function run_report($speed, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ignition, $latitude, $longitude, $x_address, $ist, $km_reading) {
	return true;
    $current = date(DATE_TIME);
    global $current_area;
    global $current_landmark;
    // //WriteLog($km_reading." ".$ignition);

    global $uRow;
    $query = "SELECT id,add_date,ignition_on,ignition_flag FROM tbl_start_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if ($ignition == 1) {
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            if ($row['ignition_flag'] == 1) {
                $query = "INSERT INTO tbl_start_report (device_id, ignition_on, start_odometer, stop_odometer, distance, address, lat, lng, current_area, current_landmark, add_date,ignition_on_address) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $km_reading . "', '" . $km_reading . "', 0,'" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $current . "','" . addslashes($x_address) . "')";
                mysql_query($query) or deadme(mysql_error() . ":" . $query);
            } else if ($row['ignition_flag'] == 0) {
                $row_id = $row['id'];
                $start = strtotime($row['ignition_on']);
                $end = strtotime($current);
                $delta = $end - $start;
                if ($delta > 5) {
                    //$hours = floor($delta / 3600);
                    //$remainder = $delta - $hours * 3600;
                    //$formattedDelta = sprintf('%02d', $hours) . date(':i:s', $remainder);
                    $datetime1 = new DateTime($row['ignition_on']);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');
                    $query = "UPDATE tbl_start_report SET ignition_off = '" . $current . "', stop_odometer = '" . $km_reading . "', duration = '" . addslashes($formattedDelta) . "', distance = ($km_reading - start_odometer) WHERE id = " . $row_id;
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }
            }
        } else if (mysql_num_rows($res) == 0) {
            $query = "INSERT INTO tbl_start_report (device_id, ignition_on, start_odometer, stop_odometer, distance, address, lat, lng, current_area, current_landmark, add_date,ignition_on_address) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $km_reading . "', '" . $km_reading . "', 0,'" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $current . "','" . addslashes($x_address) . "')";
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
        }
    } else {
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $add_date = $row['add_date'];
            $avg_speed = 0;
            $max_speed = 0;
           /* $maxspeed_query = "select max(speed) as max_speed, avg(speed) as avg_speed from tbl_track where dt BETWEEN  '" . $add_date . "' AND '" . $current . "' and assets_id = $assets_id";
            $res1 = mysql_query($maxspeed_query) or deadme(mysql_error() . ":" . $maxspeed_query);
            if (mysql_num_rows($res1) == 1) {
                $row_speed = mysql_fetch_array($res1);
                $max_speed = $row_speed['max_speed'];
                $avg_speed = $row_speed['avg_speed'];
                // //WriteLog($avg_speed." ".$max_speed." ".$maxspeed_query);
            }*/
            // If the data is coming for smae date.
//				if(date('Y-m-d', strtotime($add_date)) == date('Y-m-d')) {
            $row_id = $row['id'];
            if ($row['ignition_flag'] == 0) {

                $start = strtotime($row['ignition_on']);
                $end = strtotime($current);
                $delta = $end - $start;
                if ($delta > 5) {
                    //$hours = floor($delta / 3600);
                    //$remainder = $delta - $hours * 3600;
                    //$formattedDelta = sprintf('%02d', $hours) . date(':i:s', $remainder);
                    $datetime1 = new DateTime($row['ignition_on']);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');
                    //$query="UPDATE tbl_start_report SET ignition_off = '".$current."', stop_odometer = '".$km_reading."', distance = ($km_reading - start_odometer), duration='". addslashes($formattedDelta)."', ignition_flag = 1 , ignition_off_address = '".addslashes($x_address)."' WHERE id = " . $row_id;
                    $query = "UPDATE tbl_start_report SET ignition_off = '" . $current . "', stop_odometer = '" . $km_reading . "', distance = ($km_reading - start_odometer), duration='" . addslashes($formattedDelta) . "', ignition_flag = 1 , ignition_off_address = '" . addslashes($x_address) . "'  ,max_speed = '" . $max_speed . "' ,avg_speed = '" . $avg_speed . "' WHERE id = " . $row_id;
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }
            }
//				}
        }
    }
}
function ignitionAlert($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ignition, $latitude, $longitude, $x_address, $current, $ignitionAlertFlag, $odomVal) {
    global $current_area;
    global $current_landmark;

    global $dts;
    global $uRow;
    $ist = $current;
    $sensor1 = "";
    $sensor2 = "";
    $give_alert = false;
    $ac = $ignition;
    /* $user_sql = "SELECT um.user_id, um.sensor1,um.sensor2 FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='".$assets_id."' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
      ////WriteLog($user_sql);
      $user_rs = mysql_query($user_sql) or deadme(mysql_error().":".$user_sql);
      while($row = mysql_fetch_array($user_rs)){
      $user_id = $row['user_id'];
      $sensor1 = $row['sensor1'];
      $sensor2 = $row['sensor2']; */
    ////echo 'ignitionAlertFlag '.$ignitionAlertFlag.' '.$user_id.' '.$sensor1.' '.$sensor2;
    /*  if($sensor1 != "" && strpos($sensor1,'AC') !== false && $ignitionAlertFlag == 0){
      ////WriteLog("AC:".$sensor1);
      $ignitionAlertFlag = 1;
      ac_report($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ac, $latitude, $longitude, $x_address, $ist, $ignitionAlertFlag);
      // return 1;
      //echo 'ignitionAlertFlag1 '.$ignitionAlertFlag;
      }else *///{
    ////WriteLog("Ignition:".$ignition);
    //  //echo $ignition;
    if ($ignition == 0) {
        $ignition_type = "ignition_off";
        $current_event = "Ignition Off";
    } else if($ignition==1){
        $ignition_type = "ignition_on";
        $current_event = "Ignition On";
    }

    if ($odomVal == "") {
        $sql = "SELECT km_reading FROM assests_master WHERE id = '" . $assets_id . "'";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) > 0) {
            $row_odo = mysql_fetch_array($rs);
            $odomVal = $row_odo['km_reading'];
        }
    }

    $query = "SELECT id, ignition_status, lat, lng FROM tbl_ignition_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    // //echo $query;
    if (mysql_num_rows($res) == 0) {
        $query = "INSERT INTO tbl_ignition_report (device_id, ignition_status, date_time, address, lat, lng, current_area, current_landmark, odometer) VALUES ('" . addslashes($assets_id) . "','ignition_off','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $odomVal . "')";
        //  //echo $query;
        mysql_query($query) or deadme(mysql_error() . ":" . $query);

        $give_alert = true;
        current_event_track($current_event, $assets_id, $unit_no, '');
    } else {
        $row = mysql_fetch_array($res);
        if (trim($row['ignition_status'] != $ignition_type)) {

            $query = "INSERT INTO tbl_ignition_report (device_id, ignition_status, date_time, address, lat, lng, current_area, current_landmark, odometer) VALUES ('" . addslashes($assets_id) . "','$ignition_type','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $odomVal . "')";
            ////echo $query;
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
            $give_alert = true;
            current_event_track($current_event, $assets_id, $unit_no, '');
        } else {
            // //WriteLog("in else".$assets_id.$row['ignition_status'].$ignition_type);
            $give_alert = false;
        }
    }
    ////echo 'give_alert '.$give_alert;
    if ($give_alert) {

        $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.sensor1, um.sensor2, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert, sr.ignition_on_alert as ignition_on_alert1, sr.ignition_off_alert as ignition_off_alert1 FROM user_assets_map lm 
		left join tbl_users um on um.user_id = lm.user_id 
		left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 
		WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
		
		//WriteLogROZ(" [debugs] ".$us_sql);
		
        ////WriteLog($us_sql);
        // //echo '<br>'.$us_sql;
        $rs = mysql_query($us_sql) or deadme(mysql_error() . ":" . $us_sql);
        while ($row = mysql_fetch_array($rs)) {
            $user_id = $row['user_id'];
            $fname = $row['first_name'];
            $mobile = $row['mobile_number']; 
            $sensor1 = $row['sensor1'];
            $sensor2 = $row['sensor2'];
            $email = $row['email_address'];
            $user_sms_alert = $row['user_sms_alert'];
            $user_email_alert = $row['user_email_alert'];
            $ignition_on_alert = $row['ignition_on_alert'];
            $ignition_off_alert = $row['ignition_off_alert'];
            $ignition_on_alert1 = $row['ignition_on_alert1'];
            $ignition_off_alert1 = $row['ignition_off_alert1'];
            $language = $row['language'];
            // //WriteLog($fname." ".$language);

            if ($language == "portuguese") {
                $file = "portuguese_alert_lang.php";
            } else {
                $file = "english_alert_lang.php";
            }
            ////WriteLog($ignition_type);
            include($file);
            ////WriteLog($ignition_type);
            if ($ignition_type == "ignition_on") {
                $ignition_alert = $ignition_on_alert1;
                if($sensor1 != ""){
                    $ignition_alert_text = $sensor1." On";
                }else{
                    $ignition_alert_text = $lang['Ignition On'];
                }
            } else if ($ignition_type == "ignition_off") {
                $ignition_alert = $ignition_off_alert1;
                if($sensor1 != ""){
                    $ignition_alert_text = $sensor1." Off";
                }else{
                    $ignition_alert_text = $lang['Ignition Off'];
                }
            }
            //WriteLog($assets_name." ignition_alert_text ".$ignition_alert_text);
            // $srt=$lang['Dear'];
            // $encoded= mb_detect_encoding($str);
            // //echo  $encoded;
            //  //echo 'ignition alert '.$ignition_alert;
            if ($ignition_alert == 1) {
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
//                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                $smsText = $lang['Dear'] . " $fname, $assets_name $str $ignition_alert_text";

                if ($current_landmark != '') {
                    $smsText .= " " . $lang['near Landmark'] . " $current_landmark";
                } else if ($current_area != "") {
                    $smsText .= " " . $lang['in Area'] . " $current_area";
                } else if ($x_address) {
                    $smsText .= " " . $lang['at'] . " $x_address";
                }

                $smsText .= ", " . convert_time_zone($current, $dts, DISP_TIME); //.date(DISP_TIME, 

                if ($mobile != "" && $user_sms_alert == 1) {
                    send_sms($mobile, $smsText, '', '');
                    //send_sms($mobile, $alert_text);
                    sms_log($mobile, $smsText, $user_id);
                    ////WriteLog("mobile:".$mobile."".$smsText);
                }

                if ($email != "" && $user_email_alert == 1) {

                    send_email($email, $lang['Vehicle'] . " $ignition_alert_text " . $lang['Alert'], $smsText);
                    email_log($email, $smsText, $user_id, $lang['Vehicle'] . " $ignition_alert_text " . $lang['Alert']);
                    ////WriteLog("email:".$email."".$smsText);
                    // chat_alert($email, $smsText);
                }
                //insert in alert master
                sendMobileNotification($user_id, $smsText,'Alert',$assets_id);

                $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( '" . $ignition_alert_text . " " . $lang['Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."','".$x_address."')";
//                 echo "<br />$alertSql";
                ////WriteLog($alertSql);  
                mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
            }
        }
    }
    //return 0;
    //}
    //}
}

/*function ignitionAlert($last_ignition,$unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ignition, $latitude, $longitude, $x_address, $current, $ignitionAlertFlag, $odomVal) {
    global $current_area;
    global $current_landmark;

    global $dts;
    global $uRow;
    $ist = $current;
    $sensor1 = "";
    $sensor2 = "";
    $give_alert = 0;
    $ac = $ignition;
	//WriteLog('ighnition'.$ignition.'vehicle'.$assets_id);
    /* $user_sql = "SELECT um.user_id, um.sensor1,um.sensor2 FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='".$assets_id."' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
      ////WriteLog($user_sql);
      $user_rs = mysql_query($user_sql) or deadme(mysql_error().":".$user_sql);
      while($row = mysql_fetch_array($user_rs)){
      $user_id = $row['user_id'];
      $sensor1 = $row['sensor1'];
      $sensor2 = $row['sensor2']; */
    ////echo 'ignitionAlertFlag '.$ignitionAlertFlag.' '.$user_id.' '.$sensor1.' '.$sensor2;
    /*  if($sensor1 != "" && strpos($sensor1,'AC') !== false && $ignitionAlertFlag == 0){
      ////WriteLog("AC:".$sensor1);
      $ignitionAlertFlag = 1;
      ac_report($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ac, $latitude, $longitude, $x_address, $ist, $ignitionAlertFlag);
      // return 1;
      //echo 'ignitionAlertFlag1 '.$ignitionAlertFlag;
      }else *///{
    ////WriteLog("Ignition:".$ignition);
    //  //echo $ignition;
    /*if ($ignition == 0) {
        $ignition_type = "ignition_off";
        $current_event = "Ignition Off";
    } else if($ignition == 1){
        $ignition_type = "ignition_on";
        $current_event = "Ignition On";
    }

    if ($odomVal == "") {
        $sql = "SELECT km_reading FROM assests_master WHERE id = '" . $assets_id . "'";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) > 0) {
            $row_odo = mysql_fetch_array($rs);
            $odomVal = $row_odo['km_reading'];
        }
    }

    $query = "SELECT id, ignition_status, lat, lng FROM tbl_ignition_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    // //echo $query;
    if (mysql_num_rows($res) == 0) {
        $query = "INSERT INTO tbl_ignition_report (device_id, ignition_status, date_time, address, lat, lng, current_area, current_landmark, odometer) VALUES ('" . addslashes($assets_id) . "','ignition_off','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $odomVal . "')";
        //  //echo $query;
        mysql_query($query) or deadme(mysql_error() . ":" . $query);

        $give_alert = 1;
        current_event_track($current_event, $assets_id, $unit_no, '');
    } else {
        $row = mysql_fetch_array($res);
        if (trim($row['ignition_status'] != $ignition_type)) {

            $query = "INSERT INTO tbl_ignition_report (device_id, ignition_status, date_time, address, lat, lng, current_area, current_landmark, odometer) VALUES ('" . addslashes($assets_id) . "','$ignition_type','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $odomVal . "')";
            ////echo $query;
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
            $give_alert = 1;
            current_event_track($current_event, $assets_id, $unit_no, '');
        } else {
            // //WriteLog("in else".$assets_id.$row['ignition_status'].$ignition_type);
            $give_alert = 0;
        }
    }
    //WriteLog( 'give_alert '.$give_alert);
    if ($give_alert==1) {
	// //WriteLog("in else2".$assets_id.$row['ignition_status'].$ignition_type);
        $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.sensor1, um.sensor2, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert, sr.ignition_on_alert as ignition_on_alert1, sr.ignition_off_alert as ignition_off_alert1 FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
        ////WriteLog($us_sql);
        // //echo '<br>'.$us_sql;
        $rs = mysql_query($us_sql) or deadme(mysql_error() . ":" . $us_sql);
        while ($row = mysql_fetch_array($rs)) {
            $user_id = $row['user_id'];
            $fname = $row['first_name'];
            $mobile = $row['mobile_number'];
            $sensor1 = $row['sensor1'];
            $sensor2 = $row['sensor2'];
            $email = $row['email_address'];
            $user_sms_alert = $row['user_sms_alert'];
            $user_email_alert = $row['user_email_alert'];
            $ignition_on_alert = $row['ignition_on_alert'];
            $ignition_off_alert = $row['ignition_off_alert'];
            $ignition_on_alert1 = $row['ignition_on_alert1'];
            $ignition_off_alert1 = $row['ignition_off_alert1'];
            $language = $row['language'];
            // //WriteLog($fname." ".$language);

            if ($language == "portuguese") {
                $file = "portuguese_alert_lang.php";
            } else {
                $file = "english_alert_lang.php";
            }
            ////WriteLog($ignition_type);
            include($file);
            ////WriteLog($ignition_type);
            if ($ignition_type == "ignition_on") {
                $ignition_alert = $ignition_on_alert1;
                //$ignition_alert_text = $lang['Ignition On'];
                if($sensor1 != ""){
                    $ignition_alert_text = $sensor1." On";
                }else{
                    $ignition_alert_text = $lang['Ignition On'];
                }
            } else if ($ignition_type == "ignition_off") {
                $ignition_alert = $ignition_off_alert1;
                //$ignition_alert_text = $lang['Ignition Off'];
                if($sensor1 != ""){
                    $ignition_alert_text = $sensor1." Off";
                }else{
                    $ignition_alert_text = $lang['Ignition Off'];
                }
            }
            // $srt=$lang['Dear'];
            // $encoded= mb_detect_encoding($str);
            // //echo  $encoded;
            //  //echo 'ignition alert '.$ignition_alert;
            if ($ignition_alert == 1) {
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                $smsText = $lang['Dear'] . " $fname, $assets_name $str $ignition_alert_text";

                if ($current_landmark != '') {
                    $smsText .= " " . $lang['near Landmark'] . " $current_landmark";
                } else if ($current_area != "") {
                    $smsText .= " " . $lang['in Area'] . " $current_area";
                } else if ($x_address) {
                    $smsText .= " " . $lang['at'] . " $x_address";
                }

                $smsText .= ", " . convert_time_zone($current, $dts, DISP_TIME); //.date(DISP_TIME, 

                if ($mobile != "" && $user_sms_alert == 1) {
                    send_sms($mobile, $smsText, '', '');
                    //send_sms($mobile, $alert_text);
                    sms_log($mobile, $smsText, $user_id);
                    ////WriteLog("mobile:".$mobile."".$smsText);
                }

                if ($email != "" && $user_email_alert == 1) {

                    send_email($email, $lang['Vehicle'] . " $ignition_alert_text " . $lang['Alert'], $smsText);
                    email_log($email, $smsText, $user_id, $lang['Vehicle'] . " $ignition_alert_text " . $lang['Alert']);
                    ////WriteLog("email:".$email."".$smsText);
                    // chat_alert($email, $smsText);
                }
                //insert in alert master
               sendMobileNotification($user_id, $smsText,'Alert',$assets_id);

                $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ( '" . $ignition_alert_text . " " . $lang['Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "')";
                // //echo "<br />$alertSql";
                ////WriteLog($alertSql);  
                mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
            }
        }
    }
    //return 0;
    //}
    //}
}*/
/* function ignitionAlert($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ignition, $latitude, $longitude, $x_address, $current, $ignitionAlertFlag, $odomVal) {
    global $current_area;
    global $current_landmark;
    global $dts;
    global $uRow;
    $ist = $current;
    $sensor1 = "";
    $sensor2 = "";
    $give_alert = false;
    $ac = $ignition;  
    
    if ($ignition == 0) {
        $ignition_type = "ignition_off";
        $current_event = "Ignition Off";
    } else if($ignition == 1){
        $ignition_type = "ignition_on";
        $current_event = "Ignition On";
    }

    if ($odomVal == "") {
        $sql = "SELECT km_reading FROM assests_master WHERE id = '" . $assets_id . "'";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) > 0) {
            $row_odo = mysql_fetch_array($rs);
            $odomVal = $row_odo['km_reading'];
        }
    }

    $query = "SELECT id, ignition_status, lat, lng FROM tbl_ignition_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);   
    if (mysql_num_rows($res) == 0) {
        $query = "INSERT INTO tbl_ignition_report (device_id, ignition_status, date_time, address, lat, lng, current_area, current_landmark, odometer) VALUES ('" . addslashes($assets_id) . "','ignition_off','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $odomVal . "')";
        
        mysql_query($query) or deadme(mysql_error() . ":" . $query);

        $give_alert = false;
        current_event_track($current_event, $assets_id, $unit_no, '');
    } else {
        $row = mysql_fetch_array($res);
        if (trim($row['ignition_status'] !== $ignition_type)) {

            $query = "INSERT INTO tbl_ignition_report (device_id, ignition_status, date_time, address, lat, lng, current_area, current_landmark, odometer) VALUES ('" . addslashes($assets_id) . "','$ignition_type','" . $current . "','" . addslashes($x_address) . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "', '" . $current_area . "', '" . $current_landmark . "', '" . $odomVal . "')";
            ////echo $query;
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
            $give_alert = true;
            current_event_track($current_event, $assets_id, $unit_no, '');
        } else {
            // //WriteLog("in else".$assets_id.$row['ignition_status'].$ignition_type);
            $give_alert = false;
        }
    }
    ////echo 'give_alert '.$give_alert;
    if ($give_alert) {
        $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.sensor1, um.sensor2, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert, sr.ignition_on_alert as ignition_on_alert1, sr.ignition_off_alert as ignition_off_alert1 FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";
        ////WriteLog($us_sql);
        // //echo '<br>'.$us_sql;
        $rs = mysql_query($us_sql) or deadme(mysql_error() . ":" . $us_sql);
        while ($row = mysql_fetch_array($rs)) {
            $user_id = $row['user_id'];
            $fname = $row['first_name'];
            $mobile = $row['mobile_number']; 
            $sensor1 = $row['sensor1'];
            $sensor2 = $row['sensor2'];
            $email = $row['email_address'];
            $user_sms_alert = $row['user_sms_alert'];
            $user_email_alert = $row['user_email_alert'];
            $ignition_on_alert = $row['ignition_on_alert'];
            $ignition_off_alert = $row['ignition_off_alert'];
            $ignition_on_alert1 = $row['ignition_on_alert1'];
            $ignition_off_alert1 = $row['ignition_off_alert1'];
            $language = $row['language'];
            // //WriteLog($fname." ".$language);

            if ($language == "portuguese") {
                $file = "portuguese_alert_lang.php";
            } else {
                $file = "english_alert_lang.php";
            }
            ////WriteLog($ignition_type);
            include($file);
            ////WriteLog($ignition_type);
            if ($ignition_type == "ignition_on") {
                $ignition_alert = $ignition_on_alert1;
                if($sensor1 != ""){
                    $ignition_alert_text = $sensor1." On";
                }else{
                    $ignition_alert_text = $lang['Ignition On'];
                }
            } else if ($ignition_type == "ignition_off") {
                $ignition_alert = $ignition_off_alert1;
                if($sensor1 != ""){
                    $ignition_alert_text = $sensor1." Off";
                }else{
                    $ignition_alert_text = $lang['Ignition Off'];
                }
            }
            //WriteLog($assets_name." ignition_alert_text ".$ignition_alert_text);
            // $srt=$lang['Dear'];
            // $encoded= mb_detect_encoding($str);
            // //echo  $encoded;
            //  //echo 'ignition alert '.$ignition_alert;
            if ($ignition_alert == 1) {
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                $smsText = $lang['Dear'] . " $fname, $assets_name $str $ignition_alert_text";

                if ($current_landmark != '') {
                    $smsText .= " " . $lang['near Landmark'] . " $current_landmark";
                } else if ($current_area != "") {
                    $smsText .= " " . $lang['in Area'] . " $current_area";
                } else if ($x_address) {
                    $smsText .= " " . $lang['at'] . " $x_address";
                }

                $smsText .= ", " . convert_time_zone($current, $dts, DISP_TIME); //.date(DISP_TIME, 

                if ($mobile != "" && $user_sms_alert == 1) {
                    send_sms($mobile, $smsText, '', '');
                    //send_sms($mobile, $alert_text);
                    sms_log($mobile, $smsText, $user_id);
                    ////WriteLog("mobile:".$mobile."".$smsText);
                }

                if ($email != "" && $user_email_alert == 1) {

                    send_email($email, $lang['Vehicle'] . " $ignition_alert_text " . $lang['Alert'], $smsText);
                    email_log($email, $smsText, $user_id, $lang['Vehicle'] . " $ignition_alert_text " . $lang['Alert']);
                    ////WriteLog("email:".$email."".$smsText);
                    // chat_alert($email, $smsText);
                }
                //insert in alert master
              sendMobileNotification($user_id, $smsText,'Alert',$assets_id);

                $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ( '" . $ignition_alert_text . " " . $lang['Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "')";
                
                mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
            }
        }
    }
    //return 0;
    //}
    //}
//} */
/* Added By Ashwini Gaikwad 13-10-2014 For Ignition on speed off alerts. */

function checkIgnitionOnSpeedOff($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $ignition, $speed, $last_speed, $latitude, $longitude, $x_address, $current) {
    global $alert_header;

    global $dts;
    global $uRow;
    $query = "SELECT id,motion_start_time,motion_stop_time,alert_given FROM tbl_ignition_on_speed_off WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";


    $res = mysql_query($query);


    if ($ignition == 1 && $last_speed == 0 && $speed == 0) {
        ////WriteLog("in if");

        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $stop_report_id = $row['id'];
            if (trim($row['motion_start_time'] != "")) {
                $query = "INSERT INTO tbl_ignition_on_speed_off (device_id, motion_stop_time, address, add_date, lat, lng) VALUES "
                        . "('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "', '" . $current . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "')";
                $alert_ignition = true;
            }

            //alert if stop more than given time and ignition on
            if (trim($row['motion_start_time'] == "")) {
                $start = strtotime($row['motion_stop_time']);
                $end = strtotime($current);
                $minutes = round(abs($end - $start) / 60, 2);
                $user_id = $uRow['user_id'];
                $fname = $uRow['first_name'];
                $mobile = $uRow['mobile_number'];
                $email = $uRow['email_address'];
                $user_sms_alert = $uRow['sms_alert'];
                $user_email_alert = $uRow['email_alert'];
                $alert_start_time = $uRow['alert_start_time'];
                $alert_stop_time = $uRow['alert_stop_time'];
                $max_stop_time = $uRow['ignition_on_speed_off_minutes'];
                $language = $uRow['language'];
                if ($language == "portuguese") {
                    $file = "portuguese_alert_lang.php";
                } else {
                    $file = "english_alert_lang.php";
                }
                include($file);
                ////WriteLog("start: ".$row['motion_stop_time']."end: ".$current."minutes: ".$minutes."max_stop_time ".$max_stop_time."assets_name ".$assets_name);
                ////WriteLog("start: ".$start."end: ".$end."minutes: ".$minutes);					
                //if stop time more than set time and alert not given
                if ($max_stop_time != "" && $max_stop_time != 0 && $minutes > $max_stop_time && $row['alert_given'] == 0) {
                    ////WriteLog("alert given");

                    $stop_time = sec2HourMinute($max_stop_time * 60);
                    $txt = '';
                    if (isset($nick_name) && $nick_name != '')
                        $txt .= $nick_name;
                    if (isset($driver_name) && $driver_name != '') {
                        if ($txt == '') {
                            $txt .= $driver_name;
                        } else {
                            $txt .= ', ' . $driver_name;
                        }
                    }
                    if ($txt != '')
                        $str = '(' . $txt . ')';
                    else
                        $str = '';

                    $smsText = $lang['Dear'] . " $fname, $assets_name $str " . $lang['stop more than'] . " $stop_time .";

                    $smsText1 = $lang['Dear'] . " $fname, $assets_name $str " . $lang['stop more than'] . " $stop_time .";


                    if ($mobile != "" && $user_sms_alert == 1) {
                        send_sms($mobile, $smsText1);
                        sms_log($mobile, $smsText1, $user_id);
                    }

                    if ($email != "" && $user_email_alert == 1) {
                        send_email($email, $lang['Vehicle Stop And Ignition On Alert'], $smsText);
                        email_log($email, $smsText, $user_id, $lang['Vehicle Stop And Ignition On Alert']);
                        ////WriteLog($email."sms".$smsText);
                        //chat_alert($email, $smsText);
                    }


                    //update alert given
                    $uSql = "update tbl_ignition_on_speed_off set alert_given = 1 where id = $stop_report_id";
                    @mysql_query($uSql);

                    if ($user_email_alert == 1 || $user_sms_alert == 1) {
                        //insert in alert master
                        $alert_header = $lang['Vehicle Stop And Ignition On Alert'];
                        sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,alert_address) values ( '" . $lang['Vehicle Stop And Ignition On Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . $current . "', '" . $x_address . "')";
                        //mysql_query($alertSql);
                    }
                }
            }
        } else if (mysql_num_rows($res) == 0) {
            ////WriteLog($assets_id." ".$current." ".$x_address." ".$latitude." ".$longitude);
            // //echo $assets_id." ".$current." ".$x_address." ".$latitude." ".$longitude;
            $query = "INSERT INTO tbl_ignition_on_speed_off (device_id, motion_stop_time, address, add_date, lat, lng) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "', '" . $current . "','" . addslashes($latitude) . "','" . addslashes($longitude) . "')";
        }
    } else {
        ////WriteLog("in else");


        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $row_id = $row['id'];
            if (trim($row['motion_start_time']) == "") {

                $start = strtotime($row['motion_stop_time']);
                $end = strtotime($current);
                $delta = $end - $start;
                $hours = floor($delta / 3600);
                $remainder = $delta - $hours * 3600;
                ////WriteLog("row_id:".$row_id."start:".$start"end:".$end"delta:".$delta"hours:".$hours."remainder:".$remainder);
                if ($remainder < 60) {
                    $query = "delete from tbl_ignition_on_speed_off WHERE id = " . $row_id;
                } else {
                    $formattedDelta = sprintf('%02d', $hours) . gmdate(':i:s', $remainder);

                    $query = "UPDATE tbl_ignition_on_speed_off SET motion_start_time = '" . $current . "', duration='" . addslashes($formattedDelta) . "' WHERE id = " . $row_id;
                }
            }
        }
    }
    if ($query != '') {
        @mysql_query($query);
    }
}

/* Added By Ashwini Gaikwad 13-10-2014 For Ignition off speed On alerts. */

function checkIgnitionOffSpeedOn($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $ignition, $speed, $last_speed, $latitude, $longitude, $x_address, $current) {
    global $alert_header;

    global $dts;
    global $uRow;
    $query = "SELECT ignition_start_time,id,ignition_stop_time FROM tbl_ignition_off_speed_on WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if ($ignition == 0 && $speed > 0) {
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $stop_report_id = $row['id'];
            if (trim($row['ignition_start_time'] != "")) {
                $query = "INSERT INTO tbl_ignition_off_speed_on (device_id, ignition_stop_time, address, add_date, ignition, speed) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . $current . "', '$ignition', '$speed')";
            }

            //alert if running more than given time with ignition off
            if (trim($row['ignition_start_time'] == "")) {


                $start = strtotime($row['ignition_stop_time']);
                $end = strtotime($current);
                $minutes = round(abs($end - $start) / 60, 2);

                $user_id = $uRow['user_id'];
                $fname = $uRow['first_name'];
                $mobile = $uRow['mobile_number'];
                $email = $uRow['email_address'];
                $user_sms_alert = $uRow['sms_alert'];
                $user_email_alert = $uRow['email_alert'];
                $alert_start_time = $uRow['alert_start_time'];
                $alert_stop_time = $uRow['alert_stop_time'];
                $max_stop_time = $uRow['ignition_off_speed_on_minutes'];
                $language = $uRow['language'];
                if ($language == "portuguese") {
                    $file = "portuguese_alert_lang.php";
                } else {
                    $file = "english_alert_lang.php";
                }
                include($file);
                //	//WriteLog("start: ".$row['ignition_stop_time']."end: ".$current."minutes: ".$minutes."max_stop_time ".$max_stop_time."assets_name ".$assets_name);
                ////WriteLog("user id : ".$user_id." language: ".$language);
                $minute1 = $max_stop_time;
                $query1 = "select add_date from alert_master where assets_id = '$assets_id' and alert_header = '" . $lang['Vehicle Running And Ignition Off Alert'] . "' ORDER BY id DESC LIMIT 0,1";
                $res1 = mysql_query($query1) or deadme(mysql_error() . ":" . $query1);
                if (mysql_num_rows($res1) == 1) {
                    $row1 = mysql_fetch_array($res1);
                    $before_send_alert = strtotime($row1['add_date']);

                    $minute1 = round(abs($end - $before_send_alert) / 60, 2);
                    //	//WriteLog($minute1." asset_id".$assets_id." ".$assets_name);
                }
                if ($minute1 >= $max_stop_time) {


                    //if stop time more than set time and alert not given
                    if ($max_stop_time != "" && $max_stop_time != 0 && $minutes > $max_stop_time) {
                        $stop_time = sec2HourMinute($max_stop_time * 60);
                        $txt = '';
                        if (isset($nick_name) && $nick_name != '')
                            $txt .= $nick_name;
                        if (isset($driver_name) && $driver_name != '') {
                            if ($txt == '') {
                                $txt .= $driver_name;
                            } else {
                                $txt .= ', ' . $driver_name;
                            }
                        }
                        if ($txt != '')
                            $str = '(' . $txt . ')';
                        else
                            $str = '';
                        $smsText = $lang['Dear'] . " $fname, $assets_name $str " . $lang['running more than'] . " $stop_time .";
                        $smsText1 = $lang['Dear'] . " $fname, $assets_name $str " . $lang['running more than'] . " $stop_time .";
                        if ($mobile != "" && $user_sms_alert == 1) {
                            send_sms($mobile, $smsText1);
                            sms_log($mobile, $smsText1, $user_id);
                        }
                        if ($email != "" && $user_email_alert == 1) {
                            //$email="h.kumbhar@chateglobalservices.com";
                            send_email($email, $lang['Vehicle Running And Ignition Off Alert'], $smsText);
                            email_log($email, $smsText, $user_id, $lang['Vehicle Running And Ignition Off Alert']);
                            //chat_alert($email, $smsText);
                            ////WriteLog($email."sms".$smsText);
                        }

                        //update alert given
                        $uSql = "update tbl_ignition_off_speed_on set alert_given = 1 where id = $stop_report_id";
                        mysql_query($uSql) or deadme(mysql_error() . ":" . $uSql);

                        if ($user_email_alert == 1 || $user_sms_alert == 1) {

                            //insert in alert master
                            $alert_header = 'Vehicle Running And Ignition Off Alert';
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,alert_address) values ('" . $lang['Vehicle Running And Ignition Off Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . $current . "', '" . $x_address . "')";
                            mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
                        }
                    }
                }
            }
        } else if (mysql_num_rows($res) == 0) {
            $query = "INSERT INTO tbl_ignition_off_speed_on (device_id, ignition_stop_time, address, add_date, ignition, speed) VALUES ('" . addslashes($assets_id) . "','" . $current . "','" . addslashes($x_address) . "','" . $current . "', '$ignition', '$speed')";
        }
    } else {
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $row_id = $row['id'];
            if (trim($row['ignition_start_time']) == "") {

                $start = strtotime($row['ignition_stop_time']);
                $end = strtotime($current);
                $delta = $end - $start;
                $hours = floor($delta / 3600);
                $remainder = $delta - $hours * 3600;
                $formattedDelta = sprintf('%02d', $hours) . gmdate(':i:s', $remainder);

                $query = "UPDATE tbl_ignition_off_speed_on SET ignition_start_time = '" . $current . "', duration='" . addslashes($formattedDelta) . "' WHERE id = " . $row_id;
            }
        }
    }
    if ($query != '') {
        mysql_query($query) or deadme(mysql_error() . ":" . $sql);
    }
}

/* Added by Ashwini 12-9-2014 for below new VTS alerts :
  1)SOS Alert
  2)Door Open Alert
  3)Low Battery Alert
  4)Power Off Alert

  function new_vts_alerts($device,$reason_text, $assets_id, $assets_name, $nick_name, $latitude_x, $longitude_y, $x_address, $current){

  global $dts;
  $current_time=convert_time_zone($current, $dts, DISP_TIME);
  $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

  $rs = mysql_query($us_sql);
  while($row = mysql_fetch_array($rs)){
  $user_id = $row['user_id'];
  $fname = $row['first_name'];
  $mobile = $row['mobile_number'];
  $email = $row['email_address'];
  $user_sms_alert = $row['user_sms_alert'];
  $user_email_alert = $row['user_email_alert'];
  $language = $row['language'];
  if($language == "portuguese")
  {
  include_once("portuguese_alert_lang.php");
  }
  else{
  include_once("english_alert_lang.php");
  }

  $reason_text=  strtolower($reason_text);
  switch ($reason_text) {
  case "help me":
  $alert_text = $lang['Vehicle SOS Alert'];
  $emailText = $lang['Dear']." $fname, ".$lang['your vehicle']." $assets_name ($nick_name) ".$lang['just send SOS Alert at']." $x_address, $current_time ";
  $smsText = $lang['Dear']." $fname, ".$lang['your vehicle']." $assets_name ($nick_name) ".$lang['just send SOS Alert at']." $x_address, $current_time ";
  $emailText .= ', '.$lang['Click'].' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude_x.','.$longitude_y.'('.$assets_name.')&ie=UTF8&z=12&om=1">'.$lang['here'].'</a>'.$lang['to view on map'].' .';

  break;

  case "low battery":
  $alert_text = $lang['Vehicle Low Battery Alert'];
  $emailText = $lang['Dear']." $fname, ".$lang['the voltage of your tracker device']." ($device) ".$lang['for vehicle']." $assets_name ".$lang['is low at']." $x_address, $current_time";
  $smsText = $lang['Dear']." $fname, ".$lang['the voltage of your tracker device']." ($device) ".$lang['for vehicle']." $assets_name ".$lang['is low at']." $x_address, $current_time.";
  $emailText .= ', '.$lang['Click'].' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude_x.','.$longitude_y.'('.$assets_name.')&ie=UTF8&z=12&om=1">'.$lang['here'].'</a>'.$lang['to view on map'].' .';

  break;

  case "door alarm":
  $alert_text = $lang['Vehicle Door Open Alert'];
  $emailText = $lang['Dear']." $fname, ".$lang['the door of your vehicle']." $assets_name ($nick_name) ".$lang['is open near']." $x_address, $current_time";
  $smsText = $lang['Dear']." $fname, ".$lang['the door of your vehicle']." ($nick_name) ".$lang['is open near']." $x_address, $current_time";
  $emailText .= ', '.$lang['Click'].' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude_x.','.$longitude_y.'('.$assets_name.')&ie=UTF8&z=12&om=1">'.$lang['here'].'</a>'.$lang['to view on map'].' .';
  break;

  case "power alarm":
  $alert_text = $lang['Vehicle Power Off Alert'];
  $emailText = $lang['Dear']." $fname, ".$lang['the power of your tracker device']." ($device) ".$lang['is cut off for vehicle']." $assets_name near $x_address, $current_time";
  $smsText = $lang['Dear']." $fname, ".$lang['the power of your tracker device']." ($device) ".$lang['is cut off for vehicle']." $assets_name near $x_address, $current_time";
  $emailText .= ', '.$lang['Click'].' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude_x.','.$longitude_y.'('.$assets_name.')&ie=UTF8&z=12&om=1">'.$lang['here'].'</a>'.$lang['to view on map'].' .';
  break;

  default : return true ;

  }

  if($mobile != "" && $user_sms_alert == 1 && $smsText != ""){
  //send_sms($mobile, $smsText, '', '');
  //sms_log($mobile, $smsText, $user_id);
  }

  if($email!="" && $user_email_alert == 1 && $emailText != "" ) {

  send_email($email, "$alert_text", $emailText);
  email_log($email, $emailText, $user_id, "$alert_text");
  //WriteLog($email."emailText".$emailText);
  }
  //insert into alert master table

  $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ('".$alert_text."', '".$emailText."', 'alert', '".$user_id."', '".$assets_id."', '".date(DATE_TIME)."')";

  mysql_query($alertSql) or deadme(mysql_error().":".$alertSql);
  }
  }
 */

// added by harshal all status alert 
//status_alert($device, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $status, $latitude, $longitude, $x_address, $ist);

function status_alert($unit_no, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $status, $latitude_x, $longitude_y, $x_address, $current) {

    global $current_area;
    global $current_landmark;

    $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

    $rs = mysql_query($us_sql);
    while ($row = mysql_fetch_array($rs)) {
        $user_id = $row['user_id'];
        $fname = $row['first_name'];
        $mobile = $row['mobile_number'];
        $email = $row['email_address'];
        $user_sms_alert = $row['user_sms_alert'];
        $user_email_alert = $row['user_email_alert'];
        $ignition_on_alert = $row['ignition_on_alert'];
        $ignition_off_alert = $row['ignition_off_alert'];
        $language = $row['language'];
        if ($language == "portuguese") {
            $file = "portuguese_alert_lang.php";
        } else {
            $file = "english_alert_lang.php";
        }
        include($file);
        $alert_text = "";
        $alert_header = "";
        $txt = '';
        if (isset($nick_name) && $nick_name != '')
            $txt .= $nick_name;
        if (isset($driver_name) && $driver_name != '') {
            if ($txt == '') {
                $txt .= $driver_name;
            } else {
                $txt .= ', ' . $driver_name;
            }
        }
        if (isset($driver_mobile) && $driver_mobile != '') {
            if ($txt == '') {
                $txt .= $driver_mobile;
            } else {
                $txt .= ', ' . $driver_mobile;
            }
        }
        if ($txt != '')
            $str = '(' . $txt . ')';
        else
            $str = '';
        switch ($status) {
            case "ACC OS":
                $alert_text = $lang['Displacement On Alarm'];
                $alert_header = $lang['Vehicle'] . " " . $alert_text . " " . $lang['Alert'];
                $emailText = $lang['Alarm For Asset'] . " $assets_name $txt : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $smsText = $lang['Alarm For Asset'] . " $assets_name $txt : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $emailText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $latitude_x . ',' . $longitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                break;

            case "ACC RS":
                $alert_text = $lang['Displacement Out Alarm'];
                $alert_header = $lang['Vehicle'] . " " . $alert_text . " " . $lang['Alert'];
                $emailText = $lang['Alarm For Asset'] . " $assets_name $str : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $smsText = $lang['Alarm For Asset'] . " $assets_name $str : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $emailText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $latitude_x . ',' . $longitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                break;

            case "DEF":
                $alert_text = $lang['Failure Main Battery Alarm'];
                $alert_header = $lang['Vehicle'] . " " . $alert_text . " " . $lang['Alert'];
                $emailText = $lang['Alarm For Asset'] . " $assets_name $str : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $smsText = $lang['Alarm For Asset'] . " $assets_name $str : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $emailText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $latitude_x . ',' . $longitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                break;

            case "SOS":
                $alert_text = $lang['Alarm ON'];
                $alert_header = $lang['Vehicle alarm with active SOS'];
                ////WriteLog($alert_text);
                $emailText = $lang['Alarm For Asset'] . " $assets_name $str : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $smsText = $lang['Alarm For Asset'] . " $assets_name $str : $alert_text " . $lang['at'] . " " . date(DISP_TIME);
                $emailText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $latitude_x . ',' . $longitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                break;


            default : return true;
        }
        ////WriteLog($email.''.$user_email_alert);
        if ($mobile != "" && $user_sms_alert == 1) {
            send_sms($mobile, $smsText, '', '');
            //send_sms($mobile, $alert_text);
            sms_log($mobile, $smsText, $user_id);
        }

        if ($email != "" && $user_email_alert == 1) {
            send_email($email, $alert_header, $emailText);
            email_log($email, $emailText, $user_id, "Vehicle $alert_text Alert");
            // chat_alert($email, $smsText);
            ////WriteLog($emailText);
        }
        //insert in alert master
        sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
        $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,alert_address) values ( '" . $alert_header . "', '" . $emailText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "', '" . $x_address . "')";//$x_address
        // //echo "<br />$alertSql";
        mysql_query($alertSql);
    }
}

// trip alerts added by harshal 
function startStopTrip($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $lati, $longi, $current_speed, $ist, $distance_travelled,$x_address) {

    global $dts;
    $sqlP = "SELECT trip.*, um.user_id, um.first_name, um.mobile_number, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, am.driver_name, am.km_reading FROM tbl_routes trip LEFT JOIN tbl_users um ON um.user_id = trip.add_uid left join assests_master am on am.current_trip = trip.id WHERE am.id = $assets_id and trip.del_date IS NULL AND trip.status = 1";

    $rsP = mysql_query($sqlP) or deadme("Failed to Execute, SQL : $sqlP, Error : " . mysql_error());
    if (mysql_num_rows($rsP) > 0) {
        $rowP = mysql_fetch_array($rsP);
        $isCheckForLastPoint = true;

        $user_id = $rowP['user_id'];
        $fname = $rowP['first_name'];
        $mobile = $rowP['mobile_number'];
        $email = $rowP['email_address'];
        $user_email_alert = $rowP['user_email_alert'];
        $user_sms_alert = $rowP['user_sms_alert'];
        $driver_name = $rowP['driver_name'];
        $km_reading = $rowP['km_reading'];
        $language = $rowP['language'];
        if ($language == "portuguese") {
            $file = "portuguese_alert_lang.php";
        } else {
            $file = "english_alert_lang.php";
        }
        ////WriteLog($file);
        include($file);
        $trip_id = $rowP['id'];
        $trip_name = $rowP['routename'];
        $total_time_in_minutes = $rowP['total_time_in_minutes'];
        $landmark_ids = $rowP['landmark_ids'];
        $landmark_ids = explode(",", $landmark_ids);
        $start_point = $landmark_ids[0];
        $end_point = end($landmark_ids);

        //check for start location
        $sqlL = "SELECT * FROM landmark WHERE id = $start_point";
        $rsL = mysql_query($sqlL);
        $rowL = mysql_fetch_array($rsL);

        $distance_unit = $rowL['distance_unit'];
        $distance_value = $rowL['radius'];

        if ($distance_unit == "Mile") {
            $unit = "Mile";
        } else {
            $unit = "K";
        }
        $distanceFromLandmark = getDistance($lati, $longi, $rowL['lat'], $rowL['lng'], $unit);


        if ($distance_unit == "Meter")
            $distanceFromLandmark = $distanceFromLandmark * 1000;

        if ($distanceFromLandmark > $distance_value) {
            $sql = "select * from trip_log where trip_id = $trip_id and device_id = $assets_id order by id desc limit 1";
            $rs = mysql_query($sql);
            $trip_start_alert = false;
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                if ($row['is_complete'] == 1) {

                    $ins = "insert into trip_log(trip_id, device_id, driver_name, start_km_reading, start_time) values($trip_id, $assets_id, '$driver_name', '$km_reading', '" . $ist . "')";
                    mysql_query($ins) or deadme("Failed to Execute, SQL : $ins, Error : " . mysql_error());
                    $trip_start_alert = true;

                    //update next landmark
                    $next_trip_landmark = $landmark_ids[1];
                    $nextLSql = "update assests_master set next_trip_landmark = $next_trip_landmark where id = $assets_id";
                    mysql_query($nextLSql) or deadme("Failed to Execute, SQL : $nextLSql, Error : " . mysql_error());
                } else {
                    $update = "update trip_log set distance_travelled = distance_travelled + $distance_travelled where id = " . $row['id'];
                    mysql_query($update) or deadme("Failed to Execute, SQL : $update, Error : " . mysql_error());
                }
            } else {
                $ins = "insert into trip_log(trip_id, device_id, driver_name, start_km_reading, start_time) values($trip_id, $assets_id, '$driver_name', '$km_reading', '" . $ist . "')";

                mysql_query($ins) or deadme("Failed to Execute, SQL : $ins, Error : " . mysql_error());
                $trip_start_alert = true;

                //update next landmark
                $next_trip_landmark = $landmark_ids[1];
                $nextLSql = "update assests_master set next_trip_landmark = $next_trip_landmark where id = $assets_id";
                mysql_query($nextLSql) or deadme("Failed to Execute, SQL : $nextLSql, Error : " . mysql_error());
            }
            if ($trip_start_alert == true) {
                ////WriteLog("hello");
                //send alert to sub users
                /* $sql = "select um.first_name, um.mobile_number, um.email_address, um.language, um.email_alert, um.sms_alert from tbl_users um left join user_assets_map uam on uam.user_id = um.user_id where FIND_IN_SET( $assets_id, uam.assets_ids ) and um.del_date is null and um.status = 1 and um.user_id";
                  $rs = mysql_query($sql) or deadme("Failed to Execute, SQL : $sql, Error : " . mysql_error());
                  while($row = mysql_fetch_array($rs)){
                  $fname 					= $row['first_name'];
                  $mobile 				= $row['mobile_number'];
                  $email 					= $row['email_address'];
                  $user_email_alert 		= $row['email_alert'];
                  $user_sms_alert 		= $row['sms_alert'];
                 */

                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ') ';
                else
                    $str = '';

                $smsText = $lang['Dear'] . " $fname, $assets_name " . $str . '' . $lang['started trip'] . " $trip_name " . $lang['on time'] . " " . convert_time_zone($ist, $dts, DISP_TIME);
                if ($mobile != "" && $user_sms_alert == 1) {
                    send_sms($mobile, $smsText, '', '');
                    sms_log($mobile, $smsText, $user_id);
                }

                if ($email != "" && $user_email_alert == 1) {
                    send_email($email, $lang['Trip Start Alert'], $smsText);
                    email_log($email, $smsText, $user_id, 'Trip Start Alert');
                    ////WriteLog($email." ".$smsText);
                    //chat_alert($email, $smsText);
                }

                //insert in alert master
                sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) values ( '" . $lang['Trip Start Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . gmdate(DATE_TIME, strtotime($ist)) . "','".$latitude."','".$longitude."','".$x_address."')";//$x_address
                mysql_query($alertSql);
                //}
            }
        }
        if ($isCheckForLastPoint == true) {
            //check for end location
            $sqlL = "SELECT * FROM landmark WHERE id = $end_point";
            $rs = mysql_query($sqlL);
            $row = mysql_fetch_array($rs);

            $distance_unit = $row['distance_unit'];
            $distance_value = $row['radius'];

            if ($distance_unit == "Mile") {
                $unit = "Mile";
            } else {
                $unit = "K";
            }
            $distanceFromLandmark = getDistance($lati, $longi, $row['lat'], $row['lng'], $unit);
            if ($distance_unit == "Meter")
                $distanceFromLandmark = $distanceFromLandmark * 1000;
            if ($distanceFromLandmark < $distance_value) {
                $sql = "select * from trip_log where trip_id = $trip_id and device_id = $assets_id order by id desc limit 1";
                $rs = mysql_query($sql);
                if (mysql_num_rows($rs) > 0) {
                    $row = mysql_fetch_array($rs);
                    if ($row['start_time'] != "" && $row['end_time'] == "") {

                        $ins = "update trip_log set end_km_reading = '$km_reading', end_time = '" . $ist . "', is_complete = '1' where id = '" . $row['id'] . "'";
                        mysql_query($ins);

                        $unsetTripSql = "UPDATE assests_master SET current_trip='null', next_trip_landmark = 'null' WHERE id=$assets_id";
                        mysql_query($unsetTripSql);

                        //create sms template
                        $trip_minutes = round(abs(strtotime($ist) - strtotime($row['start_time'])) / 60, 2);

                        $time_taken = sec2HourMinute($trip_minutes * 60);

                        $f1 = $fname;
                        $f2 = $assets_name;
                        $f3 = $nick_name;
                        $f4 = $driver_name;
                        $f5 = $trip_name;
                        $txt = '';
                        if (isset($nick_name) && $nick_name != '')
                            $txt .= $nick_name;
                        if (isset($driver_name) && $driver_name != '') {
                            if ($txt == '') {
                                $txt .= $driver_name;
                            } else {
                                $txt .= ', ' . $driver_name;
                            }
                        }
                        if ($txt != '')
                            $str = '(' . $txt . ') ';
                        else
                            $str = '';
                        if ($trip_minutes > $total_time_in_minutes && $total_time_in_minutes != "") {

                            //Dear [F1], [F2] ([F3], [F4]) has completed [F5] trip in [F6], [F7] Late
                            $late_minutes = $trip_minutes - $total_time_in_minutes;
                            $late = sec2HourMinute($late_minutes * 60);
                            $smsText .= "$late " . $lang['Late'] . ".";

                            $template_id = '3829';
                            $f6 = $time_taken;
                            $f7 = $late;

                            $smsText = $lang['Dear'] . " $fname, $assets_name " . $str . '' . $lang['has completed'] . " $trip_name " . $lang['trip in'] . " $time_taken, $late " . $lang['Late'] . ", " . convert_time_zone($ist, $dts, DISP_TIME); // date(DISP_TIME, strtotime($ist));
                        } else {
                            $smsText = $lang['Dear'] . " $fname, $assets_name " . $str . '' . $lang['has completed'] . " $trip_name " . $lang['trip in'] . " $time_taken, " . convert_time_zone($ist, $dts, DISP_TIME); // date(DISP_TIME, strtotime($ist));
                            //Dear [F1], [F2] ([F3], [F4]) has completed the [F5] trip in [F6][F7]
                            $template_id = '3828';

                            list($f6, $f7) = str_split($time_taken, 30);
                            if ($f7 == '')
                                $f7 = ' ';
                            $smsText = $lang['Dear'] . " $fname, $assets_name " . $str . '' . $lang['has completed'] . " $trip_name " . $lang['trip in'] . " $time_taken, " . convert_time_zone($ist, $dts, DISP_TIME); // date(DISP_TIME, strtotime($ist));
                        }
                        $f8 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                        $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6, "F7" => $f7, "F8" => $f8);
                        //

                        if ($mobile != "" && $user_sms_alert == 1) {

                            send_sms($mobile, $smsText, $template_id, $template_data);
                            sms_log($mobile, $smsText, $user_id);
                        }
                        if ($email != "" && $user_email_alert == 1) {
                            send_email($email, $lang['Trip Complete at Last Location Alert'], $smsText);
                            email_log($email, $smsText, $user_id, 'Trip Complete at Last Location Alert');
                            //chat_alert($email, $smsText);
                            ////WriteLog($email." ".$smsText);
                        }
                        //insert in alert master
                        sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,alert_address) values ( '" . $lang['Trip Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . $ist . "', '" . $x_address . "')";
                        mysql_query($alertSql);
                    }
                }
            }
        }
    }
}

function checkRoute($device_id, $assets_id, $current_trip, $assets_name, $nick_name, $driver_name, $lati, $longi, $current_speed, $ist,$x_address) {

    $insert_data = false;
    $route_flag = false;
    global $current_landmark, $dts;

    if ($current_trip != "") {
        $sqlP = "SELECT trip.*, um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert FROM tbl_routes trip left join tbl_users um on um.user_id = trip.userid WHERE trip.del_date is null and trip.status = 1 and trip.id = $current_trip";

        $rs = mysql_query($sqlP);
        $row = mysql_fetch_array($rs);

        if (mysql_num_rows($rs) > 0 && $row['distance_value'] > 0) {

            $trip_id = $row['id'];
            $distance_unit = $row['distance_unit'];
            $distance_value = $row['distance_value'];
            $routename = $row['routename'];
            $user_id = $row['user_id'];
            $fname = $row['first_name'];
            $mobile = $row['mobile_number'];
            $email = $row['email_address'];
            $user_email_alert = $row['user_email_alert'];
            $user_sms_alert = $row['user_sms_alert'];
            $route_email_alert = $row['email_alert'];
            $route_sms_alert = $row['sms_alert'];
            $pt = $row['points'];
            $total_time_in_minutes = $row['total_time_in_minutes'];
            $landmark_ids = $row['landmark_ids'];
            $language = $row['language'];
            if ($language == "portuguese") {
                $file = "portuguese_alert_lang.php";
            } else {
                $file = "english_alert_lang.php";
            }
            include($file);
            $landmark_ids = explode(",", $landmark_ids);
            $start_point = $landmark_ids[0];
            if ($row['round_trip'] == 1)
                $end_point = $start_point;
            else
                $end_point = end($landmark_ids);

            $ref = array($lati, $longi);
            $items = array();

            $points = explode(":", $pt);

            foreach ($points as $point) {
                $point = explode(",", $point);
                $items[] = array($point[0], $point[1]);
            }
            $distances = array_map(function($item) use($ref) {
                $a = array_slice($item, -2);
                return distance($a, $ref);
            }, $items);

            sort($distances);
            $distanceFromRoute = floatval($distances[0]);


            if ($distance_unit == "Meter")
                $distanceFromRoute = ($distanceFromRoute * 1.609344 * 1000);
            if ($distance_unit == "KM")
                $distanceFromRoute = ($distanceFromRoute * 1.609344);

            if ($distanceFromRoute > $distance_value) { //"Device is away from route"
                $checkLast = "SELECT on_route FROM route_out_log WHERE device_id = $assets_id AND trip_id = $trip_id order by id desc limit 1";
                $checkLastRs = mysql_query($checkLast);
                $checkLastRow = mysql_fetch_array($checkLastRs);
                if (!mysql_num_rows($checkLastRs) || $checkLastRow['on_route'] == 1) {
                    $distanceText = number_format($distanceFromRoute, 2) . ' ' . $distance_unit;
                    $ins = "INSERT INTO route_out_log (device_id, trip_id, date_time, lat, lng, distance, on_route) VALUES ($assets_id, $trip_id, '" . gmdate(DATE_TIME) . "', '$lati', '$longi', '$distanceText', 0)";
                    mysql_query($ins);
                    $insert_data = true;
                    $txt = '';
                    if (isset($nick_name) && $nick_name != '')
                        $txt .= $nick_name;
                    if (isset($driver_name) && $driver_name != '') {
                        if ($txt == '') {
                            $txt .= $driver_name;
                        } else {
                            $txt .= ', ' . $driver_name;
                        }
                    }
                    if ($txt != '')
                        $str = '(' . $txt . ') ';
                    else
                        $str = '';
                    $smsText = $lang['Dear'] . " $fname, $assets_name " . $str . '' . $lang[' is not on route'] . " : $distanceText), " . convert_time_zone($ist, $dts, DISP_TIME); //.date(DISP_TIME, strtotime($ist));
                    //sms template
                    //Dear [F1], [F2] ([F3], [F4]) is not on route [F5] (Distance from original route is : [F6])
                    $template_id = '3826';
                    $f1 = $fname;
                    $f2 = $assets_name;
                    $f3 = $nick_name;
                    $f4 = $driver_name;
                    $f5 = $routename;
                    $f6 = $distanceText;
                    $f7 = "," . convert_time_zone($ist, $dts, DISP_TIME); // date(DISP_TIME, strtotime($ist));
                    $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6, "F7" => $f7);

                    if ($mobile != "" && $user_sms_alert == 1 && $route_sms_alert == 1) {
                        send_sms($mobile, $smsText, $template_id, $template_data);
                        sms_log($mobile, $smsText, $user_id);
                    }

                    if ($email != "" && $user_email_alert == 1 && $route_email_alert == 1) {
                        send_email($email, $lang['Route Break Alert'], $smsText);
                        email_log($email, $smsText, $user_id, $lang['Route Break Alert']);
                        //chat_alert($email, $smsText);
                    }
                    //insert in alert master
                    sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                    $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude,alert_address) VALUES ( '" . $lang['Route Break Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . gmdate(DATE_TIME, strtotime($ist)) . "','".$lati."','".$longi."','".$x_address."')";
                    mysql_query($alertSql);
                }
            } else {
                $checkLast = "select on_route from route_out_log where device_id = $assets_id and trip_id = $trip_id order by id desc limit 1";
                $checkLastRs = mysql_query($checkLast);
                $checkLastRow = mysql_fetch_array($checkLastRs);
                if (mysql_num_rows($checkLastRs) > 0 && $checkLastRow['on_route'] == 0) {
                    $distanceText = number_format($distanceFromRoute, 2) . ' ' . $distance_unit;
                    $ins = "INSERT INTO route_out_log (device_id, trip_id, date_time, lat, lng, distance, on_route) VALUES ($assets_id, $trip_id, '" . gmdate(DATE_TIME) . "', '$lati', '$longi', '$distanceText', 1)";
                    mysql_query($ins);
                    $insert_data = true;
                }
            }
        }
    }
    return $insert_data;
}

function ac_report($device, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $ac, $latitude, $longitude, $x_address, $ist, $acAlertFlag, $odomVal) {

    $current = date(DATE_TIME);
    $ac_type = "";
    global $current_area;
    global $current_landmark;
    global $dts;
    global $uRow;
    $sensor2 = "";
    $give_alert = false;
    $MainPower = $ac;
    if ($odomVal == "") {
        $sql = "SELECT km_reading FROM assests_master WHERE id = '" . $assets_id . "'";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) > 0) {
            $row_odo = mysql_fetch_array($rs);
            $odomVal = $row_odo['km_reading'];
        }
    }
    /* $user_sql = "SELECT um.user_id, um.sensor2 FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='".$assets_id."' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

      $user_rs = mysql_query($user_sql) or deadme(mysql_error().":".$user_sql);
      while($row = mysql_fetch_array($user_rs)){
      $user_id = $row['user_id']; */
    //$sensor2 = $row['sensor2'];

    /* if($sensor2 != "" && strpos($sensor2,'Ignition') !== false && $acAlertFlag == 0){
      ////WriteLog("Ac report:ignition:".$sensor2);
      $acAlertFlag = 1;
      ignitionAlert($device, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $MainPower, $latitude, $longitude, addslashes($x_address), $ist, $acAlertFlag);
      //return 1;
      }else{ */
    // //WriteLog("Ac report:ac:".$sensor2);
    ////WriteLog($assets_name . " AC : " . $ac . "  asset id: " . $assets_id);
    $query = "SELECT * FROM tbl_ac_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    $ac_on = "AC ON";
    $ac_off = "AC OFF";
    if ($ac == 1) {
        $ac_type = "AC ON";
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            if ($row['ac_flag'] == 1) {
                $query = "INSERT INTO tbl_ac_report (device_id, ac_on, ac_on_lat, ac_on_lng, add_date) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $latitude . "', '" . $longitude . "', '" . $current . "')";
                mysql_query($query) or deadme(mysql_error() . ":" . $query);
                $query_ac = "SELECT id, ac_status, lat, lng FROM tbl_ac_inout_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
                $res_ac = mysql_query($query_ac) or deadme(mysql_error() . ":" . $query_ac);
                if (mysql_num_rows($res_ac) == 0) {
                    $give_alert = true;
                } else {
                    $row_c = mysql_fetch_array($res_ac);
                    ////WriteLog("ac_status".$row_c['ac_status']);
                    if ($row_c['ac_status'] != $ac_type) {
                        ////WriteLog("ac_status:".$row_c['ac_status']);
                        $give_alert = true;
                    } else {
                        // //WriteLog("in else".$assets_id.$row['ignition_status'].$ignition_type);
                        $give_alert = false;
                    }
                }
                $query_ac = "INSERT INTO tbl_ac_inout_report (device_id, ac_on, lat, lng, address, add_date, ac_status, odometer) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $latitude . "', '" . $longitude . "', '" . addslashes($x_address) . "', '" . $current . "', '" . $ac_on . "', '" . $odomVal . "')";
                mysql_query($query_ac) or deadme(mysql_error() . ":" . $query_ac);
            } else if ($row['ac_flag'] == 0) {
                $row_id = $row['id'];
                $aconstat = $row['ac_on'];
                $start = strtotime($aconstat);
                $end = strtotime($current);
                $delta = $end - $start;
                if ($delta > 5) {

                    $datetime1 = new DateTime($aconstat);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');

                    $query = "UPDATE tbl_ac_report SET  duration = '" . addslashes($formattedDelta) . "' WHERE id = " . $row_id;
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }
            }
        } else if (mysql_num_rows($res) == 0) {


            $query = "INSERT INTO tbl_ac_report (device_id, ac_on, ac_on_lat, ac_on_lng, add_date) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $latitude . "', '" . $longitude . "', '" . $current . "')";
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
            $query_ac = "SELECT id, ac_status, lat, lng FROM tbl_ac_inout_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
            $res_ac = mysql_query($query_ac) or deadme(mysql_error() . ":" . $query_ac);
            if (mysql_num_rows($res_ac) == 0) {
                $give_alert = true;
            } else {
                $row_c = mysql_fetch_array($res_ac);
                // //WriteLog("ac_status".$row_c['ac_status']);
                if ($row_c['ac_status'] != $ac_type) {
                    // //WriteLog("ac_status:".$row_c['ac_status']);
                    $give_alert = true;
                } else {
                    // //WriteLog("in else".$assets_id.$row['ignition_status'].$ignition_type);
                    $give_alert = false;
                }
            }
            $query = "INSERT INTO tbl_ac_inout_report (device_id, ac_on, lat, lng, address, add_date, ac_status, odometer) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $latitude . "', '" . $longitude . "', '" . addslashes($x_address) . "', '" . $current . "', '" . $ac_on . "', '" . $odomVal . "')";
            mysql_query($query) or deadme(mysql_error() . ":" . $query);
        }
    } else {
        $ac_type = "AC OFF";
        if (mysql_num_rows($res) == 1) {
            $row = mysql_fetch_array($res);
            $row_id = $row['id'];
            if ($row['ac_flag'] == 0) {
                $aconstat = $row['ac_on'];
                $start = strtotime($aconstat);
                $end = strtotime($current);
                $delta = $end - $start;
                if ($delta > 5) {
                    $datetime1 = new DateTime($row['ac_on']);
                    $datetime2 = new DateTime($current);
                    $interval = $datetime1->diff($datetime2);
                    $formattedDelta = $interval->format('%H:%I:%S');
                    $query_ac = "SELECT id, ac_status, lat, lng FROM tbl_ac_inout_report WHERE device_id = '$assets_id' ORDER BY id DESC LIMIT 0,1";
                    $res_ac = mysql_query($query_ac) or deadme(mysql_error() . ":" . $query_ac);
                    if (mysql_num_rows($res_ac) == 0) {
                        $give_alert = true;
                    } else {
                        $row_c = mysql_fetch_array($res_ac);
                        ////WriteLog("ac_status".$row_c['ac_status']);
                        if ($row_c['ac_status'] != $ac_type) {
                            ////WriteLog("ac_status:".$row_c['ac_status']);
                            $give_alert = true;
                        } else {
                            // //WriteLog("in else".$assets_id.$row['ignition_status'].$ignition_type);
                            $give_alert = false;
                        }
                        $old_latitude = $row_c['lat'];
                        $old_longitude = $row_c['lng'];
                        if ($old_latitude != "" && $old_longitude != "" && $old_latitude != 0 && $old_longitude != 0) {
                            $theta = $longitude - $old_longitude;
                            $dist = sin(deg2rad($latitude)) * sin(deg2rad($old_latitude)) + cos(deg2rad($latitude)) * cos(deg2rad($old_latitude)) * cos(deg2rad($theta));
                            $dist = acos($dist);
                            $dist = rad2deg($dist);
                            $miles = $dist * 60 * 1.1515 * 1.609344;
                            $distance_by_latlong = round(($miles), 2);
                        }
                    }
                    $query = "UPDATE tbl_ac_report SET ac_off = '" . $current . "', ac_off_lat = '" . $latitude . "', ac_off_lng = '" . $longitude . "', duration='" . addslashes($formattedDelta) . "', ac_flag = 1  WHERE id = " . $row_id;
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);

                    $query = "INSERT INTO tbl_ac_inout_report (device_id, ac_off, lat, lng, address, add_date, ac_status, odometer) VALUES ('" . addslashes($assets_id) . "', '" . $current . "', '" . $latitude . "', '" . $longitude . "', '" . addslashes($x_address) . "', '" . $current . "', '" . $ac_off . "', '" . $odomVal . "')";
                    mysql_query($query) or deadme(mysql_error() . ":" . $query);
                }
            }
        }
    }
    ////WriteLog("ac_type".$ac_type);


    if ($give_alert) {
////WriteLog("give_alert".$give_alert);
        $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.sensor1, um.sensor2, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, sr.ac_on_alert as ac_on_alert1, sr.ac_off_alert as ac_off_alert1 FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

        $rs = mysql_query($us_sql) or deadme(mysql_error() . ":" . $us_sql);
        while ($row = mysql_fetch_array($rs)) {
            $user_id = $row['user_id'];
            $fname = $row['first_name'];
            $mobile = $row['mobile_number'];
            $sensor1 = $row['sensor1'];
            $sensor2 = $row['sensor2'];
            $email = $row['email_address'];
            $user_sms_alert = $row['user_sms_alert'];
            $user_email_alert = $row['user_email_alert'];
            $ac_on_alert1 = $row['ac_on_alert1'];
            $ac_off_alert1 = $row['ac_off_alert1'];
            $language = $row['language'];
            // //WriteLog($fname." ".$language);

            if ($language == "portuguese") {
                $file = "portuguese_alert_lang.php";
            } else {
                $file = "english_alert_lang.php";
            }
            ////WriteLog($ignition_type);
            include($file);

            if ($ac_type == "AC ON") {
                $ac_alert = $ac_on_alert1;
                //$ac_alert_text = 'AC On';
                if($sensor2!=""){
                    $ac_alert_text = $sensor2." On";
                }else{
                    $ac_alert_text = 'AC On';
                }
            } else if ($ac_type == "AC OFF") {
                $ac_alert = $ac_off_alert1;
                //$ac_alert_text = 'AC Off';
                if($sensor2!=""){
                    $ac_alert_text = $sensor2." Off";
                }else{
                    $ac_alert_text = 'AC Off';
                }
            }
            // $srt=$lang['Dear'];
            // $encoded= mb_detect_encoding($str);
            // //echo  $encoded;
            if ($ac_alert == 1) {
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ')';
                else
                    $str = '';
                $smsText = $lang['Dear'] . " $fname, $assets_name $str $ac_alert_text";

                if ($current_landmark != '') {
                    $smsText .= " " . $lang['near Landmark'] . " $current_landmark";
                } else if ($current_area != "") {
                    $smsText .= " " . $lang['in Area'] . " $current_area";
                } else if ($x_address) {
                    $smsText .= " " . $lang['at'] . " $x_address";
                }

                $smsText .= ", " . convert_time_zone($ist, $dts, DISP_TIME); //.date(DISP_TIME, 

                if ($mobile != "" && $user_sms_alert == 1) {
                    send_sms($mobile, $smsText, '', '');
                    //send_sms($mobile, $alert_text);
                    sms_log($mobile, $smsText, $user_id);
                    ////WriteLog("mobile:".$mobile."".$smsText);
                }

                if ($email != "" && $user_email_alert == 1) {

                    send_email($email, $lang['Vehicle'] . " $ac_alert_text " . $lang['Alert'], $smsText);
                    email_log($email, $smsText, $user_id, $lang['Vehicle'] . " $ac_alert_text " . $lang['Alert']);
                    ////WriteLog("email:".$email."".$smsText);
                    // chat_alert($email, $smsText);
                }
                //insert in alert master
                sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ( '" . $ac_alert_text . " " . $lang['Alert'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "')";
                // //echo "<br />$alertSql";
                // //WriteLog($alertSql);  
                mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
            }
        }
    }
    //}
    //}
}

function city_in_out($device_id, $assets_id, $assets_name, $nick_name, $driver_name, $driver_mobile, $longitude_x, $latitude_y, $current_speed, $x_address, $ist, $odomVal='') {
	
	// Line is added by Atul (not in working)
	return true;
	
    global $current_area, $current_area_id, $dts;

    $city = "";
    $insert_data = false;
    $distance_by_latlong = 0;
    $latlng = "$longitude_x,$latitude_y";
    //$strURL = "http://172.217.3.238/maps/api/geocode/xml?latlng=". $latlng ."&sensor=false&region=in"; 
    $strURL = "https://maps.google.com/maps/api/geocode/json?latlng=" . $latlng . "&key=AIzaSyAC8z-hOALCDcVgab9-tst8R3g7r66D7fU&sensor=false&region=in";
    ////WriteLog($strURL);
    $data = @file_get_contents($strURL);
    $jsondata = json_decode($data, true);
    // city
    foreach ($jsondata["results"] as $result) {
        foreach ($result["address_components"] as $address) {
            if (in_array("locality", $address["types"])) {
                $city = $address["long_name"];
            }
        }
    }
    if ($city == "") {
        foreach ($jsondata["results"] as $result) {
            foreach ($result["address_components"] as $address) {
                if (in_array("administrative_area_level_2", $address["types"])) {
                    $city = $address["long_name"];
                }
            }
        }
    }

    /* if($city == ""){
      if($device_id == "001122334455"){
      $arr = explode(",", $x_address);
      $count = count($arr);
      $city = $arr[$count-3];
      }
      } */
    $area_name = $city;
    if ($odomVal == "") {
        $sql = "SELECT km_reading FROM assests_master WHERE id = '" . $assets_id . "'";
        $rs = mysql_query($sql);
        if (mysql_num_rows($rs) > 0) {
            $row_odo = mysql_fetch_array($rs);
            $odomVal = $row_odo['km_reading'];
        }
    }
    // //WriteLog("area_odometer:".$odomVal); 
    // //WriteLog("city_address:".$x_address);
    //$sqlP = "SELECT DISTINCT (am.polyid) AS area_id, am.out_alert, am.in_alert, am.speed_value, am.speed_unit, am.email_alert as email_alert, am.sms_alert as sms_alert, um.user_id, um.first_name, um.username, um.mobile_number, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, (SELECT group_concat(mobile_no) as mobile_no FROM addressbook where find_in_set(id, am.addressbook_ids)) as addressbook_mobile, sr.area_in_ch_alert, sr.area_out_ch_alert FROM areas am LEFT JOIN tbl_users um ON um.user_id = am.Audit_Enter_uid left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='".$assets_id."' WHERE FIND_IN_SET( $assets_id, deviceid ) and am.Audit_Del_Dt is null and am.Audit_Status = 1";
    $sqlP = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert, acm.city_in_alert, acm.city_out_alert FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master acm on acm.assets='" . $assets_id . "' and acm.status=1 WHERE FIND_IN_SET('" . $assets_id . "', lm.assets_ids) and lm.del_date is null and lm.status = 1";

    $rsP = mysql_query($sqlP) or deadme("Failed to Execute, SQL : $sqlP, Error : " . mysql_error());
    while ($rowP = mysql_fetch_array($rsP)) {
        //$area_id 	= $rowP['area_id'];
        $user_id = $rowP['user_id'];
        $fname = $rowP['first_name'];
        $mobile = $rowP['mobile_number'];
        $email = $rowP['email_address'];
        $user_email_alert = $rowP['user_email_alert'];
        $user_sms_alert = $rowP['user_sms_alert'];
        $city_in_alert = $rowP['city_in_alert'];
        $city_out_alert = $rowP['city_out_alert'];
        $language = $rowP['language'];
        if ($language == "portuguese") {
            $file = "portuguese_alert_lang.php";
        } else {
            $file = "english_alert_lang.php";
        }
        include($file);
        $status = 2;
        // if($user_id == 886){
        $sql = "SELECT * FROM city_area_inout_log where area_name = '" . $area_name . "' and device_id = '" . $assets_id . "' order by id desc limit 1";
        $rs = mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
        if (mysql_num_rows($rs) > 0) {
            $row = mysql_fetch_array($rs);
            if ($row['inout_status'] == 'in') {

                if ($row['area_name'] != $area_name) {
                    $status = 1;
                } else {
                    $status = 2;
                }
            } else {
                $status = 0;
            }
            //else
            //$status = 0;
        } else {
            $status = 0;
        }

        //if (is_in_polygon($points_polygon, $vx, $vy, $longitude_x, $latitude_y)){
        // //WriteLog("status:".$status);
        $current_area = $area_name;
        //$current_area_id = $area_id;
        if ($status == 0) {

            /* $sql = "SELECT area_name FROM city_area_inout_log where area_name = '".$area_name."' and inout_status='in' and device_id = '".$assets_id."' order by id desc limit 1";
              $rs = mysql_query($sql) or deadme(mysql_error().":".$sql);
              if(mysql_num_rows($rs) == 0){ */
            $area_name_old = "";
            $sql = "SELECT area_name FROM city_area_inout_log where area_name != '" . $area_name . "' and inout_status='in' and device_id = '" . $assets_id . "' order by id desc limit 1";
            $rs = mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $area_name_old = $row['area_name'];
            }
            ////WriteLog("area_name_old".$area_name_old);
            //$area_name = $area_name_old;
            if ($area_name_old != "") {
                $areaLogSql = "INSERT INTO city_area_inout_log (user_id, device_id, area_name, lat, lng, address, date_time, inout_status, odometer) VALUES ($user_id, '" . $assets_id . "', '" . $area_name_old . "', '" . $longitude_x . "', '" . $latitude_y . "', '" . $x_address . "', '" . date(DATE_TIME) . "', 'out', '" . $odomVal . "')";
                mysql_query($areaLogSql) or deadme(mysql_error() . ":" . $areaLogSql);
                $insert_data = true;
                //current_event_track("Area Out",$assets_id,$device_id, "Area Name : $area_name_old");
                //Dear $fname, 
                $txt = '';
                if (isset($nick_name) && $nick_name != '')
                    $txt .= $nick_name;
                if (isset($driver_name) && $driver_name != '') {
                    if ($txt == '') {
                        $txt .= $driver_name;
                    } else {
                        $txt .= ', ' . $driver_name;
                    }
                }
                if (isset($driver_mobile) && $driver_mobile != '') {
                    if ($txt == '') {
                        $txt .= $driver_mobile;
                    } else {
                        $txt .= ', ' . $driver_mobile;
                    }
                }
                if ($txt != '')
                    $str = '(' . $txt . ') ';
                else
                    $str = '';
                $smsText = "$assets_name " . $str . "" . $lang['is now out of city area'] . " " . $area_name_old . ", " . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME);
                //sms template
                //Dear [F1], [F2] ([F3], [F4]) is now out of area [F5]
                $template_id = '3823';
                $f1 = $fname;
                $f2 = $assets_name;
                $f3 = $nick_name;
                $f4 = $driver_name;
                $f5 = $area_name_old;
                $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
                $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);

                if ($mobile != "" && $user_sms_alert == 1 && $city_out_alert == 1) {
                    send_sms($mobile, $smsText, $template_id, $template_data);
                    sms_log($mobile, $smsText, $user_id);
                }
                if ($city_out_alert == 1 || $city_out_alert == NULL) {
                    sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                }
                if ($email != "" && $user_email_alert == 1 && $city_out_alert == 1) {
                    $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $longitude_x . ',' . $latitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';
                    send_email($email, $lang['Out of City Area alert to User'], $smsText);
                    email_log($email, $smsText, $user_id, 'Out of City Area alert to User');
                    // chat_alert($email, $smsText);
                }

                if ($city_out_alert == 1 || $city_out_alert == NULL) {
                    //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                    //insert in alert master//$longitude_x, $latitude_y
                    $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $lang['Out of City Area alert to User'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude_y."','".$longitude_x."')";
                    mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
                }
            }

            $areaLogSql = "insert into city_area_inout_log (user_id, device_id, area_name, lat, lng, address, date_time, inout_status, odometer) values($user_id, '" . $assets_id . "', '" . $area_name . "', '" . $longitude_x . "', '" . $latitude_y . "', '" . $x_address . "', '" . date(DATE_TIME) . "', 'in', '" . $odomVal . "')";
            mysql_query($areaLogSql) or deadme(mysql_error() . ":" . $areaLogSql);
            $insert_data = true;
            // $status = 1;
            //current_event_track("Area In",$assets_id,$device_id, "Area Name :$area_name");
            $txt = '';
            if (isset($nick_name) && $nick_name != '')
                $txt .= $nick_name;
            if (isset($driver_name) && $driver_name != '') {
                if ($txt == '') {
                    $txt .= $driver_name;
                } else {
                    $txt .= ', ' . $driver_name;
                }
            }
            if (isset($driver_mobile) && $driver_mobile != '') {
                if ($txt == '') {
                    $txt .= $driver_mobile;
                } else {
                    $txt .= ', ' . $driver_mobile;
                }
            }
            if ($txt != '')
                $str = '(' . $txt . ') ';
            else
                $str = '';
            $smsText = $lang['Alert for'] . " $assets_name " . $str . "" . $lang['Entered City Area'] . " " . $area_name . ", " . convert_time_zone($ist, $dts, DISP_TIME); // date(DISP_TIME);
            //sms template
            //Dear  [F1],  [F2] ( [F3],  [F4]) is now in area  [F5]
            $template_id = '3822';
            $f1 = $fname;
            $f2 = $assets_name;
            $f3 = $nick_name;
            $f4 = $driver_name;
            $f5 = $area_name;
            $f6 = "," . convert_time_zone($ist, $dts, DISP_TIME); // .date(DISP_TIME, strtotime($ist));
            $template_data = array("F1" => $f1, "F2" => $f2, "F3" => $f3, "F4" => $f4, "F5" => $f5, "F6" => $f6);

            //

            if ($mobile != "" && $user_sms_alert == 1 && $city_in_alert == 1) {
                send_sms($mobile, $smsText, $template_id, $template_data);
                sms_log($mobile, $smsText, $user_id);
            }
            if ($city_in_alert == 1 || $city_in_alert == NULL) {
                sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
            }
            if ($email != "" && $user_email_alert == 1 && $city_in_alert == 1) {
                // $smsText .= ', Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$longitude_x.','.$latitude_y.'('.$assets_name.')&ie=UTF8&z=12&om=1">here</a> to view on map.';
                $smsText .= ', ' . $lang['Click'] . ' <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $longitude_x . ',' . $latitude_y . '(' . $assets_name . ')&ie=UTF8&z=12&om=1">' . $lang['here'] . '</a>' . $lang['to view on map'] . ' .';

                send_email($email, $lang['In City Area Alert to User'], $smsText);
                email_log($email, $smsText, $user_id, 'In City Area Alert to User');

                // chat_alert($email, $smsText);
            }

            if ($city_in_alert == 1 || $city_in_alert == NULL) {
                // sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                //insert in alert master
                $alertSql = "insert into alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ( '" . $lang['In City Area Alert to User'] . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "')";
                mysql_query($alertSql) or deadme(mysql_error() . ":" . $alertSql);
            }
        }
        //}
    }

    return $insert_data;
}

Function fuel_removal($device, $voltage, $old_voltage, $MainPower, $ac, $address, $assets_name, $assets_id, $latitude, $longitude, $speed, $km_reading, $fuel) {
    $fuel_status = '';
    $query = "Select fuel_status, event_count from fuel_removal_report where asset_id = '" . $assets_id . "' order by id desc limit 1";
    $event_count = 0;
    $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($rs) > 0) {
        $row = mysql_fetch_array($rs);
        $fuel_status = $row['fuel_status'];
        $event_count = $row['event_count'];
    }
    if ($voltage < $old_voltage) {

        if ($fuel_status == 3) {
            $event_count = $event_count + 1;
            $Sql = "insert into fuel_removal_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',1,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);

            $query = "Select add_date,fuel,voltage from fuel_removal_report where asset_id = '" . $assets_id . "' and fuel_status = '3' order by id desc limit 1";
            //  //echo 'here<br>';
            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $old_voltage; // $row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_romved_InVoltage = $start_voltage - $voltage;
                if ($MainPower == 1) {
                    $ignition = 'On';
                } else {
                    $ignition = 'Off';
                }

                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                //  $sql = "UPDATE fuel_removal_report SET total_volt_removed = '".$fuel_romved_InVoltage."' ,total_time = '".$stop_time_in_sec."' WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' "; 
                // mysql_query($sql) or deadme(mysql_error().":".$sql);       



                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert,sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $language = $row['language'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];

                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);

                    //   //echo 'here1<br>';
                    $alert_text = "";
                    $alert_header = "";
                    // //echo $fuel_romved_InVoltage.' '.$fuel_removal_alert_at_voltage.' '.$stop_time_in_sec.' '.$fuel_removal_alert_after_time,'<br>';
                    if (($fuel_romved_InVoltage >= $fuel_removal_alert_at_voltage) && (($stop_time_in_sec >= $fuel_removal_alert_after_time) )) {
                        //  //echo 'here2<br>';
                        $str_time = stop_time_ign($assets_id);
                        $alert_header = 'Fuel Volt Dropped Alert ';
                        $smsText = "Asset name : " . $assets_name . "  Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . " Location name - $address   ";
                        $emailText = "Asset name : " . $assets_name . " Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . "  Location name - " . $address;
                        //$emailText .= ' Google link -  Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude.','.$longitude.'('.$assets_name.')&ie=UTF8&z=12&om=1"> here </a> to view on map. ';
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";

                        if ($email != "" && $user_email_alert == 1 && $fuel_removal_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_removal_alert == 1) {
                            //  //echo 'here3<br>';
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','". $latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql);
                        }
                    }
                }
            }
        }
        if ($fuel_status == 1) {
            $Sql = "insert into fuel_removal_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',2,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);

            $query = "Select add_date,fuel,voltage from fuel_removal_report where asset_id = '" . $assets_id . "' and fuel_status = '1' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_romved_InVoltage = $start_voltage - $voltage;
                if ($MainPower == 1) {
                    $ignition = 'On';
                } else {
                    $ignition = 'Off';
                }

                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                //  $sql = "UPDATE fuel_removal_report SET total_volt_removed = '".$fuel_romved_InVoltage."' ,total_time = '".$stop_time_in_sec."' WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' "; 
                // mysql_query($sql) or deadme(mysql_error().":".$sql);       



                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert,sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $language = $row['language'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];

                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);


                    $alert_text = "";
                    $alert_header = "";

                    if (($fuel_romved_InVoltage >= $fuel_removal_alert_at_voltage) && (($stop_time_in_sec >= $fuel_removal_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $alert_header = 'Fuel Volt Dropped Alert ';
                        $smsText = "Asset name : " . $assets_name . "  Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . " Location name - $address   ";
                        $emailText = "Asset name : " . $assets_name . " Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . "  Location name - " . $address;
                        //$emailText .= ' Google link -  Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude.','.$longitude.'('.$assets_name.')&ie=UTF8&z=12&om=1"> here </a> to view on map. ';
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";

                        if ($email != "" && $user_email_alert == 1 && $fuel_removal_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_removal_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql);
                        }
                    }
                }
            }
        }
        if ($fuel_status == 2) {
            $Sql = "insert into fuel_removal_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',2,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);


            $query = "Select add_date,fuel,voltage from fuel_removal_report where asset_id = '" . $assets_id . "' and fuel_status = '1' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_romved_InVoltage = $start_voltage - $voltage;
                if ($MainPower == 1) {
                    $ignition = 'On';
                } else {
                    $ignition = 'Off';
                }

                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";

                $sql = "UPDATE fuel_removal_report SET total_volt_removed = '" . $fuel_romved_InVoltage . "' ,total_time = '" . $stop_time_in_sec . "' WHERE asset_id = '" . $assets_id . "' and event_count = '" . $event_count . "' ";
                mysql_query($sql) or deadme(mysql_error() . ":" . $sql);



                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert,sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $language = $row['language'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];

                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);


                    $alert_text = "";
                    $alert_header = "";

                    if (($fuel_romved_InVoltage >= $fuel_removal_alert_at_voltage) && (($stop_time_in_sec >= $fuel_removal_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $alert_header = 'Fuel Volt Dropped Alert ';
                        $smsText = "Asset name : " . $assets_name . "  Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . " Location name - $address   ";
                        $emailText = "Asset name : " . $assets_name . " Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . "  Location name - " . $address;
                        //$emailText .= ' Google link -  Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude.','.$longitude.'('.$assets_name.')&ie=UTF8&z=12&om=1"> here </a> to view on map. ';
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";

                        if ($email != "" && $user_email_alert == 1 && $fuel_removal_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_removal_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql);
                        }
                    }
                }
            }
        }
        if ($fuel_status == 0 || $fuel_status == '') {
            $event_count = $event_count + 1;
            $Sql = "insert into fuel_removal_report(asset_id, add_date, fuel , voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',1,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);

            $query = "Select add_date,fuel,voltage from fuel_removal_report where asset_id = '" . $assets_id . "' and fuel_status = '3' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $old_voltage; // $row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_romved_InVoltage = $start_voltage - $voltage;
                if ($MainPower == 1) {
                    $ignition = 'On';
                } else {
                    $ignition = 'Off';
                }

                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                //  $sql = "UPDATE fuel_removal_report SET total_volt_removed = '".$fuel_romved_InVoltage."' ,total_time = '".$stop_time_in_sec."' WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' "; 
                // mysql_query($sql) or deadme(mysql_error().":".$sql);       



                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert,sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $language = $row['language'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];

                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);


                    $alert_text = "";
                    $alert_header = "";

                    if (($fuel_romved_InVoltage >= $fuel_removal_alert_at_voltage) && (($stop_time_in_sec >= $fuel_removal_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $alert_header = 'Fuel Volt Dropped Alert ';
                        $smsText = "Asset name : " . $assets_name . "  Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . " Location name - $address   ";
                        $emailText = "Asset name : " . $assets_name . " Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time Date Time  - " . date(DISP_TIME) . "  Location name - " . $address;
                        //$emailText .= ' Google link -  Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude.','.$longitude.'('.$assets_name.')&ie=UTF8&z=12&om=1"> here </a> to view on map. ';
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";

                        if ($email != "" && $user_email_alert == 1 && $fuel_removal_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_removal_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql);
                        }
                    }
                }
            }
        }
    }
    if ($voltage == $old_voltage) {
        if ($fuel_status == 1) {
            $sql = "UPDATE fuel_removal_report SET add_date = '" . date(DATE_TIME) . "', voltage = '" . $voltage . "' WHERE asset_id = '" . $assets_id . "' ORDER BY id DESC LIMIT 1";
            mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
            ////WriteLog($sql);
            //  //echo $sql;
        }
        if ($fuel_status == 2) {
            $Sql = "insert into fuel_removal_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',3,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);
            /*
              $query = "Select add_date,fuel,voltage from fuel_removal_report where asset_id = '".$assets_id."' and fuel_status = '1' order by id desc limit 1";

              $rs = mysql_query($query) or deadme(mysql_error().":".$query);
              if(mysql_num_rows($rs) > 0){
              $row = mysql_fetch_array($rs);
              $start_date = $row['add_date'];
              $start_fuel = $row['fuel'];
              $start_voltage = $row['voltage'];
              $end_date = date(DATE_TIME);
              $fuel_romved_InVoltage  = $start_voltage - $voltage;
              if($MainPower == 1){
              $ignition = 'On';
              }
              else{
              $ignition = 'Off';
              }

              $to_time = strtotime($end_date);
              $from_time = strtotime($start_date);
              $stop_time_in_sec  = round(abs($to_time - $from_time));
              $time = $stop_time_in_sec;
              //  $stop_time = round(abs($to_time - $from_time) / 60,2);
              $hours = floor($time / (60 * 60));
              $time -= $hours * (60 * 60);

              $minutes = floor($time / 60);
              $time -= $minutes * 60;

              $seconds = floor($time);
              $time -= $seconds;
              $time_str = '';
              if($hours >= 1 )
              {
              $time_str .= $hours.'Hours ';
              }
              if($minutes >= 1){
              $time_str .= $minutes.'Minutes ';
              }
              if($seconds >= 1){
              $time_str .= $seconds.'Seconds ';
              }
              //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";

              $sql = "UPDATE fuel_removal_report SET total_volt_removed = '".$fuel_romved_InVoltage."',total_time = '".$stop_time_in_sec."'  WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' ";
              mysql_query($sql) or deadme(mysql_error().":".$sql);

              $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert,sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='".$assets_id."' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

              $rs = mysql_query($us_sql);
              while($row = mysql_fetch_array($rs)){
              $user_id = $row['user_id'];
              $fname = $row['first_name'];
              $mobile = $row['mobile_number'];
              $email = $row['email_address'];
              $user_sms_alert = $row['user_sms_alert'];
              $user_email_alert = $row['user_email_alert'];
              $ignition_on_alert = $row['ignition_on_alert'];
              $ignition_off_alert = $row['ignition_off_alert'];
              $language = $row['language'];
              $fuel_removal_alert = $row['fuel_removal_alert'];
              $fuel_raised_alert = $row['fuel_raised_alert'];

              $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
              $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
              $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
              $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];

              if($language == "portuguese"){
              $file = "portuguese_alert_lang.php";
              }
              else{
              $file = "english_alert_lang.php";

              }
              include($file);


              $alert_text = "";
              $alert_header = "";

              if(($fuel_romved_InVoltage >= $fuel_removal_alert_at_voltage) &&  (($stop_time_in_sec >= $fuel_removal_alert_after_time))){

              $alert_header = 'Fuel Volt Dropped Alert ';
              $smsText =  "Asset name : ".$assets_name."  Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $time_str  Date Time  - ".date(DISP_TIME)." Location name - $address   ";
              $emailText =  "Asset name : ".$assets_name  ." Fuel Dropped - $fuel_romved_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $time_str  Date Time  - ".date(DISP_TIME)."  Location name - ".$address ;
              //$emailText .= ' Google link -  Click <a href="https://maps.google.com/maps?f=q&hl=en&geocode=&q='.$latitude.','.$longitude.'('.$assets_name.')&ie=UTF8&z=12&om=1"> here </a> to view on map. ';
              $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";

              if($email!="" && $user_email_alert == 1 && $fuel_removal_alert == 1) {
              send_email($email, $alert_header, $emailText);
              email_log($email, $smsText, $user_id, $alert_header);
              // chat_alert($email, $smsText);
              ////WriteLog($emailText);
              }
              //insert in alert master
              //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
              if($fuel_removal_alert == 1 ){
              sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
              $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ( '".$alert_header."', '".$smsText."', '".$lang['Alert']."', '".$user_id."', '".$assets_id."', '".date(DATE_TIME)."')";
              // //echo "<br />$alertSql";
              mysql_query($alertSql);
              }
              }
              }
              } */
        }
    }
}

Function fuel_added($device, $voltage, $old_voltage, $MainPower, $ac, $address, $assets_name, $assets_id, $latitude, $longitude, $speed, $km_reading, $fuel) {


    $fuel_status = '';
    $query = "Select fuel_status,event_count from fuel_added_report where asset_id = '" . $assets_id . "' order by id desc limit 1";
    $event_count = 0;
    $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($rs) > 0) {
        $row = mysql_fetch_array($rs);
        $fuel_status = $row['fuel_status'];
        $event_count = $row['event_count'];
    }
    if ($voltage > $old_voltage) {

        if ($fuel_status == 3) {
            $event_count = $event_count + 1;
            $Sql = "insert into fuel_added_report(asset_id, add_date,fuel, voltage, ignition, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $address . "',1,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);

            $query = "Select add_date,fuel,voltage from fuel_added_report where asset_id = '" . $assets_id . "' and fuel_status = '3' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $old_voltage; //$row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_added_InVoltage = $voltage - $start_voltage;


                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                // $sql = "UPDATE fuel_added_report SET total_volt_added = '".$fuel_added_InVoltage."',total_time = '".$stop_time_in_sec."'  WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' "; 
                // mysql_query($sql) or deadme(mysql_error().":".$sql);       
                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert, sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time  FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql) or deadme(mysql_error(). ":" . $us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $language = $row['language'];
                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);
                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];
                    $alert_header = "";

                    //  //echo $fuel_raised_alert.' '.$fuel_added_InVoltage.' '.$fuel_raised_alert_at_voltage;

                    $alert_header = 'Fuel Volt Raised Alert ';
                    if (($fuel_added_InVoltage >= $fuel_raised_alert_at_voltage) && (($stop_time_in_sec >= $fuel_raised_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $smsText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $MainPower Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        $emailText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $MainPower Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        //$emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";



                        ////WriteLog($email.''.$user_email_alert);
                        /* if($mobile != "" && $user_sms_alert == 1){
                          send_sms($mobile, $smsText, '', '');
                          //send_sms($mobile, $alert_text);
                          sms_log($mobile, $smsText, $user_id);
                          } */

                        if ($email != "" && $user_email_alert == 1 && $fuel_raised_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_raised_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql) or deadme(mysql_error().":".$alertSql);
                        }
                    }
                }
            }
        }
        if ($fuel_status == 1) {
            $Sql = "insert into fuel_added_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',2,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);

            $query = "Select add_date,fuel,voltage from fuel_added_report where asset_id = '" . $assets_id . "' and fuel_status = '1' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $old_voltage;  //$row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_added_InVoltage = $voltage - $start_voltage;


                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                // $sql = "UPDATE fuel_added_report SET total_volt_added = '".$fuel_added_InVoltage."',total_time = '".$stop_time_in_sec."'  WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' "; 
                // mysql_query($sql) or deadme(mysql_error().":".$sql);       
                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert, sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time  FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql) or deadme(mysql_error().":".$us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $language = $row['language'];
                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);
                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];
                    $alert_header = "";

                    //  //echo $fuel_raised_alert.' '.$fuel_added_InVoltage.' '.$fuel_raised_alert_at_voltage;

                    $alert_header = 'Fuel Volt Raised Alert ';
                    if (($fuel_added_InVoltage >= $fuel_raised_alert_at_voltage) && (($stop_time_in_sec >= $fuel_raised_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $smsText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $MainPower Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        $emailText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $MainPower Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        //$emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";



                        ////WriteLog($email.''.$user_email_alert);
                        /* if($mobile != "" && $user_sms_alert == 1){
                          send_sms($mobile, $smsText, '', '');
                          //send_sms($mobile, $alert_text);
                          sms_log($mobile, $smsText, $user_id);
                          } */

                        if ($email != "" && $user_email_alert == 1 && $fuel_raised_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_raised_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql) or deadme(mysql_error().":".$alertSql);
                        }
                    }
                }
            }
        }
        if ($fuel_status == 2) {
            $Sql = "insert into fuel_added_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',2,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);
            /*
              $query = "Select add_date,fuel,voltage from fuel_added_report where asset_id = '".$assets_id."' and fuel_status = '1' order by id desc limit 1";

              $rs = mysql_query($query) or deadme(mysql_error().":".$query);
              if(mysql_num_rows($rs) > 0){
              $row = mysql_fetch_array($rs);
              $start_date = $row['add_date'];
              $start_fuel = $row['fuel'];
              $start_voltage = $row['voltage'];
              $end_date = date(DATE_TIME);
              $fuel_added_InVoltage  = $voltage - $start_voltage;


              $to_time = strtotime($end_date);
              $from_time = strtotime($start_date);
              $stop_time_in_sec  = round(abs($to_time - $from_time));
              $time = $stop_time_in_sec;
              //  $stop_time = round(abs($to_time - $from_time) / 60,2);
              $hours = floor($time / (60 * 60));
              $time -= $hours * (60 * 60);

              $minutes = floor($time / 60);
              $time -= $minutes * 60;

              $seconds = floor($time);
              $time -= $seconds;
              $time_str = '';
              if($hours >= 1 )
              {
              $time_str .= $hours.'Hours ';
              }
              if($minutes >= 1){
              $time_str .= $minutes.'Minutes ';
              }
              if($seconds >= 1){
              $time_str .= $seconds.'Seconds ';
              }
              //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
              $sql = "UPDATE fuel_added_report SET total_volt_added = '".$fuel_added_InVoltage."',total_time = '".$stop_time_in_sec."'  WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' ";
              mysql_query($sql) or deadme(mysql_error().":".$sql);
              $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert, sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time  FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='".$assets_id."' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

              $rs = mysql_query($us_sql) or deadme(mysql_error());
              while($row = mysql_fetch_array($rs)){
              $user_id = $row['user_id'];
              $fname = $row['first_name'];
              $mobile = $row['mobile_number'];
              $email = $row['email_address'];
              $user_sms_alert = $row['user_sms_alert'];
              $user_email_alert = $row['user_email_alert'];
              $ignition_on_alert = $row['ignition_on_alert'];
              $ignition_off_alert = $row['ignition_off_alert'];
              $fuel_removal_alert = $row['fuel_removal_alert'];
              $fuel_raised_alert = $row['fuel_raised_alert'];

              $language = $row['language'];
              if($language == "portuguese"){
              $file = "portuguese_alert_lang.php";
              }
              else{
              $file = "english_alert_lang.php";

              }
              include($file);
              $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
              $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
              $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
              $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];
              $alert_header = "";

              //  //echo $fuel_raised_alert.' '.$fuel_added_InVoltage.' '.$fuel_raised_alert_at_voltage;

              $alert_header = 'Fuel Volt Raised Alert ';
              if(($fuel_added_InVoltage >= $fuel_raised_alert_at_voltage) && (($stop_time_in_sec >= $fuel_raised_alert_after_time) )){
              $smsText =  "Asset name : ".$assets_name."  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $time_str  Date Time  -".date(DISP_TIME)." Location name - $address  ";
              $emailText =  "Asset name : ".$assets_name."  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $time_str  Date Time  -".date(DISP_TIME)." Location name - $address  ";
              //$emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";
              $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";



              ////WriteLog($email.''.$user_email_alert);


              if($email!="" && $user_email_alert == 1 && $fuel_raised_alert == 1) {
              send_email($email, $alert_header, $emailText);
              email_log($email, $smsText, $user_id, $alert_header);
              // chat_alert($email, $smsText);
              ////WriteLog($emailText);
              }
              //insert in alert master
              //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
              if($fuel_raised_alert == 1 ){
              sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
              $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date) values ( '".$alert_header."', '".$smsText."', '".$lang['Alert']."', '".$user_id."', '".$assets_id."', '".date(DATE_TIME)."')";
              // //echo "<br />$alertSql";
              mysql_query($alertSql) or deadme(mysql_error());
              }
              }
              }
              } */
        }
        if ($fuel_status == 0 || $fuel_status == '') {
            $event_count = $event_count + 1;
            $Sql = "insert into fuel_added_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',1,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);
        }
    }
    if ($voltage == $old_voltage) {

        if ($fuel_status == 1) {
            $sql = "UPDATE fuel_added_report SET add_date = '" . date(DATE_TIME) . "', voltage = '" . $voltage . "' WHERE asset_id = '" . $assets_id . "' ORDER BY id DESC LIMIT 1";
            mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
            // //WriteLog($sql);
            $query = "Select add_date,fuel,voltage from fuel_added_report where asset_id = '" . $assets_id . "' and fuel_status = '3' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $old_voltage; //$row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_added_InVoltage = $voltage - $start_voltage;


                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                // $sql = "UPDATE fuel_added_report SET total_volt_added = '".$fuel_added_InVoltage."',total_time = '".$stop_time_in_sec."'  WHERE asset_id = '".$assets_id."' and event_count = '".$event_count."' "; 
                // mysql_query($sql) or deadme(mysql_error().":".$sql);       
                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert, sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time  FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql) or deadme(mysql_error().":".$us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $language = $row['language'];
                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);
                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];
                    $alert_header = "";

                    //  //echo $fuel_raised_alert.' '.$fuel_added_InVoltage.' '.$fuel_raised_alert_at_voltage;

                    $alert_header = 'Fuel Volt Raised Alert ';
                    if (($fuel_added_InVoltage >= $fuel_raised_alert_at_voltage) && (($stop_time_in_sec >= $fuel_raised_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $smsText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $MainPower Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        $emailText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $MainPower Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        //$emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";



                        ////WriteLog($email.''.$user_email_alert);
                        /* if($mobile != "" && $user_sms_alert == 1){
                          send_sms($mobile, $smsText, '', '');
                          //send_sms($mobile, $alert_text);
                          sms_log($mobile, $smsText, $user_id);
                          } */

                        if ($email != "" && $user_email_alert == 1 && $fuel_raised_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_raised_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql) or deadme(mysql_error().":".$alertSql);
                        }
                    }
                }
            }
        }

        if ($fuel_status == 2) {
            $Sql = "insert into fuel_added_report(asset_id, add_date,fuel, voltage, ignition, ac, address,fuel_status,lat,lng,mileage,event_count) values ( '" . $assets_id . "', '" . date(DATE_TIME) . "','" . $fuel . "', '" . $voltage . "', '" . $MainPower . "', '" . $ac . "', '" . $address . "',3,'" . $latitude . "','" . $longitude . "','" . $km_reading . "','" . $event_count . "')";
            mysql_query($Sql) or deadme(mysql_error() . ":" . $Sql);


            $query = "Select add_date,fuel,voltage from fuel_added_report where asset_id = '" . $assets_id . "' and fuel_status = '1' order by id desc limit 1";

            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
            if (mysql_num_rows($rs) > 0) {
                $row = mysql_fetch_array($rs);
                $start_date = $row['add_date'];
                $start_fuel = $row['fuel'];
                $start_voltage = $row['voltage'];
                $end_date = date(DATE_TIME);
                $fuel_added_InVoltage = $voltage - $start_voltage;
                if ($MainPower == 1) {
                    $ignition = 'On';
                } else {
                    $ignition = 'Off';
                }

                $to_time = strtotime($end_date);
                $from_time = strtotime($start_date);
                $stop_time_in_sec = round(abs($to_time - $from_time));
                $time = $stop_time_in_sec;
                //  $stop_time = round(abs($to_time - $from_time) / 60,2);
                $hours = floor($time / (60 * 60));
                $time -= $hours * (60 * 60);

                $minutes = floor($time / 60);
                $time -= $minutes * 60;

                $seconds = floor($time);
                $time -= $seconds;
                $time_str = '';
                if ($hours >= 1) {
                    $time_str .= $hours . 'Hours ';
                }
                if ($minutes >= 1) {
                    $time_str .= $minutes . 'Minutes ';
                }
                if ($seconds >= 1) {
                    $time_str .= $seconds . 'Seconds ';
                }
                //$sms_text = "Asset name : $assets_name  Fuel Dropped - $fuel_romved_InVoltage  Speed - $speed  Ignition - $ignition Stopped since - $stop_time minutes  Location name - $address  Google link -  ";
                $sql = "UPDATE fuel_added_report SET total_volt_added = '" . $fuel_added_InVoltage . "',total_time = '" . $stop_time_in_sec . "'  WHERE asset_id = '" . $assets_id . "' and event_count = '" . $event_count . "' ";
                mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
                $us_sql = "SELECT um.first_name, um.mobile_number, um.user_id, um.language, um.email_address, um.email_alert as user_email_alert, um.sms_alert as user_sms_alert, um.alert_start_time, um.alert_stop_time, um.ignition_on_alert, um.ignition_off_alert,sr.fuel_removal_alert, sr.fuel_raised_alert,sr.fuel_removal_alert_at_voltage,sr.fuel_removal_alert_after_time,sr.fuel_raised_alert_at_voltage,sr.fuel_raised_alert_after_time  FROM user_assets_map lm left join tbl_users um on um.user_id = lm.user_id left join alert_controll_master sr on sr.add_uid=um.user_id and sr.assets='" . $assets_id . "' and sr.status=1 WHERE FIND_IN_SET($assets_id, lm.assets_ids) and lm.del_date is null and lm.status = 1";

                $rs = mysql_query($us_sql) or deadme(mysql_error().":".$us_sql);
                while ($row = mysql_fetch_array($rs)) {
                    $user_id = $row['user_id'];
                    $fname = $row['first_name'];
                    $mobile = $row['mobile_number'];
                    $email = $row['email_address'];
                    $user_sms_alert = $row['user_sms_alert'];
                    $user_email_alert = $row['user_email_alert'];
                    $ignition_on_alert = $row['ignition_on_alert'];
                    $ignition_off_alert = $row['ignition_off_alert'];
                    $fuel_removal_alert = $row['fuel_removal_alert'];
                    $fuel_raised_alert = $row['fuel_raised_alert'];

                    $language = $row['language'];
                    if ($language == "portuguese") {
                        $file = "portuguese_alert_lang.php";
                    } else {
                        $file = "english_alert_lang.php";
                    }
                    include($file);
                    $fuel_removal_alert_at_voltage = $row['fuel_removal_alert_at_voltage'];
                    $fuel_removal_alert_after_time = $row['fuel_removal_alert_after_time'];
                    $fuel_raised_alert_at_voltage = $row['fuel_raised_alert_at_voltage'];
                    $fuel_raised_alert_after_time = $row['fuel_raised_alert_after_time'];
                    $alert_header = "";

                    //  //echo $fuel_raised_alert.' '.$fuel_added_InVoltage.' '.$fuel_raised_alert_at_voltage;

                    $alert_header = 'Fuel Volt Raised Alert ';
                    if (($fuel_added_InVoltage >= $fuel_raised_alert_at_voltage) && (($stop_time_in_sec >= $fuel_raised_alert_after_time) )) {
                        $str_time = stop_time_ign($assets_id);
                        $smsText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        $emailText = "Asset name : " . $assets_name . "  Fuel Volt Raised - $fuel_added_InVoltage Volt Speed - $speed  Ignition - $ignition Stopped since - $str_time  Date Time  -" . date(DISP_TIME) . " Location name - $address  ";
                        //$emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=".$latitude.",".$longitude."(".$assets_name.")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";
                        $emailText .= " Google link - Click <a href='https://maps.google.com/maps?f=q&hl=en&geocode=&q=" . $latitude . "," . $longitude . "(" . $assets_name . ")&ie=UTF8&z=12&om=1'> here </a> to view on map. ";



                        ////WriteLog($email.''.$user_email_alert);
                        /* if($mobile != "" && $user_sms_alert == 1){
                          send_sms($mobile, $smsText, '', '');
                          //send_sms($mobile, $alert_text);
                          sms_log($mobile, $smsText, $user_id);
                          } */

                        if ($email != "" && $user_email_alert == 1 && $fuel_raised_alert == 1) {
                            send_email($email, $alert_header, $emailText);
                            email_log($email, $smsText, $user_id, $alert_header);
                            // chat_alert($email, $smsText);
                            ////WriteLog($emailText);
                        }
                        //insert in alert master
                        //sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                        if ($fuel_raised_alert == 1) {
                            sendMobileNotification($user_id, $smsText,'Alert',$assets_id);
                            $alertSql = "INSERT INTO alert_master(alert_header, alert_msg, alert_type, user_id, assets_id, add_date,latitude,longitude) values ( '" . $alert_header . "', '" . $smsText . "', '" . $lang['Alert'] . "', '" . $user_id . "', '" . $assets_id . "', '" . date(DATE_TIME) . "','".$latitude."','".$longitude."')";
                            // //echo "<br />$alertSql";
                            mysql_query($alertSql) or deadme(mysql_error().":".$alertSql);
                        }
                    }
                }
            }
        }
    }
}

Function delete_fuel_removal($device, $assets_id) {
    $query = "Select fuel_status, event_count from fuel_removal_report where asset_id = '" . $assets_id . "' order by id desc limit 1";

    $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($rs) > 0) {
        $row = mysql_fetch_array($rs);
        $fuel_status = $row['fuel_status'];
        $event_count = $row['event_count'];
        if ($fuel_status != 3) {

            $query = "Delete from fuel_removal_report where asset_id = '" . $assets_id . "' and event_count = '" . $event_count . "'";
            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
        }
    }
}

Function delete_fuel_added($device, $assets_id) {
    $query = "Select fuel_status, event_count from fuel_added_report where asset_id = '" . $assets_id . "' order by id desc limit 1";

    $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($rs) > 0) {
        $row = mysql_fetch_array($rs);
        $fuel_status = $row['fuel_status'];
        $event_count = $row['event_count'];
        if ($fuel_status != 3) {

            $query = "Delete from fuel_added_report where asset_id = '" . $assets_id . "' and event_count = '" . $event_count . "'";
            $rs = mysql_query($query) or deadme(mysql_error() . ":" . $query);
        }
    }
}

function stop_time_ign($assets_id) {
    $sqlGetResult = "SELECT device_id,ignition_off FROM `tbl_stop_report` WHERE device_id='" . $assets_id . "' and ignition_on IS NULL  ORDER BY `tbl_stop_report`.`id` DESC limit 1";
    $rows_result = mysql_query($sqlGetResult);
    $single_row = mysql_fetch_row($rows_result);
    $ros = $single_row[1];
    if ($ros) {
        $minutes = (strtotime(gmdate('Y-m-d H:i:s')) - strtotime($ros)) / 60;
        $d = floor($minutes / 1440);
        $h = floor(($minutes - $d * 1440) / 60);
        $m = $minutes - ($d * 1440) - ($h * 60);
        $stop_time = '';
        if ($d > 0)
            $stop_time .= $d . " Day ";
        if ($h > 0)
            $stop_time .= $h . " Hour ";
        if ($m > 0)
            $stop_time .= intval($m) . " Min";
        return $stop_time;
    }
}

function AlertInsureAndMore($user_id, $assets_id, $date) 
{
	// Added by Atul (need to make cron for the same)
	return true;
	
    if ($user_id) {
        $sql = "SELECT am.nexttotalservice,am.totaloil,am.km_reading,CONCAT(tu.first_name,' ',tu.last_name) as username,am.device_id,am.assets_name,am.add_uid,am.ExpInsuranceDate,am.OilChangeKM,am.NextServiceKM,am.Tyresfields FROM `assests_master` am LEFT JOIN tbl_users tu ON tu.user_id=am.add_uid WHERE am.id='" . $assets_id . "' and am.status=1 limit 0,1";
        $res = mysql_query($sql) or deadme(mysql_error() . ":" . $sql);
        if (mysql_num_rows($res) > 0) {
            $data = mysql_fetch_assoc($res);
            $expdate = strtotime($data['ExpInsuranceDate']);
            $cudate = strtotime(date("Y-m-d H:i:s"));
            if (!empty($expdate)) {
                if ($cudate >= $expdate) {
                    $type = "Expired Insurance Date";
                    $smsText = "In User Name : " . $data['username'] . "<br> Assets Name :" . $data['assets_name'] . "<br/> This vehicle has been expired insurance date.";
                    $sql_i = "INSERT INTO alert_master (id,alert_header,alert_msg,alert_type,user_id,assets_id,add_date)VALUES ('','$type','$smsText','$type','$user_id','$assets_id','$date')";
					$smsTextTM="In User Name : " . $data['username'] . "Assets Name :" . $data['assets_name'] . "This vehicle has been expired insurance date.";                   
                    $res = mysql_query($sql_i) or deadme(mysql_error() . ":" . $sql_i);
                    sendMobileNotification($user_id, $smsTextTM, $type, $assets_id);
                }
            }
            $RuniingKM = round($data['totaloil']);
            $oil_km = round($data['km_reading']);
            $OilChangeKM = $data['OilChangeKM'];
            if ($OilChangeKM !=='') {
                if ($oil_km >= $RuniingKM) {
                    $type = "Engine Oil Change";
                    $smsText = "In User Name : " . $data['username'] . "<br> Assets Name :" . $data['assets_name'] . "<br/> Please Change the Engine Oil <br/>you entered Oil " . $oil . "KM passed";
                    $sql_i = "INSERT INTO alert_master (id,alert_header,alert_msg,alert_type,user_id,assets_id,add_date)VALUES ('','$type','$smsText','$type','$user_id','$assets_id','$date')";
					$smsTextNoti="In User Name : " . $data['username'] . "Assets Name :" . $data['assets_name'] . " Please Change the Engine Oil <br/>you entered Oil " . $oil . " KM passed";
                    $res = mysql_query($sql_i) or deadme(mysql_error() . ":" . $sql_i);
                    sendMobileNotification($user_id, $smsTextNoti, $type, $assets_id);
                }
            }
            $NextService = round($data['nexttotalservice']);
            $NextKM = $data['NextServiceKM'];
            if ($NextKM !=='') {
                if ($oil_km >= $NextService) { 
                    $type = "Next Service is Expired";
                    $smsText = "In User Name : " . $data['username'] . "<br> Assets Name :" . $data['assets_name'] . "<br/> Please Check the Next service " . $NextKM . "KM passed";
                    $sql_i = "INSERT INTO alert_master (id,alert_header,alert_msg,alert_type,user_id,assets_id,add_date)VALUES ('','$type','$smsText','$type','$user_id','$assets_id','$date')";
					$smsTextNT="In User Name : " . $data['username'] . " Assets Name :" . $data['assets_name'] . "Please Check the Next service " . $NextKM . "KM passed";
                    $res = mysql_query($sql_i) or deadme(mysql_error() . ":" . $sql_i);
                    sendMobileNotification($user_id, $smsTextNT, $type, $assets_id);
                }
            }
        }
    }
}

?>
