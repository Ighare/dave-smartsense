<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);  
date_default_timezone_set('Australia/Brisbane');
//echo date('Y-m-d H:i:sP');
$mtime = microtime();
set_time_limit(0);
$log_stamp[0] = time(); 
// Include the file for Database Connection
require_once("../db.php");
require_once("../functions.php");
$input = $_REQUEST['data'];
$input_not=$_REQUEST['data'];
$datainput = explode("{", $input, 2);
$input1 = $datainput[0]; 
//print_r(date(DATE_TIME));
//4700596154,0.0,0.0,0,0.0,0.0,false,{"batteryLevel":100,"distance":0.0,"totalDistance":0.0,"motion":false}
//protocol marged->A19 and EV0W and FS28
list($device, $latitude, $longitude, $datetime, $speedKPH, $headingDeg, $valid,$protocol) = @explode(",", $input1);
$device_dt=date("Y-m-d H:i:s",$datetime/1000);
WriteLog("Device Time: ".$device_dt.' and Timezone as client time:'.date(DATE_TIME));
$device_timestamp=$datetime;

if (trim($input) == '')
    die("Blank Input String '$input'");
$attributes = "{" . $datainput[1];
$obj = json_decode(trim($attributes));
if(empty($obj->{'alarm'})){$alarm ="";}else{$alarm =$obj->{'alarm'};} 
if(empty($obj->{'power'})){$ext_batt_volt ="";}else{$ext_batt_volt =$obj->{'power'};}
if(empty($obj->{'gsm'})){$gsm ="";}else{$gsm =$obj->{'gsm'};}
if(empty($obj->{'index'})){$index ="";}else{$index =$obj->{'index'};}
if(empty($obj->{'odometer'})){$odomVal ="";}else{$odomVal =$obj->{'odometer'};}
if(empty($obj->{'status'})){$status ="";}else{$status =$obj->{'status'};}
if(empty($obj->{'ignition'})){$ignition ="";}else{$ignition =$obj->{'ignition'};}
if(empty($obj->{'charge'})){$charge ="";}else{$charge =$obj->{'charge'};}
if(empty($obj->{'cid'})){$cid ="";}else{$cid =$obj->{'cid'};}
if(empty($obj->{'sat'})){$sat ="";}else{$sat =$obj->{'sat'};}
if(empty($obj->{'type'})){$type ="";}else{$type =$obj->{'type'};}
if(empty($obj->{'steps'})){$steps ="";}else{$steps =$obj->{'steps'};}
//added new as per protocol
if(empty($obj->{'motion'})){$motion ="";}else{$motion =$obj->{'motion'};}
if(empty($obj->{'batteryLevel'})){$batteryLevel ="";}else{$batteryLevel =$obj->{'batteryLevel'};}
if(empty($obj->{'distance'})){$distance ="";}else{$distance =$obj->{'distance'};}
if(!isset($batteryLevel)) $batteryLevel="0";
if(!isset($distance)) $distance="";
if(!isset($motion)) $motion="";
$speedKPH = $speedKPH * 1.609344;
$speedKPH=round($speedKPH,2);
$count = CheckDeviceID($device,$conn);
$asset_id=$count[0];
//var_dump($asset_id);
if($count[1] <= 0)
{	
mysqli_close($conn); 
WriteLog(die("Device not found"));
}
if(abs($latitude)==0 or abs($longitude)==0){
	WriteLog(' [die] Wrong Latlong recode_file: ' . $latitude.','.$longitude);
	die("lat long");
	mysqli_close($conn); 
} 
$query = "SELECT td.id as asset_id,td.device_name,td.add_uid,td.timezone,tll.id,tll.device_id,tll.location_time,tll.address,tll.latitude,tll.longitude,tll.location_type,tll.device_speed,tll.device_bettery,tll.add_date,tll.device_timestamp  FROM `tbl_devices` td LEFT JOIN tbl_last_locations tll ON tll.device_id=td.device_id WHERE tll.asset_id= '" . addslashes($asset_id) . "'";
$result = mysqli_query($conn,$query);
$arr_row = mysqli_fetch_array($result);
$id_last=$arr_row['id']; 
$device_id_last=$arr_row['device_id'];
$device_name=$arr_row['device_name'];
$user_id=$arr_row['add_uid'];
//$asset_id=$arr_row['asset_id'];
$location_time_last=$arr_row['location_time'];
$address_last=$arr_row['address'];
$latitude_last=$arr_row['latitude'];
$longitude_last=$arr_row['longitude'];
$location_type_last=$arr_row['location_type'];
$device_speed_last=$arr_row['device_speed'];
$device_bettery_last=$arr_row['device_bettery'];
$add_date_last=$arr_row['add_date'];
$device_timestamp_last=$arr_row['device_timestamp'];
$device_timezone=$arr_row['timezone'];
WriteLog("device time:".gmdate('Y-m-d H:i:s', strtotime($device_dt))); 
$device_dt=gmdate('Y-m-d H:i:s', strtotime($device_dt));
//var_dump($device_dt);
if($device_timezone !="" && $device_timezone !="None"){	
$dt=explode(":",$device_timezone);
$device_dt = date('Y-m-d H:i:s',strtotime(''.$dt[0].' hour '.$dt[1].' minutes',strtotime($device_dt)));
}   
//var_dump($device_dt);exit; 
log_raw_data($asset_id,$device,$device_dt,$input,$conn);

 
WriteLog('Data ='.$input);  
if($protocol=="minifinder"){
if($type=="D"){ 
	$type="GPS";
}
}elseif($protocol=="minifinder2"){
	$type="GPS";
}else if($protocol=="watch"){
	$type="GPS";
}
/*if($steps==0){
	$steps="SOS";	
}*/ 
if($batteryLevel==""){
	$batteryLevel=0;
}
if($latitude_last == $latitude  && $longitude_last == $longitude ){
	$x_address = $address_last;
}else{
$x_address = googleGeocode($latitude, $longitude,$conn);
}
$address=$x_address;
$device_users=date_create($device_dt);
$device_users=date_format($device_users,"d F Y,H:i:s");
if($alarm=="lowBattery"){
	//Battery Low	
	$steps="lowBattery";
	//$smsText1="The vehicle $device_name bettery is low";
	$smsText1="The Device has $device_name Low Battery at $address. Date time $device_users.";
	sendMobileNotification($conn,$user_id, $smsText1, $asset_id="");
}
if($alarm=="sos"){
	//SOS Alert	
	$steps="SOS";
	$smsText1="The device $device_name pressed SOS at $address. Date time $device_users";
	sendMobileNotification($conn,$user_id, $smsText1, $asset_id="");
}
if($alarm=="fallDown"){ 
	//fall alertsss
	$steps="fallDown";
	$smsText1="The device $device_name fall down at $address. Date time $device_users";
	sendMobileNotification($conn,$user_id, $smsText1, $asset_id="");
} 
if($latitude_last == $latitude  && $longitude_last == $longitude ){	
$update_query="UPDATE `tbl_last_locations` SET `asset_id`='".$asset_id."', `location_time`='".$device_dt."',`address`='".$address."',`latitude`='".$latitude."',`longitude`='".$longitude."',`location_type`='".$type."',`device_speed`='".$speedKPH."',`device_bettery`='".$batteryLevel."',`device_timestamp`='".$device_timestamp."' WHERE asset_id='".$asset_id."'";
mysqli_query($conn,$update_query);  
die("duplicate lat long from device");
}

if (mysqli_num_rows($result) > 0){
$update_query="UPDATE `tbl_last_locations` SET `asset_id`='".$asset_id."',`location_time`='".$device_dt."',`address`='".$address."',`latitude`='".$latitude."',`longitude`='".$longitude."',`location_type`='".$type."',`device_speed`='".$speedKPH."',`device_bettery`='".$batteryLevel."',`device_timestamp`='".$device_timestamp."',`event`='".$steps."' WHERE asset_id='".$asset_id."'";
//print_r($update_query);
//print_r('affected row: '.mysqli_affected_rows($conn));
mysqli_query($conn,$update_query);
}else{
$insert_last="INSERT INTO `tbl_last_locations`(`asset_id`,`device_id`, `location_time`, `address`, `latitude`, `longitude`, `location_type`, `device_speed`, `device_bettery`, `add_date`,`device_timestamp`) VALUES ('".$asset_id."','".$device."','".$device_dt."','".$address."','".$latitude."','".$longitude."','".$type."','".$speedKPH."','".$batteryLevel."','".$device_dt."','".$device_timestamp."')";
//print_r($insert_last);
mysqli_query($conn,$insert_last); 
}
$insert_q="INSERT INTO `tbl_location_history`(`device_timestamp`,`device_id`, `location_time`, `address`, `latitude`, `longitude`, `location_type`, `device_speed`, `device_bettery`, `add_date`, `event`,`asset_id`) VALUES ('".$device_timestamp."','".$device."','".$device_dt."','".$address."','".$latitude."','".$longitude."','".$type."','".$speedKPH."','".$batteryLevel."','".$device_dt."','".$steps."','".$asset_id."')";
mysqli_query($conn,$insert_q);
check_inout_geofence_alert($conn,$asset_id,$device_name,$device_dt,$address,$latitude,$longitude); 	

mysqli_close($conn);
WriteLog(die("Data Received"));

?>