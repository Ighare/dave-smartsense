<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller { 		
	function __construct(){
       parent::__construct();
	   //$this->load->library('messages');
	    $this->load->helper(array('url', 'form'));
    }
	function index(){ 
		$location=isset($_REQUEST['location'])?$_REQUEST['location']:"";
		$data=array();
		$data['token']=$this->session->userdata('token');
		$data['asset_id']=$location;
		if(isset($data['asset_id']) && $data['asset_id'] !==""){
			$this->db->select("device_id,device_model");
			$this->db->from("tbl_devices");
			$this->db->where("id",$data['asset_id']);
			$this->db->where("id",$data['asset_id']);
			$sqls=' device_model NOT IN ("Montville 4","Mons","Highworth")';
			$this->db->where($sqls);
			$results=$this->db->get();
			if($results->num_rows() > 0){
			$results=$results->row();
			$device_model=$results->device_model;
			$device_id=$results->device_id;
			$data['locate_button']="0";
			}else{
				$device_model="";
				$device_id=""; 
				$data['locate_button']="1";
			}
		}
		//print_r($data);
		
		$_GET['map']=isset($_GET['map'])?$_GET['map']:"";
		$data['map']=$_GET['map'];
		$data['device_id']=$device_id;
		$data['device_model']=$device_model;
		if ($this->session->userdata('token') != "") {
			if($_GET['map']==true){
			$this->load->view('header',$data);
			$this->load->view('home',$data);
			$this->load->view('footer',$data);
			}else{
			 redirect('device_list');
			}
			
        } else {
            redirect('Sessions');
        }
	}
  
    }
    