<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
class Sessions extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('session'));
        $this->load->model('Api_model');      
        $this->load->database();
        $this->load->helper('url');
		$this->load->helper('cookie');	
    }

    public function index()
    {	
		//$this->load->helper('cookie');
		 echo $this->session->userdata('token');
        $this->load->helper(array('url', 'form'));
        if ($this->session->userdata('token') != "") {
            redirect('home');
        } else {
            redirect('sessions/login');
        }
    }
	public function login(){
		$data=array();
		$this->load->model('Api_model'); 
		
		
		if($_POST){
		$username=$_POST['username'];
		$password=$_POST['password'];
		$check_user=$this->Api_model->valid_user($username,$password);
		
		//Added by Ramesh. 21-11-2019.
		/*$check_user = json_decode($check_user);
		if($check_user->result === "true"){
			$login_data = $check_user->data;
			$data['user_id'] = $login_data->user_id;
			$data['token'] = $login_data->token;
			$data['user_type'] = $login_data->user_type;
			$data['is_superadmin'] = $login_data->is_superadmin;
			$data['devices_count'] = $login_data->devices_count;
			$data['assigned_devices'] = $login_data->assigned_devices;
			$data['assigned_names'] = $login_data->assigned_names;
			
		}else{
			
		}*/ 
		//End. 21-11-2019.
		
		
		if($check_user !=0){
		$data['user_id']=$check_user['user_id'];
		$data['token']=$check_user['token'];            
		$data['user_type']=$check_user['user_type'];
		$data['is_superadmin']=$check_user['is_superadmin'];
		$data['devices_count']=sizeof(explode(',',$check_user['devices_list']));
		$data['assigned_devices']=$check_user['devices_list'];
		$timezone = $this->input->post('timezone');
		$this->session->set_userdata('user_id',$data['user_id']);
		$this->session->set_userdata('token',$data['token']);
		$this->session->set_userdata('user_type',$data['user_type']);
		$this->session->set_userdata('is_superadmin',$data['is_superadmin']);
		$this->session->set_userdata('devices_count',$data['devices_count']);
		$this->session->set_userdata('assigned_devices',$data['assigned_devices']);
		$this->session->userdata('token');
		
		//$this->load->helper('cookie');
		
			if(!empty($_POST["remember"])) {
				
				//setcookie("member_login",$_POST["username"],time()+ (10 * 365 * 24 * 60 * 60));
				set_cookie('member_login',$_POST["username"],time()+ (10 * 365 * 24 * 60 * 60));
				
				//setcookie("member_login_p",$_POST["password"],time()+ (10 * 365 * 24 * 60 * 60));
				set_cookie('member_login_p',$_POST["password"],time()+ (10 * 365 * 24 * 60 * 60));
				
			} else {
				if(isset($_COOKIE["member_login"])) {
					setcookie("member_login","");
					set_cookie('member_login',"");
					//setcookie("member_login_p","");
					set_cookie('member_login_p',"");
				}
			}
		
		redirect('home');
		}else{
		$data["result"]="false";
		$data["msg"]="Username or password incorrect!";
		$this->load->view('login',$data);		
		}
		}else{
			$this->load->view('login');
		}
	}
	function logout(){
		$this->load->helper('url');
		$this->session->sess_destroy();
		redirect('sessions/login');
	}
	
}
