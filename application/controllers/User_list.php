<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
class User_list extends CI_Controller {
    public function __construct() {
        parent::__construct();       
        
        $this->load->library(array('session'));
        $this->load->model('User_list_model');
        $this->load->model('Curd_model');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('form_validation');
		if ($this->session->userdata('token') == "") { 
            redirect('Sessions');
        }
    }
    public function index() {
        $this->load->view("user/list");
    }
    public function loaddata() {
        $user_id = $this->check_token();
        $token = $this->session->userdata("token");
        $list = $this->User_list_model->getmemberList($user_id);
        $count = $this->User_list_model->countall($user_id);
        $data = array();
		$i=1;
		
		//print_r($list);
		//die;
        foreach ($list as $user) {
            $row = array();
			//$row[] = $i;
            $row[] = '<input id="b_id_' . $user->id . '" name="b_id[]" class="text-nowrap checkbox"  value="' . $user->id . '" type="checkbox">';
			
            $row[] = $user->user_name;
            $row[] = $user->user_email;
			$row[] = $user->login_username;
			$row[] = $user->user_password_read;
			if($user->user_type==1){ 
			$row[] = $user->assign_devices_name="";
			}else{
			$row[] = $user->assign_devices_name;
			}
			
			$row[] = $user->sim_account;
			$row[] = $user->sim_password;
			$row[] = $user->sim_number;
			$row[] = $user->user_type == 1 ? "Admin" : "User";
			$row[] = $user->user_contact;
			$row[] = $user->user_note;
			
			
			if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') {
				$action = '<button class="btn btn-primary" type="button" onclick="edit('.$user->id.')">Edit</button> ';
				$action .= ' <button class="btn btn-danger" type="button" onclick="del('.$user->id.')">Delete</button>';
			} else { 
				$action = '<button class="btn btn-primary" type="button" onclick="edit('.$user->id.')">Edit</button> ';
			}
			//$row[] = $action;
            
			$data[] = $row;
			$i++;
        }
        $output = array("draw" => $_POST['draw'], "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data,);
        //output to json format
        echo json_encode($output);
    }
    public function check_token() {
        $token = $this->session->userdata("token");
        $this->db->select('tbl_user.id,tbl_user.user_type,is_superadmin');
        $this->db->from('tbl_user');
        $this->db->join('tc_user_token', 'tc_user_token.user_id=tbl_user.id');
        $this->db->where('tc_user_token.token', $token);
        $this->db->where("tbl_user.status", "1");
        $this->db->limit(1);
        $user_data = $this->db->get();
        if ($user_data->num_rows() > 0) {
            $user_id = 0;
            foreach ($user_data->result_array() as $value) {
                $this->user_type = $value['is_superadmin'];
                return $value['id'];
            }
        }
    }
    public function add() {
		$this->load->view("user/add");
    }
    public function save() {
        $user_id = $this->check_token();
        $this->db->select("id");
        $this->db->from("tbl_user");
        $this->db->where("login_username", $this->input->get('login_username'));
        $checkusers = $this->db->get();
        if ($checkusers->num_rows() > 0) {
            exit;
        }
        $member = array('login_username' => $this->input->post('login_username'), 'user_email' => $this->input->post('user_email'), 'user_password' => md5($this->input->post('user_password')), 'user_password_read' => $this->input->post('user_password'), 'user_name' => $this->input->post('user_name'), 'user_contact' => $this->input->post('user_contact'), 'user_note' => $this->input->post('user_note'), 'user_type' => $this->input->post('user_type'),
        //'devices_list'=>$device_assign,
        'user_status' => $this->input->post('user_type'), 'sim_number' => $this->input->post('sim_number'), 'sim_account' => $this->input->post('sim_account'), 'sim_password' => $this->input->post('sim_password'), 'add_uid' => $user_id, 'parent_id' => $user_id, 'add_date' => gmdate("Y-m-d H:i:m"),);
        $this->db->insert('tbl_user', $member);
        $insert_id = $this->db->insert_id();
        $data['user_id'] = $insert_id;
    }
    public function get() {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function edit($id) {
        $data = array();
        //$data['users'] = $this->User_list_model->get($id);
		
		$operation_result = $this->Curd_model->do_operation(array('user_id'=>$id),'get_user_data');
		$data['users'] = $this->Curd_model->decode_json($operation_result);
        $data['id'] = $id;
		$this->load->view('user/edit', $data);
    }
    public function update() {
        $user_id = $this->check_token();
        $data = array('login_username' => $this->input->post('login_username'), 'user_email' => $this->input->post('user_email'), 'user_password' => md5($this->input->post('user_password')), 'user_password_read' => $this->input->post('user_password'), 'user_name' => $this->input->post('user_name'), 'user_contact' => $this->input->post('user_contact'), 'user_note' => $this->input->post('user_note'), 'user_type' => $this->input->post('user_type'), 'sim_number' => $this->input->post('sim_number'), 'sim_account' => $this->input->post('sim_account'), 'sim_password' => $this->input->post('sim_password'), 'modified_date' => gmdate("Y-m-d H:i:m"),);
        $this->User_list_model->update(array('id' => $this->input->post('id')), $data);
    }
    public function delete() {
        // POST values
        $user_ids = $this->input->post('user_ids');
        // Delete records
        $this->User_list_model->delete($user_ids);
    }
}
