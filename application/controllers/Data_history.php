<?php
class Data_history extends CI_Controller {
    public function __construct() {
        parent::__construct();        
        $this->load->model('Device_history_model');
        $this->load->model('Curd_model');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('form_validation');
		//var_dump($this->session->userdata('token'));exit;
		if ($this->session->userdata('token') == "") { 
            redirect('Sessions');
        }
    }
    public function index() {
		$user_id = $this->check_token();
		$device_op = $this->Device_history_model->get_device_list($user_id);
		$option ="<option value=''>Please Select</option>";
		foreach ($device_op as $device_value){
			$option .="<option value=".$device_value->id.">".$device_value->device_name."</option>";			
		}
		$data=array();
		$data['device_option']=$option;
        $this->load->view("data_history/list",$data);
    }
    public function loaddata() {
        $user_id = $this->check_token();
        $token = $this->session->userdata("token");
        $usertype = $this->session->userdata("user_type");
		$start_date= isset($_POST['start_date']) ? $_POST['start_date'] : "";
        $end_date= isset($_POST['end_date']) ? $_POST['end_date'] : "";
        $device_id= isset($_POST['device_id']) ? $_POST['device_id'] : "";		
        $list = $this->Device_history_model->getdeviceList($user_id,$device_id,$start_date,$end_date);		
        $count = $this->Device_history_model->countall($user_id,$device_id,$start_date,$end_date);
        $data = array();
		$i = 1;
		foreach ($list as $device) {
			$row = array();				
			//$row[] = $i;
			$row[] = date("d-m-Y H:i:s",strtotime($device->add_date));
			//$row[] = $device->device_name;			
			$row[] = $device->log_data; 				
			$i++;
            $data[] = $row;
        }		
		if(isset($_POST['order']['0']['column']) && $_POST['order']['0']['column']==3){
			$dir_ords=$_POST['order']['0']['dir'];
			if($dir_ords=="desc"){
			usort($data, function($a, $b) {
			return $a['3'] <=> $b['3'];
			});
			}else{ 
			usort($data, function($a, $b) {
			return $b['3'] <=> $a['3'];
			});	
			}
		}		
        $output = array("draw" => $_POST['draw'], "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data,);     
        echo json_encode($output);
    }
    public function check_token() {
        $token = $this->session->userdata("token");
        $this->db->select('tbl_user.id,tbl_user.user_type,is_superadmin');
        $this->db->from('tbl_user');
        $this->db->join('tc_user_token', 'tc_user_token.user_id=tbl_user.id');
        $this->db->where('tc_user_token.token', $token);
        $this->db->where("tbl_user.status", "1");
        $this->db->limit(1);
        $user_data = $this->db->get();
        if ($user_data->num_rows() > 0) {
            $user_id = 0;
            foreach ($user_data->result_array() as $value) {
                $this->user_type = $value['is_superadmin'];
                return $value['id'];
            }
        }
    }
    public function add() {
        $this->load->view("device/add");
    }
    public function __save(){
        //$user_id = $this->check_token();
		$user_id = $this->session->userdata('user_id');
        $assigned_users = $this->input->post('assigned_users') ? $this->input->post('assigned_users') : '';
        $data = array(
			'device_name' => $this->input->post('device_name'),
			'device_id' => $this->input->post('device_id'),
			'add_date' => gmdate("Y-m-d H:i:m"),
			'device_model' => $this->input->post('device_model'),
			'authorised_numbers' => $this->input->post('authorised_numbers'),
			'add_uid' => $user_id,
			'sim_number' => $this->input->post('sim_number'),
			'sim_account' => $this->input->post('sim_account'),
			'sim_password' => $this->input->post('sim_password'),
			'device_description' => $this->input->post('device_description'),
			'timezone' => $this->input->post('timezone'),
			'assigned_users' =>$this->input->post('assigned_users')
		);
		$operation_result = $this->Curd_model->do_operation($data,'add_device');
		//$result = $this->Curd_model->decode_json($operation_result);
		echo $operation_result;
		die;
		
		/*Commented by Ramesh.
        $this->db->insert('tbl_devices', $data);
        $insert_id = $this->db->insert_id($data);
        //$user_id = $data['add_uid'];
        if (!empty($insert_id)) {
            $this->db->select('*');
            $this->db->from("tbl_user");
            if ($assigned_users != "") {
                $this->db->where("( id in(" . $assigned_users . " ) or id='" . $user_id . "' ) and status='1' ");
            } else {
                $this->db->where(" id='" . $user_id . "' and status='1' ");
            }
            $query = $this->db->get();
            //echo $this->db->last_query();
            foreach ($query->result() as $row) {
                if ($row->devices_list != "") {
                    $temp_as = explode(',', $row->devices_list);
                    if (!in_array($insert_id, $temp_as)) {
                        $this->db->where('id', $row->id);
                        $this->db->set('devices_list', 'CONCAT(devices_list,\',' . $insert_id . '\')', FALSE);
                        $this->db->update('tbl_user');
                    }
                } else {
                    $this->db->where('id', $row->id);
                    $this->db->set('devices_list', $insert_id);
                    $this->db->update('tbl_user');
                }
            }
        }
		*/
    }
    public function getdeviceid() {
		
		$id = $this->input->post('id');
        $result_html = '';
        $result_set = $this->Device_history_model->getuser($id);
        foreach ($result_set as $result) {
            $result_html.= '<option value="' . $result->id . '">' . $result->user_name . '</option>';
        }
		
        echo json_encode($result_html);
    }
    public function edit($id) {
        $data = array();
		//$data['device'] = $this->Device_history_model->get($id);
		$operation_result = $this->Curd_model->do_operation(array('asset_id'=>$id),'get_device_data');
		$data['device'] = $this->Curd_model->decode_json($operation_result);
		//print_r($data);
		$this->load->view('device/edit', $data);
    }
    public function update() {
        $id = $this->input->post('id');
        $assigned_users = $this->input->post('assigned_users') ? $this->input->post('assigned_users') : '';
        $user_id = $this->check_token();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $data = array('device_name' => $this->input->post('device_name'), 'device_id' => $this->input->post('device_id'), 'modified_date' => gmdate("Y-m-d H:i:m"), 'device_model' => $this->input->post('device_model'), 'authorised_numbers' => $this->input->post('authorised_numbers'), 'add_uid' => $user_id, 'sim_number' => $this->input->post('sim_number'), 'sim_account' => $this->input->post('sim_account'), 'sim_password' => $this->input->post('sim_password'), 'device_description' => $this->input->post('device_description'),);
        $this->Device_history_model->update(array('id' => $this->input->post('id')), $data);
        $insert_id = $id;
        if (!empty($insert_id)) {
            $this->db->select('*');
            $this->db->from("tbl_user");
            $this->db->where("(  find_in_set(" . $id . ",devices_list ) ) and status='1' and user_type='2' ");
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                if ($row->devices_list != "") {
                    $temp_as = explode(',', $row->devices_list);
                    foreach ($temp_as as $key => $value) {
                        if ($value == $id) unset($temp_as[$key]);
                        //echo $value;
                        
                    }
                    $temp_as = implode(',', $temp_as);
                    $this->db->where('id', $row->id);
                    $this->db->set('devices_list', $temp_as, TRUE);
                    $this->db->update('tbl_user');
                }
                // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
                
            }
            $assigned_users = '';
            $this->db->select('*');
            $this->db->from("tbl_user");
            if ($assigned_users != "") {
                $this->db->where("( id in(" . $assigned_users . " ) or id='" . $user_id . "' ) and status='1' ");
            } else {
                $this->db->where(" id='" . $user_id . "' and status='1' ");
            }
            //$this->db->where("(  id in(".$assigned_users." ) ) and status='1' " );
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                if ($row->devices_list != "") {
                    $temp_as = explode(',', $row->devices_list);
                    if (!in_array($insert_id, $temp_as)) {
                        $this->db->where('id', $row->id);
                        $this->db->set('devices_list', 'CONCAT(devices_list,\',' . $insert_id . '\')', FALSE);
                        $this->db->update('tbl_user');
                    }
                } else {
                    $this->db->where('id', $row->id);
                    $this->db->set('devices_list', $insert_id);
                    $this->db->update('tbl_user');
                }
                // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
                
            }
        } else {
            $this->db->where('id', $id);
            $this->db->update('tbl_user', array('devices_list' => $id));
        }
    }
    /* public function delete($id)
    {
      
        $this->Device_history_model->delete($id);
       echo "Records deleted Successfully";
    
    }*/
    public function delete() {
        $user_id = $this->check_token();
        // POST values
        $user_ids = $this->input->post('user_ids');
         
        $device_id = implode(", ", $user_ids);        // Delete records
        $this->Device_history_model->delete($user_ids);
        
       $this->db->select('*');
        $this->db->from("tbl_user");
        $this->db->where(" find_in_set(".$device_id.",devices_list) and status='1' " );

        $query = $this->db->get();
        foreach ($query->result() as $row){ 
        if ($row->devices_list != ""){ 
        $temp_as = explode(',', $row->devices_list);
        foreach ($temp_as as $key => $value) {
        if($value==$device_id)  unset($temp_as[$key]);
        //echo $value;
        }

        // print_r($temp_as);die();
        $temp_as = implode(',', $temp_as);                   
        $this->db->where('id',$row->id);
        $this->db->set('devices_list', $temp_as);
        $this->db->update('tbl_user');
        } 
    }

}
    public function unassigned(){
        $user_id= $this->check_token();
        $asset_id=$this->input->post('id');
        
        if(!empty($user_id)){
                $this->db->select('*');
                $this->db->from("tbl_user");
                $this->db->where("id",$user_id);
                $this->db->where("(  find_in_set(".$asset_id.",devices_list ) ) and status='1'" );
                $query = $this->db->get();
				//echo $this->db->last_query();
                foreach ($query->result() as $row){  
                      if ($row->devices_list != ""){ 
                        $temp_as = explode(',', $row->devices_list);
                        foreach ($temp_as as $key => $value) { 
                            if($value==$asset_id)  unset($temp_as[$key]);
                            //echo $value;
                        }
                        $temp_as = implode(',', $temp_as);                   
                        $this->db->where('id',$row->id); 
                        $this->db->set('devices_list', $temp_as, TRUE);
                        $this->db->update('tbl_user'); 
                    }  
                }
            }

       echo "user removed";
    }
}
