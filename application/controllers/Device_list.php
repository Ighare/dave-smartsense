<?php

class Device_list extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Device_list_model');
        $this->load->model('Curd_model');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('form_validation');
        //var_dump($this->session->userdata('token'));exit;
        if ($this->session->userdata('token') == "") {
            redirect('Sessions');
        }
    }

    public function index() {

        $this->load->view("device/list");
    }

    public function loaddata() {
        $user_id = $this->check_token();
        $token = $this->session->userdata("token");
        $usertype = $this->session->userdata("user_type");
        $list = $this->Device_list_model->getdeviceList($user_id);
        $count = $this->Device_list_model->countall($user_id);
        $data = array();
        $i = 1;

        //print_r($list);
        //die();
        foreach ($list as $device) {
            $row = array();

            if ($usertype == 2) {
                $row[] = '<input id="b_id_' . $device->id . '" name="b_id[]" class="text-nowrap checkbox remove_' . $i . '" data-info="' . $i . '"  value="' . $device->id . '" type="checkbox">';
                //$row[] = $i;
                if ($device->expiry_date !== null && $device->expiry_date !== "" && $device->expiry_date !== '0000-00-00') {
                    $expiry_date = date('d-m-Y', strtotime($device->expiry_date));
                } else {
                    $expiry_date = "";
                }

                $row[] = $device->device_name;
                $row[] = $device->device_id;
                $row[] = $device->assign_users_name;
                $row[] = $device->device_model;
                $row[] = $device->sim_number;
                $row[] = $device->sim_account;
                $row[] = $device->sim_password;
                $row[] = $expiry_date;
                $row[] = $device->emergency_number;
                $row[] = "HH";
                $row[] = $i;
                $row[] = $device->id;
                //$row[] = $device->authorised_numbers;
                //$row[] = '<button class="btn btn-primary" type="button" onclick="edit('.$device->id.')">Edit</button>';
            } else {
                //$row[] = '<input id="b_id_' . $device->id . '" name="b_id[]" class="text-nowrap checkbox"  value="' . $device->id . '" type="checkbox">';

                if ($device->last_date_time !== null && $device->last_date_time !== "") {
                    $updated_date = date('d-m-Y H:i:s', strtotime($device->last_date_time));
                } else {
                    $updated_date = "";
                }
                if ($device->expiry_date !== null && $device->expiry_date !== "" && $device->expiry_date !== '0000-00-00') {
                    $expiry_date = date('d-m-Y', strtotime($device->expiry_date));
                } else {
                    $expiry_date = "";
                }

                $row[] = '<input id="b_id_' . $device->id . '" name="b_id[]" class="text-nowrap checkbox remove_' . $i . '" data-info="' . $i . '"  value="' . $device->id . '" type="checkbox">';
                $row[] = $device->device_name;
                $row[] = $device->device_id;
                $row[] = $device->assign_users_name;
                $row[] = $updated_date;
                $row[] = $device->device_model;
                $row[] = $device->sim_number;
                $row[] = $device->sim_account;
                $row[] = $device->sim_password;
                $row[] = $expiry_date;
                $row[] = $device->emergency_number;
                //$row[] = $device->authorised_numbers;
                $row[] = $device->device_description;
                $row[] = $i;
                $row[] = $device->id;

                /* $row[] = '<button class="btn btn-primary" type="button" onclick="edit('.$device->id.')">Edit</button>
                  <button class="btn btn-danger" type="button" onclick="del('.$device->id.')">Delete</button>'; */
            }
            $i++;
            $data[] = $row;
        }
        if (isset($_POST['order']['0']['column']) && $_POST['order']['0']['column'] == 3) {
            $dir_ords = $_POST['order']['0']['dir'];
            if ($dir_ords == "desc") {
                usort($data, function ($a, $b) {
                    return $a['3'] <=> $b['3'];
                });
            } else {
                usort($data, function ($a, $b) {
                    return $b['3'] <=> $a['3'];
                });
            }
        } else if (isset($_POST['order']['0']['column']) && $_POST['order']['0']['column'] == 9) {
            $dir_ords = $_POST['order']['0']['dir'];
            //print_r($data);
            if ($dir_ords == "desc") {
                usort($data, function ($a, $b) {
                    return strtotime($a['9']) <=> strtotime($b['9']);
                });
            } else {
                usort($data, function ($a, $b) {
                    return strtotime($b['9']) <=> strtotime($a['9']);
                });
            }
        }

        $output = array("draw" => $_POST['draw'], "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data,);
        //output to json format
        echo json_encode($output);
    }

    function compareByTimeStamp($time1, $time2) {
        if (strtotime($time1) < strtotime($time2))
            return 1;
        else if (strtotime($time1) > strtotime($time2))
            return -1;
        else
            return 0;
    }

    public function check_token() {
        $token = $this->session->userdata("token");
        $this->db->select('tbl_user.id,tbl_user.user_type,is_superadmin');
        $this->db->from('tbl_user');
        $this->db->join('tc_user_token', 'tc_user_token.user_id=tbl_user.id');
        $this->db->where('tc_user_token.token', $token);
        $this->db->where("tbl_user.status", "1");
        $this->db->limit(1);
        $user_data = $this->db->get();
        if ($user_data->num_rows() > 0) {
            $user_id = 0;
            foreach ($user_data->result_array() as $value) {
                $this->user_type = $value['is_superadmin'];
                return $value['id'];
            }
        }
    }

    public function add() {
        $data['model_list'] = $this->Device_list_model->get_model_list();
        $this->load->view("device/add", $data);
    }

    public function __save() {
        //$user_id = $this->check_token();
        $user_id = $this->session->userdata('user_id');
        $assigned_users = $this->input->post('assigned_users') ? $this->input->post('assigned_users') : '';
        $data = array(
            'device_name' => $this->input->post('device_name'),
            'device_id' => $this->input->post('device_id'),
            'add_date' => gmdate("Y-m-d H:i:m"),
            'device_model' => $this->input->post('device_model'),
            'authorised_numbers' => $this->input->post('authorised_numbers'),
            'add_uid' => $user_id,
            'sim_number' => $this->input->post('sim_number'),
            'sim_account' => $this->input->post('sim_account'),
            'sim_password' => $this->input->post('sim_password'),
            'device_description' => $this->input->post('device_description'),
            'timezone' => $this->input->post('timezone'),
            'assigned_users' => $this->input->post('assigned_users')
        );
        $operation_result = $this->Curd_model->do_operation($data, 'add_device');
        //$result = $this->Curd_model->decode_json($operation_result);
        echo $operation_result;
        die;

        /* Commented by Ramesh.
          $this->db->insert('tbl_devices', $data);
          $insert_id = $this->db->insert_id($data);
          //$user_id = $data['add_uid'];
          if (!empty($insert_id)) {
          $this->db->select('*');
          $this->db->from("tbl_user");
          if ($assigned_users != "") {
          $this->db->where("( id in(" . $assigned_users . " ) or id='" . $user_id . "' ) and status='1' ");
          } else {
          $this->db->where(" id='" . $user_id . "' and status='1' ");
          }
          $query = $this->db->get();
          //echo $this->db->last_query();
          foreach ($query->result() as $row) {
          if ($row->devices_list != "") {
          $temp_as = explode(',', $row->devices_list);
          if (!in_array($insert_id, $temp_as)) {
          $this->db->where('id', $row->id);
          $this->db->set('devices_list', 'CONCAT(devices_list,\',' . $insert_id . '\')', FALSE);
          $this->db->update('tbl_user');
          }
          } else {
          $this->db->where('id', $row->id);
          $this->db->set('devices_list', $insert_id);
          $this->db->update('tbl_user');
          }
          }
          }
         */
    }

    public function getdeviceid() {

        $id = $this->input->post('id');
        $result_html = '';
        $result_set = $this->Device_list_model->getuser($id);
        foreach ($result_set as $result) {
            $result_html .= '<option value="' . $result->id . '">' . $result->user_name . '</option>';
        }

        echo json_encode($result_html);
    }

    public function edit($id) {
        $data = array();
        $data['model_list'] = $this->Device_list_model->get_model_list();
        //$data['recent_cmd_list']=$this->Device_list_model->get_recent_cmd_list($id);
        //$data['device'] = $this->Device_list_model->get($id);
        $operation_result = $this->Curd_model->do_operation(array('asset_id' => $id), 'get_device_data');
        $data['device'] = $this->Curd_model->decode_json($operation_result);
        //print_r($data);
        $this->load->view('device/edit', $data);
    }

    public function recent_command_list() {
        $id = $this->input->post('assets_id');
        $device_timezone = $this->input->post('timezone');
        $del_id = $this->input->post('del_id');
        if ($del_id != "") {
            $data_del = array();
            $data_del = $this->Device_list_model->get_recent_cmd_list($del_id);
            foreach ($data_del as $c_rows_del) {
                if ($c_rows_del->device_id != "") {
                    //var_dump($c_rows_del->device_id);
                    $this->db->where("device_id", $c_rows_del->device_id);
                    $this->db->delete("tbl_history_command_record");
                }
            }
        }
        $data = $this->Device_list_model->get_recent_cmd_list($id);
        $text = "";
        if (sizeof($data) > 0) {
            foreach ($data as $c_rows) {
                if ($c_rows->command_value != "") {
                    $device_dt = "";
                    if ($device_timezone != "" && $device_timezone != "None") {
                        $dt = explode(":", $device_timezone);

                        $device_dt = date('d/m/Y H:i', strtotime('' . $dt[0] . ' hour ' . $dt[1] . ' minutes', strtotime($c_rows->add_date)));
                    } else {
                        $device_dt = date('d/m/Y H:i', strtotime($c_rows->add_date));
                    }
                    $text .= $device_dt . '  ' . ($c_rows->command_value) . '&#13;&#10;';
                }
            }
            echo $text;
        } else {
            echo "No record found!";
        }
    }

    public function update() {
        $id = $this->input->post('id');
        $assigned_users = $this->input->post('assigned_users') ? $this->input->post('assigned_users') : '';
        $user_id = $this->check_token();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $data = array('device_name' => $this->input->post('device_name'), 'device_id' => $this->input->post('device_id'), 'modified_date' => gmdate("Y-m-d H:i:m"), 'device_model' => $this->input->post('device_model'), 'authorised_numbers' => $this->input->post('authorised_numbers'), 'add_uid' => $user_id, 'sim_number' => $this->input->post('sim_number'), 'sim_account' => $this->input->post('sim_account'), 'sim_password' => $this->input->post('sim_password'), 'device_description' => $this->input->post('device_description'),);
        $this->Device_list_model->update(array('id' => $this->input->post('id')), $data);
        $insert_id = $id;
        if (!empty($insert_id)) {
            $this->db->select('*');
            $this->db->from("tbl_user");
            $this->db->where("(  find_in_set(" . $id . ",devices_list ) ) and status='1' and user_type='2' ");
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                if ($row->devices_list != "") {
                    $temp_as = explode(',', $row->devices_list);
                    foreach ($temp_as as $key => $value) {
                        if ($value == $id)
                            unset($temp_as[$key]);
                        //echo $value;
                    }
                    $temp_as = implode(',', $temp_as);
                    $this->db->where('id', $row->id);
                    $this->db->set('devices_list', $temp_as, TRUE);
                    $this->db->update('tbl_user');
                }
                // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
            }
            $assigned_users = '';
            $this->db->select('*');
            $this->db->from("tbl_user");
            if ($assigned_users != "") {
                $this->db->where("( id in(" . $assigned_users . " ) or id='" . $user_id . "' ) and status='1' ");
            } else {
                $this->db->where(" id='" . $user_id . "' and status='1' ");
            }
            //$this->db->where("(  id in(".$assigned_users." ) ) and status='1' " );
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                if ($row->devices_list != "") {
                    $temp_as = explode(',', $row->devices_list);
                    if (!in_array($insert_id, $temp_as)) {
                        $this->db->where('id', $row->id);
                        $this->db->set('devices_list', 'CONCAT(devices_list,\',' . $insert_id . '\')', FALSE);
                        $this->db->update('tbl_user');
                    }
                } else {
                    $this->db->where('id', $row->id);
                    $this->db->set('devices_list', $insert_id);
                    $this->db->update('tbl_user');
                }
                // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
            }
        } else {
            $this->db->where('id', $id);
            $this->db->update('tbl_user', array('devices_list' => $id));
        }
    }

    /* public function delete($id)
      {

      $this->Device_list_model->delete($id);
      echo "Records deleted Successfully";

      } */

    public function delete() {
        $user_id = $this->check_token();
        // POST values
        $user_ids = $this->input->post('user_ids');

        $device_id = implode(", ", $user_ids);        // Delete records
        $this->Device_list_model->delete($user_ids);

        $this->db->select('*');
        $this->db->from("tbl_user");
        $this->db->where(" find_in_set(" . $device_id . ",devices_list) and status='1' ");

        $query = $this->db->get();
        foreach ($query->result() as $row) {
            if ($row->devices_list != "") {
                $temp_as = explode(',', $row->devices_list);
                foreach ($temp_as as $key => $value) {
                    if ($value == $device_id)
                        unset($temp_as[$key]);
                    //echo $value;
                }

                // print_r($temp_as);die();
                $temp_as = implode(',', $temp_as);
                $this->db->where('id', $row->id);
                $this->db->set('devices_list', $temp_as);
                $this->db->update('tbl_user');
            }
        }
    }

    public function unassigned() {
        $user_id = $this->check_token();
        $asset_id = $this->input->post('id');

        if (!empty($user_id)) {
            $this->db->select('*');
            $this->db->from("tbl_user");
            $this->db->where("id", $user_id);
            $this->db->where("(  find_in_set(" . $asset_id . ",devices_list ) ) and status='1'");
            $query = $this->db->get();
            //echo $this->db->last_query();
            foreach ($query->result() as $row) {
                if ($row->devices_list != "") {
                    $temp_as = explode(',', $row->devices_list);
                    foreach ($temp_as as $key => $value) {
                        if ($value == $asset_id)
                            unset($temp_as[$key]);
                        //echo $value;
                    }
                    $temp_as = implode(',', $temp_as);
                    $this->db->where('id', $row->id);
                    $this->db->set('devices_list', $temp_as, TRUE);
                    $this->db->update('tbl_user');
                }
            }
        }

        echo "user removed";
    }

}
