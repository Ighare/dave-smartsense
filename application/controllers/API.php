<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller { 
		public  $host='http://104.237.1.162:8082';
		public  $adminEmail    = 'admin';
		public  $adminPassword = 'admin';
		public  $cookie;
		public  $json='Content-Type: application/json';
		public  $urlencoded='Content-Type: application/x-www-form-urlencoded';
	function __construct(){
        parent::__construct(); 
		file_put_contents(FCPATH.'/logs/date_'.date("d.m.Y").'.txt',  "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].PHP_EOL , FILE_APPEND);
		file_put_contents(FCPATH.'/logs/date_'.date("d.m.Y").'.txt', print_r($_REQUEST,true).PHP_EOL , FILE_APPEND);
		if(isset($_FILE))
		file_put_contents(FCPATH.'/logs/date_'.date("d.m.Y").'.txt', print_r($_FILE,true).PHP_EOL , FILE_APPEND);

         $this->load->model("Api_model"); 
         $this->CI =& get_instance();
        $this->load->database();

        $this->user_type = "0";
    }
	function logins($adminEmail,$adminPassword){
		$data='email='.$adminEmail.'&password='.$adminPassword;		
		return $this->curl('/api/session','POST','',$data,array($this->urlencoded));
	}
	function curl($task,$method,$cookie,$data,$header)
	{
    $res=new stdClass();
    $res->responseCode='';
    $res->error='';   
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ($this->host).$task);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    if($method=='POST' || $method=='PUT' || $method=='DELETE') {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $header[]="Cookie: ".$cookie;
    }
    else
    {
        $header = array('Accept: application/json',"Cookie: ".$cookie);
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    $data=curl_exec($ch);
    $size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', substr($data, 0, $size), $c) == 1) $this->cookie = $c[1];
    $res->response = substr($data, $size);
    if(!curl_errno($ch)) { 
        $res->responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		if($res->responseCode==200){
			$res->msg="Command has been sent to device.";
			$res->result="true"; 
		}
		if($res->responseCode==401){
		$res->msg="Command not sent to device.";
		$res->result="false";
		}
    }
    else  {
        $res->responseCode=400;
        $res->error= curl_error($ch);
		$res->msg=curl_error($ch); 
		$res->result="false";
    }
    curl_close($ch);	
    return $res;
	}
	function set_notification_token($user_id,$imei,$token_id,$assets_type,$app_version){
		if($user_id!= "" && $imei!= "" && $token_id!=""){
			$this->db->query("delete from tbl_notifications where imei='$imei'");
			$insert = array(
				"user_id"=>$user_id,
				"device_token"=>$token_id,
				"assets_type"=>$assets_type,
				"app_version"=>$app_version,
				"imei"=>$imei,
			);
			$this->db->insert("tbl_notifications",$insert);
		}
	}
    public function login(){  
  
        $username    = isset($_POST['username']) ? $_POST['username'] : "";
        $password    = isset($_POST['password']) ? $_POST['password'] : "";
        $os_version    = isset($_POST['app_version']) ? $_POST['app_version'] : "";
        $device_token    = isset($_POST['device_token']) ? $_POST['device_token'] : "Android";
        $device_type    = isset($_POST['assets_type']) ? $_POST['assets_type'] : "";
        $device_imei    = isset($_POST['device_imei']) ? $_POST['device_imei'] : "";
        if ($username == "" || $password == "" ) {
            $data = array("result" => "false", "msg" => "Input parameter missing!");
            die(json_encode($data));
        }
        $check_user=$this->Api_model->valid_user($username,$password,$os_version,$device_token,$device_type);
        if($check_user!=0){
			$this->set_notification_token($check_user['user_id'],$device_token,$device_token,$device_type,$os_version);
        	$data['user_id']=$check_user['user_id'];
            $data['token']=$check_user['token'];
            
            $data['user_type']=$check_user['user_type'];
            $data['is_superadmin']=$check_user['is_superadmin'];

            $data['devices_count']=sizeof(explode(',',$check_user['devices_list']));
            $data['assigned_devices']=$check_user['devices_list'];

    		$data = array("result" => "true", "data" => $data);
    		die(json_encode($data)); 
        }else{
        	$data = array("result" => "false", "msg" => "Username or password incorrect!");
        	die(json_encode($data));
        }

    } 
     public function add_member(){ 
       $user_id= $this->check_token();
        $login    = isset($_POST['login']) ? $_POST['login'] : ""; 
        $email_id    = isset($_POST['email_id']) ? $_POST['email_id'] : "";
        $password    = isset($_POST['password']) ? $_POST['password'] : "";
        $name    = isset($_POST['name']) ? $_POST['name'] : "";
        $status=isset($_POST['status']) ? $_POST['status'] : "";
        $emergency_number=isset($_POST['emergency_number']) ? $_POST['emergency_number'] : ""; 
        $device_assign=isset($_POST['device_assign']) ? $_POST['device_assign'] : "";
        $notes=isset($_POST['notes']) ? $_POST['notes'] : "";
        $sim_number=isset($_POST['sim_number'])? $_POST['sim_number']:"";
        $sim_account=isset($_POST['sim_account'])? $_POST['sim_account']:"";
        $sim_password=isset($_POST['sim_password'])? $_POST['sim_password']:"";
       // isset($_POST['user_id']) ? $_POST['user_id'] : "";
        
        if ($password == "" || $name == "" || $login=="") {
            $data = array("result" => "false", "msg" => "Input parameter missing!");
            die(json_encode($data));
        }
        $this->db->select("id");
        $this->db->from("tbl_user");
        $this->db->where("login_username",$login);
        $checkusers=$this->db->get();
        if($checkusers->num_rows()>0){
            $data = array("result" => "false", "msg" => "Login username already used.");
            die(json_encode($data));
        } 
        $member=array(
            'login_username'=>$login,
            'user_email'=>$email_id, 
            'user_password'=>md5($password),
            'user_password_read'=>$password,
            'user_name'=>$name,
            'user_contact'=>$emergency_number,
            'devices_list'=>$device_assign,
            'user_note'=>$notes,
            'parent_id'=>$user_id,
            'user_status'=>$status,
            'user_type'=>$status,
            'sim_number'=>$sim_number,
            'sim_account'=>$sim_account,
            'sim_password'=>$sim_password,  
            'add_uid'=>$user_id, 
            'add_date'=>gmdate("Y-m-d H:i:m"), 
        );
        $this->db->insert('tbl_user',$member);
        $insert_id = $this->db->insert_id();
        $data['user_id']=$insert_id;
        $data = array("result" => "true","msg" => "User successfully Added","data" => $data); 
        die(json_encode($data));
    }
    public function edit_member(){
        $user_id= $this->check_token();
        $email_id    = isset($_POST['email_id']) ? $_POST['email_id'] : "";
       // $password    = isset($_POST['password']) ? $_POST['password'] : "";
        $name = isset($_POST['name']) ? $_POST['name'] : "";
        $status=isset($_POST['status']) ? $_POST['status'] : "";
        $emergency_number=isset($_POST['emergency_number']) ? $_POST['emergency_number'] : "";
        $device_assign=isset($_POST['device_assign']) ? $_POST['device_assign'] : "";
        $notes=isset($_POST['notes']) ? $_POST['notes'] : "";
        //isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $update_id=isset($_POST['update_id']) ? $_POST['update_id'] : "";
          $sim_number=isset($_POST['sim_number'])? $_POST['sim_number']:"";
        $sim_account=isset($_POST['sim_account'])? $_POST['sim_account']:""; 
        $sim_password=isset($_POST['sim_password'])? $_POST['sim_password']:""; 

        $login    = isset($_POST['login']) ? $_POST['login'] : ""; 
 
        if ($update_id =="" || $email_id == "" ||  $name == "" ||  $login=="" ) {
            $data = array("result" => "false", "msg" => "Input parameter missing!");
            die(json_encode($data));
        }
        $member=array( 
            'login_username'=>$login,
            'user_email'=>$email_id,  
            'user_name'=>$name,
            'user_contact'=>$emergency_number,
            'devices_list'=>$device_assign,
            'user_note'=>$notes,  
            'sim_number'=>$sim_number,
            'sim_account'=>$sim_account,       
            'sim_password'=>$sim_password,  
            'modified_date'=>gmdate("Y-m-d H:i:m")
        );
        $this->db->where('id',$update_id); 
        $this->db->update('tbl_user',$member); 
        $data['user_id']=$update_id;
        $data = array("result" => "true","msg"=>"User successfully updated.","data" => $data);
        die(json_encode($data)); 
    } 
    public function member_list(){
        $user_id=$this->check_token();
            //= isset($_POST['user_id']) ? $_POST['user_id'] : "";
        if($user_id !='') 
        {
            $list=  $this->Api_model->getmemberList($user_id);
            if(is_array($list)){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }

    }
    public function last_location_list(){
       $user_id  = $this->check_token();
         // = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        if($user_id !='') 
        {
            $list=  $this->Api_model->lastlocationList($user_id);
            if(is_array($list)){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }
    }
    public function check_token(){ 
        if(isset($_POST['token'] ) &&  $_POST['token']!=""){
            $token=$_POST['token'];
            $this->db->select('tbl_user.id,tbl_user.user_type,is_superadmin');
            $this->db->from('tbl_user');
            $this->db->join('tc_user_token','tc_user_token.user_id=tbl_user.id');
            $this->db->where('tc_user_token.token',$token);
            $this->db->where("tbl_user.status","1"); 
            $this->db->limit(1);
            $user_data=$this->db->get();  
            if($user_data->num_rows() > 0){
                $user_id=0;
                foreach ($user_data->result_array() as $value) {
                    $this->user_type=$value['is_superadmin'];
                  return  $value['id']; 
                }
            }else{
                $data = array("result" => "false", "msg" => "Invalid Token");
                die(json_encode($data));
            }
        }else{ 
                $data = array("result" => "false", "msg" => "Token not found");
                die(json_encode($data));
        }
        
    } 

    public function all_locations(){
        $user_id  = $this->check_token();
         // = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $start_date= isset($_POST['start_date']) ? $_POST['start_date'] : "";
        $end_date= isset($_POST['end_date']) ? $_POST['end_date'] : "";
        $device_id= isset($_POST['device_id']) ? $_POST['device_id'] : "";
        $page= isset($_POST['page']) ? $_POST['page'] : "1";
        $limit= isset($_POST['limit']) ? $_POST['limit'] : "100";


        if($user_id !='' ||  $start_date!='' || $end_date!='' || $device_id !='' )
        {
            $list=  $this->Api_model->alllastlocationList($device_id,$start_date,$end_date, $user_id );
            if(is_array($list)){  
                $data = array("result" => "true", "data" => $list['data'],'page'=>$list['page'],'count'=>$list['count'],'total_pages'=>$list['total_pages']);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else 
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }
    }

    public function data_history(){
		
        $user_id  = $this->check_token();
         // = isset($_POST['user_id']) ? $_POST['user_id'] : "";
        $start_date= isset($_POST['start_date']) ? $_POST['start_date'] : "";
        $end_date= isset($_POST['end_date']) ? $_POST['end_date'] : "";
        $device_id= isset($_POST['device_id']) ? $_POST['device_id'] : "";


        if($user_id !='' ||  $start_date!='' || $end_date!='' || $device_id !='' )
        {
            $list=  $this->Api_model->data_history($device_id,$start_date,$end_date);
            if(is_array($list)){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }
    }

    public function  delete_geofences(){
        $user_id= $this->check_token();
        $geo_id = isset($_POST['geo_id']) ? $_POST['geo_id'] : "";

        if($geo_id !='' )
        {
                $this->db->where('id',$geo_id);
                $this->db->update('tbl_geofence',array('status'=>'0','del_uid'=>$user_id,'del_date'=>gmdate("Y-m-d H:i:s")));
                 $json_data = array("result" => "true","msg"    => "Geofence successfully deleted.");               
                die(json_encode($json_data));
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }
    }

    public function  delete_device(){
        $user_id= $this->check_token();
        $device_id = isset($_POST['device_id']) ? $_POST['device_id'] : "";

        if($device_id !='' )
        {
                $this->db->where('id',$device_id);
                $this->db->update('tbl_devices',array('status'=>'0','del_uid'=>$user_id,'del_date'=>gmdate("Y-m-d H:i:s")));

                $this->db->select('*');
                $this->db->from("tbl_user");
                $this->db->where("  find_in_set(".$device_id.",devices_list) and status='1' " );

                $query = $this->db->get();
                foreach ($query->result() as $row){ 
                    if ($row->devices_list != ""){ 
                        $temp_as = explode(',', $row->devices_list);
                        foreach ($temp_as as $key => $value) {
                            if($value==$device_id)  unset($temp_as[$key]);
                            //echo $value;
                        }
                          
                      // print_r($temp_as);die();
						$temp_as = implode(',', $temp_as);                   
                        $this->db->where('id',$row->id);
                        $this->db->set('devices_list', $temp_as);
                        $this->db->update('tbl_user'); 
                    } 
                   // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
                }

                 $json_data = array("result" => "true","msg"    => "Devices successfully deleted.");               
                die(json_encode($json_data));
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }
    }

    public function  delete_user(){
        $user_id= $this->check_token();
        $device_id = isset($_POST['user_id']) ? $_POST['user_id'] : "";

        if($device_id !='' )
        {
                $this->db->where('id',$device_id);
                $this->db->update('tbl_user',array('status'=>'0','del_uid'=>$user_id,'del_date'=>gmdate("Y-m-d H:i:s")));
                 $json_data = array("result" => "true","msg"    => "User successfully deleted.");               
                die(json_encode($json_data));
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }
    }
    public function  add_geofences(){
        $user_id= $this->check_token();

        $geofence_name = isset($_POST['name']) ? $_POST['name'] : "";
        $geofence_address = isset($_POST['address']) ? $_POST['address'] : "";
        $geofence_latitude = isset($_POST['latitude']) ? $_POST['latitude'] : "";
        $geofence_longitude=isset($_POST['longitude']) ? $_POST['longitude'] : "";
        $geofence_in_alert=isset($_POST['in_alert']) ? $_POST['in_alert'] : "0";
        $geofence_out_alert=isset($_POST['out_alert']) ? $_POST['out_alert'] : "0";
        $geofence_radius=isset($_POST['radius']) ? $_POST['radius'] : "0";
        $assigned_devices= isset($_POST['assigned_devices']) ? $_POST['assigned_devices'] : "";
        
        if ($geofence_name == "" || $geofence_address == "" || $geofence_latitude == "" || $geofence_longitude == "" || $assigned_devices=="" ) {
            $data = array("result" => "false", "msg" => "Input parameter missing!");
            die(json_encode($data));
        }
        $tbl_geofence=array(
            'geofence_name'=>$geofence_name, 
            'geofence_address'=>$geofence_address,
            'geofence_latitude'=>$geofence_latitude,
            'geofence_longitude'=>$geofence_longitude,
            'geofence_in_alert'=>$geofence_in_alert,
            'geofence_out_alert'=>$geofence_out_alert,
            'geofence_radius'=>$geofence_radius,
            'assigned_devices'=>$assigned_devices,
            'status'=>'1',
              'add_uid'=>$user_id, 
            'add_date'=>gmdate("Y-m-d H:i:m"), 
        );
        $this->db->insert('tbl_geofence',$tbl_geofence);
        $insert_id = $this->db->insert_id();
        $data['geofence_id']=$insert_id;
        $data = array("result" => "true","msg"=>"Geofence added successfully.", "data" => $data);
        die(json_encode($data));
    }
     public function  edit_geofences(){
        $user_id= $this->check_token();
        $geo_id = isset($_POST['geo_id']) ? $_POST['geo_id'] : ""; 
        $geofence_name = isset($_POST['name']) ? $_POST['name'] : "";
        $geofence_address = isset($_POST['address']) ? $_POST['address'] : "";
        $geofence_latitude = isset($_POST['latitude']) ? $_POST['latitude'] : "";
        $geofence_longitude=isset($_POST['longitude']) ? $_POST['longitude'] : "";
        $geofence_in_alert=isset($_POST['in_alert']) ? $_POST['in_alert'] : "0";
        $geofence_out_alert=isset($_POST['out_alert']) ? $_POST['out_alert'] : "0";
        $geofence_radius=isset($_POST['radius']) ? $_POST['radius'] : "0";
        $assigned_devices= isset($_POST['assigned_devices']) ? $_POST['assigned_devices'] : "";
        
        if ($geofence_name == "" || $geofence_address == "" || $geofence_latitude == "" || $geofence_longitude == "" || $assigned_devices=="" ) {
            $data = array("result" => "false", "msg" => "Input parameter missing!");
            die(json_encode($data));
        }
        $tbl_geofence=array(
            'geofence_name'=>$geofence_name, 
            'geofence_address'=>$geofence_address,
            'geofence_latitude'=>$geofence_latitude,
            'geofence_longitude'=>$geofence_longitude,
            'geofence_in_alert'=>$geofence_in_alert,
            'geofence_out_alert'=>$geofence_out_alert,
            'geofence_radius'=>$geofence_radius,
            'assigned_devices'=>$assigned_devices,
            'status'=>'1',
            'modified_date'=>gmdate("Y-m-d H:i:m"), 
        ); 
         $this->db->where('id',$geo_id); 
        $this->db->update('tbl_geofence',$tbl_geofence); 
        $data['geo']=$geo_id;
        $data = array("result" => "true","msg"=>"Geofence successfully updated.","data" => $data);
        die(json_encode($data));  
    }
     public function geofence_list(){
        $user_id=$this->check_token();
            //= isset($_POST['user_id']) ? $_POST['user_id'] : "";
        if($user_id !='') 
        {
            $list=  $this->Api_model->getgeoList($user_id);
            if(is_array($list)){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }

    }
    function add_device(){
     $user_id=$this->check_token();
    //add below post method to get data from API.
    //$user_id=isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
    $device_id=isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : "";
    $device_name=isset($_REQUEST['device_name']) ? $_REQUEST['device_name'] : "";
    $device_number=isset($_REQUEST['device_number']) ? $_REQUEST['device_number'] : "";
    $device_description=isset($_REQUEST['device_description']) ? $_REQUEST['device_description'] : "";
    $expiry_date=isset($_REQUEST['expiry_date']) ? $_REQUEST['expiry_date'] : "";
    $device_model=isset($_REQUEST['device_model']) ? $_REQUEST['device_model'] : "";
    $sim_number=isset($_REQUEST['sim_number']) ? $_REQUEST['sim_number'] : "";
    $sim_account=isset($_REQUEST['sim_account']) ? $_REQUEST['sim_account'] : "";
    $sim_password=isset($_REQUEST['sim_password']) ? $_REQUEST['sim_password'] : "";
    $emergency_number=isset($_REQUEST['emergency_number']) ? $_REQUEST['emergency_number'] : "";
    $address=isset($_REQUEST['address']) ? $_REQUEST['address'] : "";
    $authorised_numbers=isset($_REQUEST['authorised_numbers']) ? $_REQUEST['authorised_numbers'] : "";
    $assigned_users=isset($_REQUEST['assigned_users']) ? $_REQUEST['assigned_users'] : "";
    $timezone=isset($_REQUEST['timezone']) ? $_REQUEST['timezone'] : "NULL";
     
     
    //added requred validation 
    if($device_id =="" OR $device_id=="" OR $device_name=="" OR   $device_model==""  ){  
    $data = array("result" => "false", "msg" => "Input parameter missing!");
     die(json_encode($data));
    }
    $device_data=array(
            'device_id'=>$device_id, 
            'device_name'=>$device_name,
            'device_number'=>$device_number,
            'device_description'=>$device_description,
            'expiry_date'=>$expiry_date, 
            'address'=>$address,  
            'authorised_numbers'=>$authorised_numbers, 
            'device_model'=>$device_model, 
            'add_uid'=>$user_id, 
            'add_date'=>gmdate("Y-m-d H:i:m"), 
            'sim_number'=>$sim_number,
            'sim_account'=>$sim_account,
            'sim_password'=>$sim_password,
            'emergency_number'=>$emergency_number,
            'timezone'=>$timezone,
        );
		//add device in traccar
		$this->logins($this->adminEmail,$this->adminPassword);		
		 $data='{"id":-1,"name":"'.$device_name.'","uniqueId":"'.$device_id.'","status":"","lastUpdate":null,"groupId":0}';		
		$this->curl('/api/devices','POST',$this->cookie,$data,array($this->json));
		//end traccar add code
         die(json_encode($this->Api_model->add_device_model($device_data,$assigned_users)));
    } 
    function edit_device(){
     $user_id=$this->check_token();
    //add below post method to get data from API.
    //$user_id=isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
    $device_id=isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : "";
    $device_name=isset($_REQUEST['device_name']) ? $_REQUEST['device_name'] : "";
    $device_number=isset($_REQUEST['device_number']) ? $_REQUEST['device_number'] : "";
    $device_description=isset($_REQUEST['device_description']) ? $_REQUEST['device_description'] : "";
    $expiry_date=isset($_REQUEST['expiry_date']) ? $_REQUEST['expiry_date'] : "";
    $device_model=isset($_REQUEST['device_model']) ? $_REQUEST['device_model'] : "";
   $sim_number=isset($_REQUEST['sim_number']) ? $_REQUEST['sim_number'] : "";
    $sim_account=isset($_REQUEST['sim_account']) ? $_REQUEST['sim_account'] : "";
    $sim_password=isset($_REQUEST['sim_password']) ? $_REQUEST['sim_password'] : "";
    $emergency_number=isset($_REQUEST['emergency_number']) ? $_REQUEST['emergency_number'] : "";
    $address=isset($_REQUEST['address']) ? $_REQUEST['address'] : "";
    $authorised_numbers=isset($_REQUEST['authorised_numbers']) ? $_REQUEST['authorised_numbers'] : "";
    $edit_device_id=isset($_REQUEST['edit_device_id']) ? $_REQUEST['edit_device_id'] : "";
	$timezone=isset($_REQUEST['timezone']) ? $_REQUEST['timezone'] : "NULL";
      $assigned_users=isset($_REQUEST['assigned_users']) ? $_REQUEST['assigned_users'] : "";
    
    //added requred validation 
    if($device_id =="" OR $device_id=="" OR $device_name=="" OR    $device_model=="" OR $edit_device_id ==""  ){
    $data = array("result" => "false", "msg" => "Input parameter missing!"); 
     die(json_encode($data));
    }
    $device_data=array(
            'device_id'=>$device_id, 
            'device_name'=>$device_name,
            'device_number'=>$device_number,
            'device_description'=>$device_description, 
            'expiry_date'=>$expiry_date,
            'sim_number'=>$sim_number,
            'sim_account'=>$sim_account,
            'sim_password'=>$sim_password,
            'emergency_number'=>$emergency_number,
            'address'=>$address, 
            'authorised_numbers'=>$authorised_numbers, 
            'device_model'=>$device_model,  
            'modified_date'=>gmdate("Y-m-d H:i:m"),  
            'timezone'=>$timezone,  
        );
         die(json_encode($this->Api_model->edit_device_model($device_data,$edit_device_id,$assigned_users,$user_id)));
    }
	public function get_device_data(){
		 $user_id=$this->check_token();
		  $device_id=isset($_REQUEST['asset_id']) ? $_REQUEST['asset_id'] : "";
		 if($user_id !='' && $device_id !="") 
        {
			
            $list=  $this->Api_model->get_device($user_id,$device_id);
            if(isset($list) && $list){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));     
            }else{
                     $json_data = array("result" => "false","msg" =>'No records found');               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }

	}
	public function get_user_data(){
		 $user_id=$this->check_token();
		 $user_id_sub=isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
		 if($user_id !='' && $user_id_sub !="") 
        {
			
            $list=  $this->Api_model->get_user($user_id,$user_id_sub);
           if(isset($list) && $list){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg"=>'No records found');               
             die(json_encode($json_data));
            } 
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }

	}
     public function deviceList(){
        $user_id=$this->check_token();
            //= isset($_POST['user_id']) ? $_POST['user_id'] : "";
        if($user_id !='') 
        {
            $list=  $this->Api_model->getdeviceList($user_id);
            if(is_array($list)){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }

    }

     public function deviceList_unassigned(){
        $user_id=$this->check_token();
            //= isset($_POST['user_id']) ? $_POST['user_id'] : "";
        if($user_id !='') 
        {
            $list=  $this->Api_model->getdeviceList($user_id,"1");
            if(is_array($list)){ 
                $data = array("result" => "true", "data" => $list);
                die(json_encode($data));    
            }else{
                     $json_data = array("result" => "false","msg" =>$list);               
             die(json_encode($json_data));
            }
        }else
        {
           $json_data = array("result" => "false","msg"    => "Input parameter missing!");               
             die(json_encode($json_data));
        }

    }
     function get_unique_key(){
         
        $unique_key = uniqid('',true); 
        $unique_key = str_replace('.','',$unique_key);
        
        $response = array("result"=>"true","msg"=>"","unique_key"=>$unique_key);
        die(json_encode($response));
    }
    function forgotpassword() {
        $username = isset($_REQUEST['username'])?$_REQUEST['username']:"";
        if($username!=""){
            $data = array();
            $this->load->helper(array('url', 'form'));
            $this->load->library('email');
            $this->load->library('user_agent'); 
             $user_email = $this->check_valid_email_('tbl_user', 'login_username', $username);
            if ($user_email) {
                $data['result']="true";
                $data['msg'] = "Your password has been reset and emailed to you.";
            } else {
                $data['result']="false";
                $data['msg'] = "The email id for this username is not found in our database.";
            }
            $this->die_json($data);
        }else{
            $data['result']="false";
            $data['msg'] = "The email id for this username is not found in our database.";
            $this->die_json($data);
        }
    }
    function check_valid_email_($table, $email_field, $login_username) {
       // echo "select id,user_email,user_name from tbl_user where login_username = '$login_username' and status=1";
        $result1 = $this->db->query("select id,user_email,user_name from tbl_user where login_username = '".$login_username."' and status=1;");
        //print_r($result1->num_rows()) ;die();
        if ($result1->num_rows() > 0) {
            $res = $result1->result();
            $user = $res[0];
            //echo "1";
            //call this function to reset password, save new password in database and send email to user.
            if($user->user_email != ''){
                $this->resetpassword($user);
            }
            return true;

        } else {
          //  echo "0";
            return false;
        }
    }
    function resetpassword($user) {
        $this->load->helper('string');
        $password = random_string('alnum', 16);
        $this->db->where('id', $user->id);
        $this->db->update('tbl_user', array('user_password' => MD5($password)));

        //Send email using codeigniter
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "smartsense.tracking@gmail.com"; 
        $config['smtp_pass'] = "Abcdef1**_";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n"; 

        $this->email->initialize($config);
        $this->email->from('smartsense.tracking@gmail.com', 'Smartsense Tracking');

        $mails = explode(',' ,$user->user_email); 
        $this->email->to($mails); 
        $this->email->subject('Smartsense Tracking Password Reset');
        $msg = "Hello " . $user->user_name . ",<br><br>As per your request your password has been reset and your new temporary password is: <b>" . $password . "</b><br>Login with your temporary password.";
           $imgSrc =  base_url().'assets/images_login/angles_eye_logo.png';
        // Change image src to your site specific settings
        $content = '<!DOCTYPE HTML>' .'<head>' .'<meta http-equiv="content-type" content="text/html">' .'<title>Email notification</title>' .'</head>' .'<body>' .'<div class="message">&nbsp;</div>' .'<div class="message">' . $msg . '</div>' .'<div class="message">&nbsp;</div>' .'<div class="message">&nbsp;</div>' .'<div class="message">&nbsp;</div>' .'<div>Thanks</div>' .'<div>Smartsense Tracking Team</div>' .'<div>&nbsp;</div>' .'<div class="logo"><img alt="Smartsense Tracking" height="84" src="' . $imgSrc . '" title="Smartsense Tracking" width="282" /></div>' .'<div class="logo">&nbsp;</div>' .'</body>'; 
        $this->email->message($content);
        $this->email->send();
        //var_dump($this->email->print_debugger());
        //echo $this->email->print_debugger();
    }
	function device_command(){
		$user_id= $this->check_token();
		$device_id=isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : "";
		$command_name=isset($_REQUEST['command_name']) ? $_REQUEST['command_name'] : "";
		$model_type=isset($_REQUEST['model_type']) ? $_REQUEST['model_type'] : "";
		$command_type=isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
		if($device_id =="" && $command_name=="" && $model_type==""){
		$data = array("result" => "false", "msg" => "Input parameter missing!");
		die(json_encode($data));
		}else{
		$this->logins($this->adminEmail,$this->adminPassword);
		$cookie=$this->cookie;		
		$query = "SELECT * FROM `tc_devices` Where ";
		if($device_id){
		$query .=" uniqueid='$device_id'";
		}else{
		$data = array("result" => "false", "msg" => "Device not found");
		die(json_encode($data)); 	
		}
		$query .=" AND lastupdate is not null and positionid is not null";
		$res = $this->db->query($query);
		if ($res->num_rows() > 0) {
		$row = $res->row();
		$ids=$row->id;		//{"id":0,"description":"New\u2026","deviceId":2,"type":"rebootDevice","textChannel":false,"attributes":{}} 
		//if($command_type==0){
		$data='{"id":0,"attributes":{"data":"'.$command_name.'"},"deviceId":'.$ids.',"type":"custom","textChannel":false,"description":"New…"}';
		//}  
		if($command_type==1){
			if($command_name=="work_mode"){
				if($model_type=="EV07W"){
				$data='{"id":0,"attributes":{"data":"123456M,002"},"deviceId":'.$ids.',"type":"custom","textChannel":false,"description":"New…"}';
				}else if($model_type=="EV07B"){
				$data = array("result" => "false", "msg" => "This protocol is not yet done.");

				}else if($model_type=="ES825"){
				$data = array("result" => "false", "msg" => "This protocol is not yet done.");
				}else if($model_type=="A19&A28"){
					
				}
				
			}else if($command_name=="reboot"){
				if($model_type=="EV07W"){
				$data='{"id":0,"attributes":{"data":"123456T"},"deviceId":'.$ids.',"type":"custom","textChannel":false,"description":"New…"}';
				}else if($model_type=="EV07B"){				
				$data='{"id":0,"attributes":{"data":"5265626f6f74"},"deviceId":'.$ids.',"type":"custom","textChannel":false,"description":"New…"}';
				}else if($model_type=="ES825"){
				$data = array("result" => "false", "msg" => "This protocol is not yet done.");
				die(json_encode($data));
				}else if($model_type=="A19&A28"){
				$data='{"id":0,"attributes":{},"deviceId":'.$ids.',"type":"rebootDevice","textChannel":false,"description":"New…"}';				
				}
			}else if($command_name=="medication_alert"){
				
			}else if($command_name=="alarm_clock"){
				
			}else if($command_name=="find_me"){
				
			}else if($command_name=="continuous_locate"){
				
			}else if($command_name=="get_location"){				
				if($model_type=="EV07W"){ 
				$data='{"id":0,"attributes":{"data":"123456LOC"},"deviceId":'.$ids.',"type":"custom","textChannel":false,"description":"New…"}';
				}else if($model_type=="EV07B"){				
				$data='{"id":0,"attributes":{"data":""},"deviceId":'.$ids.',"type":"custom","textChannel":false,"description":"New…"}';
				}else if($model_type=="ES825"){
				$data = array("result" => "false", "msg" => "This protocol is not yet done.");
				die(json_encode($data));
				}else if($model_type=="A19&A28"){
				$data='{"id":0,"attributes":{},"deviceId":'.$ids.',"type":"rebootDevice","textChannel":false,"description":"New…"}';
				}
			}else if($command_name=="emergency_nos"){
				
			}else if($command_name=="location_type"){
				
			}
			
		}//command type end  		
		$responce=$this->curl('/api/commands/send','POST',$cookie,$data,array($this->json));  	
		die(json_encode($responce));
		}else{
		$data = array("result" => "false", "msg" => "Device is not online");
		die(json_encode($data));
		}
		//$data='{"id":0,"attributes":{"data":"'.$command_name.'"},"deviceId":'.$id.',"type":"custom","textChannel":false,"description":"New…"}';
		//$this->curl('/api/commands/send','POST',$cookie,$data,array($this->json));
		//{"id":0,"attributes":{"data":"test"},"deviceId":1,"type":"custom","textChannel":false,"description":"New…"}
		//$data='{"deviceId":'.$id.',"type":"engineResume","id":-1}';
		//$on_data=$this->sendCommand_oil_start($this->cookie,$id);
		//$data = array("result" => "true", "msg" => "Command sent to device successfully.");
		//die(json_encode($data)); 
		} 
	} 
     
    function die_json($array){
        die(json_encode($array));
    }
    function test(){
    	 echo $this->Api_model->test();
    }
    function unassigned_devices(){

        $user_id= $this->check_token();
        $user_id=isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
        if($user_id =="" ){
            $data = array("result" => "false", "msg" => "Input parameter missing!"); 
            die(json_encode($data));
        }
        $update_data=array(
                'devices_list'=>''
        );
        $this->db->where('user_type','2');
        $this->db->where_in('id',explode(",", $user_id));
        $this->db->update('tbl_user',$update_data);
        $data = array("result" => "true", "msg" => "Devices removed!");
        die(json_encode($data));
    }

    function unassigned_user(){

        $user_id= $this->check_token();
        $asset_id=isset($_REQUEST['asset_id']) ? $_REQUEST['asset_id'] : "";
        $user_id=isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
         if($user_id =="" && $asset_id ==""){
            $data = array("result" => "false", "msg" => "Input parameter missing!"); 
            die(json_encode($data));
        }



        if(!empty($user_id)){
                $this->db->select('*');
                $this->db->from("tbl_user");
                $this->db->where("id",$user_id);
                $this->db->where("(  find_in_set(".$asset_id.",devices_list ) ) and status='1' and user_type='2' " );
                $query = $this->db->get();
                foreach ($query->result() as $row){  
                      if ($row->devices_list != ""){ 
                        $temp_as = explode(',', $row->devices_list);
                        foreach ($temp_as as $key => $value) { 
                            if($value==$asset_id)  unset($temp_as[$key]);
                            //echo $value;
                        }
                        $temp_as = implode(',', $temp_as);                   
                        $this->db->where('id',$row->id); 
                        $this->db->set('devices_list', $temp_as, TRUE);
                        $this->db->update('tbl_user'); 
                    }  
                }
            }

        $data = array("result" => "true", "msg" => "User removed!");
        die(json_encode($data));

    }
}
