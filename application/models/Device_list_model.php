<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Device_list_model extends CI_Model {

    var $column_order = array("", 'device_name', 'device_id', 'assign_users', "last_date_time", 'device_model', 'sim_number', 'sim_account', 'sim_password', 'emergency_number', 'authorised_numbers', 'device_description'); //set column field database for order and search  
    var $order = array("device_name" => "ASC");

    function __construct() {
        parent::__construct();
    }

    function getdeviceList($user_id, $type = "0") {
        $columns = array('device_name', 'device_id', 'device_model', 'sim_account', 'sim_number', 'sim_password', 'emergency_number', 'authorised_numbers', 'device_description'); //set column field database for datatable orderable       
        $GLOBLE_SEARCH = $this->input->post('search');
        $GLOBLE_ORDER = $this->input->post('order');
        $global_search = $GLOBLE_SEARCH['value'];
        $this->db->select("devices_list");
        $this->db->from("tbl_user");
        $this->db->where("id", $user_id);
        $query = $this->db->get();
        $result = $query->result();
        $devices_list = explode(",", $result[0]->devices_list);
        if ($type == "1") {
            $subQuery = $this->db->query(" SELECT  GROUP_CONCAT(DISTINCT(devices_list)) as assined from tbl_user WHERE parent_id='" . $user_id . "'   ");
            foreach ($subQuery->result() as $row) {
                $assined = explode(",", $row->assined);
                if (count($assined) > 0 && $assined != $devices_list) {
                    $devices_list = array_diff($devices_list, $assined);
                }
            }
        }
        $this->db->select("(SELECT location_time FROM `tbl_last_locations` WHERE asset_id=tbl_devices.id limit 1) as last_date_time,tbl_devices.*,(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users,(SELECT GROUP_CONCAT(user_name)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users_name");
        $this->db->from('tbl_devices');
        $this->db->where("status", "1");
        if ($type == "0") {
            $this->db->where_in('tbl_devices.id', $devices_list);
        } else {
            $this->db->where_in('tbl_devices.id', $devices_list);
        }
        if (!empty($global_search)) {
            $this->db->group_start();
            foreach ($columns as $_key => $_value) {

                $this->db->or_like($_value, $global_search);
            }
            $this->db->group_end();
        }
        if (isset($_POST['order'])) { // here order processing
            if ($this->column_order[$_POST['order']['0']['column']] == "assign_users") {
                //(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list))
                $sls = "(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id,tbl_user.devices_list)) " . ($_POST['order']['0']['dir']) . "";
                $this->db->order_by($sls);
            } else {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
        }
        if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //echo $this->db->last_query(); 		
        return $query->result();
    }

    function get_model_list() {
        $this->db->select("*");
        $this->db->where("status", 1);
        $this->db->from("tbl_device_model_list");
        $this->db->order_by("model_value", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function get_recent_cmd_list($asset_id) {
        $this->db->select("thcr.command_value,thcr.add_date,thcr.device_id");
        $this->db->from("tbl_history_command_record as thcr");
        $this->db->where("td.id=", $asset_id);
        $this->db->join("tbl_devices td", " td.device_id=thcr.device_id", "INNER");
        $this->db->order_by("thcr.data_id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function countall($user_id, $type = "0") {
        $columns = array('device_name', 'device_id', 'device_model', 'sim_account', 'sim_number', 'sim_password', 'emergency_number', 'device_description'); //set column field database for datatable orderable
        $column_order = array('device_name');
        $GLOBLE_SEARCH = $this->input->post('search');
        $GLOBLE_ORDER = $this->input->post('order');
        $global_search = $GLOBLE_SEARCH['value'];
        $this->db->select("devices_list");
        $this->db->from("tbl_user");
        $this->db->where("id", $user_id);
        $query = $this->db->get();
        //echo $this->db->last_query();//die;
        $result = $query->result();
        $devices_list = explode(",", $result[0]->devices_list);
        if ($type == "1") {
            $subQuery = $this->db->query(" SELECT  GROUP_CONCAT(DISTINCT(devices_list)) as assined from tbl_user WHERE parent_id='" . $user_id . "'   ");
            // $query = $subQuery->get();
            foreach ($subQuery->result() as $row) {
                $assined = explode(",", $row->assined);
                if (count($assined) > 0 && $assined != $devices_list) {
                    $devices_list = array_diff($devices_list, $assined);
                }
            }
        }
        $this->db->select("(SELECT location_time FROM `tbl_last_locations` WHERE device_id=tbl_devices.device_id) as last_date_time,tbl_devices.*,(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users,(SELECT GROUP_CONCAT(user_name)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users_name");
        $this->db->from('tbl_devices');
        $this->db->where("status", "1");
        if ($type == "0") {
            $this->db->where_in('tbl_devices.id', $devices_list);
        } else {
            $this->db->where_in('tbl_devices.id', $devices_list);
        }
        //  $this->db->order_by('tbl_devices.device_name','asc');
        if (!empty($global_search)) {
            $this->db->group_start();
            foreach ($columns as $_key => $_value) {

                $this->db->or_like($_value, $global_search);

                /* if ($_value == 'device_name') {
                  $this->db->like('device_name', $global_search);
                  } */
            }
            $this->db->group_end();
        }

        return $this->db->count_all_results();
    }

    public function get($id) {
        $this->db->select('td.*, tu.devices_list, tu.user_name');
        $this->db->from('tbl_devices td');
        $this->db->join('tbl_user tu', 'td.id= tu.devices_list', 'left');
        $this->db->where("td.id", $id);

        $query = $this->db->get();
        return $query->result();
    }

    public function update($where, $data) {
        $this->db->update('tbl_devices', $data, $where);
    }

    public function getuser($id) {
        if ($id != '') {
            $this->db->select('tu.*, td.*');
            $this->db->from('tbl_user tu');
            $this->db->join('tbl_devices td', 'td.id= tu.devices_list', 'left');
            $this->db->where('find_in_set("' . $id . '", tu.devices_list)');
            $this->db->where('tu.status', '1');
            $query = $this->db->get();
        } else {
            $id = $this->session->userdata('user_id');
            $this->db->select("tbl_user.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) ) as assign_devices_name");
            $this->db->from('tbl_user');
            $this->db->where("(tbl_user.parent_id ='" . $id . "' or  tbl_user.id = '" . $id . "' )");

            $this->db->where('status', '1');
            $query = $this->db->get();
            //echo $this->db->last_query();
        }
        return $query->result();
    }

    public function delete($user_ids = array()) {
        foreach ($user_ids as $userid) {
            $this->db->delete('tbl_devices', array('id' => $userid));
        }
    }

}
