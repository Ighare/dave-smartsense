<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class User_list_model extends CI_Model {
    var $column_order = array("",'user_name','user_email','login_username','','','sim_account','user_password_read','user_password_read','sim_account','sim_password','sim_number','user_contact','user_note'); //set column field database for datatable searchable      
	var $order=array("user_name"=>"ASC");//when open grid it sorting by asc order added by Rajesh ighare
	function __construct() {
        parent::__construct();
    }
	
    function getmemberList($user_id, $member_id = "") {
        $columns = array('user_name', 'user_type','user_email','login_username','user_password_read','sim_account','sim_password','sim_number','user_contact','user_note'); //set column field database for datatable orderable
        $GLOBLE_SEARCH = $this->input->post('search');
        $GLOBLE_ORDER = $this->input->post('order');
        $global_search = $GLOBLE_SEARCH['value'];
        $this->db->select("tbl_user.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) ) as assign_devices_name");
        $this->db->from('tbl_user');
        $this->db->where("(tbl_user.parent_id ='" . $user_id . "' or  tbl_user.id = '" . $user_id . "' )");
        if ($member_id != "") {
            $this->db->where("tbl_user.id", $member_id);
        }
        if ($this->user_type == "1") {
            $this->db->where('user_type!=2');
        } else {
            //$this->db->where('user_type=2');
            
        }
        $this->db->where('status', '1');
        if (!empty($global_search)) {
			$this->db->group_start();
            foreach ($columns as $_key => $_value) {                
				$this->db->or_like($_value, $global_search);
            }
			$this->db->group_end();
        }
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
		if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order) ]);
        }
		
		if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
		
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    function countall($user_id, $member_id = "") {
        $columns = array('user_name', 'user_type','user_email','login_username','user_password_read','sim_account','sim_password','sim_number','user_contact','user_note'); //set column field database for datatable orderable
        $GLOBLE_SEARCH = $this->input->post('search');
        $GLOBLE_ORDER = $this->input->post('order');
        $global_search = $GLOBLE_SEARCH['value'];
        $this->db->select("tbl_user.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) ) as assign_devices_name");
        $this->db->from('tbl_user');
        $this->db->where("(tbl_user.parent_id ='" . $user_id . "' or  tbl_user.id = '" . $user_id . "' )");
        if ($member_id != "") {
            $this->db->where("tbl_user.id", $member_id);
        }
        if ($this->user_type == "1") {
            $this->db->where('user_type!=2');
        } else {
            //$this->db->where('user_type=2');
            
        }
        $this->db->where('status', '1');
        if (!empty($global_search)) {
			$this->db->group_start();
            foreach ($columns as $_key => $_value) {
                /*if ($_value == 'user_name') {
                    $this->db->like('user_name', $global_search);
                }
                if ($_value == 'user_type') {
                    $this->db->or_like('user_type', $global_search);
                }*/
				$this->db->or_like($_value, $global_search);
            }
			$this->db->group_end();
        }
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order) ]);
        }
        $query = $this->db->get();
        //$last = $this->db->last_query(); 
        return $query->num_rows();
    }
    public function get($id) {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function update($where, $data) {
        $this->db->update('tbl_user', $data, $where);
    }
    public function delete($user_ids = array()) {
        foreach ($user_ids as $userid) {
            $this->db->delete('tbl_user', array('id' => $userid));
        }
    }
}
