<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

	 
	function __construct(){
//$this->load->database('database2', TRUE);		
		parent::__construct(); 
	
	} 
	function valid_user($username,$password,$os_version="",$device_token="",$device_type=""){
		
    //Added By Ramesh. 21-11-2019.
	/*$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,API_URL.'login');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,"username=$username&password=$password");

	// Receive server response ...
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	
	curl_close ($ch);
	return $server_output;
	*/  
	 //End 21-11-2019.
	 
	 $this->db->select("tbl_user.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) ) as device_name");
     $this->db->from("tbl_user");
     $this->db->where("login_username",$username);
     $this->db->where("user_password",md5($password));
     $this->db->where("status","1"); 
     $this->db->limit(1);
     $user_data=$this->db->get(); 
	 //echo $this->db->last_query();
     $retutn_array=array();
     if($user_data->num_rows() > 0){
        $user_id=0;
        foreach ($user_data->result_array() as $value) {
           $retutn_array['user_id']=	$value['id'];
           $retutn_array['token']= md5(time()."_D@vE");
           $retutn_array['user_type']= $value['user_type']; 
           $retutn_array['is_superadmin']= $value['is_superadmin'];  

           $retutn_array['devices_list']= $value['devices_list']; 
           $retutn_array['devices_name']= $value['device_name']; 
            $this->db->insert('tc_user_token',array('user_id' => $value['id'],'token'=>$retutn_array['token'],'add_date'=>gmdate("Y-m-d H:i:s") ));


           $this->db->where('id',$value['id']);
           $update=array(
              'os_version'=>$os_version,
              'device_token'=>$device_token,
              'device_type'=>$device_type
          );
           $this->db->update('tbl_user',$update);
       }
       return $retutn_array;
   }else{
    return 0;
   }
   
}
function getmemberList($user_id,$member_id=""){ 
    $this->db->select("tbl_user.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) ) as assign_devices_name");
    $this->db->from('tbl_user'); 
    $this->db->where("(tbl_user.parent_id ='".$user_id."' or  tbl_user.id = '".$user_id."' )"); 
    if($member_id!=""){
       $this->db->where("tbl_user.id" ,$member_id);	  
    } 
    if($this->user_type =="1"){ 
       $this->db->where('user_type!=2');
    }else{
       //$this->db->where('user_type=2'); 
    }
   $this->db->where('status','1');
 
   $query = $this->db->get();        
		   // echo $this->db->last_query();
   $result = $query->result();
   $count_all = sizeof($result);
   $data = array();
   if(!empty($result))
   { 
    foreach ($result as $patient)
    {         
	//var_dump($patient->user_contacts);
        $row = array(); 
        $row['login_username'] = $patient->login_username;

        $row['user_name'] = $patient->user_name;
        $row['user_email'] = $patient->user_email; 
        $row['user_status'] = $patient->user_type; 
        $row['devices_list'] = $patient->devices_list;
        $row['assign_devices_name'] = $patient->assign_devices_name;
        $row['user_contact'] =trim($patient->user_contact);
        $row['user_note'] = $patient->user_note;
        $row['sim_number']= $patient->sim_number;
        $row['sim_account']= $patient->sim_account;
        $row['sim_password']= $patient->sim_password;  
        $row['id'] = $patient->id;
        $data[] = $row;  
    }
    return $data;        
} else {
   return "No Record Found.";
}
}
function lastlocationList($user_id,$member_id=""){ 
            //SELECT * FROM `tbl_last_locations` join tbl_user  on tbl_last_locations.device_id in (tbl_user.devices_list) and tbl_user.id=2
    $this->db->select("tbl_last_locations.*,tbl_devices.device_name,tbl_devices.id as asset_id");
    $this->db->from('tbl_last_locations'); 
    $this->db->join("tbl_devices","tbl_last_locations.device_id = tbl_devices.device_id ");
    if($user_id!="1"){
        $this->db->join("tbl_user"," find_in_set(tbl_devices.id, tbl_user.devices_list )  and tbl_user.id='".$user_id."'  ");
    }else{
        $this->db->join("tbl_user","find_in_set(tbl_devices.id, tbl_user.devices_list )");
    }

  
   $this->db->where('tbl_devices.status','1');
            //$this->db->where("tbl_user.parent_id" ,$user_id);
           /* if($member_id!=""){
            	$this->db->where("tbl_user.id" ,$member_id);	 
            } */
            $query = $this->db->get();       
		   // echo $this->db->last_query(); 
            $result = $query->result();
            $count_all = sizeof($result);
            $data = array();
            if(!empty($result))
            { 
                foreach ($result as $patient)
                {         
                    $row = array(); 
                    $row['device_name'] = $patient->device_name;
                    $row['asset_id'] = $patient->asset_id;

                    $row['location_time'] = $patient->location_time;
                    $row['address'] = $patient->address; 
                    $row['latitude'] = $patient->latitude;
                    $row['longitude'] = $patient->longitude;
                    $row['location_type'] = $patient->location_type;
                    $row['device_speed'] = $patient->device_speed;
                    $row['device_bettery'] = $patient->device_bettery;
                    $data[] = $row;
                }
                return $data;        
            } else {
            	return "No Record Found.";
            }
        }

        function alllastlocationList($device_id,$start_date,$end_date,$user_id){ 
            //SELECT * FROM `tbl_last_locations` join tbl_user  on tbl_last_locations.device_id in (tbl_user.devices_list) and tbl_user.id=2
            $this->db->select("count(*) as total");
            $this->db->from("tbl_location_history");
            $this->db->where('tbl_location_history.asset_id',$device_id);
            $this->db->where('tbl_location_history.location_time BETWEEN "'. date('Y-m-d H:i:s', strtotime($start_date)). '" and "'. date('Y-m-d H:i:s',strtotime($end_date)).'"');
            $query = $this->db->get();    
            $result = $query->result_array(); 
            $count = $result[0]['total'];
            $page = isset($_REQUEST["page"])?$_REQUEST["page"]:1; 
          
            $limit = isset($_REQUEST["limit"])?$_REQUEST["limit"]:100; 
            if( $count > 0 ) {
              $total_pages = ceil($count/$limit);
              $start = ($limit*$page) - $limit;  
            } else {
              $total_pages = 0;
              $start = 0;
            }
             
            if ($page > $total_pages) 
              $page = $total_pages;

            $this->db->select("tbl_location_history.*,device_name");
            $this->db->from('tbl_location_history');  
            $this->db->join("tbl_devices","tbl_location_history.asset_id = tbl_devices.id");
             if($user_id!="1"){
                 // $this->db->join("tbl_user"," find_in_set(tbl_devices.id, tbl_user.devices_list ) "); 
              }else{
               // $this->db->join("tbl_user","find_in_set(tbl_devices.id, tbl_user.devices_list )");
            }

            $this->db->where('tbl_location_history.asset_id',$device_id);
            $this->db->where('tbl_location_history.location_time BETWEEN "'. date('Y-m-d H:i:s', strtotime($start_date)). '" and "'. date('Y-m-d H:i:s',strtotime($end_date)).'"');
      			$this->db->where('tbl_devices.status','1');
      			$this->db->order_by("tbl_location_history.location_time", "desc");
            $this->db->limit(  $limit,$start);
            $query = $this->db->get();       
		    //echo $this->db->last_query();exit; 
            $result = $query->result();
            $count_all = sizeof($result);
            $data = array();
            if(!empty($result))
            { 
                foreach ($result as $patient)
                {         
                    $row = array(); 
                    $row['device_name'] = $patient->device_name;
                    $row['location_time'] = $patient->location_time;
                    $row['address'] = $patient->address; 
                    $row['latitude'] = $patient->latitude;
                    $row['longitude'] = $patient->longitude;
                    $row['location_type'] = $patient->location_type;
                    $row['device_speed'] = $patient->device_speed;
                    $row['device_bettery'] = $patient->device_bettery;
                    $data[] = $row;
                }
                $return['data']=$data;;;
                $return['page'] = $page;
                $return['total_pages'] = $total_pages;
                $return['count'] = $count;
                return $return;        
            } else {
            	return "No Record Found.";
            }
        }

        function data_history($device_id,$start_date,$end_date){ 
            //SELECT * FROM `tbl_last_locations` join tbl_user  on tbl_last_locations.device_id in (tbl_user.devices_list) and tbl_user.id=2
            $this->db->select("tbl_raw_data.*,tbl_devices.device_name");
            $this->db->from('tbl_raw_data');  
            $this->db->join("tbl_devices","tbl_raw_data.asset_id = tbl_devices.id ");

            $this->db->where('tbl_raw_data.asset_id',$device_id);
            $this->db->where('tbl_devices.status',1);
            $this->db->where('tbl_raw_data.add_date BETWEEN "'. date('Y-m-d H:i:s', strtotime($start_date)). '" and "'. date('Y-m-d H:i:s', strtotime($end_date)).'"');
			$this->db->order_by("tbl_raw_data.add_date", "desc");
            $query = $this->db->get(); 
		  //  echo $this->db->last_query();die();  
            $result = $query->result();
            $count_all = sizeof($result);
            $data = array();
            if(!empty($result))
            { 
                foreach ($result as $patient)
                {        
                    $row = array(); 
                    $row['device_name'] = $patient->device_name;
                    $row['data'] = $patient->log_data;
                    $row['time'] = $patient->add_date;  
                    $data[] = $row;
                }
                return $data;        
            } else {
            	return "No Record Found.";
            }
        }
		function get_device($user_id,$device_id){
			$this->db->select("id,device_id,timezone,device_name,device_number,device_description,expiry_date,device_model,sim_account,sim_number,sim_password,emergency_number,address,device_status,authorised_numbers,add_date,add_uid,(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE  user_type='2' and  FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assigned_users_id,(SELECT GROUP_CONCAT(user_name)as assign_users FROM `tbl_user` WHERE user_type='2' and  FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assigned_users_names");
            $this->db->from('tbl_devices');
            $this->db->where('id',$device_id); 
            $this->db->or_where('id',$device_id); 
			$query = $this->db->get();         
            $result = $query->result();
			$data = array();
			if($query->num_rows()){
				return $query->row();
			}
			return 0;
		}
		function get_user($user_id,$user_id_sub){
			$this->db->select("login_username,user_email,user_name,user_last_name,devices_list as assigned_devices,user_password_read,user_contact,parent_id,sim_number,sim_account,sim_password,user_note,user_status,user_type,user_token,device_token,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) and tbl_devices.status=1 ) as assign_devices_name"); 
            $this->db->from('tbl_user');
            $this->db->where('id',$user_id_sub);  
			$query = $this->db->get(); 		
            $result = $query->result();
			$data = array();
			if($query->num_rows()){
				return $query->row();
			}
			return 0;
		} 
        function getgeoList($user_id){ 
            $this->db->select(" tbl_geofence.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,tbl_geofence.assigned_devices) ) as devices");
            $this->db->from('tbl_geofence');   
            if($user_id!="1"){
                $this->db->where('tbl_geofence.add_uid',$user_id); 
            } 
            $this->db->where('tbl_geofence.status','1');
           // $this->db->join('tbl_devices','tbl_devices.device_name tbl')
            $query = $this->db->get(); 
			//echo $this->db->last_query();exit; 
            $result = $query->result(); 
            $count_all = sizeof($result);
            $data = array();
            if(!empty($result))
            { 
                foreach ($result as $patient)
                {         
                    $row = array(); 
                    $row['geo_id'] = $patient->id;
                    $row['name'] = $patient->geofence_name;
                    $row['address'] = $patient->geofence_address;
                    $row['latitude'] = $patient->geofence_latitude;  
                    $row['longitude'] = $patient->geofence_longitude;  
                    $row['in_alert'] = $patient->geofence_in_alert;  
                    $row['out_alert'] = $patient->geofence_out_alert;  
                    $row['radius'] = $patient->geofence_radius;  
                    $row['assigned_devices'] = $patient->assigned_devices;  
                    $row['assigned_devices_names']=$patient->devices;
                    $data[] = $row;
                } 
                return $data;        
            } else {
            	return "No Record Found.";
            }
        }

        function getdeviceList($user_id,$type="0"){
            //if($user_id!="1"){			
			
                $this->db->select("devices_list");
                $this->db->from("tbl_user");
                $this->db->where("id",$user_id);
                $query = $this->db->get();        
                $result = $query->result();				
                $devices_list=explode(",",$result[0]->devices_list);
                if($type=="1"){
                            $subQuery= $this->db->query("SELECT  GROUP_CONCAT(DISTINCT(devices_list)) as assined from tbl_user WHERE parent_id='".$user_id."'");
                            foreach ( $subQuery->result() as  $row) {
                                $assined=explode(",",$row->assined);
								//added by Rajesh to remove empty array
								foreach($assined as $key => $value)          
								if(empty($value)) 
								unset($assined[$key]);
								//end by rajesh
								//var_dump($assined); 
                                if(count($assined )>0 && $assined!=$devices_list){							
                              $devices_list=array_diff($devices_list,$assined);
							  //var_dump($devices_list);
                                }
                             }
							 } 					
                    $this->db->select("(SELECT location_time FROM `tbl_last_locations` WHERE device_id=tbl_devices.device_id limit 1) as last_date_time,tbl_devices.*,(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users,(SELECT GROUP_CONCAT(user_name)as assign_users FROM `tbl_user` WHERE user_type='2' and FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users_name");
                    $this->db->from('tbl_devices'); 
                    $this->db->where("status","1");
                    if($type=="0"){
                          $this->db->where_in('tbl_devices.id',$devices_list); 
                    }else{
                          $this->db->where_in('tbl_devices.id',$devices_list); 

                    }
           /* }else{
                $this->db->select("tbl_devices.*,(SELECT GROUP_CONCAT(id)as assign_users FROM `tbl_user` WHERE FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users ,(SELECT GROUP_CONCAT(user_name)as assign_users FROM `tbl_user` WHERE FIND_IN_SET(tbl_devices.id, tbl_user.devices_list)) as assign_users_name");
                $this->db->from('tbl_devices');
            	if($type=="1"){ 
                      $this->db->where('!FIND_IN_SET(id,(SELECT GROUP_CONCAT(devices_list) FROM `tbl_user` WHERE devices_list)) ');   
 
                }
                $this->db->where("status","1");  
            }*/
            $this->db->order_by('tbl_devices.device_name','asc');
			
            $query = $this->db->get();    
			//echo $this->db->last_query();exit; 
            $result = $query->result();
            $count_all = sizeof($result);
            $data = array();
            if(!empty($result))
            { 
                foreach ($result as $patient)
                {         
                    $row = array(); 
                    $row['device_id'] = $patient->device_id;
                    $row['last_date_time'] = $patient->last_date_time;
                    $row['asset_id'] = $patient->id; 
                    $row['name'] = $patient->device_name;
                    $row['device_number'] = $patient->device_number;  
                    $row['device_description'] = $patient->device_description;  
                    $row['expiry_date'] = $patient->expiry_date;  
                    $row['device_model'] = $patient->device_model;

 
                    $row['sim_number'] = $patient->sim_number;  
                    $row['sim_account'] = $patient->sim_account;  
                    $row['sim_password'] = $patient->sim_password;  
                    $row['emergency_number'] = $patient->emergency_number;  

                    $row['device_status'] = $patient->device_status; 
                    $row['address'] = $patient->address;  
                    $row['assigned_users'] = (empty($patient->assign_users)? '' :$patient->assign_users);   
                    $row['assigned_users_name'] =  (empty($patient->assign_users_name)? '' :$patient->assign_users_name);   
                     
                    $row['authorised_numbers'] = $patient->authorised_numbers;  
                    $row['timezone'] = $patient->timezone;  


                    $data[] = $row;
                }
                return $data;        
            } else {
                return "No Record Found.";
            }
        }

         function add_device_model($data,$assigned_users){
             $this->db->insert("tbl_devices",$data);
             $insert_id=$this->db->insert_id();
             $user_id=$data['add_uid'];
             if(!empty($insert_id) ){
                $this->db->select('*');
                $this->db->from("tbl_user");
                if($assigned_users !=""){
                    $this->db->where("( id in(".$assigned_users." ) or id='".$user_id."' ) and status='1' " );
                }else{
                    $this->db->where(" id='".$user_id."' and status='1' " );
                }
                $query = $this->db->get();
                foreach ($query->result() as $row){
                    if ($row->devices_list != ""){
                        $temp_as = explode(',', $row->devices_list);
                        if (!in_array($insert_id, $temp_as)){                   
                            $this->db->where('id',$row->id);
                            $this->db->set('devices_list', 'CONCAT(devices_list,\','.$insert_id.'\')', FALSE);
                            $this->db->update('tbl_user');
                        }               
                    }else {
                        $this->db->where('id',$row->id);
                        $this->db->set('devices_list',$insert_id);
                        $this->db->update('tbl_user');
                    }
                   // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
                }
                 $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
             }else{

                  $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
             }
             return $data;
        }
        function edit_device_model($data,$id,$assigned_users,$user_id){
            $this->db->where('id',$id);
            $this->db->update("tbl_devices",$data); 
			
             $insert_id=$id;
             if(!empty($insert_id)){
                $this->db->select('*');
                $this->db->from("tbl_user");
                $this->db->where("(  find_in_set(".$id.",devices_list ) ) and status='1' and user_type='2' " );
                $query = $this->db->get();
                foreach ($query->result() as $row){  
                      if ($row->devices_list != ""){ 
                        $temp_as = explode(',', $row->devices_list);
                        foreach ($temp_as as $key => $value) { 
                            if($value==$id)  unset($temp_as[$key]);
                            //echo $value;
                        }
                        $temp_as = implode(',', $temp_as);                   
                        $this->db->where('id',$row->id); 
                        $this->db->set('devices_list', $temp_as, TRUE);
                        $this->db->update('tbl_user'); 
                    }  
                   // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
                }


                $this->db->select('*');
                $this->db->from("tbl_user");
                 if($assigned_users !=""){
                    $this->db->where("( id in('".$assigned_users."' ) or id='".$user_id."' ) and status='1' " );
                }else{
                    $this->db->where(" id='".$user_id."' and status='1' " );
                }
                //$this->db->where("(  id in(".$assigned_users." ) ) and status='1' " );
                $query = $this->db->get();
                foreach ($query->result() as $row){ 
                    if ($row->devices_list != ""){ 
                        $temp_as = explode(',', $row->devices_list);

                        if (!in_array($insert_id, $temp_as)){                    
                            $this->db->where('id',$row->id);
                            $this->db->set('devices_list', 'CONCAT(devices_list,\','.$insert_id.'\')', FALSE);
                            $this->db->update('tbl_user');
                        }               
                    }else {  
                        $this->db->where('id',$row->id); 
                        $this->db->set('devices_list',$insert_id);
                        $this->db->update('tbl_user');
                    }
                   // $data = array("result" => "true", "msg" => "The Device added successfully.","device_id"=>$insert_id);
                }
                 $data = array("result" => "true", "msg" => "The Device updated successfully.","device_id"=>$id);
             }else{
                $this->db->where('id',$id);
                $this->db->update('tbl_user',array('devices_list'=>$id));
                 $data = array("result" => "true", "msg" => "The Device updated successfully.","device_id"=>$id);
             } 
             return $data;
        }
        function test(){
            echo $this->user_type;
        }
    }