<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curd_model extends CI_Model {

	 
	function __construct(){ 
		parent::__construct(); 
	} 
	
	//Added By Ramesh. 21-11-2019.
	function do_operation($post , $api){
	//var_dump($post);	
	//echo API_URL . $api;
	//die;
		$post['token'] = $this->session->userdata('token');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,API_URL.$api);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		
		
		curl_close ($ch);
		return $server_output;
	}
	
	function decode_json($result){
		return json_decode($result);
	}
	//End 21-11-2019.
}