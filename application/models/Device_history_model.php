<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Device_history_model extends CI_Model { 
	 var $column_order = array('tbl_raw_data.add_date','log_data'); //set column field database for datatable orderable  
	var $order=array("tbl_raw_data.add_date"=>"desc");
	
    function __construct() { 
        parent::__construct(); 
    }
	function get_device_list($user_id, $type = "0"){
		$this->db->select("devices_list");
        $this->db->from("tbl_user");
        $this->db->where("id", $user_id);
        $query = $this->db->get();       
        $result = $query->result();
        $devices_list = explode(",", $result[0]->devices_list);
		$this->db->select("tbl_devices.id,tbl_devices.device_name,tbl_devices.device_id");
        $this->db->from('tbl_devices');
        $this->db->where("status", "1");
        if ($type == "0") {
            $this->db->where_in('tbl_devices.id', $devices_list);
        } else {
            $this->db->where_in('tbl_devices.id', $devices_list);
        }  
		$this->db->order_by("tbl_devices.device_name", "asc");
		$query = $this->db->get(); 			
        return $query->result();
	}
	
    function getdeviceList($user_id,$device_id,$start_date,$end_date) {
        $GLOBLE_SEARCH = $this->input->post('search');
        $GLOBLE_ORDER = $this->input->post('order');
        $global_search = $GLOBLE_SEARCH['value'];
         
        $this->db->select("tbl_raw_data.*,tbl_devices.device_name");
        $this->db->from('tbl_raw_data');       
		$this->db->join("tbl_devices","tbl_raw_data.asset_id = tbl_devices.id ");
		$this->db->where('tbl_raw_data.asset_id',$device_id);
        $this->db->where('tbl_devices.status',1);
		$this->db->where('tbl_raw_data.add_date BETWEEN "'. date('Y-m-d H:i:s', strtotime($start_date)). '" and "'. date('Y-m-d H:i:s', strtotime($end_date)).'"');
             
        if (!empty($global_search)) {
			$this->db->group_start();
            foreach ($columns as $_key => $_value) {			
				 $this->db->or_like($_value, $global_search); 
				
            }
			$this->db->group_end();
        }
        if (isset($_POST['order'])) // here order processing
        {
			 $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
		if (isset($this->order)) {
           $order = $this->order;		 
           $this->db->order_by(key($order), $order[key($order) ]);
        }
		//$this->db->order_by("tbl_raw_data.add_date", "desc");
		if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
		
        $query = $this->db->get(); 	
//echo $this->db->last_query();		
        return $query->result();
    }
    function countall($user_id,$device_id,$start_date,$end_date) {
        $columns = array('device_name','device_id','device_model','sim_account','sim_number','sim_password','emergency_number','device_description'); //set column field database for datatable orderable
		$order=array("tbl_raw_data.add_date"=>"desc");
        $column_order = array('device_name');
        $GLOBLE_SEARCH = $this->input->post('search');
        $GLOBLE_ORDER = $this->input->post('order');
        $global_search = $GLOBLE_SEARCH['value'];
        
       $this->db->select("tbl_raw_data.*,tbl_devices.device_name");
        $this->db->from('tbl_raw_data');       
		$this->db->join("tbl_devices","tbl_raw_data.asset_id = tbl_devices.id ");
		$this->db->where('tbl_raw_data.asset_id',$device_id);
        $this->db->where('tbl_devices.status',1);
		$this->db->where('tbl_raw_data.add_date BETWEEN "'. date('Y-m-d H:i:s', strtotime($start_date)). '" and "'. date('Y-m-d H:i:s', strtotime($end_date)).'"');
		
        if (!empty($global_search)) {
			$this->db->group_start();
            foreach ($columns as $_key => $_value) {
                
				 $this->db->or_like($_value, $global_search);
				 
				/*if ($_value == 'device_name') {
                    $this->db->like('device_name', $global_search);
                }*/
            }
			$this->db->group_end();
        }
        
		return $this->db->count_all_results();
		
    }
    public function get($id) {
        $this->db->select('td.*, tu.devices_list, tu.user_name');
        $this->db->from('tbl_devices td');
        $this->db->join('tbl_user tu', 'td.id= tu.devices_list','left');
        $this->db->where("td.id", $id);
		
        $query = $this->db->get();
        return $query->result();
    }
    public function update($where, $data) {
        $this->db->update('tbl_devices', $data, $where);
    }
    public function getuser($id) {
	if($id != ''){
        $this->db->select('tu.*, td.*');
        $this->db->from('tbl_user tu');
        $this->db->join('tbl_devices td', 'td.id= tu.devices_list','left');
		 $this->db->where('find_in_set("'.$id.'", tu.devices_list)');
         $this->db->where('tu.status', '1');
		$query = $this->db->get();
	}
	else
	{
		$id = $this->session->userdata('user_id');
		$this->db->select("tbl_user.*,(SELECT GROUP_CONCAT(tbl_devices.device_name) as devices from tbl_devices WHERE find_in_set(id,devices_list) ) as assign_devices_name");
        $this->db->from('tbl_user');
        $this->db->where("(tbl_user.parent_id ='" . $id . "' or  tbl_user.id = '" . $id . "' )");
        
        $this->db->where('status', '1');
        $query = $this->db->get();
	//echo $this->db->last_query();
	}
        return $query->result();
    }
    public function delete($user_ids = array()) {
        foreach ($user_ids as $userid) {
            $this->db->delete('tbl_devices', array('id' => $userid));
        }
    }
}
