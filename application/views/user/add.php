<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
  <!-- Content Wrapper. Contains page content -->
<div class="panel panel-inverse" >
   <div class="panel-heading">
     
      <h4 class="panel-title">Add User</h4>
   </div>
   <!-- begin panel-body -->
   <div class="panel-body">
   
      <?php
		if($this->session->userdata('is_superadmin') == 1){
			$user_type_option = "<option value='1'>Admin</option>";
		}else if($this->session->userdata('user_type') == 1){
			$user_type_option = "<option value='2'>User</option>";
		} 
		
	  ?>
	  <div class="alert alert-danger print-error-msg" style="display:none"></div>
      <form data-parsley-validate="parsley" id="add_form" method="post" action="javascript:void(0);" enctype="multipart/form-data">
         <input type="hidden" value="" id="id" name="" >
		 <input type="hidden"   id="token" name="token"  value="<?php echo $this->session->userdata('token');?>">
         <!-- /.box-header -->
         <div class="box-body">
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Name <span class="text-red">*</span></label>
                     <input type="text" class="form-control" name="name" id="user_name" placeholder="Name"  required="true">
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Email Address <span class="text-red">*</span></label>
                     <input type="email" class="form-control" name="email_id" id="user_email" placeholder="Email Address" required="true" >
                  </div>
               </div>
            </div>
             <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Login <span class="text-red">*</span></label>
                     <input type="text" class="form-control" name="login" id="login_username" placeholder="Login" required="true">
                  </div>
               </div> 
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Password <span class="text-red">*</span></label>
                     <input type="text" class="form-control" name="password" id="user_password" placeholder="Password"  required="true">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label >User type</label>
                     <select class="form-control" id="status" name="status" placeholder="User type">
					  <?php echo $user_type_option; ?>
					 </select>
                  </div>
               </div> 
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Contact Detail</label>
                    <!--- <input type="text" class="form-control" name="emergency_number" id="emergency_number" placeholder="Contact Detail" maxlength ='20' data-parsley-minlength="6" onkeypress='onlyNumber(event)'>--->
					  <textarea class="form-control"  name="emergency_number" id="emergency_number"  rows="3" placeholder="Contact Detail"></textarea> 
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>SIM Number(s)</label>
                     <input type="text" class="form-control"  name="sim_number" id="sim_number"  placeholder="SIM Number(s)" onkeypress='onlyNumber(event)'>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>SIM Account</label>
                     <input type="text" class="form-control"  name="sim_account" id="sim_account"  placeholder="SIM Account" onkeypress='onlyNumber(event)'>
                  </div>
               </div>
            </div> 
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>SIM Password</label>
                     <input type="text" class="form-control"  name="sim_password" id="sim_password"  placeholder="SIM Password">
                  </div>
               </div>
			   
               <div class="col-md-6">
                  <div class="form-group">
                     <label>User Assigned To</label>
					 <input type="hidden" name="device_assign" id="device_assign" value=""/>
                     <select name="_device_assign[]" class="_device_assign form-control select2" id="_device_assign" multiple="multiple"></select>
                  </div>
               </div> 			  
            </div>
			<div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                    <label>Notes</label>
                     <textarea class="form-control"  name="notes" id="notes"  rows="3" placeholder="Notes"></textarea> 
                  </div>
               </div> 
			</div>  
         </div>
         <!-- /.box-body -->

   <div class="row" style="display:inline; text-align: center;">
   <div class="box-footer">
   <button type="submit" class="btn btn-primary">ADD</button>
    <button type="button" value="Cancel" onClick="window.location='<?php echo site_url('user_list'); ?>';" class="btn btn-secondary">Cancel</button>
   </div>
   </div>
   </div>
   </form>
</div>

<link href="<?php echo base_url(); ?>assets/plugins/parsley/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/plugins/parsley/parsley.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
     $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	
	$("#_device_assign").select2({ 
            placeholder: "select",
    });

	$("#_device_assign").on("change",function(e){
		$("#device_assign").val($("#_device_assign").val());
	});	
	
	var API_URL = '<?php echo API_URL.'deviceList_unassigned'; ?>';
	var result_html;
	$.post(API_URL, {token: '<?php echo $this->session->userdata("token");?>' }, function (response) {
		
		var obj = JSON.parse(response);
		
		if (obj.result == "true") {
				var i = 0;
				$(obj.data).each(function () {
					result_html += '<option value="' +obj.data[i]['asset_id']+ '">' +obj.data[i]['name']+ '</option>';
					i++;
				});
		} else {
			result_html = "<option value=''> select </option>";
		}
		
		$('#_device_assign').html(result_html);
		$("#_device_assign").trigger("change");
		
		
	}).fail(function (response) {
	  
	   
		alert('Error: ' + response.responseText);
	});
    
    $("#add_form").on('submit', function (event) {  
        // validate form with parsley.
           
            $(this).parsley().validate();
            $(".print-error-msg").show();
            // if this form is valid
            if ($(this).parsley().isValid()) {
                // show alert message
                $(".print-error-msg").hide();
                  var id = $("#id").val();
				
				
				var API_URL = '<?php echo API_URL.'add_member'; ?>';
				$.post(API_URL, $("#add_form").serialize(), function (data) {
                    
					var obj = JSON.parse(data);
                    if (obj.result == "true") {
                        reload_grid();
						alert(obj.msg, "success");
					} else {
                        if (id != "") {
						    alert(obj.msg, "warning");
                        } else {
                            alert(obj.msg, "warning");
                        }
                    }
				}).fail(function (response) {
                   alert('Error: ' + response.responseText);
                });
            }else{
                $(".print-error-msg").show();
            }

            event.preventDefault();
        });

    });
 function reload_grid(){
        $('#user_form_div').hide();
		$('#user_div').show();
        table.ajax.reload( null, false );
  }
  
function onlyNumber(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
</script> 
<style>
.select2-container--default .select2-selection--multiple .select2-selection__choice{
	       background-color: #348fe2;	
} 
</style>
<!-- ================== END PAGE LEVEL JS ================== -->

   
