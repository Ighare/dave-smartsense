<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">


  <!-- Content Wrapper. Contains page content -->
<div class="panel panel-inverse" >
   <div class="panel-heading">
     
      <h4 class="panel-title">Edit User</h4>
   </div>
   <!-- begin panel-body -->
   <div class="panel-body">
   <?php //var_dump($users);?>
   
	<?php $var = $users->data->user_type;
    if ($var == 1) {
        $text = "Admin";
    } else {
        $text = "User";
    }
	
	if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') {
		$is_can_edit = "";
	} else { 
		$is_can_edit = "readonly";
	}	
		
?>
     <p><?php echo $this->session->flashdata('success'); ?></p>
        <div class="alert alert-danger print-error-msg" style="display:none"></div>
      <form data-parsley-validate="parsley" id="update_form"  method="post" action="javascript:void(0);" enctype="multipart/form-data">
         <input type="hidden" value="<?php echo $id ?>" id="id" name="id" >
         <input type="hidden" value="<?php echo $id ?>" id="update_id" name="update_id" >
		 <input type="hidden"   id="token" name="token"  value="<?php echo $this->session->userdata('token');?>">
         <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
	<?php if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') { ?>
                     <label >Name <span class="text-red">*</span></label>
                     <input type="text" class="form-control" value="<?php echo $users->data->user_name ?>" name="name" id="user_name" placeholder="Name"  required="true">
                     <?php
    } else { ?>
                       <label >Name</label>
                      <input type="text" readonly class="form-control" value="<?php echo $users->data->user_name ?>" name="name" id="user_name" placeholder="Name"  required="true">
                     <?php
    } ?> 
                    
                     
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Email Address <span class="text-red">*</span></label>
                     <input type="email" class="form-control" value="<?php echo $users->data->user_email ?>" name="email_id" id="user_email" placeholder="Email Address" required="true">
                  </div>
               </div>
            </div>
             <div class="row">
               
			   <div class="col-md-6">
                  <div class="form-group">
                     <label >Login <span class="text-red">*</span></label>
                     <input type="text" <?php echo $is_can_edit;?> class="form-control" value="<?php echo $users->data->login_username ?>" name="login" id="login_username" placeholder="Login"  required="true">
                  </div>
               </div>
			   
               <div class="col-md-6">
                  <div class="form-group">
                     <label >Password <span class="text-red">*</span></label>
                     <input type="text" class="form-control" value="<?php echo $users->data->user_password_read ?>" name="password" id="user_password" placeholder="Password"  required="true">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                   <?php if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') { ?>
                     <label >User type</label>
                     <input type="text"  class="form-control" readonly value="<?php echo $text ?>" name="user_type" id="user_type" placeholder="User type" >
                    <?php
    } else { ?>
      <label >User type</label>
<input type="text" readonly class="form-control" value="<?php echo $text ?>"  name="user_type" id="user_type" placeholder="User type" >
                     <?php
    } ?> 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
				  <!--onkeypress='onlyNumber(event)'-->
                     <label >Contact Detail</label>
                    <!-- <input type="text" class="form-control" value="<?php echo $users->data->user_contact ?>"  name="user_contact" id="user_contact" placeholder="Contact Detail" >--->
					  <textarea class="form-control"  name="emergency_number" id="emergency_number"  rows="3" placeholder="Contact Detail"><?php echo $users->data->user_contact ?></textarea> 
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                       <?php if ($this->session->userdata("user_type") == '2') { ?>
                     <label>SIM Number(s)</label>
                     <input type="text" readonly class="form-control" value="<?php echo $users->data->sim_number ?>" onkeypress='onlyNumber(event)'  name="sim_number" id="sim_number"  placeholder="SIM Number(s)">
                     <?php
    } else { ?>
                      <label>SIM Number(s)</label>
                      <input type="text" class="form-control" value="<?php echo $users->data->sim_number ?>" onkeypress='onlyNumber(event)'  name="sim_number" id="sim_number"  placeholder="SIM Number(s)">
                       <?php
    } ?>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                    <?php if ($this->session->userdata("user_type") == '2') { ?>
                     <label>SIM Account</label>
                     <input type="text" readonly class="form-control" value="<?php echo $users->data->sim_account ?>" onkeypress='onlyNumber(event)'  name="sim_account" id="sim_account"  placeholder="SIM Account">
                   <?php
    } else { ?>
                    <label>SIM Account</label>
                     <input type="text" class="form-control" value="<?php echo $users->data->sim_account ?>" onkeypress='onlyNumber(event)'  name="sim_account" id="sim_account"  placeholder="SIM Account">
                       <?php
    } ?>
                  </div>
               </div>
            </div> 
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
	<?php if ($this->session->userdata("user_type") == '2') { ?>
                     <label>SIM Password</label>
                     <input type="text" readonly class="form-control" value="<?php echo $users->data->sim_password ?>"  name="sim_password" id="sim_password"  placeholder="SIM Password">
                      <?php
    } else { ?>
                      <label>SIM Password</label>
                     <input type="text" class="form-control" value="<?php echo $users->data->sim_password ?>"  name="sim_password" id="sim_password"  placeholder="SIM Password">
                     <?php
    } ?>
                  </div>
               </div>
			   <?php if($users->data->user_type !=='1'){?>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>User Assigned To</label>
					 <input type="hidden" name="device_assign" id="device_assign" value="<?php echo $users->data->assigned_devices;?>"/>
					 <?php if($is_can_edit == "") {?>
                     <select name="_device_assign[]" class="_device_assign form-control select2" id="_device_assign" multiple="multiple"></select>
					 <?php }else{ ?>
					 <input type="text" class="form-control" name="__device_assign" <?php echo $is_can_edit;?>  value="<?php echo $users->data->assign_devices_name;?>"/>
					 <?php } ?>
					 
                  </div>
				</div>
			   <?php } ?>
			   <?php if($is_can_edit == ""){?>
			   <div class="col-md-6">
                  <div class="form-group">
					<label>Notes</label>
                     <textarea <?php echo $is_can_edit;?> class="form-control" 
					  name="notes" id="notes"  rows="3" placeholder="Notes"><?php echo $users->data->user_note; ?></textarea>
				 </div>
               </div>
			   <?php } ?>
			   
            </div>
			
         </div> 
         <!-- /.box-body -->

   <div class="row" style="display:inline; text-align: center;">
   <div class="box-footer">
   <button type="submit" class="btn btn-primary">Update</button>
   <button type="button" value="Cancel" onClick="window.location='<?php echo site_url('User_list'); ?>';" class="btn btn-secondary">Cancel</button>
   </div>
   </div>
   </div>
   </form>
</div>

<link href="<?php echo base_url(); ?>assets/plugins/parsley/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/plugins/parsley/parsley.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $("#_device_assign").select2({  
            placeholder: "Please Select",
			 width: 'auto;' 
    });
	
	
	$("#_device_assign").on("change",function(e){
		$("#device_assign").val($("#_device_assign").val());
	});	
	
	var API_URL = '<?php echo API_URL.'deviceList_unassigned'; ?>';
	var result_html;
	var Assign_devices_ar = [];
	$.post(API_URL, {token: '<?php echo $this->session->userdata("token");?>' }, function (response) {
		
		var obj = JSON.parse(response);
		
		if (obj.result == "true") {
				var i = 0;
				var Assign_devices = '<?php echo trim($users->data->assigned_devices);?>';
				
				if(Assign_devices !== ""){
					Assign_devices_ar = Assign_devices.split(',');
				}
				
				var Assign_devices_name = '<?php echo $users->data->assign_devices_name;?>';
				var Assign_devices_name_ar = Assign_devices_name.split(',');
				
				
				if(Assign_devices_ar !== "" && Assign_devices_ar.length !== 0){
					for(var j =0 ;j<Assign_devices_ar.length;j++){
						result_html += '<option value="' +Assign_devices_ar[j]+ '" selected>' +Assign_devices_name_ar[j]+ '</option>';
					}
					
				}
				
				$(obj.data).each(function () {
					
					result_html += '<option value="' +obj.data[i]['asset_id']+ '">' +obj.data[i]['name']+ '</option>';
					i++;
				});
		} else {
			result_html = "<option value=''> Please Select </option>";
		}
		
		$('#_device_assign').html(result_html);
		$("#_device_assign").trigger("change");
		
		
	}).fail(function (response) {
		alert('Error: ' + response.responseText);
	});	
    
    $("#update_form").on('submit', function (event) {  
        // validate form with parsley.
           
            $(this).parsley().validate();
            $(".print-error-msg").show();
            // if this form is valid
            if ($(this).parsley().isValid()) {
                // show alert message
                $(".print-error-msg").hide();
                  var id = $("#id").val();
              
               var API_URL = '<?php echo API_URL.'edit_member'; ?>';
			    $.post(API_URL, $("#update_form").serialize(), function (data) {
                    
					var obj = JSON.parse(data);
                    if (obj.result == "true") {
                        reload_grid();
						alert(obj.msg, "success");
					} else {
                        if (id != "") {
						    alert(obj.msg, "warning");
                        } else {
                            alert(obj.msg, "warning");
                        }
                    }
					/*if ($.trim(data)) {
                        $("#user_form_div").html(data);

                    } else {
                        console.log(id);
                        if (id != "") {
                            reload_grid();
                            alert("Good job!", "User Updated successfully.", "success");
                        } else {
                            reload_grid();
                            alert("Good job!", "User Updated successfully.", "success");
                        }
                       
                        
                    }*/
                }).fail(function (response) {

                    alert('Error: ' + response.responseText);
                });
            }else{
                $(".print-error-msg").show();
              
            }

            event.preventDefault();
        });

    });
 function reload_grid(){
        $('#user_form_div').hide();
    $('#user_div').show();
        table.ajax.reload( null, false );
  }
 function onlyNumber(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 
</script>
<style>
.select2-container--default .select2-selection--multiple .select2-selection__choice{
	    background-color: #348fe2;	
} 
</style>
<!-- ================== END PAGE LEVEL JS ================== -->

   
