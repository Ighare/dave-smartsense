<!--<link href="<?php //echo base_url(); ?>assets/plugins/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="<?php //echo base_url(); ?>assets/plugins/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" /> 

<link href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css" rel="stylesheet" /> -->

 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header.php");
?>
<style type="text/css">
  .form-inline{display: block !important}
  
  body .dataTables_wrapper table.dataTable{
	/* margin: 0 0!important; */
  }
  
  tbody>tr.even>:nth-child(2) , tbody>tr.even>:nth-child(1){
	    background: #ffffff;
	}
	
	tbody>tr.old>:nth-child(2) , tbody>tr.old>:nth-child(1){
	    background: #EAF3D9;
	}
	
  tbody#show_data td {
    height: 18px;
    padding-top: 5px;
    padding-bottom: 5px;
}
	.top-l , .top-r {
		width:50%;
	}
	.top-l {
		float:left;
	}
	.top-r {
		float:right;
	}
	#table_filter {
		width:50%;
		float: left;
	}
	#table_paginate {
		width:50%;
		float: right;
	}
	
	#table_length {
		margin-left: 15px;
	}
	table.dataTable tbody th,
table.dataTable tbody td {
    white-space: nowrap;
}
 .DTFC_LeftBodyLiner{
    overflow-x: hidden;
  }
</style>
<script type="text/javascript">
   function add()
    { 

        $('#user_div').hide();
        $('#user_form_div').show();
        $('#user_form_div').load('<?php echo base_url() ?>index.php/User_list/add');
    }
    function edit()
    {
        //alert("Under development!");
		//return false;
		
		var ischecked = false;
        var hk_count = 0;
        $('[id^=b_id_]').each(function () {
            if ($(this).prop("checked") == true)
            {
                hk_count = hk_count + 1;
            }
        });
        if (hk_count == 0) {
            alert("Please select atleast one record");
            return false;
        } else if (hk_count == 1) {
            ischecked = true;
        } else if (hk_count > 1) {
            alert("Please select only one record");
            return false;
        }
        if (ischecked == true)
        {

         
            var id = "";
            $('[id^=b_id_]').each(function () {
                if ($(this).prop("checked") == true)
                {
                    id += $(this).prop("id").replace('b_id_', '');
                }
            });
            $('#user_div').hide();
            $('#user_form_div').show();
            $('#user_form_div').load('<?php echo base_url() ?>index.php/User_list/edit/' + id);
          
        }
		
		/*$('#user_div').hide();
        $('#user_form_div').show();
        $('#user_form_div').load('<?php echo base_url() ?>index.php/User_list/edit/' + id);
		*/

    } 
    function del(){
		//alert("Under development!");
		//return false;
		
		var ischecked = false;
        var hk_count = 0;
        $('[id^=b_id_]').each(function () {
            if ($(this).prop("checked") == true)
            {
                hk_count = hk_count + 1;
            }
        });
        if (hk_count == 0) {
            alert("Please select atleast one record");
            return false;
        } else if (hk_count == 1) {
            ischecked = true;
        } else if (hk_count > 1) {
            alert("Please select only one record");
            return false;
        }
        if (ischecked == true)
        {

         
            var id = "";
            $('[id^=b_id_]').each(function () {
                if ($(this).prop("checked") == true)
                {
                    id += $(this).prop("id").replace('b_id_', '');
                }
            });
            var cfm = confirm("You want to delete this user?");
			if (cfm == true)
			{	
				$('.swal-button--confirm').unbind().click(function(){
			// AJAX request 
			var API_URL = '<?php echo API_URL.'delete_user'; ?>';
			 $.ajax({
				url: API_URL,
				type: 'post',
				data: {user_id: id,token:'<?php echo $this->session->userdata('token');?>'},
				success: function(response){
				   var obj = JSON.parse(response);	
				   if(obj.result == "true"){
						reload_grid();
						//alert("Good job!", obj.msg, "success");
						alert(obj.msg, "success");
				   }else{
					   alert(obj.msg, "warning");
				   }
				}
			 });
			 });
			}
          
        }
	   /*
	   var ischecked = false;
        $('[id^=b_id_]').each(function () {
            if ($(this).prop("checked") == true)
            {
                ischecked = true;
                return false;

            }
        });
        if (ischecked == true)
        {
           // Confirm alert
          
              // Get userid from checked checkboxes
              var users_arr = [];
              $(".checkbox:checked").each(function(){
                  var userid = $(this).val();

                  users_arr.push(userid);
              });

              // Array length
              var length = users_arr.length;

              if(length > 0){

                 // AJAX request
                 $.ajax({
                    url: '<?=base_url() ?>index.php/User_list/delete',
                    type: 'post',
                    data: {user_ids: users_arr},
                    success: function(response){

                       // Remove <tr>
                       $(".checkbox:checked").each(function(){
                           var userid = $(this).val();

                           $('#tr_'+userid).remove();
                       });
                         reload_grid();
                       alert("Good job!", "Selected record(s) deleted successfully!')", "success");
                    }
                 });
              }
         
           else
           {

           }

        }
     else
        {
            alert("Please select atleast one record");
            return false;
        }
		*/
		
		/*
		var cfm = confirm("You want to delete this user?");
        if (cfm == true)
        {	
			$('.swal-button--confirm').unbind().click(function(){
		// AJAX request 
		var API_URL = '<?php echo API_URL.'delete_user'; ?>';
		 $.ajax({
			url: API_URL,
			type: 'post',
			data: {user_id: id,token:'<?php echo $this->session->userdata('token');?>'},
			success: function(response){
			   var obj = JSON.parse(response);	
			   if(obj.result == "true"){
					reload_grid();
					alert("Good job!", obj.msg, "success");
			   }else{
				   alert("Warning", obj.msg, "warning");
			   }
			}
		 });
		 });
		}*/

    }
    function checkalldata()
    {
        if ($('#checkall').prop("checked") == true)
        {
            $('[id^=b_id_]').prop('checked', true);

        } else
        {
            $('[id^=b_id_]').prop('checked', false);
        }
    }
     /*function reload_grid(){
        $('#user_form_div').hide();
    $('#user_div').show();
        table.ajax.reload( null, false );
  }*/
  
  function reload_grid(){
        $('#user_form_div').hide();
    $('#user_div').show();
        $('#table').DataTable().ajax.reload();
         
  }
  </script>
  <!-- Content Wrapper. Contains page content -->
  <div id="content" class="content1">
 <div id="user_div">
   <div class="panel panel-inverse">
      <div class="panel-heading">
         <?php if($this->session->userdata('user_type') == 0 || $this->session->userdata('user_type') == 1) {?>
         <h4 class="panel-title">User List</h4>
          <?php } else{?>
            <h4 class="panel-title">About User</h4>
           <?php } ?>
      </div>
      <!-- Page Heading -->
      <div class="panel-body">
         <!--<div class="table-responsive">
            <div class="aedbtn">
              <?php //if($this->session->userdata('user_type') == 0 || $this->session->userdata('user_type') == 1) {?>
              <button class="btn btn-primary" type="button" onclick="add()">Add</button>
            <?php //} ?>
               <button class="btn btn-primary" type="button" onclick="edit()">Edit</button>
               <button class="btn btn-danger" type="button" onclick="del()">Delete</button>
            </div>
         </div>-->
         <div class="container-fluid">
            <div class="row">
               <div class="col-12">
			   <!---  --->
			   
                  <table id="table" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                     <thead>
                        <tr>
                           <th style="width:20px !important" class="check_input col_no"></th>
                           <th class="text-nowrap">Name</th>
                           <th class="text-nowrap">Email</th>
                           <th class="text-nowrap">Login Name</th>
                           <th class="text-nowrap">Password</th>
                           <th class="text-nowrap">Device</th>
                           <th class="text-nowrap">SIM Account</th>
                           <th class="text-nowrap">SIM Password</th>
                           <th class="text-nowrap">SIM Phone</th>
						   <th class="text-nowrap">User Type</th>
                           <th class="text-nowrap">Contact Details</th>
                           <th class="text-nowrap">Notes</th>
						</tr>
                     </thead>
                     <tbody id="show_data">
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="user_form_div" style="display: none;"></div>

<?php
$this->load->view("footer.php");
?>
 
 
 <!--<script src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script>-->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->


<script>
  var table;
  var scroll = false;
  if(window.screen.width < 1200){
    scroll = true;
  }
//lBfrtip , Bfrtip , lrtip , <"wrapper"flipt> , <lf<t>ip> , <"top"i>rt<"bottom"flp><"clear">
  $(document).ready(function(){
    table = $('#table').DataTable({ 
		
		dom: '<"top-l"Bl><"top-r"fp>tri<"bottom"><"clear">',
        buttons: [
            <?php if($this->session->userdata('user_type') == 0 || $this->session->userdata('user_type') == 1) {?>
			{
                text: 'Add',
				className: 'btn btn-primary',
                action: function ( e, dt, node, config ) {
                    add();
                }
            },
			<?php } ?>
			{
                text: 'Edit',
				className: 'btn btn-primary',
                action: function ( e, dt, node, config ) {
                    edit();
                }
				
            },{
                text: 'Delete',
				className: 'btn btn-danger',
                action: function ( e, dt, node, config ) {
                    del();
                }
            }
        ],
		
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        'type': 'post',
        "searching": true,
		"autoWidth": true,
        "pageLength": -1, 
        "Orderable" : true,
	    "aLengthMenu": [[-1,25, 50, 100], ["All","25", "50", "100"]],
        "columnDefs": [
			{ "orderable": false, "targets": [0,4,5,7,8,9,10,11] }
		],
		"scrollX":"scroll",
		"scrollCollapse": true, 		
	   "scrollY":"76vh",
       "language": {                
            "infoFiltered": ""
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('User_list/loaddata') ?>",
            "type": "POST"
        },		
		//Set column definition initialisation properties.
       
 
    });
     $('#table tbody, #table tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $(this).find('input').attr("checked",false);
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
           $('#table tbody, #table tbody').find('input').attr("checked",false);
            $(this).find('input').attr("checked",true);
        }
    } ); 

    });


  
 </script>

<!-- ================== END PAGE LEVEL JS ================== -->

   
