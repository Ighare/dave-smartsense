<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<style>
p.login-box-msg {
    color: red; 
}
</style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SmartSense | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  
  <!-- Ionicons -->
  
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
   
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.css">

   <link href="<?php echo base_url(); ?>assets/plugins/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo base_url(); ?>assets/plugins/DataTables/css/style.min.css" rel="stylesheet" />
   
  <link href="<?php echo base_url(); ?>assets/plugins/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert2/sweetalert2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <link href="<?php echo base_url() ?>assets/plugins/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url() ?>assets/plugins/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" /> 

<link href="<?php echo base_url() ?>assets/plugins/DataTables/css/fixedColumns.dataTables.min.css" rel="stylesheet" />

<link href="<?php echo base_url() ?>assets/plugins/DataTables/css/buttons.dataTables.min.css" rel="stylesheet" />

<style> 

 </style> 
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
<?php
	
	$map=isset($map)?$map:"";
	if($map !=="true"){

  ?>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light"  > 
   
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          
          
          <a href="<?php echo site_url('sessions/logout');?>" class="dropdown-item">
            <span class="dropdown-item dropdown-header">Logout</span>
          </a>
		  <div class="dropdown-divider"></div>
          
        </div>
      </li>
      
	  <!--<li class="nav-item"><a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fas fa-th-large"></i></a></li>-->
	   
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="overflow: hidden;" >
    <!-- Brand Logo -->
    <a href="<?php echo base_url(); ?>" class="brand-link navbar-light">
        <img src="<?php echo base_url(); ?>assets/dist/img/logo.png" alt="Figure Megamart" class="brand-image  "
        style="opacity: .8;float: none !important;"> 
      </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <!--<div class="image">
          <img src="<?php //echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>-->
        <div class="info"> <h5 style="color: white;">Menu</h5>
			<?php /*if ($this->session->userdata("is_superadmin") == '1' ) {
				?><a href="#" class="d-block">Super Admin</a><?php
			}else if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') {
				?><a href="#" class="d-block">Admin</a><?php
			} else { 
				?><a href="#" class="d-block">User</a><?php
			}*/ ?>
			
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open"></li>
		  <!---
         <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/home" class="nav-link">Dashboard</a>
          </li>-->
        <?php
		if ($this->session->userdata("is_superadmin") != '1' ) {
		?>	
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/device_list" class="nav-link">
              <!--<i class="nav-icon far fa-calendar-alt"></i>-->
              <?php if($this->session->userdata('user_type') == 2){?>
                  <p>
                About Device
                <!--<span class="badge badge-info right">2</span>-->
              </p>
            <?php } else {?>
              <p>
                Device List
                <!--<span class="badge badge-info right">2</span>-->
              </p>
            <?php  }?>
            </a>
          </li>
		<?php } ?>  
         <li class="nav-item">
            <a href="<?php echo base_url(); ?>index.php/user_list" class="nav-link">
              <!--<i class="nav-icon far fa-calendar-alt"></i>-->
              <?php if($this->session->userdata('user_type') == 2){?>
                  <p>
                About User
                <!--<span class="badge badge-info right">2</span>-->
              </p>
            <?php } else {?>
                  <p>
                User List
                <!--<span class="badge badge-info right">2</span>-->
              </p>
              <?php  }?>   
            </a>
          </li>
		   <?php //admin =1 
		   if($this->session->userdata('user_type') == 1){?>
			<li class="nav-item">
			
            <a href="<?php echo base_url(); ?>index.php/data_history" class="nav-link">
			
              Data History
            </a>
			</li> 
		   <?php } ?>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
	<?php } ?>