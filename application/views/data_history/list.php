
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header.php");
date_default_timezone_set('Australia/Brisbane');
?>
<link href="<?php echo base_url() ?>assets/jquery.dataTables.min.css" rel="stylesheet" />
<style type="text/css">
	div#table_wrapper {
		margin-top: -37px;
	}
.select2-results__option{		
		color: black;
}
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
    background: #EAF3D9;
}
.table thead th.table>thead>tr>th {
    color: #343a40; 
}
th.text-nowrap {
    color: #343a40;
}
  .form-inline{display: block !important}  
    
  tbody>tr.even>:nth-child(1) , tbody>tr.even>:nth-child(1){
   background: #ffffff;
 }
 tbody>tr.old>:nth-child(1) , tbody>tr.old>:nth-child(1){
   background: #EAF3D9;
 }

 tbody#show_data td {
  height: 18px;
  padding-top: 5px;
  padding-bottom: 5px;
}

.top-l , .top-r {
  width:50%;
}
.top-l {
  float:left;
}
.top-r {
  float:right;
}
#table_filter, #usertable_filter {
  width:50%;
  float: left;
}
#table_length, #usertable_length{
  margin-left: 15px;
}
table.dataTable tbody th,
table.dataTable tbody td {
  white-space: nowrap;
}
.table-bordered th, .table-bordered td {
    border: 1px solid #dee2e6;
} 
button.btn.btn-default {
    float: right;
    margin-left: auto;
}
.container-fluid { 
    margin-bottom: 42px;
}
	span#select2-device_id-container {
		font-size: 13px;
		color: black;
	}
	input#from_date {
		font-size: 13px;
		color: black;
	}
	input#to_date {
		font-size: 13px;
		color: black;
	}
</style>

<!-- Content Wrapper. Contains page content -->
<div id="content" class="content1"> 
   <div class="panel panel-inverse">
  
    <div class="panel-heading">    
       <h4 class="panel-title">Data History</h4>     
  </div>
    
  <!-- Page Heading -->
  <div class="panel-body_form"> 
			<div class="card-body">
				<form role="form" id="data_history_form_id" method="post" name="data_history_form">
					<div class="row">
						<div class="col-sm-0">
							<div class="form-group">
								<button type="button" onclick="load_data_history_reports();" style="margin-top: 26px;" class="btn btn-primary">Search</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<div class="form-group">
									<label>Device Name</label>
									<select name="device_id" id="device_id" class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
										<?php print_r($device_option); ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="from_date">From Date</label>
								
								<input type="datetime-local" class="form-control" id="from_date" value="<?php echo date("Y-m-d\TH:i", time()-86400); ?>" name="from_date" placeholder="Enter From Date">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="to_date">To Date</label>
								<input type="datetime-local" class="form-control" id="to_date" value="<?php echo date("Y-m-d\TH:i",time()+86400); ?>" name="to_date" placeholder="Enter To Date">
							</div>
						</div>						
						
					</div>
				</form>
			</div>         
   <div class="container-fluid">
    <div class="row">
     <div class="col-12"> 
		<table id="table" class="display table-striped table-bordered" width="100%"></table> 
        <!--<table id="table" class="table table-striped table-bordered"  width="100%">
         <thead> 
          <tr>
           <th style="width:20%">Sr.No</th>
           <th class="text-nowrap">Device Name</th>
           <th class="text-nowrap">Davice Time</th>
           <th class="text-nowrap">Device Data</th>         
         </tr>
       </thead>
       <tbody id="show_data">
       </tbody>
     </table> -->
	</div> 
</div> 
</div>
</div>
</div>
 <div class="modal fade" id="modal-default_form">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Alert</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p style="font-weight: 700 !important;" id="element_text"></p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>             
            </div>
          </div> 
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<div id="device_form_div" style="display: none;"></div>

<?php
$this->load->view("footer.php");
?>


<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script> 
  var table;  
  var scroll = false;
  if(window.screen.width < 1200){
    scroll = true;
  } 
  function load_data_history_reports(){
	  console.log("clicked");
	  var from_date=$("#from_date").val();
	  var to_date=$("#to_date").val();
	  var device_id=$("#device_id").val();
	  if(from_date==""){
		  $('#modal-default_form').modal(); 
		  $('#element_text').html("Please select From Date.");		  
		  return false;
	  }else if(to_date==""){
		  $('#modal-default_form').modal(); 
		  $('#element_text').html("Please select To Date.");	
		  return false;
	  }else if(device_id==""){
		  $('#modal-default_form').modal(); 
		  $('#element_text').html("Please select Device Name.");	
		  return false;
	  }
	 
	 load_history_report();
	 
  }
  
  
  $(document).ready(function(){	  
    $('.select2').select2();	
	// $('#from_date').datetimepicker();
	
   /* table = $('#table').DataTable({     
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
		"paging": true,		
        'type': 'post',
        "searching": false,
        "pageLength": 50, 
        "ordering":true,        
        "aLengthMenu": ["All","25", "50", "100","500","1000"],        
		"scrollX": "scroll",
		"scrollCollapse": false,
		"scrollY":"76vh",
      "language": {                
        "infoFiltered": ""
      },  
      "ajax": {
        "url": "<?php echo site_url('data_history/loaddata') ?>",
        "type": "POST"
      }, 
    }); */	
	
  });
  function load_history_report(){
	  var from_date=$("#from_date").val();
	  var to_date=$("#to_date").val();
	  var device_id=$("#device_id").val();
	  $('#table').DataTable( {
		  'type': 'post',
		  "columnDefs": [		
		{ "width": "20%", "targets": 0 ,className: "text-nowrap"},
		{ "width": "80%", "targets": 1 ,className: "text-nowrap"}		
		],
		"order": [[ 0, "desc" ]],
		"processing": true,
		"serverSide": true,
		"searching": false,
		"paging": false,
		"destroy": true,
		"ajax": {
        "url": "<?php echo site_url('data_history/loaddata') ?>",
        "type": "POST",
		"data": {
        "start_date": from_date,
        "end_date": to_date,
        "device_id": device_id       
		}
      },
        "pageLength": -1, 
        "ordering":true,        
        "aLengthMenu": ["All","25", "50", "100","500","1000"],        
		"scrollX": "scroll",
		"scrollCollapse": false,
		"scrollY":"76vh",
      "language": {                
        "infoFiltered": ""
      },         
        columns: [           
            { title: "Datetime" },           
            { title: "Raw Data" }            
        ]
		} ); 
  }//end function 
  


  
</script>
<script src="<?php echo base_url() ?>assets/jquery.dataTables.min.js"></script>


<!-- ================== END PAGE LEVEL JS ================== -->


