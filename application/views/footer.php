<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>
</div>
<style>
	.main-footer{
	position: fixed;
    bottom: 0;
    width: 100%;
	}
</style>
<?php
	
	$map=isset($map)?$map:"";
	if($map !=="true"){

  ?>
 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="javascript:void(0);">Smart Sense</a>.</strong>
    All rights reserved.
    <!--<div class="float-right d-none d-sm-inline-block"><b>Version</b> 3.0.1-pre</div>-->
  </footer>
	<?php } ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(); ?>assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!--<script src="<?php echo base_url(); ?>assets/plugins/sparklines/sparkline.js"></script>-->
<!-- JQVMap -->
<!--<script src="<?php echo base_url(); ?>assets/plugins/jqvmap/jquery.vmap.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>-->
 <!-- ================== Datatable JS ================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/DataTables/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/DataTables/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/ColReorderWithResize.js"></script>
    
    <!-- ================== Datatable JS ================== -->
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard.js"></script>-->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/js.cookie.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/DataTables/js/dataTables.fixedColumns.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/DataTables/js/dataTables.buttons.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url() ?>assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
  <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
   <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaKFHzPHYVJ5Qhrd-hR793h09TU3iElvU&callback=initMap"
      defer
    ></script>
<script>
$('#click_refresh').click(function() {
    location.reload();
});

$('#click_locate').click(function() {
	var command_val = "CR";
	var device_model = '<?php echo isset($device_model)?$device_model:"0"; ?>';
	var device_id = '<?php echo isset($device_id)?$device_id:"0"; ?>';
	if (command_val == "") {
	 alert("Please enter device Command.");
	 return false;
	}
	if (device_id == "") {
	 alert("Please enter device id.");
	 return false;
	}
	var formData = {token: "<?php echo $this->session->userdata('token'); ?>", device_id: device_id, model_type: device_model, command_value: command_val, command_name: 'webcommand'};
	$.ajax({
	 url: "<?php echo API_URL . 'gprs_command'; ?>",
	 type: "POST",
	 data: formData,
	 success: function (response) {
		 var JSONArray = $.parseJSON(response);
		 if (JSONArray.result == 'true') {
			 alert(JSONArray.msg);
		 } else {
			 alert(JSONArray.msg);
		 }
	 }
	})
});
let map;
//AIzaSyCaKFHzPHYVJ5Qhrd-hR793h09TU3iElvU
// Initialize and add the map
<?php if(isset($asset_id) && $asset_id !==""){ ?>

function initMap() {
	var latitude= -25.344; 
	var longitude= 131.031; 
	 var API_URL = '<?php echo API_URL.'getdeviceListsingle_details'; ?>';	
		$.ajax({
        url: API_URL,
        type: 'post',
        data: {device_id:'<?php echo $asset_id; ?>',token:'<?php echo $this->session->userdata('token');?>'},
        success: function(response){
         var obj = JSON.parse(response);	
		 console.log(obj);
				latitude=parseFloat(obj.data[0].latitude);
				longitude=parseFloat(obj.data[0].longitude);
				
				$("#battery span").text(obj.data[0].device_bettery);
				$("#type span").text(obj.data[0].location_type);
				$("#time span").text(obj.data[0].last_date_time);
				//document.getElementById('devicename').innerHTML = obj.data[0].name;
				var device="";
				device +=" Device: "+obj.data[0].name+',&nbsp;&nbsp;'+obj.data[0].address+'.';
				document.getElementById('devicename').innerHTML = device;
				var otherdetails="";
				otherdetails +="Update On "+obj.data[0].last_date_time+', '+obj.data[0].location_type+', '+obj.data[0].device_bettery;
				document.getElementById('otherdetails').innerHTML =otherdetails;
				var coordinate="";
				coordinate +="Coordinates: "+latitude+", "+longitude;
				document.getElementById('coordinate').innerHTML =coordinate;
				var image_icon="<?php echo base_url();?>/assets/icon/map_icon.ico";
				
				var icon = {
						url: image_icon,
						scaledSize: new google.maps.Size(45,45)
					}
				 // The location of Uluru
			  const uluru = { lat: latitude, lng: longitude };
			 // console.log(uluru);
			  // The map, centered at Uluru
			  // console.log(icon);
			  const map = new google.maps.Map(document.getElementById("world-map-markers"), {
				zoom: 16,
				center: uluru,
			  });
			  
			  // The marker, positioned at Uluru
			  const marker = new google.maps.Marker({
				position: uluru,
				map: map,
				icon: icon,
			  });
			
        }
      });
	//console.log(longitude);
 
  
}
<?php }else{ ?>
function initMap() {
	$('.card-pane-right').hide();
	const latitude= -25.344; 
	const longitude= 131.031; 
	var image_icon="<?php echo base_url();?>/assets/icon/map_icon.ico";
	var icon = {
						url: image_icon,
						scaledSize: new google.maps.Size(45,45)
					}
	 const uluru = { lat: latitude, lng: longitude };
			  //console.log(image_icon);
			  // The map, centered at Uluru
			  const map = new google.maps.Map(document.getElementById("world-map-markers"), {
				zoom: 6,
				center: uluru,
			  });
			  // The marker, positioned at Uluru
			  const marker = new google.maps.Marker({
				position: uluru,
				map: map,
				icon: icon,
			  });
}

<?php } ?>
window.initMap = initMap;

</script>

<script type="text/javascript">
//added by Rajesh to get timezoen as per user 
//This is logic also daylight timezone 
function getTimeZone() {  
    var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
    return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
} 
//end 
 

   $(document).ready(function () {

      
 
window.alert = function(a,b=null,c=null) {
            if(b!=null && c!=null){
                swal(a,b,c);
            }else{
            swal(a);
            }

            return true;
        }
        window.confirm = function(a,b=null,c=null) {
          if(b==null) b ="Are you sure?";
            swal({
              title: b,
              text: a,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
          
            return true;
        }
      });


    </script>    
</body>
</html>
