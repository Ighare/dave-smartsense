<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<style>
.check_box_wifi {
    float: right;
}
</style>

<?php
//print_r($model_list);
$device_models = array('Mons' => 'Mons', 'Highworth' => 'Highworth', 'Montville 4' => 'Montville 4', 'Montville 3' => 'Montville 3', 'Coolum' => 'Coolum');
?>
<!-- Content Wrapper. Contains page content -->
<div class="panel panel-inverse" >
    <div class="panel-heading">

        <h4 class="panel-title">Add Device</h4>
    </div>
    <!-- begin panel-body -->
    <div class="panel-body">

        <div class="alert alert-danger print-error-msg" style="display:none"></div>
        <form data-parsley-validate="parsley" id="add_form" method="post" action="javascript:void(0);" enctype="multipart/form-data">
            <input type="hidden" value="" id="id" name="" > 
            <input type="hidden"   id="assignuserval" name="assigned_users"  value="">
            <input type="hidden"   id="token" name="token"  value="<?php echo $this->session->userdata('token'); ?>">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Device Name <span class="text-red">*</span></label>
                            <input type="text" class="form-control" name="device_name" id="device_name" placeholder="Device Name" required="true" data-parsley-required-message="Please Enter Device Name" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Device ID <span class="text-red">*</span></label>
                            <input type="text" class="form-control" name="device_id" id="device_id" placeholder="Device ID" required="true" data-parsley-required-message="Please Enter Device ID">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" class="input-group date">
                            <label >Activation Date <span class="text-red">*</span></label>
                            <input type="text" value="<?php echo date('d-m-Y') ?>" class="form-control"   name="expiry_date" id="datepicker12"  placeholder="Activation Date" required="true" data-parsley-required-message="Please Select Activation Date">


                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Device Model <span class="text-red">*</span></label>

                    <!--<input type="text" class="form-control"  name="device_model" id="device_model"  placeholder="Device Model" required="true" data-parsley-required-message="Please enter select device model">-->

                            <select name="device_model" class="device_model form-control select2" id="device_model" required="true" data-parsley-required-message="Please Select Device Model">
                                <option value=""> </option>
                                <?php foreach ($model_list as $v) {
                                    ?>
                                    <option value="<?php echo $v->model_value; ?>"><?php echo $v->model_name; ?></option>
                                    <?php }
                                ?>
                            </select> 

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Emergency Nos.</label><input  placeholder="Emergency Nos." id="emergency_number" name="emergency_number"  class="form-control">

                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label>SIM Account</label>
                            <input type="text" readonly class="form-control"  name="sim_account" id="sim_account"  placeholder="SIM Account">
                        </div>
                    </div>              
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>SIM Number</label>
                            <input type="text" class="form-control"  name="sim_number" id="sim_number"  placeholder="SIM Number">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>SIM Password</label>
                            <input type="text" readonly class="form-control"  name="sim_password" id="sim_password"  placeholder="SIM Password">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="width: 100%">User Assign To</label>
                            <select name="assigned_users" class="_assigned_users form-control select2" id="_assigned_users" ></select>					 
                        </div>
                    </div>			   
                    <div class="col-md-6"> 
                        <input type="hidden" name="time_zone_tick" id="time_zone_tick"  >
                        <label for="checkboxPrimary3">
                            Timezone
                        </label>
                        <select name="timezone" class="timezone form-control select2" id="timezone" >
                            <option value=""> Please Select </option>
                            <?php
                            $timezone_ar = array(
                                '+08:00',
                                '+08:45',
                                '+09:00',
                                '+09:30',
                                '+10:00',
                                '+10:30',
                                '+11:00',
                                '+12:00',
                                '+13:00'
                            );
                            foreach ($timezone_ar as $v) {
                                ?>
                                <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                <?php }
                            ?>
                        </select> <br>
                        <button type="button" id="Browser_Timezone"  class="btn btn-secondary">Get Browser Timezone</button>
                         <div class="check_box_wifi">
                                <label>WIFI Location</label>
                                <input type="checkbox" id="toggle-event" checked  value="" data-size="sm" data-toggle="toggle"  data-bootstrap-switch data-off-color="danger" data-on-color="success">	
                                <input type="hidden" name="wifi_enable" id="wifi_enable" value="1" />

                            </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Notes</label>
                            <textarea class="form-control"  name="device_description" id="device_description"  rows="3" placeholder="Notes"></textarea>
                        </div>
                    </div>
                   <!--- <div class="col-md-6">
                        <div class="form-group">
                            <div class="check_box_wifi">
                                <label>WIFI Location</label>
                                <input type="checkbox" id="toggle-event" checked  value="" data-size="sm" data-toggle="toggle"  data-bootstrap-switch data-off-color="danger" data-on-color="success">	
                                <input type="hidden" name="wifi_enable" id="wifi_enable" value="1" />

                            </div>
                        </div>
                    </div> -->

                </div>
            </div>
            <!-- /.box-body -->

            <div class="row" style="display:inline; text-align: center;">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Add</button>
                    <button type="button" value="Cancel" onClick="window.location = '<?php echo site_url('Device_list'); ?>';" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
    </div>
</form>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">User List</h4>
            </div>
            <div class="modal-body">
                <select class="form-control" id="userlist"></select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary ok_dialog">Ok</button>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/plugins/parsley/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/plugins/parsley/parsley.min.js"></script>


<script type="text/javascript">

        $(document).ready(function () { 

            $("input[data-bootstrap-switch]").each(function () {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });
            $('.bootstrap-switch-handle-off').click(function () {
                $("#wifi_enable").val(1);
            });
            $('.bootstrap-switch-handle-on').click(function () {
                $("#wifi_enable").val(0);
            });
            $(".check_box_wifi").hide();
            $("#device_model").change(function () {
                if ($('#device_model').val() == "Coolum" || $('#device_model').val() == "Montville" || $('#device_model').val() == "Obi") {
                    $(".check_box_wifi").show();
                } else {
                    $(".check_box_wifi").hide();
                }
            });
            $('#Browser_Timezone').click(function () {
                var timezone = getTimeZone();
                var optionExists = ($('#timezone option[value="' + timezone + '"]').length > 0);
                if (!optionExists)
                {
                    $('#timezone').append("<option value='" + timezone + "'>" + timezone + "</option>");
                }
                $("#timezone").val(timezone).select2();
                if (timezone != "") {
                    $('#time_zone_tick').val('on');
                } else {
                    $('#time_zone_tick').val('');
                }
            })

            $('#timezone').change(function () {
                if ($("#timezone").val() != "") {
                    $('#time_zone_tick').val('on');
                } else {
                    $('#time_zone_tick').val('');
                }

            });
            $('.select2').select2({
                width: '100%'
            });

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            $("#_assigned_users").select2({
                placeholder: "Please Select",
                width: '100%'
            });

            $("#timezone").select2({
                width: '100%'
            });
            $("#_assigned_users").on("change", function (e) {
                $("#assignuserval").val($("#_assigned_users").val());
            });


            /*$('#datepicker12').datepicker({
             autoclose: true,
             dateFormat: 'mm/dd/yy',//check change
             changeMonth: true,
             changeYear: true
             });*/

            $("#datepicker12").datepicker({format: 'dd-mm-yyyy', autoclose: true});

            var API_URL = '<?php echo API_URL . 'member_list'; ?>';
            var result_html;
            $.post(API_URL, {token: '<?php echo $this->session->userdata("token"); ?>'}, function (response) {

                var obj = JSON.parse(response);

                if (obj.result == "true") {
                    var i = 0;
                    result_html = "<option value=' '> Please Select </option>";
                    $(obj.data).each(function () {
                        result_html += '<option value="' + obj.data[i]['id'] + '">' + obj.data[i]['user_name'] + '</option>';
                        i++;
                    });
                } else {
                    result_html = "<option value=''> Please Select </option>";
                }

                $('#_assigned_users').html(result_html);
                $("#_assigned_users").trigger("change");

            }).fail(function (response) {
                alert('Error: ' + response.responseText);
            });
            $("#add_form").on('submit', function (event) {
                // validate form with parsley.

                $(this).parsley().validate();
                $(".print-error-msg").show();
                // if this form is valid
                if ($(this).parsley().isValid()) {
                    // show alert message
                    $(".print-error-msg").hide();
                    var id = $("#id").val();

                    if (id != "") {
                        var API_URL = '<?php echo API_URL . 'edit_device'; ?>';
                    } else {
                        var API_URL = '<?php echo API_URL . 'add_device'; ?>';
                    }
                    $.post(API_URL, $("#add_form").serialize(), function (data) {
                        var obj = JSON.parse(data);
                        if (obj.result == "true") {
                            reload_grid();
                            alert(obj.msg, "success");
                        } else {
                            if (id != "") {
                                alert(obj.msg, "warning");
                            } else {
                                alert(obj.msg, "warning");
                            }
                        }
                    }).fail(function (response) {


                        alert('Error: ' + response.responseText);
                    });
                } else {
                    $(".print-error-msg").show();

                }

                event.preventDefault();
            });

        });
        function reload_grid() {
            $('#device_form_div').hide();
            $('#device_div').show();
            table.ajax.reload(null, false);
        }
</script>

<!-- ================== END PAGE LEVEL JS ================== -->


