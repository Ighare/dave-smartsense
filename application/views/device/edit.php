<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
.check_box_wifi {
    float: right;
}
.form-group.locations {
   
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="panel panel-inverse" >
    <div class="panel-heading">
        <h4 class="panel-title">Edit Device</h4>
    </div>
    <!-- begin panel-body -->
    <div class="panel-body">

        <?php
        if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') {
            $is_can_edit = "";
        } else {
            $is_can_edit = "readonly";
        }

        $device_models = array('Mons' => 'Mons', 'Highworth' => 'Highworth', 'Montville 4' => 'Montville 4', 'Montville 3' => 'Montville 3', 'Coolum' => 'Coolum');
        ?>
        <p><?php echo $this->session->flashdata('success'); ?></p>
        <div class="alert alert-danger print-error-msg" style="display:none"></div>
        <form data-parsley-validate="parsley" id="update_form"  method="post" action="javascript:void(0);" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $device->data->id ?>" id="id" name="deviceid" >

            <input type="hidden" value="<?php echo $device->data->id ?>" id="edit_device_id" name="edit_device_id" >

            <input type="hidden"   id="assignuserval" name="assigned_users"  value="">
            <input type="hidden"   id="token" name="token"  value="<?php echo $this->session->userdata('token'); ?>">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Device Name <span class="text-red">*</span></label>
                            <input type="text" <?php echo $is_can_edit; ?> class="form-control" value="<?php echo $device->data->device_name ?>" name="device_name" id="device_name" placeholder="Device Name" required="true" data-parsley-required-message="Please enter name">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Device ID <span class="text-red">*</span></label>
                            <input type="text" <?php echo $is_can_edit; ?> class="form-control" value="<?php echo $device->data->device_id ?>" name="device_id" id="device_id" placeholder="Device ID" required="true" data-parsley-required-message="Please enter device id">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Activation Date</label>
                            <?php if ($is_can_edit == "") { ?>
                                <input type="text" <?php echo $is_can_edit; ?>   value="<?php echo date('d-m-Y', strtotime($device->data->expiry_date)); ?>" name="expiry_date" class="form-control"  placeholder="Activation Date" required="true" data-parsley-required-message="Please select Activation date" id="datepicker13">
                            <?php } else { ?>
                                <input type="text" <?php echo $is_can_edit; ?>   value="<?php echo date('d-m-Y', strtotime($device->data->expiry_date)); ?>" name="expiry_date" class="form-control"  placeholder="Activation Date" required="true" data-parsley-required-message="Please select Activation date">
                            <?php } ?>
                        </div>
                    </div>  
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Device Model <span class="text-red">*</span></label>
                            <?php if ($is_can_edit == "") { ?>
                                <select name="device_model" class="device_model form-control select2" id="device_model" required="true" data-parsley-required-message="Please enter select device model">
                                    <option value=""> </option>
                                    <?php
                                    foreach ($model_list as $v) {
                                        $s = "";
                                        if ($device->data->device_model == $v->model_value) {
                                            $s = 'selected';
                                        }
                                        ?>
                                        <option value="<?php echo $v->model_value; ?>" <?php echo $s; ?>><?php echo $v->model_name; ?></option>
                                    <?php }
                                ?>
                                </select>
<?php } else { ?>
                                <input type="text" <?php echo $is_can_edit; ?> class="form-control"  name="device_model" value="<?php echo $device->data->device_model; ?>" id="device_model"  placeholder="Device Model" required="true" data-parsley-required-message="Please enter select device model">
<?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Emergency Nos.</label><input type="text" <?php echo $is_can_edit; ?>  value="<?php echo $device->data->emergency_number; ?>" placeholder="Emergency Nos." id="emergency_number" name="emergency_number"  class="form-control">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>SIM Account</label>
                            <input type="text" readonly class="form-control"  name="sim_account"  value="<?php echo $device->data->sim_account ?>" id="sim_account"  placeholder="SIM Account">
                        </div> 
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label>SIM Number</label>
                            <input type="text"  class="form-control"  name="sim_number" value="<?php echo $device->data->sim_number ?>"  id="sim_number"  placeholder="SIM Number">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>SIM Password</label>
                            <input type="text" readonly  class="form-control"  name="sim_password" value="<?php echo $device->data->sim_password ?>" id="sim_password"  placeholder="SIM Password">
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Notes</label>
                            <textarea class="form-control" <?php echo $is_can_edit; ?> value=""  name="device_description" id="device_description"  rows="3" placeholder="Notes"><?php echo $device->data->device_description ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
<?php if ($is_can_edit == "") { ?>  
                            <div class="form-group">
                                <label style="width: 100%">User Assign To</label> 
                                <select name="assigned_users" class="_assigned_users form-control select2" id="_assigned_users" ></select>				  
                            </div> 
                            <button type="button" data-toggle="modal" data-target="#myModalclear" class="btn btn-secondary">Clear User</button>

<?php } else { //assigned_users
    ?>
                            <div class="form-group">
                                <label style="width: 100%">User Assign To</label>
                                <input type="textbox" <?php echo $is_can_edit; ?> class="form-control" readonly id="assignuservallabel" value="<?php echo $device->data->assigned_users_names; ?>" />
                            </div>
    <?php }
?>

                    </div>
                     <div class="col-md-6">               
                        <label></label><br>
                        <div class="form-group input-group-sm">
                            <input id="command_value" placeholder="Enter Command.."  style="width: 400px;float: left;margin-top: 9px;" type="text" class="form-control">
                            <span class="input-group-append">
                                <button type="button" style="margin-top: 9px;" id="command_send_to_device" class="btn btn-info btn-flat">SEND</button>
                            </span>

                        </div> 
                    </div>	
                    <div class="col-md-6"> 
                        <input type="hidden" value="<?php if ($device->data->time_zone_tick == 'on') {
    echo "on";
} ?>" name="time_zone_tick" id="time_zone_tick" style="display: none;">
                        <label for="checkboxPrimary3">
                            Timezone
                        </label>
                        <select name="timezone"    <?php echo ($is_can_edit != "") ? 'disabled="" ' : '';
; ?> class=" form-control " id="timezone" placeholder="Please Select">
                            <option value=""> Please Select </option>
                            <?php
                            $timezone_ar = array(
                                '+08:00',
                                '+08:45',
                                '+09:00',
                                '+09:30',
                                '+10:00',
                                '+10:30',
                                '+11:00',
                                '+12:00',
                                '+13:00'
                            );
                            if (!in_array($device->data->timezone, $timezone_ar) && $device->data->timezone != "") {
                                $timezone_ar[] = $device->data->timezone;
                            }
                            foreach ($timezone_ar as $v) {
                                $selected = ($v == $device->data->timezone) ? ' selected="selected" ' : '';
                                ?>
                                <option value="<?php echo $v; ?>" <?php echo $selected; ?> ><?php echo $v; ?></option>
    <?php }
?>
                        </select> <br>
                        <button type="button" id="Browser_Timezone"  class="btn btn-secondary">Get Browser Timezone</button>
                        <div class="check_box_wifi">
                            <label>WIFI Location</label>
                            <input type="checkbox" id="toggle-event" <?php if ($device->data->wifi_enable == 1) {
    echo 'checked';
} ?>  value="" data-size="sm" data-toggle="toggle"  data-bootstrap-switch data-off-color="danger" data-on-color="success">	
                            <input type="hidden" name="wifi_enable" id="wifi_enable" value="<?php echo $device->data->wifi_enable; ?>" /> 			
                        </div>
                    </div>	
                <div class="col-md-6 check_box_wifi">
                    <div class="form-group">
                        <label>Recent Command History  <small><button onClick="clear_history()" type="button" class="btn btn-xs btn-danger">Clear History</button></small></label>
                        <textarea class="form-control" id="recent_cmd_list" rows="8" cols="50">
                        </textarea> 
                    </div>
                </div> 
                 <div class="col-md-6"> 
                  <div class="form-group locations">               
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="button" value="Cancel" onClick="window.location = '<?php echo site_url('Device_list'); ?>';" class="btn btn-secondary">Cancel</button>
                    </div>
                   
                 </div>
                   			

                </div>	
               <!--- <div class="row" style="display:inline; text-align: center;">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="button" value="Cancel" onClick="window.location = '<?php echo site_url('Device_list'); ?>';" class="btn btn-secondary">Cancel</button>
                    </div>
                </div> --->


            </div>
           <!--- <div class ="row">
                <div class="col-md-6 check_box_wifi">
                    <div class="form-group">
                        <label>Recent Command History  <small><button onClick="clear_history()" type="button" class="btn btn-xs btn-danger">Clear History</button></small></label>
                        <textarea class="form-control" id="recent_cmd_list" rows="8" cols="50">
                        </textarea> 
                    </div>
                </div> 
            </div>-->


            <!-- /.box-body -->

            <!--<div class="row" style="display:inline; text-align: center;">
            <div class="box-footer">
            <button type="submit" class="btn btn-primary">Update</button>
            <button type="button" value="Cancel" onClick="window.location='<?php echo site_url('Device_list'); ?>';" class="btn btn-secondary">Cancel</button>
            </div>
            </div>-->
    </div>
</form>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalclear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="display:inline-block;text-align: center;">
                <div class="icon-box">
                    <i class="material-icons" style="font-size:48px;color:red">error_outline</i>
                </div>        
                <h3 class="modal-title">Are you sure!</h3>  
                <p>you want to clear user ?</p>
            </div> 
            <div class="modal-footer" style="text-align: center; display:inline">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger clear_user">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">User List</h4>
            </div>
            <div class="modal-body">
                <select class="form-control" id="userlist">

                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary ok_dialog">Ok</button>
            </div>
        </div>
    </div>
</div>
<link href="<?php echo base_url(); ?>assets/plugins/parsley/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/plugins/parsley/parsley.min.js"></script>
<script type="text/javascript">
                         $(document).ready(function () {
                             $("input[data-bootstrap-switch]").each(function () {
                                 $(this).bootstrapSwitch('state', $(this).prop('checked'));
                             });
                             $('.bootstrap-switch-handle-off').click(function () {
                                 $("#wifi_enable").val(1);
                             });
                             $('.bootstrap-switch-handle-on').click(function () {
                                 $("#wifi_enable").val(0);
                             });
                             $("#device_model").each(function () {
                                 if ($('#device_model').val() == "Coolum" || $('#device_model').val() == "Montville" || $('#device_model').val() == "Obi") {
                                     $(".check_box_wifi").show();
                                     $(".locations").css("padding","87px");
                                     $('.locations').css('float','');
                                 } else {
                                     $(".check_box_wifi").hide();
                                      $('.locations').css('float','right');
                                       $(".locations").css("padding","");
                                 }
                             });

                             $("#device_model").change(function () {
                                 if ($('#device_model').val() == "Coolum" || $('#device_model').val() == "Montville" || $('#device_model').val() == "Obi") {
                                     $(".check_box_wifi").show();
                                      $(".locations").css("padding","87px");
                                       $('.locations').css('float','');
                                 } else {
                                     $(".check_box_wifi").hide();
                                      $('.locations').css('float','right');
                                      $(".locations").css("padding","");
                                 }
                             });


                             $('#Browser_Timezone').click(function () {
                                 var timezone = getTimeZone();
                                 var optionExists = ($('#timezone option[value="' + timezone + '"]').length > 0);
                                 if (!optionExists)
                                 {
                                     $('#timezone').append("<option value='" + timezone + "'>" + timezone + "</option>");
                                 }

                                 $("#timezone").val(timezone).select2();
                                 if (timezone != "") {
                                     $('#time_zone_tick').val('on');
                                 } else {
                                     $('#time_zone_tick').val('');
                                 }
                             });
                             $('#timezone').change(function () {
                                 if ($("#timezone").val() != "") {
                                     $('#time_zone_tick').val('on');
                                 } else {
                                     $('#time_zone_tick').val('');
                                 }

                             });
                             $('.select2').select2({
                                 width: 'auto',
                             });

                             $("#timezone").select2({
                                 width: '100%'
                             });
                             //Initialize Select2 Elements
                             $('.select2bs4').select2({
                                 theme: 'bootstrap4'
                             })

                             $("#_assigned_users").select2({
                                 placeholder: "Please Select",
                                 width: 'auto;',
                                 //width: '100%' 
                             });

                             $("#_assigned_users").on("change", function (e) {
                                 $("#assignuserval").val($("#_assigned_users").val());
                             });
                             loadMembers();
                             function loadMembers(refresh = false) {
                                 console.log('member is load');
                                 var API_URL = '<?php echo API_URL . 'member_list'; ?>';
                                 var result_html;
                                 $.post(API_URL, {token: '<?php echo $this->session->userdata("token"); ?>'}, function (response) {
                                     var obj = JSON.parse(response);
                                     if (obj.result == "true") {
                                         var i = 0; 
                                         result_html = "<option value=' '> Please Select </option>";
                                         $(obj.data).each(function () {
                                             if (obj.data[i]['id'] == '<?php echo $device->data->assigned_users_id ?>' && refresh == false) {
                                                 result_html += '<option value="' + obj.data[i]['id'] + '" selected>' + obj.data[i]['user_name'] + '</option>';
                                             } else {
                                                 result_html += '<option value="' + obj.data[i]['id'] + '">' + obj.data[i]['user_name'] + '</option>';
                                             }
                                             i++;
                                         });
                                     } else {
                                         result_html = "<option value=''> Please Select </option>";
                                     }
                                     $('#_assigned_users').html(result_html);
                                     $("#_assigned_users").trigger("change");

                                 }).fail(function (response) {
                                     alert('Error: ' + response.responseText);
                                 });
                             }
                             //End.
                             $(".ok_dialog").click(function () {
                                 var x = $("#userlist option:selected").val();
                                 var y = $("#userlist option:selected").text();
                                 console.log(x);
                                 $('#assignuserval').val(x);
                                 $('#assignuservallabel').val(y);
                                 $('#myModal').modal('hide');
                             });


                             /*$('#datepicker').datepicker({
                              autoclose: true,
                              dateFormat: "yy-mm-dd"
                              })*/
                             //$( "#datepicker" ).datepicker('disable');
                             $("#datepicker13").datepicker({format: 'dd-mm-yyyy', autoclose: true});


                             $('#myModal').on('show.bs.modal', function () {
                                 var id = $('#id').val();
                                 var API_URL = '<?php echo API_URL . 'member_list'; ?>';
                                 $.ajax({
                                     type: 'POST',
                                     dataType: 'JSON',
                                     url: API_URL, //Here you will fetch records 
                                     data: {"token": '<?php echo $this->session->userdata('token'); ?>'},
                                     success: function (response) {
                                         var result_html;
                                         if (response.result == "true") {
                                             var i = 0;
                                             $(response.data).each(function () {
                                                 result_html += '<option value="' + response.data[i]['id'] + '">' + response.data[i]['user_name'] + '</option>';
                                                 i++;
                                             });
                                         } else {
                                             alert(response.msg, "warning");
                                         }
                                         $('#userlist').html(result_html);
                                     }
                                 });

                                 /*$.ajax({
                                  type: 'POST',
                                  dataType: 'JSON',
                                  data :{id:id},
                                  url : '<?php echo base_url('index.php/Device_list/getdeviceid') ?>', //Here you will fetch records 
                                  success:function (data) {	
                                  $('#userlist').html(data);
                                  }
                                  });*/
                             });

//unassigned_user
//edit_device_id 
//assigned_users_id

                             $(".clear_user").click(function () {
                                 $('#myModalclear').modal('hide');
                                 var id = '<?php echo $device->data->id ?>';
                                 var user_id = '<?php echo $device->data->assigned_users_id ?>';
                                 var API_URL = '<?php echo API_URL . 'unassigned_user'; ?>';
                                 $.ajax({
                                     type: 'POST',
                                     url: API_URL, //Here you will fetch records 
                                     data: {"asset_id": id, 'user_id': user_id, 'token': '<?php echo $this->session->userdata("token"); ?>'},
                                     success: function (data) {
                                         var obj = JSON.parse(data);
                                         if (obj.result == "true") {
                                             loadMembers(true);
                                             alert(obj.msg, "success");
                                             /*$('#_assigned_users').select2({
                                              placeholder: "select"
                                              });
                                              $("#_assigned_users").trigger("change");
                                              */
                                             $("#assignuserval").val("");

                                         } else {
                                             alert(obj.msg, "warning");
                                         }

                                         /*console.log('success');
                                          var x = '';
                                          var y = $("#userlist option:selected").text();
                                          $('#assignuservallabel').val(x);
                                          $('#assignuserval').val(x);
                                          */
                                     }

                                 });

                             });

                             $("#update_form").on('submit', function (event) {
                                 // validate form with parsley.

                                 $(this).parsley().validate();
                                 $(".print-error-msg").show();
                                 // if this form is valid
                                 if ($(this).parsley().isValid()) {
                                     // show alert message
                                     $(".print-error-msg").hide();

                                     var id = $("#id").val();
                                     if (id != "") {
                                         var API_URL = '<?php echo API_URL . 'edit_device'; ?>';
                                     } else {
                                         var API_URL = '<?php echo API_URL . 'add_device'; ?>';
                                     }


                                     $.post(API_URL, $("#update_form").serialize(), function (data) {

                                         var obj = JSON.parse(data);
                                         if (obj.result == "true") {
                                             reload_grid();
                                             alert(obj.msg, "success");
                                         } else {
                                             if (id != "") {
                                                 alert(obj.msg, "warning");
                                             } else {
                                                 alert(obj.msg, "warning");
                                             }
                                         }


                                     }).fail(function (response) {

                                         alert('Error: ' + response.responseText);
                                     });

                                 } else {
                                     $(".print-error-msg").show();

                                 }

                                 event.preventDefault();
                             });

                         });

                         $("#command_send_to_device").click(function () {
                             var command_val = $("#command_value").val();
                             var device_model = $("#device_model").val();
                             var device_id = $("#device_id").val();
                             if (command_val == "") {
                                 alert("Please enter device Command.");
                                 return false;
                             }
                             if (device_id == "") {
                                 alert("Please enter device id.");
                                 return false;
                             }
                             var formData = {token: "<?php echo $this->session->userdata('token'); ?>", device_id: device_id, model_type: device_model, command_value: command_val, command_name: 'webcommand'};
                             $.ajax({
                                 url: "<?php echo API_URL . 'gprs_command'; ?>",
                                 type: "POST",
                                 data: formData,
                                 success: function (response) {
                                     var JSONArray = $.parseJSON(response);
                                     if (JSONArray.result == 'true') {
                                         alert(JSONArray.msg);
                                     } else {
                                         alert(JSONArray.msg);
                                     }
                                 }
                             })
                         });

                         function reload_grid() {
                             $('#device_form_div').hide();
                             $('#device_div').show();
                             table.ajax.reload(null, false);
                         }
                         setInterval(function () {
                             recent_command_show();
                         }, 5000);
                         function recent_command_show() {
                             var id = $('#id').val();
                             var timezone = $('#timezone').val();
                             $.post("<?php echo base_url() ?>index.php/Device_list/recent_command_list", {assets_id: id, timezone: timezone}, function (row) {
                                 //console.log(row);
                                 $("#recent_cmd_list").html(row);
                             });

                         }
                         function clear_history() {
                             var id = $('#id').val();
                             var timezone = $('#timezone').val();
                             $.post("<?php echo base_url() ?>index.php/Device_list/recent_command_list", {del_id: id, timezone: timezone}, function (row) {
                                 $("#recent_cmd_list").html(row);
                             });
                         }
</script>

<!-- ================== END PAGE LEVEL JS ================== -->


