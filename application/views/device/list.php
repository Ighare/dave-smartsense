<!--<link href="<?php //echo base_url() ?>assets/plugins/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="<?php //echo base_url() ?>assets/plugins/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" /> 

<link href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css" rel="stylesheet" />
-->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view("header.php");
?>
<style type="text/css">
  .form-inline{display: block !important}
  
  body .dataTables_wrapper table.dataTable{
    /*margin: 0 0!important;*/
  }
  
  tbody>tr.even>:nth-child(2) , tbody>tr.even>:nth-child(1){
   background: #ffffff;
 }

 tbody>tr.old>:nth-child(2) , tbody>tr.old>:nth-child(1){
   background: #EAF3D9;
 }

 tbody#show_data td {
  height: 18px;
  padding-top: 5px;
  padding-bottom: 5px;
}

.top-l , .top-r {
  width:50%;
}
.top-l {
  float:left;
}
.top-r {
  float:right;
}
#table_filter, #usertable_filter {
  width:50%;
  float: left;
}
#table_paginate, #usertable_paginate  {
  width:50%;
  float: right;
}

#table_length, #usertable_length{
  margin-left: 15px;
}
table.dataTable tbody th,
table.dataTable tbody td {
  white-space: nowrap;
}

</style>
<script type="text/javascript">
var table;
var table1;
var data_array=[];
var device_array=[];
 function add()
 { 

  $('#device_div').hide();
  $('#device_form_div').show();
  $('#device_form_div').load('<?php echo base_url() ?>index.php/Device_list/add');
}
function load_map(){
	
	var ischecked = false;
	var hk_count = 0;
  //var vi=1;
 /* $('[id^=b_id_]').each(function () {
	if(data_array[0]==vi){
		 hk_count = hk_count + 1;
		 ischecked = true;
	}else{
		$('.remove_'+vi).prop('checked', false);
	}
	vi++;
  }); */ 
  var id="";
  if(device_array.length > 0){
	  hk_count=1;
	  ischecked=true;
	  id=device_array[0];
  }
  if (hk_count == 0) {
    alert("Please select atleast one record");
    return false;
  } 
  //console.log(data_array);
  if (ischecked == true)
  {
	var viewportwidth = document.documentElement.clientWidth;
	var viewportheight = document.documentElement.clientHeight;
	//console.log(viewportwidth);
	//console.log(viewportheight);
	
	window.open ("<?php echo base_url();?>index.php/home?map=true&location="+id,"mywindow","menubar=1,left="+(viewportwidth-300)+",resizable=1,width=763,height=700");

  }
}
function edit()
{
  var ischecked = false;
  var hk_count = 0;
  /*$('[id^=b_id_]').each(function () {
    if ($(this).prop("checked") == true)
    {
      hk_count = hk_count + 1;
    }
  });*/
  var id="";
  if(device_array.length > 0){
	  hk_count=1;
	  ischecked=true;
	  id=device_array[0];
  }
  if (hk_count == 0) {
    alert("Please select atleast one record");
    return false;
  }/*  else if (hk_count == 1) {
    ischecked = true;
  } else if (hk_count > 1) {
    alert("Please select only one record");
    return false;
  } */
  if (ischecked == true)
  {


   // var id = "";
    $('[id^=b_id_]').each(function () {
      if ($(this).prop("checked") == true)
      {
        id += $(this).prop("id").replace('b_id_', '');
      }
    });
    $('#device_div').hide();
    $('#device_form_div').show();
    $('#device_form_div').load('<?php echo base_url() ?>index.php/Device_list/edit/' + id);

  }
} 
function del(){
  var ischecked = false;
  var hk_count = 0;
  /*$('[id^=b_id_]').each(function () {
    if ($(this).prop("checked") == true)
    {
      hk_count = hk_count + 1;
    }
  });*/
  var id="";
  if(device_array.length > 0){
	  hk_count=1;
	  ischecked=true;
	  id=device_array[0];
  }
  if (hk_count == 0) {
    alert("Please select atleast one record");
    return false;
  } /* else if (hk_count == 1) {
    ischecked = true;
  } else if (hk_count > 1) {
    alert("Please select only one record");
    return false;
  } */
  if (ischecked == true)
  {


   // var id = "";
    $('[id^=b_id_]').each(function () {
      if ($(this).prop("checked") == true)
      {
        id += $(this).prop("id").replace('b_id_', '');
      }
    });

			//Ajax Start 
			var cfm = confirm("You want to delete this device?");
			if (cfm == true)
			{	
       $('.swal-button--confirm').unbind().click(function(){
         var API_URL = '<?php echo API_URL.'delete_device'; ?>';	
			 // AJAX request
			 $.ajax({
        url: API_URL,
        type: 'post',
        data: {device_id: id,token:'<?php echo $this->session->userdata('token');?>'},
        success: function(response){
         var obj = JSON.parse(response);	
				   // Remove <tr>
				   if(obj.result == "true"){
            reload_grid();
						//alert("Good job!", obj.msg, "success");
						alert(obj.msg); 
						
         }else{
					   //alert("Warning", obj.msg, "warning");
					   alert(obj.msg); 
          }
        }
      });
      });
     }
		 //Ajax End

    }	

  }
function checkalldata()
  {
    if ($('#checkall').prop("checked") == true)
    {
      $('[id^=b_id_]').prop('checked', true);

    } else
    {
      $('[id^=b_id_]').prop('checked', false);
    }
  }
  function reload_grid(){
    $('#device_form_div').hide();
    $('#device_div').show(); 
    $('#table').DataTable().ajax.reload();

  }
</script>
<!-- Content Wrapper. Contains page content -->
<div id="content" class="content1">
 <div id="device_div">
   <div class="panel panel-inverse">
    <div class="panel-heading">
     <?php if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') { ?>
       <h4 class="panel-title">Device List</h4>
       <?php
     } else { ?>
      <h4 class="panel-title">About Device</h4>
      <?php
    } ?> 
  </div>
  <!-- Page Heading -->
  <div class="panel-body">         
   <div class="container-fluid">
    <div class="row">
     <div class="col-12">
       <?php if ($this->session->userdata("user_type") == '0' || $this->session->userdata("user_type") == '1') { ?>
        <table id="table" class="table display table-striped table-bordered nowrap"  width="100%">
         <thead> 
          <tr>
           <th style="width:20px !important" class="check_input col_no"></th>
          
           <th class="text-nowrap">Device Name</th>
           <th class="text-nowrap">Device ID</th>
           <th class="text-nowrap">User</th>
           <th class="text-nowrap" >Last Update</th>
           <th class="text-nowrap">Device</th>
           <th class="text-nowrap">SIM Number</th>
           <th class="text-nowrap">SIM Account</th>
           <th class="text-nowrap">SIM Password</th>
           <th class="text-nowrap" id="Activation">Activation</th>
           <th class="text-nowrap" id="Emergency">Emergency Nos</th>
           <th class="text-nowrap" id="Notes">Notes</th>
		    <th class="text-nowrap">index</th>
		    <th class="text-nowrap">Device ID</th>
         </tr>
       </thead>
       <tbody id="show_data">
       </tbody>
     </table>
   <?php } else { ?>
    <table id="usertable" style="margin-bottom: 10px !important" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
     <thead>
      <tr>     
       <th style="width:20px !important" class="check_input col_no"></th>
       
       <th class="text-nowrap" >Device Name</th>
       <th class="text-nowrap">Device ID</th>
       <th class="text-nowrap">User</th>
       <th class="text-nowrap">Device</th>
       <th class="text-nowrap">SIM Number</th>
       <th class="text-nowrap">SIM Account</th>
       <th class="text-nowrap">SIM Password</th>
       <th class="text-nowrap">Activation</th>
       <th class="text-nowrap">Emergency Nos</th>
	   <th class="text-nowrap" >index</th>
	    <th class="text-nowrap">Device ID</th>
     </tr>
   </thead>
   <tbody id="show_data">
   </tbody>
 </table>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>

<div id="device_form_div" style="display: none;"></div>

<?php
$this->load->view("footer.php");
?>


<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script>
  
 
  var scroll = false;
  if(window.screen.width < 1200){
    scroll = true;
  } 

  $(document).ready(function(){ 
  var Emergency=Cookies.get('Emergency');  
  var Notes=Cookies.get('Notes');
	
    table = $('#table').DataTable({ 		
      dom: '<"top-l"Bl><"top-r"fp>tri<"bottom"><"clear">',
      buttons: [
      {
        text: 'Add',
        className: 'btn btn-primary',
        action: function ( e, dt, node, config ) {
          add();
        }
      },{
        text: 'Edit',
        className: 'btn btn-primary',
        action: function ( e, dt, node, config ) {
          edit();
        }

      },{
        text: 'Delete',
        className: 'btn btn-danger',
        action: function ( e, dt, node, config ) {
          del();
        }
      },{
        text: 'Refresh',
        className: 'btn btn-warning',
        action: function ( e, dt, node, config ) {
          reload_grid();
        }
      },
	  {
        text: 'Map',
        className: 'btn btn-info',
        action: function ( e, dt, node, config ) {
          load_map();
        }
      }
      ],	  
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
		"paging": false,		
        'type': 'post',
        "searching": true,
        "pageLength": -1, 
        "ordering":true,
        "selected": true,
        "aLengthMenu": [[-1,25, 50, 100], ["All","25", "50", "100"]],
        "columnDefs": [
        {
         "targets": [0,7,8,10,11],
         "orderable": false,

       },
	   { "visible": false, "targets": [0,12,13] },
       ], 
       "scrollX": "scroll",
       "scrollCollapse": false, 

      "scrollY":"76vh",
      "language": {                
        "infoFiltered": ""
      },  
      "ajax": {
        "url": "<?php echo site_url('Device_list/loaddata') ?>",
        "type": "POST"
      }, 
    });
    table1 = $('#usertable').DataTable({ 

      dom: '<"top-l"Bl><"top-r"fp>tri<"bottom"><"clear">',
      buttons: [
      {
        text: 'Edit',
        className: 'btn btn-primary',
        action: function ( e, dt, node, config ) {
          edit();
        }

      },{
        text: 'Delete',
        className: 'btn btn-danger',
        action: function ( e, dt, node, config ) {
          del();
        }
      }
      ],
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        'type': 'post',
        "searching": true,
        "pageLength": 10, 
        "ordering":true, 
        "selected": true,
        "aLengthMenu": [[-1,25, 50, 100], ["All","25", "50", "100"]],
        "columnDefs": [
        {
          "targets": [0,2],
          "orderable": false
        },
		{ "visible": false, "targets": [0,11,12] },
        ],
        "scrollX": "scroll",
        "scrollCollapse": true, 
        "scrollY":"auto",
        "language": {                
          "infoFiltered": ""
        },
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo site_url('Device_list/loaddata') ?>",
          "type": "POST"
        },

        //Set column definition initialisation properties.

      });
	 
    $('#table tbody, #table tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $(this).find('input').attr("checked",false);
			device_array=[];
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
           $('#table tbody, #table tbody').find('input').attr("checked",false);
            $(this).find('input').attr("checked",true);
			console.log($(this).hasClass('selected'));
			if ($(this).hasClass('selected')){
			data_array=[];
			device_array=[];
			data_array.push(table.cell('.selected',12).data());
			device_array.push(table.cell('.selected',13).data());
			console.log(device_array);
			return false;			
			}
			
        }
    } );
	 $('#usertable tbody, #usertable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $(this).find('input').attr("checked",false);
			device_array=[];
        }
        else {
            table1.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
           $('#usertable tbody, #usertable tbody').find('input').attr("checked",false);
            $(this).find('input').attr("checked",true);
			console.log($(this).hasClass('selected'));
			if ($(this).hasClass('selected')){
			data_array=[];
			device_array=[];
			data_array.push(table1.cell('.selected',11).data());
			device_array.push(table1.cell('.selected',12).data());
			console.log(device_array);
			return false;			
			}
			
        }
    } );
	$("#Emergency").on('mouseover',function(){
		 var emergency=$('#Emergency').width();
		Cookies.set('Emergency', emergency, {expires: 365});
	});
	$("#Notes").on("mouseover",function(){
		 var Notes=$('#Notes').width();
		Cookies.set('Notes', Notes, {expires: 365});	
	});
	
	
  });
  


  
</script>

<!-- ================== END PAGE LEVEL JS ================== -->


