#!/usr/bin/env bash

find /var/www/html/smartsensegps/traccar_logs -mtime +3 -type f -delete
find /var/www/html/smartsensegps/telnet/5093 -type f -name "*.txt" -mtime +3 -delete
find /var/www/html/smartsensegps/logs -mtime +3 -type f -delete