#!/usr/bin/env bash

# Add your backup dir location, s3 location, password, mysql location and mysqldump location
DATE=$(date +%d-%m-%Y)
BACKUP_DIR="/home/backup"
DAYS_TO_KEEP=2
MYSQL_USER=root
MYSQL_PASSWORD=aDmin@202284
MYSQL=/usr/bin/mysql
MYSQLDUMP=/usr/bin/mysqldump

# purge old backups
find $BACKUP_DIR -mindepth 1 -mtime +$DAYS_TO_KEEP -type f -delete

# To create a new directory into backup directory location
mkdir -p $BACKUP_DIR/$DATE

# get a list of databases
databases=`$MYSQL -u$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema)"`

# dump each database in separate name
for db in $databases; do
echo $db
$MYSQLDUMP --force --opt --user=$MYSQL_USER -p$MYSQL_PASSWORD --skip-lock-tables --databases $db | gzip > "$BACKUP_DIR/$DATE/$db.sql.gz"

done

