#!/bin/bash

# What to backup. 
backup_files="/var/www/html /opt"

# Where to backup to.
BACKUP_DIR="/home/backup"
DATE=$(date +%d-%m-%Y)
DAYS_TO_KEEP=5

# purge old backups
find $BACKUP_DIR -mindepth 1 -mtime +$DAYS_TO_KEEP -type f -delete

tar czf $BACKUP_DIR/$DATE/$DATE.tgz $backup_files

rsync -avz -e ssh ~/.ssh/authorized_keys /etc/opt/* root@192.99.15.42:/home/bt/dave_server

