<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>
</div>
<style>
	.main-footer{
	position: fixed;
    bottom: 0;
    width: 100%;
	}
	
</style>
 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="javascript:void(0);">Smart Sense</a>.</strong>
    All rights reserved.
    <!--<div class="float-right d-none d-sm-inline-block"><b>Version</b> 3.0.1-pre</div>-->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(); ?>assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!--<script src="<?php echo base_url(); ?>assets/plugins/sparklines/sparkline.js"></script>-->
<!-- JQVMap -->
<!--<script src="<?php echo base_url(); ?>assets/plugins/jqvmap/jquery.vmap.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>-->
 <!-- ================== Datatable JS ================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/DataTables/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/DataTables/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/ColReorderWithResize.js"></script>
    
    <!-- ================== Datatable JS ================== -->
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(); ?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard.js"></script>-->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/js.cookie.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/DataTables/js/dataTables.fixedColumns.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/DataTables/js/dataTables.buttons.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.js"></script>
<script type="text/javascript">
//added by Rajesh to get timezoen as per user 
//This is logic also daylight timezone 
function getTimeZone() {  
    var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
    return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
} 
//end 
 

   $(document).ready(function () {

      
 
window.alert = function(a,b=null,c=null) {
            if(b!=null && c!=null){
                swal(a,b,c);
            }else{
            swal(a);
            }

            return true;
        }
        window.confirm = function(a,b=null,c=null) {
          if(b==null) b ="Are you sure?";
            swal({
              title: b,
              text: a,
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
          
            return true;
        }
      });

   
    </script>    
</body>
</html>
