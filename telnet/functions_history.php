<?php

// //echo phpinfo();
require_once("db.php");
//require_once('PHPMailer/class.phpmailer.php');
function WriteLogROZ($string) {
    $myFile = "log_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}
function deadme($str){
	WriteLogROZ(" [sqls] ".$str);
	die("");
}
function validateLatLong($lat, $long) {
  return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $lat.','.$long);
}
function deadme_di($str){
	WriteLogNewDevice(" Device: ".$str);
	//die("");
}
function alerts_log($string) {
    $t_date = date('Y-m-d');
    $myFile = "alert_log" . $t_date . ".txt";
    $fh = fopen($myFile, 'a') or die("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
    die($string);
}
// This function is used to get the time difference from two dates passed into it.
// Difference can have different formats, such as seconds, minutes, hours, weeks, days et.c

function timeBetween($startDate, $endDate, $format = 1) {
    list($date, $time) = explode(' ', $endDate);
    $startdate = explode("-", $date);
    $starttime = explode(":", $time);

    list($date, $time) = explode(' ', $startDate);
    $enddate = explode("-", $date);
    $endtime = explode(":", $time);

    $secondsDifference = mktime($endtime[0], $endtime[1], $endtime[2], $enddate[1], $enddate[2], $enddate[0]) - mktime($starttime[0], $starttime[1], $starttime[2], $startdate[1], $startdate[2], $startdate[0]);

    switch ($format) {
        // Difference in Minutes
        case 1:
            return floor($secondsDifference / 60);
        // Difference in Hours    
        case 2:
            return floor($secondsDifference / 60 / 60);
        // Difference in Days    
        case 3:
            return floor($secondsDifference / 60 / 60 / 24);
        // Difference in Weeks    
        case 4:
            return floor($secondsDifference / 60 / 60 / 24 / 7);
        // Difference in Months    
        case 5:
            return floor($secondsDifference / 60 / 60 / 24 / 7 / 4);
        // Difference in Years    
        default:
            return floor($secondsDifference / 365 / 60 / 60 / 24);
    }
}
function show_voltage($adc){ 
		$intercept = -4863.73564;
		$B1 = 2.36374;
		$B2 = -0.000281595;
		$MIN_ADC = 3500;
		$MAX_ADC = 4100;
		if (!$adc) return 0;
		$adc = floatval($adc);
		$y = 0;
		if ($adc < $MIN_ADC){
		$y = 1;	
		}else if ($adc > $MAX_ADC){
		$y = 100;	
		}else{
		$y = $intercept + $B1 * $adc + $B2 * pow($adc, 2);	
		}
		if ($y > 100) $y = 100;
		if ($y <= 1) $y = 10;
		return floor($y);
}
function sendMobileNotification($conn,$user_id, $smsText1, $asset_id="") 
{
	$type="Alert";
	//$smsText1=$smsText.'<br/>DateTime: '.date('Y-m-d H:i:s');
    $url = 'http://199.217.112.41/dev/pushnotification.php?user_id=' . urlencode($user_id) . '&msg=' . urlencode($smsText1) . '&type=' . urlencode($type) . '&assets_id=' . urlencode($asset_id) . '';	
	$query = "INSERT INTO tbl_pending_notifications ( `user_id`, `msg`, `type`, `assets_id`, `add_date` ) VALUES ('". ($user_id)."','". ($smsText1)."','". ($type)."','". ($assets_id)."','". gmdate(DATE_TIME)."' )";
    mysqli_query($conn,$query) or deadme(mysqli_error($conn) . ":" . $query); 
	WriteLogROZ($url);
    $curl = curl_init();  
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($curl, CURLOPT_HEADER, false);
    curl_exec($curl); 	
    if (curl_errno($curl)) {
        $error = curl_error($curl); 

        WriteLogROZ("CURL Error: in sendMobileNotification(): " . $error);
    } else {
        WriteLogROZ("in sendMobileNotification(): mobile_no:  " . $user_id . "SMS text: " . $smsText1);
        curl_close($curl);
    } 
} 
function check_inout_geofence_alert($conn,$asset_id,$device_name,$device_dt,$address,$latitude,$longitude){ 
	$data_inserted=false;
	$device_users=date_create($device_dt);
	$device_users=date_format($device_users,"d F Y,H:i:s");
	global $current_landmark, $current_landmark_id, $dts;	
	$query="SELECT * FROM `tbl_geofence` WHERE FIND_IN_SET($asset_id,assigned_devices) and status=1";
	$rs = mysqli_query($conn,$query);
	while ($row = mysqli_fetch_array($rs)){ 		
		$geo_id = $row['id'];
		$distance_value = $row['geofence_radius'];
		$geofence_name = $row['geofence_name'];
		$geofence_latitude = $row['geofence_latitude'];
		$geofence_longitude = $row['geofence_longitude'];
		$geofence_in_alert = $row['geofence_in_alert'];
		$geofence_out_alert = $row['geofence_out_alert'];		 
		$user_id = $row['add_uid'];				
		$unit="KM";
		$distanceFromLandmark =(getDistance($latitude, $longitude, $geofence_latitude, $geofence_longitude, $unit) * 1000);			
	if($distanceFromLandmark < $distance_value){
			//in part of geo-fence s
		$checkLast = "SELECT geo_id, status FROM tbl_geofence_log WHERE device_id = $asset_id and geo_id = $geo_id order by id desc limit 1";		
		$checkRs = mysqli_query($conn,$checkLast) or deadme(mysqli_error($conn) . ":" . $checkLast);
		$checkRow = mysqli_fetch_array($checkRs);		
        $lastLandmarkId = $checkRow['geo_id'];
        $inOutStatus = $checkRow['status'];
		$current_landmark = $geofence_name;
        $current_landmark_id = $lastLandmarkId;
		//var_dump(mysqli_num_rows($checkRs));
		if (!mysqli_num_rows($checkRs)) {  //check for first time log
                $inOutStatus = 'out';
         }
		 if ($inOutStatus == 'out'){
			 $insert="INSERT INTO `tbl_geofence_log`(`device_id`, `status`, `distance`, `geo_id`) VALUES ('$asset_id','in','$distanceFromLandmark','$geo_id')";			
			  mysqli_query($conn,$insert) or deadme(mysqli_error($conn) . ":" . $insert);
			  $data_inserted=true; 
			  //var_dump($geofence_in_alerts);
			 if($geofence_in_alert==1){
				 //The vehicle MRS0125 is entered in jalna geofence at 2019-11-07 11:30 AM
				 $smsText="The vehicle $device_name is entered in $geofence_name at $device_users.";
				 sendMobileNotification($conn,$user_id,$smsText,$asset_id);
			 }
		 }
		}else{
			//out part of geo-fence
			$checkLast = "select id from tbl_geofence_log where device_id = $asset_id and geo_id = $geo_id and status = 'in' order by id desc limit 1";			
            $checkRs = mysqli_query($conn,$checkLast) or deadme(mysqli_error($conn) . ":" . $checkLast);
			 if (mysqli_num_rows($checkRs) > 0){
				$checkRow = mysqli_fetch_array($checkRs);
                $lId = $checkRow['id'];
				$uLSql = "update tbl_geofence_log  set status = 'out',distance = '$distanceFromLandmark' where id = $lId";	
                mysqli_query($conn,$uLSql) or deadme(mysqli_error($conn) . ":" . $uLSql);
			if($geofence_out_alert==1){
			$smsText="The vehicle $device_name is exited in $geofence_name at $device_users.";
			sendMobileNotification($conn,$user_id,$smsText,$asset_id);	
			}
			 }
			
		}
	}
	
}
function convertSpeed($kms, $unit = "miles") {

    if ($unit == 'angstrom') {
        $ratio = 10000000000000;
        $result = $kms * $ratio;
    }
    if ($unit == 'centimeters') {
        $ratio = 100000;
        $result = $kms * $ratio;
    }
    if ($unit == 'feet') {
        $ratio = 3280.84;
        $result = $kms * $ratio;
    }
    if ($unit == 'furlongs') {
        $ratio = 4.97;
        $result = $kms * $ratio;
    }
    if ($unit == 'inches') {
        $ratio = 39370.08;
        $result = $kms * $ratio;
    }
    if ($unit == ' meters') {
        $ratio = 1000;
        $result = $kms * $ratio;
    }
    if ($unit == 'microns') {
        $ratio = 1000000000;
        $result = $kms * $ratio;
    }
    if ($unit == 'miles') {
        $ratio = 1.609344;
        $result = $kms / $ratio;
    }
    if ($unit == 'millimeters') {
        $ratio = 1000000;
        $result = $kms * $ratio;
    }
    if ($unit == 'yards') {
        $ratio = 1093.61;
        $result = $kms * $ratio;
    }
    return $result;
}
function convert_time_zone($date_time, $to_tz, $ret_format = 'Y-m-d H:i:s', $from_tz = 'UTC') {
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    $time_object->setTimezone(new DateTimeZone($to_tz));
    return $time_object->format($ret_format);
}
function deg_to_decimal_satish($deg) {

    if ($deg == '')
        return 0.000000;

    $sign = substr($deg, -1);

    if (strtoupper($sign) == "E" || strtoupper($sign) == "W")
        $sign = -1;
    if (strtoupper($sign) == "N" || strtoupper($sign) == "S")
        $sign = 1;

    $deg = substr($deg, 0, strlen($deg) - 1);

    $deg = preg_replace('/^0+/', '', $deg);

    $t_deg = explode('.', $deg);

    if (strlen($t_deg[0]) > 4)
        $len = 3;
    elseif ($sign == -1)
        $len = 1;
    else
        $len = 2;

    $degree = substr($deg, 0, $len);

    $deg = substr($deg, $len);

    $decimal = $sign * number_format(floatval((($degree * 1.0) + ($deg / 60))), 6);

    return $decimal;
}

function deg_to_decimal($deg) {

    if ($deg == '')
        return 0.000000;

    $sign = substr($deg, -1);

    if (strtoupper($sign) == "N" || strtoupper($sign) == "E")
        $sign = 1;
    if (strtoupper($sign) == "W" || strtoupper($sign) == "S")
        $sign = -1;

    $deg = substr($deg, 0, strlen($deg) - 1);

    // $deg = floatval($deg);

    $degree = substr($deg, 0, -7);
    $decimal = substr($deg, -7);

    ////echo "Degree : $degree, Decimal : $decimal";
    ////echo "$sign * number_format(floatval((($degree * 1.0) + ($deg/60))),6);";

    $decimal = $sign * number_format(floatval((($degree * 1.0) + ($decimal / 60))), 6);

    return $decimal;
}

function deg_to_decimal_new($deg) {

    if ($deg == '')
        return 0.000000;

    if ($deg < 0)
        $sign = -1;
    else
        $sign = 1;

    $deg = substr($deg, 1, strlen($deg));

    $deg = preg_replace('/^0+/', '', $deg);

    $degree = substr($deg, 0, 2);

    $deg = substr($deg, 2);

    $decimal = $sign * number_format(floatval((($degree * 1.0) + ($deg / 60))), 6);

    return $decimal;
}

function getNearestaddress($lat, $lng,$conn) {
	//return "INDIA";//Rajesh did this  
	$lat = round($lat,3);
	$lng = round($lng,3);
	$lat1 = round($lat,1);
	$lng1 = round($lng,1);
   $query = "SELECT  address, 
    ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
     * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) 
     * sin( radians( latitude ) ) ) ) AS distance 
   FROM tbl_geodata where latitude between ".($lat1-0.2)." and ".($lat1+0.2)." and longitude between ".($lng1-0.2)." and ".($lng1+0.2)."  HAVING distance < 0.25
   ORDER BY distance ASC LIMIT 1";     
    $res = mysqli_query($conn,$query) or deadme(mysqli_error($conn,$query) . ":" . $query);
    if (mysqli_num_rows($res) > 0) {
        $row = mysqli_fetch_assoc($res);
		print_r($row);
        return trim(trim(trim($row['address']),","));
    } else{
        $temp = getAddress($lat, $lng);
		print_r($temp);
        return $temp;
    } 
} 
function googleGeocode($lat, $lng) {
    //Google Map
	//https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&location_type=ROOFTOP&result_type=street_address&key=AIzaSyCuuefygL4r3OzV8qLQHOf_QAmTrcWdsY8
    $latlng = $lat . "," . $lng;    
    //$strURL = "https://maps.google.com/maps/api/geocode/xml?latlng=" . $latlng . "&key=AIzaSyAC8z-hOALCDcVgab9-tst8R3g7r66D7fU&sensor=false&region=in";
    $strURL = "https://maps.google.com/maps/api/geocode/xml?latlng=" . $latlng . "&key=AIzaSyBDc4TzkQ86YERrtCOCexTAyFe1MXAfVf8&sensor=false";
    $address = "";
//    $string = " Address = ".$strURL;
//        alerts_log($string);
   WriteLog('google->'.$strURL);
    $resURL = curl_init();
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYHOST, FALSE);
    $xmlstr = curl_exec($resURL);
	//WriteLog('add-issu:'.$xmlstr);

    $objDOM = new DOMDocument();
    

    if (!@$objDOM->loadXML($xmlstr)) {
       
        return '';
    }

    $address = @$objDOM->getElementsByTagName("formatted_address")->item(0)->nodeValue;
    
    $address = iconv('UTF-8', '', $address);
    if ($address != "") {
        return $address;
    }else{
        return '';
    }
}

function getNearest($lat, $lng) {
	//return 'INDIA';   
		$address=googleGeocode($lat, $lng);
		//print_r($address);exit;
		return $address;
	 
	
	
	
/*
	$conn_79 = @mysql_connect("localhost","root","admin123") or deadme('{"success2":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db("geocode" ,$conn_79);
*/
//commented by Rajesh ighare 2017-08-18
	/* $lat = round($lat,3);
	$lng = round($lng,3);
	$lat1 = round($lat,1);
	$lng1 = round($lng,1);
   $query = "SELECT  address, 
    ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
     * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) 
     * sin( radians( latitude ) ) ) ) AS distance 
   FROM tbl_geodata where latitude between ".($lat1-0.2)." and ".($lat1+0.2)." and longitude between ".($lng1-0.2)." and ".($lng1+0.2)."  HAVING distance < 0.25
   ORDER BY distance ASC LIMIT 1";   
    $res = mysql_query($query) or deadme(mysql_error() . ":" . $query);
    if (mysql_num_rows($res) > 0) {
        $row = mysql_fetch_assoc($res);        
        return trim(trim(trim($row['address']),","));
    }else{
        $temp = getAddress($lat, $lng);
        return $temp;
    } */
	//end
} 
function OurOwnGeocoding($lat, $lng) { 
//return 'INDIA'; 
	// Developed by Rajesh ighare 2017-08-18
	//This is for our geocode server and using json formate.
	//http://94.23.203.31/nominatim/reverse.php?format=json&lat=24.016362&lon=79.544752
	/*$strURL="http://94.23.203.31/nominatim/reverse.php?format=json&lat=$lat&lon=$lng";	
	$address = "";	
	$resURL = curl_init();
	//WriteLog($strURL); 
    curl_setopt($resURL, CURLOPT_URL, $strURL);
    curl_setopt($resURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($resURL, CURLOPT_SSL_VERIFYHOST, FALSE);
    $jsonstr = curl_exec($resURL);
	if (!curl_errno($resURL)) {
		if(curl_getinfo($resURL, CURLINFO_HTTP_CODE)==200){
			$StringAddress = json_decode($jsonstr);		
			$address="";
			if(isset($StringAddress->address->road)) 
				$address .= $StringAddress->address->road.",";
			if(isset($StringAddress->address->neighbourhood)) 
				$address .= $StringAddress->address->neighbourhood.",";
			if(isset($StringAddress->address->village)) 
				$address .= $StringAddress->address->village.",";
			if(isset($StringAddress->address->town)) 
				$address .= $StringAddress->address->town.",";
			if(isset($StringAddress->address->city)) 
				$address .= $StringAddress->address->city.",";
			if(isset($StringAddress->address->state)) 
				$address .= $StringAddress->address->state.",";
			if(isset($StringAddress->address->postcode)) 
				$address .= $StringAddress->address->postcode.",";
			if(isset($StringAddress->address->country)) 
				$address .= $StringAddress->address->country;
			setlocale(LC_ALL, "as_IN.UTF-8");
			$address=iconv('UTF-8', 'ASCII//TRANSLIT',$address);
		}else{
			WriteLogROZ(" [nominatim Code] : ".curl_getinfo($resURL, CURLINFO_HTTP_CODE));
			$address = getNearestaddress($lat, $lng);
		}
	}else{
		$address = getNearestaddress($lat, $lng);
	}*/
	//$address = getNearestaddress($lat, $lng);
	//curl_close($resURL);
     if ($address != "") {
        return $address;
    } else{
		return getAddress($lat, $lng);
	}
}

function distance($a, $b) {
    list($lat1, $lon1) = $a;
    list($lat2, $lon2) = $b;
    $theta = $lon1 - $lon2;
    $dist = sin(@deg2rad($lat1)) * sin(@deg2rad($lat2)) + cos(@deg2rad($lat1)) * cos(@deg2rad($lat2)) * cos(@deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    return $miles;
}
function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y) {
    $i = $j = $c = 0;
    for ($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i++) {
        if ((($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
                ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i])))
            $c = !$c;
			}
    return $c;
}

function getAddress($lat, $lng) 
{
	WriteLogROZ(" [Google Address] : ".$lat." : ".$lng);
	//return "";
    $today = gmdate(DATE);
    if (intval($lat) == 0 && intval($lng) == 0) 
    {
        return '';
    }
	$conn = @mysql_connect(DIHOST,DIUSER,DIPASS) or deadme('{"success3":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db(DIDB,$conn);


        $limit_query = "SELECT google, mapquest, yahoo FROM api_requests WHERE add_date = '$today'";
        $limit_res = mysql_query($limit_query) or deadme(mysql_error() . ":" . $limit_query);

        if (mysql_num_rows($limit_res) > 0) 
	{
            $row = mysql_fetch_assoc($limit_res);
            if ($row['google'] <= 100000) 
	    {
             	$address = googleGeocode($lat, $lng);	
                $qryUpdt = "UPDATE api_requests SET google=" . intval($row['google'] + 1) . " WHERE add_date='$today'";
                $insrtGeo = mysql_query($qryUpdt) or deadme("SQL : $qryUpdt : Error : " . mysql_error());
            } 
	    elseif ($row['yahoo'] <= 4500) 
	    {
            	$address = yahooGeocode($lat, $lng);				
            	$qryUpdt = "UPDATE api_requests SET yahoo=" . intval($row['yahoo'] + 1) . " WHERE add_date='$today'";
                $insrtGeo = mysql_query($qryUpdt) or deadme(mysql_error() . ":" . $qryUpdt);
            } 
	    else 
	    { 
                $address =googleGeocode($lng, $lat );//photonGeocode($lng,$lat);//return address from Photon reverse geocode
            }
        } 
	else 
	{
		$qryGeo = "Insert into api_requests values(NULL,1,0,0,'$today')";
		$insrtGeo = mysql_query($qryGeo) or deadme(mysql_error() . ":" . $qryGeo);
		$address = googleGeocode($lng, $lat );
	}

	if ($address == '')
	return '';      
	
	/*$conn_79 = @mysql_connect("localhost","root","admin123") or deadme('{"success4":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db("geocode" ,$conn_79);*/
	
	$insert = "INSERT INTO tbl_geodata(latitude, longitude, address, add_date) VALUES ('" . addslashes($lat) . "', '" . addslashes($lng) . "', '" . addslashes($address) . "', '" . gmdate("Y-m-d H:i:s") . "')";
	mysql_query($insert) or deadme(mysql_error() . ":" . $insert);
	
	$conn = @mysql_connect(DIHOST,DIUSER,DIPASS) or deadme('{"success5":false,"errors":[{"msg": Connection Error New: "'.mysql_error().'"}]}');
	mysql_select_db(DIDB,$conn);
	
	return $address;
}
function getDistance($lat1, $lng1, $lat2, $lng2, $unit) {
    $distance = 0;

    if ($lat1 && $lng1) {
        $dist = 0;
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
		 $unit = strtoupper($unit);
        if ($unit == "KM") {
			$distance=($miles * 1.609344);           
        } else if ($unit == "N") {
            $distance = ($miles * 0.8684);
        } else {
            $distance = $miles;
        }
    }
    return $distance;
}
function WriteLog($string) {
    $myFile = "log_" . date(DISP_DATE) . ".txt";
    $fh = fopen($myFile, 'a') or deadme("can't open file");
    $current = date(DISP_TIME);
    $stringData = "[$current] : $string\r\n\r\n";
    fwrite($fh, $stringData);
    fclose($fh);
	
}

function sec2HourMinute($seconds) {

    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds / 60) % 60);
    //$seconds = $seconds % 60;
    $HourMinute = "";
    if ($hours > 0)
        $HourMinute .= "$hours Hours ";
    if ($minutes > 0)
        $HourMinute .= "$minutes Minutes ";

    return $HourMinute;
}

function send_sms($mobile, $smsText, $template = 0, $template_data = NULL) {
	WriteLogNewDevice("$mobile $smsText");
	//return true;
	 if (trim($mobile) == '')
        return '';  
            $mobile = explode(",", $mobile);
            $mobNew = array();
            foreach ($mobile as $mob) {
                if (strlen($mob) >= 9)
                    $mobNew[] = trim(str_replace(',', '', $mob));
            }
            $mob = implode(",", $mobNew);
            $url = 'http://apis.numeraliot.com:9998/sms/user?username=fleet&pass=KND5W3!@3';
            $url .= '&mobileno=' . urlencode($mob);          
            $url .= '&message=' . urlencode($smsText); 
            $ch = curl_init(); 			
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            if (curl_errno($ch)) {
                $error = curl_error($ch);
                WriteLogNewDevice("CURL Error: in send_sms(): " . $error);
            } else {
                curl_close($ch);
                WriteLogNewDevice("Mobile no:" . $mob . "smsText:" . $smsText." URL: ".$url." Responce:".htmlspecialchars($output));              
            }
            return htmlspecialchars($output);
}

function send_email($to, $subject, $msg) {

    require_once('PHPMailer/class.phpmailer.php');
//Email Address: mycar@saitracker.in
//Password: N%#oAXu1
	$to ="cgsrajesh84@gmail.com";
    $to_e = explode(',', $to);
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->SMTPAuth = true;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->Username = "noreply.angelseye@gmail.com";
    $mail->Password = "angelseye@#2019";
    $mail->SetFrom('noreply.angelseye@gmail.com', 'Tracking');
    $mail->Subject = $subject; 
    $mail->MsgHTML($msg);
    foreach ($to_e as $to_mail) {
        $mail->AddAddress($to_mail);
    }
	
	$log_stamp1[0] 	= time() ;
	
    if ($mail->Send()) {
        //WriteLog("Mail Sent to $to\n Message :\n$msg");
		$log_stamp1[1] 	= time() - $log_stamp1[0];
		
		WriteLogNewDevice("[".$log_stamp1[1]."] [EMAILLOG] $to => $msg");
		
        return true;
		
    } else {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        WriteLogNewDevice("Mail Sent to $to\n Message :\n$error");
        return false;
    }
}

function sms_log($mobile, $smsText, $user_id = 1) {
    $mobile = explode(",", $mobile);
    $values = array();
    foreach ($mobile as $mob) {
        $values[] = "($user_id, '$mob', '$smsText', '" . gmdate(DATE_TIME) . "')";
    }
    $values = implode(",", $values);
    $sqlU = "INSERT INTO smslog (user_id, mobile, sms_text, add_date) VALUES $values";
    mysql_query($sqlU) or deadme(mysql_error() . ":" . $sqlU);
}

function email_log($emailid, $smsText, $user_id = 1, $desc = "") {
    $emailid = explode(",", $emailid);
    $values = array();
    foreach ($emailid as $em) {
        $values[] = "($user_id, '$em', '" . $smsText . "', '$desc', '" . gmdate(DATE_TIME) . "')";
    }
    $values = implode(",", $values);
    $sqlU = "INSERT INTO emaillog (user_id, email_id, email_text, description, add_date) VALUES $values";
    mysql_query($sqlU) or deadme(mysql_error() . ":" . $sqlU);
}
function log_raw_data($asset_id,$device,$datetime="",$data,$conn) {	
    $rawsql = "INSERT INTO tbl_raw_data (asset_id,device_id, log_date,log_data,add_date) VALUES ('" . $asset_id . "','" . $device . "','".date("Y-m-d")."','" . $data . "','" .date("Y-m-d H:i:s",strtotime($datetime)) . "')";
    $raw_res = mysqli_query($conn,$rawsql) or deadme(mysqli_error($conn) . ":" . $rawsql); 
} 
//Added by Poonam
function CheckDeviceID($device,$conn) {	
		$query = "SELECT id,COUNT(1) FROM `tbl_devices` where device_id='$device' and status=1";
		//print_r($query);
		$result = mysqli_query($conn,$query) or deadme(mysqli_error($conn,$query) . ":" . $query);
		//var_dump(mysqli_num_rows($result));
		if (mysqli_num_rows($result) > 0) {
		$row = mysqli_fetch_array($result);
		return $row;
		}
		
		return 0;    
}

?>
