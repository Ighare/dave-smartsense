<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);  
date_default_timezone_set('Australia/Brisbane');
//echo date('Y-m-d H:i:sP');
$mtime = microtime();
set_time_limit(0); 
$log_stamp[0] = time(); 
// Include the file for Database Connection
require_once("../db.php");
require_once("../functions_bit.php");
$input = $_REQUEST['data'];
WriteLog("data:".$input);
//die();  
//$latitude, $longitude, $datetime, $speedKPH, $headingDeg, $valid 
list($fixheader,$current_mode,$device,$datatype,$reportdata) = @explode(";", $input);
//ET;6;866425032478580;R0;7 191101093244 -26.63614 153.09950 0.00 146 2 4181 125 -113
if($fixheader=="ET"){ 
	$array_datatype=array("R0"=>"GPS data","R1"=>"WIFI data","R12"=>"WIFI+GSM cell","R13"=>"WIFI +LTE cell","R2"=>"GSM cell data","R3"=>"LTE cell data","RH"=>"Heart beat data","RC"=>"Configuration information","B"=>"Device binding data");
	$datatype_disc=$array_datatype[$datatype];
if($datatype=="R0"){
	list($satellite,$utc_dt,$latitude,$longitude,$speedKPH,$headingDeg,$event_id,$batteryLevel,$signal) = @explode(" ",$reportdata);
	$device_timestamp=$utc_dt;	
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";	
	}else{
	$datetime=date(DATE_TIME);
	}
	$massage_type=array("0"=>"Device power on","1"=>"Socket connect","2"=>"Auto report","3"=>"Auto report","4"=>"Cell report","5"=>"sos","7"=>"Key Bind","8"=>"lowBattery","11"=>"Heart beat","12"=>"Battery power off","13"=>"Battery charging","14"=>"Battery power full","15"=>"Vibration sensor power on","16"=>"Vibration sensor power off"); 
	if($event_id < 16){
		$alarm=$massage_type[$event_id];
	}
	//$batteryLevel=$batteryLevel.'%';
	
	$type="GPS"; 
	
}else if($datatype=="R2"){
	//GSM cell data
	//ET;1;866425031379169;R2;181129081017+0,21681,20616,460+4+3976+0+-100 
	list($utc_dt,$mnc_data,$pcid,$event_id,$batteryLevel,$signal) = @explode(" ",$reportdata);
	$type="GSM";	
	$device_timestamp=$utc_dt;
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";
	
	}else{
	$datetime=date(DATE_TIME);
	}	
	list($mnc,$cell_id,$lac,$mcc)=@explode(",",$mnc_data);	
}else if($datatype=="R3"){
	//LTE cell data
	//ET;1;866425032478580;R3;181129081017+0,1030,168267587,460,406+4+3976+0+-100
	list($utc_dt,$mnc_data,$pcid,$event_id,$batteryLevel,$signal) = @explode(" ",$reportdata);	
	$device_timestamp=$utc_dt;
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";
	
	}else{
	$datetime=date(DATE_TIME);
	}	
	list($mnc,$cell_id,$lac,$mcc,$pcid)=@explode(",",$mnc_data);
	$type="LTE";
}else if($datatype=="R1"){
	//WIFI data 	//ET;6;866425031377981;R1;190108024848+6a:db:54:5a:79:6d,-91,00:9a:cd:a2:e6:21,-94+3+3831+0+-100
	
	list($utc_dt,$pcid,$event_id,$batteryLevel,$his,$signal) = @explode(" ",$reportdata);
	$device_timestamp=$utc_dt;
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";
	
	}else{
	$datetime=date(DATE_TIME);
	}

}else if($datatype=="R12"){
	//GSM + WIFI cell data	//ET;6;866425031377981;R12;190108024848+6a:db:54:5a:79:6d,-91,00:9a:cd:a2:e6:21,-94+0,21681,20616,460+3+3831+0+-100
	list($utc_dt,$mac_address,$mnc_address2,$event_id,$picid,$batteryLevel,$sequence_num,$signal) = @explode(" ",$reportdata);	
	$device_timestamp=$utc_dt;
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";	
	}else{
	$datetime=date(DATE_TIME);
	}	
	list($mac,$rssi,$mac2,$rssi2)=@explode(",",$mac_address);
	list($mnc,$cell_id,$lac,$mcc,$mcc2)=@explode(",",$mnc_address2);
	//var_dump($mcc2);
	$type="WIFI+GSM";
	
}else if($datatype=="R13"){
	//WIFI+LTE cell data	
	//ET;6;866425032478580;R13;191102094020 18:f1:45:9e:72:a7,-27,8c:3b:ad:04:16:6f,-74 1,145967116,28694,505,74 3 3884 61 -106
	//$satellite,
	list($utc_dt,$mac_address,$mnc_address2,$event_id,$batteryLevel,$sequence_num,$signal) = @explode(" ",$reportdata);	
	$device_timestamp=$utc_dt;	
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";	
	}else{
	$datetime=date(DATE_TIME);
	}	
	list($mac,$rssi,$mac2,$rssi2)=@explode(",",$mac_address);
	list($mnc,$cell_id,$lac,$mcc,$mcc2)=@explode(",",$mnc_address2);
	$massage_type=array("0"=>"Device power on","1"=>"Socket connect","2"=>"Auto report","3"=>"Auto report","4"=>"Cell report","5"=>"sos","7"=>"Key Bind","8"=>"lowBattery","11"=>"Heart beat","12"=>"Battery power off","13"=>"Battery charging","14"=>"Battery power full","15"=>"Vibration sensor power on","16"=>"Vibration sensor power off"); 
	if($event_id < 16){
		$alarm=$massage_type[$event_id];
	}	
	$type="WIFI+LTE";
}else if($datatype=="RH"){
	//Heart data only
	//ET;5;866425031379169;RH;5+190116112648+0+0+0+0+11+3954+1+-100	
	list($mac,$utc_dt) = @explode(" ",$reportdata);
	$device_timestamp=$utc_dt;
	if(strlen($utc_dt)==12){
	$yy = substr($utc_dt, 0, 2);
	$mm = substr($utc_dt, 2, 2);
	$dd = substr($utc_dt, 4, 2);
	$hh = substr($utc_dt, 6, 2);
	$mi = substr($utc_dt, 8, 2);
	$ss = substr($utc_dt, 10, 2);
	$datetime="20$yy-$mm-$dd $hh:$mi:$ss";	
	}else{
	$datetime=date(DATE_TIME);
	}	
}else if($datatype=="RC"){
//Report configuration data
//ET;5;866425031379169;RC;ES821_ESKY_B1.03V1.7_20190116+0+600+8+600+1+32+1+1,31.123,120.456,500+0
}

}

if (trim($input) == '')
    die("Blank Input String '$input'");

$count = CheckDeviceID($device,$conn); 
$asset_id=$count[0];
//var_dump($asset_id);
if($count[1] <= 0)
{	
mysqli_close($conn); 
WriteLog(die("Device not found"));
}
$batteryLevel=show_voltage($batteryLevel).'%';
print_r($batteryLevel);
die("HH");
WriteLog("Device volt:".$batteryLevel);
$query = "SELECT td.id as asset_id,td.device_name,td.add_uid,td.timezone,tll.id,tll.device_id,tll.location_time,tll.address,tll.latitude,tll.longitude,tll.location_type,tll.device_speed,tll.device_bettery,tll.add_date,tll.device_timestamp  FROM `tbl_devices` td LEFT JOIN tbl_last_locations tll ON tll.device_id=td.device_id WHERE tll.asset_id= '" . addslashes($asset_id) . "'";
$result = mysqli_query($conn,$query);
$arr_row = mysqli_fetch_array($result);
$id_last=$arr_row['id']; 
$device_id_last=$arr_row['device_id'];
$device_name=$arr_row['device_name'];
$user_id=$arr_row['add_uid'];
//$asset_id=$arr_row['asset_id'];
$location_time_last=$arr_row['location_time'];
$address_last=$arr_row['address'];
$latitude_last=$arr_row['latitude'];
$longitude_last=$arr_row['longitude'];
$location_type_last=$arr_row['location_type'];
$device_speed_last=$arr_row['device_speed'];
$device_bettery_last=$arr_row['device_bettery'];
$add_date_last=$arr_row['add_date'];
$device_timestamp_last=$arr_row['device_timestamp'];
//var_dump($device_timestamp_last);
$device_timezone=$arr_row['timezone'];
WriteLog("device time:".$datetime);
//var_dump($datetime); 
if($device_timezone !=""){	
$dt=explode(":",$device_timezone);	
$datetime = date('Y-m-d H:i:s',strtotime(''.$dt[0].' hour '.$dt[1].' minutes',strtotime($datetime)));
}
log_raw_data($asset_id,$device,$datetime,$input,$conn);
WriteLog("device converted time: $device_timezone :".$datetime);
WriteLog('ES825 Data='.$input); 
if(abs($latitude)==0 or abs($longitude)==0){
	$latitude=$latitude_last;
	$longitude=$longitude_last;
	WriteLog(' [die] Wrong Latlong recode_file latest and putted old: ' . $latitude.','.$longitude);
	//die("lat long");
	//mysqli_close($conn); 
} 
if($latitude_last == $latitude  && $longitude_last == $longitude ){
	$x_address = $address_last;
}else{
$x_address = googleGeocode($latitude, $longitude,$conn);
}
$address=$x_address;

if($alarm=="lowBattery"){
	//Battery Low
	$device_users=date_create($datetime);
	$device_users=date_format($device_users,"d F Y,H:i:s");
	$steps="lowBattery";
	//$smsText1="The vehicle $device_name bettery is low";
	$smsText1="The Device has $device_name Low Battery at $address. Date time $device_users.";
	sendMobileNotification($conn,$user_id, $smsText1, $asset_id="");
}
if($alarm=="sos"){
	//SOS Alert
	$steps="SOS";
	$smsText1="The device $device_name pressed SOS at $address. Date time $datetime";
	sendMobileNotification($conn,$user_id, $smsText1, $asset_id="");
}
if($alarm=="fallDown"){ 
	//fall alertsss
	$steps="fallDown";
	$smsText1="The device $device_name fall down at $address. Date time $datetime";
	sendMobileNotification($conn,$user_id, $smsText1, $asset_id="");
}
if($latitude_last == $latitude  && $longitude_last == $longitude ){
$update_query="UPDATE `tbl_last_locations` SET `location_time`='".$datetime."',`address`='".$address."',`latitude`='".$latitude."',`longitude`='".$longitude."',`location_type`='".$type."',`device_speed`='".$speedKPH."',`device_bettery`='".$batteryLevel."',`device_timestamp`='".$device_timestamp."' WHERE asset_id='".$asset_id."'";
mysqli_query($conn,$update_query); 
die("duplicate lat long from device");
}
if (mysqli_num_rows($result) > 0){
$update_query="UPDATE `tbl_last_locations` SET `location_time`='".$datetime."',`address`='".$address."',`latitude`='".$latitude."',`longitude`='".$longitude."',`location_type`='".$type."',`device_speed`='".$speedKPH."',`device_bettery`='".$batteryLevel."',`device_timestamp`='".$device_timestamp."' WHERE asset_id='".$asset_id."'";
mysqli_query($conn,$update_query);
}else{
$insert_last="INSERT INTO `tbl_last_locations`(`asset_id`,`device_id`, `location_time`, `address`, `latitude`, `longitude`, `location_type`, `device_speed`, `device_bettery`, `add_date`,`device_timestamp`,`event`) VALUES ('".$asset_id."','".$device."','".$datetime."','".$address."','".$latitude."','".$longitude."','".$type."','".$speedKPH."','".$batteryLevel."','".$datetime."','".$device_timestamp."','".$event_type."')";
mysqli_query($conn,$insert_last);  
}
$insert_q="INSERT INTO `tbl_location_history`(`asset_id`,`device_timestamp`,`device_id`, `location_time`, `address`, `latitude`, `longitude`, `location_type`, `device_speed`, `device_bettery`, `add_date`) VALUES ('".$asset_id."','".$device_timestamp."','".$device."','".$datetime."','".$address."','".$latitude."','".$longitude."','".$type."','".$speedKPH."','".$batteryLevel."','".$datetime."')";
mysqli_query($conn,$insert_q);  	
check_inout_geofence_alert($conn,$asset_id,$device_name,$datetime,$address,$latitude,$longitude); 

mysqli_close($conn);
WriteLog(die("Data Received"));

?>